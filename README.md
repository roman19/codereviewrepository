### Requirements     
1. JRE v.8      
2. Tomcat server v.8 or higher    
3. MySQL Server v.5.7 
### Before run app  
1. Import schema from [codereview_dump.sql](https://drive.google.com/file/d/0BykaHUW0jtLuei1BYWpXYmRIODg/view?usp=sharing)
2. Download [CodeReview.war](https://drive.google.com/file/d/0BykaHUW0jtLuWHNhbzROTEh0UXM/view?usp=sharing)       
3. Change connection properties in project *META-INF/context.xml*, such as **username**, **password** and **url** for appropriate.           
4. Unpack archive with resources [Epam.rar](https://drive.google.com/file/d/0BykaHUW0jtLuTDFGU2dGMnI2X2M/view?usp=sharing) somewhere on you PC and change path to the **EPAM** folder in *WEB-INF/web.xml* for **file-upload** and **review-project-upload** context parameters  
```
#!XML
    <context-param>
        <description>Location to store uploaded file</description>
        <param-name>file-upload</param-name>
        <param-value>
            D:/EPAM/CODE_REVIEW/images/
        </param-value>
    </context-param>
    <context-param>
        <description>Location to store uploaded user projects</description>
        <param-name>review-project-upload</param-name>
        <param-value>
            D:/EPAM/CODE_REVIEW/projects/
        </param-value>
    </context-param>
```
### Steps to run app     
1. Start MySQL Server       
2. Deploy **CodeReview.war** on tomcat server and set application context **/CodeReview**          
3. Run tomcat server       
4. Go to [localhost:8080/CodeReview](localhost:8080/CodeReview/) and follow guide instructions