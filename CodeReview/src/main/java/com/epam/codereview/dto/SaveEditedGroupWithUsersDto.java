package com.epam.codereview.dto;

import java.util.List;

public class SaveEditedGroupWithUsersDto {

	private Integer groupId;
	private String name;
	private List<Integer> users;
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Integer> getUsers() {
		return users;
	}
	public void setUsers(List<Integer> users) {
		this.users = users;
	}
	@Override
	public String toString() {
		return "SaveEditedGroupWithUsersDto [groupId=" + groupId + ", name=" + name + ", users=" + users + "]";
	}
	
	
	
}
