package com.epam.codereview.dto;

import java.sql.Timestamp;
import java.util.List;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.model.User;

public class ReplyNotificationDto {
	private String notificationType = "REPLY";
	
	@Column(name="owner_id")
	private Integer ownerId;
	
	@Column(name="reply_id")
	private Integer replyId;
	
	@Column(name="message")
	private String message;
	
	@Column(name="issue_id")
	private Integer issueId;
	
	@Column(name="title")
	private String issueTitle;
	
	@Column(name="review_id")
	private Integer reviewId;
	
	@Column(name="review_name")
	private String reviewName;
	
	@Column(name="file_id")
	private Integer fileId;
	
	@Column(name="time")
	private Timestamp date;
	
	private User owner;
	
	private List<Integer> users;

	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getIssueId() {
		return issueId;
	}

	public void setIssueId(Integer issueId) {
		this.issueId = issueId;
	}

	public String getIssueTitle() {
		return issueTitle;
	}

	public void setIssueTitle(String issueTitle) {
		this.issueTitle = issueTitle;
	}

	public Integer getReviewId() {
		return reviewId;
	}

	public void setReviewId(Integer reviewId) {
		this.reviewId = reviewId;
	}

	public String getReviewName() {
		return reviewName;
	}

	public void setReviewName(String reviewName) {
		this.reviewName = reviewName;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Integer getReplyId() {
		return replyId;
	}

	public void setReplyId(Integer replyId) {
		this.replyId = replyId;
	}

	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public List<Integer> getUsers() {
		return users;
	}

	public void setUsers(List<Integer> users) {
		this.users = users;
	}
}
