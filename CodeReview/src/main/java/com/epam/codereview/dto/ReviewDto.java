package com.epam.codereview.dto;

import java.sql.Timestamp;

public class ReviewDto implements Comparable<ReviewDto> {

	private Integer codeReviewId;
	private String name;
	private Integer userId;
	private String userName;
	private String reviewerStatus;
	private boolean active;
	private Double coverageAvgPercent;
	private Timestamp creationDate;
	private Integer totalIssues;
	private String gitURL;
	private boolean deleted;
	private String totalSearchName;
	

	public String getTotalSearchName() {
		return totalSearchName;
	}

	public void setTotalSearchName(String totalSearchName) {
		this.totalSearchName = totalSearchName;
	}

	public String getGitURL() {
		return gitURL;
	}

	public void setGitURL(String gitURL) {
		this.gitURL = gitURL;
	}

	public Integer getCodeReviewId() {
		return codeReviewId;
	}

	public void setCodeReviewId(Integer codeReviewId) {
		this.codeReviewId = codeReviewId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean isActive) {
		this.active = isActive;
	}

	public Double getCoverageAvgPercent() {
		return coverageAvgPercent;
	}

	public void setCoverageAvgPercent(Double coverageAvgPercent) {
		this.coverageAvgPercent = coverageAvgPercent;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getTotalIssues() {
		return totalIssues;
	}

	public void setTotalIssues(Integer totalIssues) {
		this.totalIssues = totalIssues;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public int compareTo(ReviewDto o) {
		return (-1) * creationDate.compareTo(o.getCreationDate());
	}

	@Override
	public String toString() {
		return "ReviewDto [codeReviewId=" + codeReviewId + ", name=" + name + ", userId=" + userId + ", userName="
				+ userName + ", isActive=" + active + ", coverageAvgPercent=" + coverageAvgPercent
				+ ", creationDate=" + creationDate + ", totalIssues=" + totalIssues + ", gitURL=" + gitURL + "]";
	}

	public String getReviewerStatus() {
		return reviewerStatus;
	}

	public void setReviewerStatus(String reviewerStatus) {
		this.reviewerStatus = reviewerStatus;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

}
