package com.epam.codereview.dto;

import com.epam.codereview.manager.db.Column;

public class CodeReviewUserDto implements Comparable<CodeReviewUserDto>{

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "login")
    private String login;

    @Column(name = "fname")
    private String firstName;

    @Column(name = "lname")
    private String lastName;

    @Column(name = "status")
    private String status;

    @Column(name = "issues_count")
    private long issuesCount;

    private double coveragePercent;
 
    public CodeReviewUserDto() {
    }

    public Integer getUserId() {
        return userId;
    }

	public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStatus() {
        return status.toLowerCase();
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getCoveragePercent() {
        return coveragePercent;
    }

    public void setCoveragePercent(double coveragePercent) {
        this.coveragePercent = coveragePercent;
    }

    public long getIssuesCount() {
        return issuesCount;
    }

    public void setIssuesCount(long issuesCount) {
        this.issuesCount = issuesCount;
    }

    private String getFullName() {
        return firstName + " " + lastName;
    }

    private int getStatusPriority(String status) {
        switch (status) {
            case "CONFIRMED" : return 1;
            case "PENDING" : return 2;
            case "REJECTED" : return 3;
            case "DELETED_BY_ADMIN" : return 4;
            case "LEFT" : return 5;
            default: return 6;
        }
    }

    @Override
    public int compareTo(CodeReviewUserDto o) {
        int thisStatus = this.getStatusPriority(this.status);
        int oStatus = o.getStatusPriority(o.status);
        return  (thisStatus < oStatus) ? -1 : ((thisStatus == oStatus) ? (this.getFullName()).compareTo(o.getFullName()) : 1);
    }
}
