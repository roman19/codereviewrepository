package com.epam.codereview.dto;

import com.epam.codereview.validator.RegExps;
import com.epam.codereview.validator.Validation;

public class LoginDto {

	@Validation(regexp = RegExps.LETTERS_AND_DIGITS, minLength = 5, maxLength = 45)
	private String login;

	@Validation(regexp = RegExps.PASSWORD, minLength = 6, maxLength = 45)
	private String password;

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

}
