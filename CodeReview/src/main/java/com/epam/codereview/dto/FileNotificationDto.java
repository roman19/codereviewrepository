package com.epam.codereview.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.epam.codereview.model.User;
import com.epam.codereview.service.UserService;

public class FileNotificationDto {
	private Integer reviewId;
	private String reviewName;
	private Timestamp date;
	private User user;

	private List<FilePlainDto> files;

	public FileNotificationDto() {
	}

	public FileNotificationDto(List<FilePlainNotificationDto> notifications) {
		if (notifications.size() != 0) {
			UserService userService = new UserService();

			this.reviewId = notifications.get(0).getReviewId();
			this.reviewName = notifications.get(0).getReviewName();
			this.user = userService.get(notifications.get(0).getUserId());

			final Comparator<FilePlainNotificationDto> comp = (n1, n2) -> n1
					.getDate().compareTo(n2.getDate());
			this.date = notifications.stream().max(comp).get().getDate();

			files = new ArrayList<>();

			for (FilePlainNotificationDto dto : notifications) {
				FilePlainDto filePlainDto = new FilePlainDto();
				filePlainDto.setDate(dto.getDate());
				filePlainDto.setId(dto.getId());
				filePlainDto.setName(dto.getName());
				files.add(filePlainDto);
			}

			Collections.sort(files,
					(f1, f2) -> f2.getDate().compareTo(f1.getDate()));
		}
	}

	public Integer getReviewId() {
		return reviewId;
	}

	public void setReviewId(Integer reviewId) {
		this.reviewId = reviewId;
	}

	public String getReviewName() {
		return reviewName;
	}

	public void setReviewName(String reviewName) {
		this.reviewName = reviewName;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<FilePlainDto> getFiles() {
		return files;
	}

	public void setFiles(List<FilePlainDto> files) {
		this.files = files;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "FileNotificationDto [reviewId=" + reviewId + ", reviewName="
				+ reviewName + ", date=" + date + ", user=" + user.getLogin() + ", files="
				+ files + "]";
	}
}
