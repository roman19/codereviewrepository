package com.epam.codereview.dto;

import com.epam.codereview.model.Branch;
import com.epam.codereview.model.CodeReview;
import com.epam.codereview.model.Project;

import java.util.List;

public class CodeReviewDto {

    private CodeReview codeReview;
    private Project project;
    private Branch branch;
    private List<FileDto> fileDtos;
    private int issuesCount;
    private double coveragePercentTotal;

    public CodeReviewDto() {
    }

    public CodeReview getCodeReview() {
        return codeReview;
    }

    public void setCodeReview(CodeReview codeReview) {
        this.codeReview = codeReview;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<FileDto> getFileDtos() {
        return fileDtos;
    }

    public void setFileDtos(List<FileDto> fileDtos) {
        this.fileDtos = fileDtos;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public int getIssuesCount() {
        return issuesCount;
    }

    public void setIssuesCount(int issuesCount) {
        this.issuesCount = issuesCount;
    }

    public double getCoveragePercentTotal() {
        return coveragePercentTotal;
    }

    public void setCoveragePercentTotal(double coveragePercentTotal) {
        this.coveragePercentTotal = coveragePercentTotal;
    }
}
