package com.epam.codereview.dto;

import java.util.List;

import com.epam.codereview.model.User;

public class PlainOnlineNotificationDto {
	private String notificationType;
	private User owner;
	private Integer itemId;
	private String itemName;
	private List<Integer> users;
	
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String type) {
		this.notificationType = type;
	}
	
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer groupId) {
		this.itemId = groupId;
	}
	public List<Integer> getUsers() {
		return users;
	}
	public void setUsers(List<Integer> users) {
		this.users = users;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String groupName) {
		this.itemName = groupName;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
}
