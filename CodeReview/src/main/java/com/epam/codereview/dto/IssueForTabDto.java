package com.epam.codereview.dto;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.model.CodeReview;
import com.epam.codereview.model.Issue;
import com.epam.codereview.model.Reply;
import com.epam.codereview.model.User;
import com.google.gson.Gson;

public class IssueForTabDto {

    @Column(name="id")
    private Integer id;

    @Column(name="type")
    private String type;

    @Column(name="user_id")
    private Integer userId;

    @Column(name="login")
    private String userLogin;

    @Column(name="fname")
    private String firstName;

    @Column(name="lname")
    private String lastName;

    @Column(name="title")
    private String title;

    @Column(name="body")
    private String body;

    @Column(name="line_from")
    private Integer lineFrom;

    @Column(name="line_to")
    private Integer lineTo;

    @Column(name="file_id")
    private Integer fileId;

    @Column(name = "url")
    private String filePath;

    @Column(name="time")
    private Timestamp time;

    @Column(name="brush")
    private String fileBrush;

    private boolean isEditable = false;

    private int[] lines;

    private String fileLinesContent;
    
    private List<Reply> replies;

    private String userFullName;

    public IssueForTabDto(Issue issue, User user, List<Reply> repliesByIssueId) {
        id = issue.getId();
        userId = issue.getUserId();
        userLogin = user.getLogin();
        userFullName = user.getFirstName()+" "+user.getLastName();
        type = issue.getType();
        title = issue.getTitle();
        body = issue.getBody();
        fileId = issue.getFileId();
        time = issue.getTime();
        lineFrom = issue.getLineFrom();
        lineTo = issue.getLineTo();
        lines = IntStream.range(issue.getLineFrom(), issue.getLineTo() + 1).toArray();
        replies = repliesByIssueId;
    }

    public static class Sort {

        public static Comparator<IssueForTabDto> byDateASC() {
            return Comparator.comparing(IssueForTabDto::getTime);
        }

        public static Comparator<IssueForTabDto> byDateDESC() {
            return Comparator.comparing(IssueForTabDto::getTime).reversed();
        }

        public static Comparator<IssueForTabDto> byFilePathAndDateDESC() {
            return Comparator.comparing(IssueForTabDto::getFilePath).thenComparing(byDateDESC());
        }
    }

    public IssueForTabDto() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getLineFrom() {
        return lineFrom;
    }

    public void setLineFrom(Integer lineFrom) {
        this.lineFrom = lineFrom;
    }

    public Integer getLineTo() {
        return lineTo;
    }

    public void setLineTo(Integer lineTo) {
        this.lineTo = lineTo;
    }

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public boolean getIsEditable() {
        return isEditable;
    }

    public void setIsEditable(boolean isEditable) {
        this.isEditable = isEditable;
    }

    public int[] getLines() {
        if (lines == null) {
            lines = IntStream.range(lineFrom, lineTo + 1).toArray();
        }
        return lines;
    }

    public void setLines(int[] lines) {
        this.lines = lines;
    }

    public String getFileLinesContent() {
        return fileLinesContent;
    }

    public void setFileLinesContent(String fileLinesContent) {
        this.fileLinesContent = fileLinesContent;
    }

    public String getFileBrush() {
        return fileBrush;
    }

    public void setFileBrush(String fileBrush) {
        this.fileBrush = fileBrush;
    }

    public String getUserFullName() {
        return firstName + " " + lastName;
    }


    public List<Reply> getReplies() {
		return replies;
	}

	public void setReplies(List<Reply> replies) {
		this.replies = replies;
	}

	public String toJSONObject() {
        return new Gson().toJson(this);
    }
}
