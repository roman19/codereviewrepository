package com.epam.codereview.dto;

public class UserNotificationsDto {
	private boolean enabledAll;
	private boolean review;
	private boolean group;
	private boolean issue;
	private boolean reply;
	private boolean fileAdded;
	private boolean isSound;

	public boolean isEnabledAll() {
		return enabledAll;
	}

	public boolean isReview() {
		return review;
	}

	public boolean isGroup() {
		return group;
	}

	public boolean isIssue() {
		return issue;
	}

	public boolean isReply() {
		return reply;
	}

	public boolean isFileAdded() {
		return fileAdded;
	}

	public boolean isSound() {
		return isSound;
	}

}
