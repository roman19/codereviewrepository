package com.epam.codereview.dto;

import java.sql.Timestamp;

import com.epam.codereview.model.User;

public class GroupNotificationDto {
	private Integer groupId;
	private User user;
	private String groupName;
	private Integer countMembers;
	private Timestamp date;
	
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Integer getCountMembers() {
		return countMembers;
	}
	public void setCountMembers(Integer countMembers) {
		this.countMembers = countMembers;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "GroupNotificationDto [user=" + user.getLogin() + ", groupName="
				+ groupName + "]";
	}
}
