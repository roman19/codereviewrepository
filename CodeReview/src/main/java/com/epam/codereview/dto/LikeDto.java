package com.epam.codereview.dto;

public class LikeDto {

	public Integer totalLikes;
	public Integer totalDisLikes;
	public Integer likeValue;
	public Integer getTotalLikes() {
		return totalLikes;
	}
	public void setTotalLikes(Integer totalLikes) {
		this.totalLikes = totalLikes;
	}
	public Integer getTotalDisLikes() {
		return totalDisLikes;
	}
	public void setTotalDisLikes(Integer totalDisLikes) {
		this.totalDisLikes = totalDisLikes;
	}
	public Integer getLikeValue() {
		return likeValue;
	}
	public void setLikeValue(Integer likeValue) {
		this.likeValue = likeValue;
	}
	@Override
	public String toString() {
		return "LikeIssueDto [totalLikes=" + totalLikes + ", totalDisLikes=" + totalDisLikes + ", likeValue="
				+ likeValue + "]";
	}
	
	
	
}
