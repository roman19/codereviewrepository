package com.epam.codereview.dto;

import java.sql.Timestamp;

public class FilePlainDto {
	private Integer id;
	private String name;
	private Timestamp date;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "\nFilePlainDto [id=" + id + ", name=" + name + ", date=" + date
				+ "]";
	}
}
