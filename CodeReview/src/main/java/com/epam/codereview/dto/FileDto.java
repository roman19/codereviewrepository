package com.epam.codereview.dto;

import com.epam.codereview.model.File;

public class FileDto {

    private File file;
    private String fullPath;

    public FileDto() {
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }
}
