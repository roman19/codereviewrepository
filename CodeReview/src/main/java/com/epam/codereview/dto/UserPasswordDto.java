package com.epam.codereview.dto;

import com.epam.codereview.validator.RegExps;
import com.epam.codereview.validator.Validation;

public class UserPasswordDto {
	private String currentPassword;
	@Validation(regexp=RegExps.PASSWORD, minLength=6, maxLength=45)
	private String newPassword;
	private String repeatPassword;

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getRepeatPassword() {
		return repeatPassword;
	}

	public void setRepeatPassword(String repeatPassword) {
		this.repeatPassword = repeatPassword;
	}
}
