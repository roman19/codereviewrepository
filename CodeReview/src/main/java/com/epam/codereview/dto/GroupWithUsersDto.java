package com.epam.codereview.dto;

import java.util.List;

public class GroupWithUsersDto {
	private String name;
	private boolean image;
	private List<Integer> users;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Integer> getUsers() {
		return users;
	}
	public void setUsers(List<Integer> users) {
		this.users = users;
	}
	public boolean isImage() {
		return image;
	}
	public void setImage(boolean image) {
		this.image = image;
	}
	@Override
	public String toString() {
		return "Group [name=" + name + ", users=" + users + "]";
	}
	
}
