package com.epam.codereview.dto;

import java.sql.Timestamp;

import com.epam.codereview.manager.db.Column;

public class FilePlainNotificationDto {
	@Column(name="id")
	private Integer id;
	
	@Column(name="url")
	private String name;
	
	@Column(name="notification_date")
	private Timestamp date;
	
	@Column(name="reviewId")
	private Integer reviewId;
	
	@Column(name="reviewName")
	private String reviewName;
	
	@Column(name="user_id")
	private Integer userId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	public Integer getReviewId() {
		return reviewId;
	}
	public void setReviewId(Integer reviewId) {
		this.reviewId = reviewId;
	}
	public String getReviewName() {
		return reviewName;
	}
	public void setReviewName(String reviewName) {
		this.reviewName = reviewName;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
