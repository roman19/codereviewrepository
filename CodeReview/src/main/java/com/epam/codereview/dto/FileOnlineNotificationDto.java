package com.epam.codereview.dto;

import java.util.List;

import com.epam.codereview.model.CodeReview;
import com.epam.codereview.model.User;
import com.epam.codereview.service.CodeReviewService;
import com.epam.codereview.service.CodeReviewUserService;
import com.epam.codereview.service.UserService;

public class FileOnlineNotificationDto {
	private String notificationType = "FILE_ADDED";
	private User owner;
	private Integer countFiles;
	private Integer reviewId;
	private String reviewName;
	private List<Integer> users;
	
	public FileOnlineNotificationDto(){}
	
	public FileOnlineNotificationDto(Integer ownerId, Integer reviewId, Integer countFiles){
		UserService userDao= new UserService();
		CodeReviewService codeReviewDao = new CodeReviewService();
		CodeReviewUserService codeReviewUserService = new CodeReviewUserService();
		CodeReview codeReview  = codeReviewDao.get(reviewId);
		
		this.owner = userDao.get(ownerId);
		this.countFiles = countFiles;
		this.reviewId = codeReview.getId();
		this.reviewName = codeReview.getName();
		this.users = codeReviewUserService.getUsersByReviewId(reviewId);
	}
	
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public Integer getCountFiles() {
		return countFiles;
	}
	public void setCountFiles(Integer countFiles) {
		this.countFiles = countFiles;
	}
	public Integer getReviewId() {
		return reviewId;
	}
	public void setReviewId(Integer reviewId) {
		this.reviewId = reviewId;
	}
	public String getReviewName() {
		return reviewName;
	}
	public void setReviewName(String reviewName) {
		this.reviewName = reviewName;
	}
	public List<Integer> getUsers() {
		return users;
	}
	public void setUsers(List<Integer> users) {
		this.users = users;
	}
}
