package com.epam.codereview.dto;

import java.sql.Timestamp;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.model.User;

public class IssueNotificationDto {
	@Column(name="id")
	private Integer issueId;
	
	@Column(name="user_id")
	private Integer userId;
	
	@Column(name="title")
	private String issueTitle;
	
	@Column(name="name")
	private String reviewName;
	
	@Column(name="body")
	private String issueBody;
	
	@Column(name="reviewid")
	private Integer reviewId;
	
	@Column(name="url")
	private String fileName;
	
	@Column(name="fileid")
	private Integer fileId;
	
	@Column(name="time")
	private Timestamp date;
	
	private User user;
	
	public Integer getIssueId() {
		return issueId;
	}
	public void setIssueId(Integer issueId) {
		this.issueId = issueId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getIssueTitle() {
		return issueTitle;
	}
	public void setIssueTitle(String issueTitle) {
		this.issueTitle = issueTitle;
	}
	public String getReviewName() {
		return reviewName;
	}
	public void setReviewName(String reviewName) {
		this.reviewName = reviewName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Integer getFileId() {
		return fileId;
	}
	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public Integer getReviewId() {
		return reviewId;
	}
	public void setReviewId(Integer reviewId) {
		this.reviewId = reviewId;
	}
	public String getIssueBody() {
		return issueBody;
	}
	public void setIssueBody(String issueBody) {
		this.issueBody = issueBody;
	}
	@Override
	public String toString() {
		return "IssueNotificationDto [issueTitle=" + issueTitle
				+ ", reviewName=" + reviewName + ", fileName=" + fileName
				+ ", fileId=" + fileId + ", date=" + date + ", user=" + user.getLogin()
				+ "]";
	}
	
	
}
