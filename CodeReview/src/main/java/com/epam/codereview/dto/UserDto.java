package com.epam.codereview.dto;

import com.epam.codereview.validator.RegExps;
import com.epam.codereview.validator.Validation;

public class UserDto {
	@Validation(regexp=RegExps.ONLY_LETTERS, minLength=1, maxLength=45)
	private String name;
	@Validation(regexp=RegExps.ONLY_LETTERS, minLength=1, maxLength=45)
	private String surname;
	@Validation(regexp=RegExps.LOGIN, minLength=5, maxLength=45)
	private String login;
	@Validation(regexp=RegExps.EMAIL, minLength=6, maxLength=45)
	private String email;
	@Validation(regexp=RegExps.PASSWORD, minLength=6, maxLength=45)
	private String password;
	@Validation(regexp=RegExps.PASSWORD, minLength=6, maxLength=45)
	private String cpassword;
	public String getName() {
		return name;
	}
	public String getSurname() {
		return surname;
	}
	public String getLogin() {
		return login;
	}
	public String getEmail() {
		return email;
	}
	public String getPassword() {
		return password;
	}
	public String getCpassword() {
		return cpassword;
	}
	
	
}
