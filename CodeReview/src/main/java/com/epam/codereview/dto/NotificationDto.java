package com.epam.codereview.dto;

import java.sql.Timestamp;

public class NotificationDto {
	private NotificationType type;
	private Timestamp date;
	private Object notification;

	public NotificationDto(NotificationType type, Timestamp date,
			Object notification) {
		this.type = type;
		this.date = date;
		this.notification = notification;
	}

	public NotificationType getType() {
		return type;
	}

	public Object getNotification() {
		return notification;
	}

	public Timestamp getDate() {
		return date;
	}

	@Override
	public String toString() {
		return "NotificationDto [type=" + type + ", date=" + date
				+ ", notification=" + notification + "]";
	}
}
