package com.epam.codereview.dto;

/*
 * We use this class to add users to the group
 * @author Vitalii Horban
 * 
 * */
public class UserGroupDto {
	private Integer id;
	private String fname;
	private String lname;
	private String login;
	private String photo;
	private boolean imageFromServer;
	private String totalSearchedName;

	public String getTotalSearchedName() {
		return totalSearchedName;
	}

	public void setTotalSearchedName(String totalSearchedName) {
		this.totalSearchedName = totalSearchedName;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getFname() {
		return fname;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public boolean isImageFromServer() {
		return imageFromServer;
	}

	public void setImageFromServer(boolean imageFromServer) {
		this.imageFromServer = imageFromServer;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", fname=" + fname + ", lname=" + lname
				+ ", login=" + login + ", photo=" + photo
				+ ", totalSearchedName=" + totalSearchedName + "]";
	}

}
