package com.epam.codereview.dto;

public class UserPersonalDataDto {
	private String name;
	private String surname;
	private String login;

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getLogin() {
		return login;
	}

}
