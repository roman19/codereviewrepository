package com.epam.codereview.dto;

import java.util.List;

public class EditGroupWithUsersDto {
	private String name;
	private List<UserGroupDto> users;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<UserGroupDto> getUsers() {
		return users;
	}
	public void setUsers(List<UserGroupDto> users) {
		this.users = users;
	}
	@Override
	public String toString() {
		return "Group [name=" + name + ", users=" + users + "]";
	}
}
