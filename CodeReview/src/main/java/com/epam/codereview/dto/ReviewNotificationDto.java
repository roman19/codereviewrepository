package com.epam.codereview.dto;

import java.sql.Timestamp;

import com.epam.codereview.model.User;

public class ReviewNotificationDto {
	private Integer reviewId;
	private String reviewName;
	private User user;
	private Timestamp date;
	
	public Integer getReviewId() {
		return reviewId;
	}
	public void setReviewId(Integer reviewId) {
		this.reviewId = reviewId;
	}
	public String getReviewName() {
		return reviewName;
	}
	public void setReviewName(String reviewName) {
		this.reviewName = reviewName;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "ReviewNotificationDto [reviewName=" + reviewName + ", user="
				+ user.getLogin() + "]";
	}
}
