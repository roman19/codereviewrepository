package com.epam.codereview.dto;

import java.sql.Timestamp;
import java.util.stream.IntStream;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.model.CodeReview;
import com.epam.codereview.model.Issue;
import com.epam.codereview.model.User;
import com.google.gson.Gson;

public class IssueDto {

    @Column(name="id")
    private Integer id;

    @Column(name="type")
    private String type;

    @Column(name="user_id")
    private Integer userId;

    @Column(name="login")
    private String userLogin;

    @Column(name="title")
    private String title;

    @Column(name="body")
    private String body;

    @Column(name="line_from")
    private Integer lineFrom;

    @Column(name="line_to")
    private Integer lineTo;

    @Column(name="file_id")
    private Integer fileId;

    @Column(name="time")
    private Timestamp time;

    private boolean isEditable = false;

    private int[] lines;
    
    private Integer userTo;
    private String userFullName;
    private boolean imageFromServer;
    private String imageUrl;
    private String notificationType = "ISSUE";
    private String reviewName;
    private Integer reviewId;
    
    private Integer countOfReplies;
    

    public IssueDto() {
    }

    public IssueDto(Issue issue, User user, CodeReview review, Integer userToId) {
        id = issue.getId();
        userId = issue.getUserId();
        userTo = userToId;
        userLogin = user.getLogin();
        userFullName = user.getFirstName()+" "+user.getLastName();
        imageFromServer = user.isImageFromServer();
        imageUrl = user.getImageUrl();
        type = issue.getType();
        title = issue.getTitle();
        body = issue.getBody();
        fileId = issue.getFileId();
        time = issue.getTime();
        lineFrom = issue.getLineFrom();
        lineTo = issue.getLineTo();
        lines = IntStream.range(issue.getLineFrom(), issue.getLineTo() + 1).toArray();
        
        reviewId = review.getId();
        reviewName = review.getName();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public int[] getLines() {
        if (lines == null) {
            lines = IntStream.range(lineFrom, lineTo + 1).toArray();
        }
        return lines;
    }

    public void setLines(int[] lines) {
        this.lines = lines;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean getIsEditable() {
        return isEditable;
    }

    public void setIsEditable(boolean isEditable) {
        this.isEditable = isEditable;
    }

    public void setLineFrom(Integer lineFrom) {
        this.lineFrom = lineFrom;
    }

    public void setLineTo(Integer lineTo) {
        this.lineTo = lineTo;
    }

    public void setRequiredFields(Integer currentUserId) {
        lines = IntStream.range(lineFrom, lineTo + 1).toArray();
        isEditable = userId.equals(currentUserId);
    }

    public Integer getUserTo() {
		return userTo;
	}
    
	public String getNotificationType() {
		return notificationType;
	}

	public String getUserFullName() {
		return userFullName;
	}

	public boolean isImageFromServer() {
		return imageFromServer;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public String getReviewName() {
		return reviewName;
	}

	public Integer getReviewId() {
		return reviewId;
	}

	public Integer getCountOfReplies() {
		return countOfReplies;
	}

	public void setCountOfReplies(Integer countOfReplies) {
		this.countOfReplies = countOfReplies;
	}

	public String toJSONObject() {
        return new Gson().toJson(this);
    }
}
