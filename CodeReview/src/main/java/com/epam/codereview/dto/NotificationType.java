package com.epam.codereview.dto;

public enum NotificationType {
	GROUP, REVIEW, ISSUE, REPLY, FILE_ADDED

}
