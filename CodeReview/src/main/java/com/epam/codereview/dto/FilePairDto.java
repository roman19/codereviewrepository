package com.epam.codereview.dto;

public class FilePairDto {
	private String fullPath;
	private String path;

	public FilePairDto(String fullPath, String path) {
		this.fullPath = fullPath;
		this.path = path;
	}
	
	public String getFullPath() {
		return fullPath;
	}
	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
}
