package com.epam.codereview.dto;

public class GroupDto implements Comparable<GroupDto>{
	private Integer groupId;	
	private String userFirstName;
	private String userLastName;
	private Integer userId;
	private String groupName;
	private Boolean isActiveGroup;
	private Integer totalUserNumerous;
	private String  image_url;
	
	
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Boolean getIsActiveGroup() {
		return isActiveGroup;
	}
	public void setIsActiveGroup(Boolean isActiveGroup) {
		this.isActiveGroup = isActiveGroup;
	}
	
	@Override
	public int compareTo(GroupDto o) {	
		return groupName.compareTo(o.getGroupName());
	}
	public Integer getTotalUserNumerous() {
		return totalUserNumerous;
	}
	public void setTotalUserNumerous(Integer totalUserNumerous) {
		this.totalUserNumerous = totalUserNumerous;
	}
	@Override
	public String toString() {
		return "GroupDto [groupId=" + groupId + ", userFirstName=" + userFirstName + ", userLastName=" + userLastName
				+ ", userId=" + userId + ", groupName=" + groupName + ", isActiveGroup=" + isActiveGroup
				+ ", totalUserNumerous=" + totalUserNumerous + "]";
	}
	
	
	
}
