package com.epam.codereview.dto;

import com.epam.codereview.validator.RegExps;
import com.epam.codereview.validator.Validation;

public class NewPasswordDto {
	
	@Validation(regexp = RegExps.EMAIL, minLength = 6, maxLength = 45)
	private String email;

	@Validation(regexp = RegExps.PASSWORD, minLength = 6, maxLength = 45)
	private String password1;
	
	@Validation(regexp = RegExps.PASSWORD, minLength = 6, maxLength = 45)
	private String password2;

	public String getEmail() {
		return email;
	}

	public String getPassword1() {
		return password1;
	}

	public String getPassword2() {
		return password2;
	}
}