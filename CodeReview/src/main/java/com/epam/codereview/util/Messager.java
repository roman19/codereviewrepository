package com.epam.codereview.util;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.ResourceBundle;

public class Messager {
	public static String getMessage(String key, String localeString)
			throws UnsupportedEncodingException {
		String language = localeString.split("_")[0];
		Locale locale = new Locale(language);
		ResourceBundle messages = ResourceBundle.getBundle("messages", locale);
		return new String(messages.getString(key).getBytes("ISO-8859-1"),
				"UTF-8");
	}
}