package com.epam.codereview.util;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class FileUtil {

    private final static Logger LOG = Logger.getLogger(FileUtil.class);

    public static List<String> readFileInLines(String path) {
        List<String> lines = null;
        try {
            lines = FileUtils.readLines(new File(path), "utf-8");
        } catch (IOException e) {
            LOG.error("Error while reading file: " + path, e);
        }
        return lines;
    }


    public static String readFileInString(String path) {
        String fileString = null;
        try {
            fileString = FileUtils.readFileToString(new File(path), "utf-8").replaceAll("<", "&lt;");
        } catch (IOException e) {
            LOG.error("Error while reading file: " + path, e);
        }
        return fileString;
    }
}
