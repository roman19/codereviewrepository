package com.epam.codereview.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import com.epam.codereview.exception.WrongInputDataException;

public class ImageSaver {
	public static String save(HttpServletRequest request, String what, Integer id)
			throws IOException, ServletException, WrongInputDataException {
		String imageUrl = "";
		String path = request.getServletContext().getInitParameter("file-upload");

		File fileSaveDir = new File(path + what);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}

		path += what + "/";

		Part filePart = request.getPart("image");
		if (filePart != null) {
			String ct = filePart.getContentType();
			int i = ct.lastIndexOf("/");
			String format = ct.substring(i+1, ct.length());
			imageUrl = id + "." + format;
			path += imageUrl;
		} else {
			throw new WrongInputDataException();
		}

		filePart.write(path);

		return imageUrl;
	}
	
	public static String saveWithAjax(HttpServletRequest request, String what) throws Exception{
		if (!ServletFileUpload.isMultipartContent(request)) {
			throw new NullPointerException();
		}
		String id = null;
		String imageUrl = null;
		String path = request.getServletContext().getInitParameter("file-upload");

		File fileSaveDir = new File(path + what);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}

		path += what + "/";
		
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		ServletFileUpload upload = new ServletFileUpload(factory);
		
		try {
			List<FileItem> formItems = upload.parseRequest(request);
			for (FileItem item : formItems) {

				if (!item.isFormField()) {
					String fileName = new File(item.getName()).getName();
					String extension = FilenameUtils.getExtension(fileName);
					if (!extension.equalsIgnoreCase("jpg")
							&& !extension.equalsIgnoreCase("jpeg")
							&& !extension.equalsIgnoreCase("png")
							&& !extension.equalsIgnoreCase("raw")
							&& !extension.equalsIgnoreCase("tiff")
							&& !extension.equalsIgnoreCase("wmf")
							&& !extension.equalsIgnoreCase("jp2")
							&& !extension.equalsIgnoreCase("gif")
							&& !extension.equalsIgnoreCase("psd"))
						throw new Exception();
					imageUrl = id + "." + extension;
					path += imageUrl;
					File storeFile = new File(path);
					while (!storeFile.exists()) {
						item.write(storeFile);
					}
				} else {
					String fieldname = item.getFieldName();
					if ("groupId".equals(fieldname)) {
						String fieldvalue = item.getString();
						if (fieldvalue != null) {
							id = fieldvalue;
						}
					}
				}
			}
		} catch (Exception ex) {
			throw ex;
		}
		
		return imageUrl;
	}

}
