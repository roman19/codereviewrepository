package com.epam.codereview.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class LoginFetcher {
	public static List<String> getLoginsFromMessage(String message) {
		List<String> logins = new ArrayList<>();

		Pattern p = Pattern.compile("(?<=@)[0-9a-zA-Z_\\.]{5,45}(?=(\\s|,|$))");
		Matcher m = p.matcher(message);

		while (m.find()) {
			logins.add(m.group());
		}

		return logins.stream().distinct().collect(Collectors.toList());
	}
}
