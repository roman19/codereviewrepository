package com.epam.codereview.util;

public class PathGetter {
	public static String getLast(String path) {
		int i = path.lastIndexOf("/");
		return path.substring(i + 1, path.length());
	}

}
