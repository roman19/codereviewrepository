package com.epam.codereview.util;

public class AsyncSender implements Runnable {
	private Sender sender = new Sender();
	private String subject;
	private String text;
	private String toEmail;

	public AsyncSender(String subject, String text, String toEmail) {
		this.subject = subject;
		this.text = text;
		this.toEmail = toEmail;
	}

	@Override
	public void run() {
		sender.send(subject, text, toEmail);
	}

}
