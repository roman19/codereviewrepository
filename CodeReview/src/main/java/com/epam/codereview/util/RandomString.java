package com.epam.codereview.util;

import java.util.Random;

public class RandomString {
	private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static Random rnd = new Random();

	public static String generate(int length) 
	{
	   StringBuilder sb = new StringBuilder(length);
	   for( int i = 0; i < length; i++ ) 
	      sb.append( AB.charAt(rnd.nextInt(AB.length())));
	   return sb.toString();
	}
}
