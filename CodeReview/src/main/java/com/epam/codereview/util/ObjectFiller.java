package com.epam.codereview.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;

public class ObjectFiller {
	public static <T> void fill(T object, HttpServletRequest request)
			throws NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		Class<?> classT = object.getClass();
		for (Field field : classT.getDeclaredFields()) {

			field.setAccessible(true);
			field.set(object, request.getParameter(field.getName()));
		}

	}
}
