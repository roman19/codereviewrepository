package com.epam.codereview.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TimestampUtil {
	public static boolean isToday(Timestamp time){
		Calendar today = Calendar.getInstance();

		Calendar t = Calendar.getInstance();
		t.setTime(time);

		return (today.get(Calendar.YEAR) == t.get(Calendar.YEAR)
				&& today.get(Calendar.DAY_OF_YEAR) == t.get(Calendar.DAY_OF_YEAR));
	}
	
	public static boolean isYesterday(Timestamp time){
		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DAY_OF_YEAR, -1);

		Calendar t = Calendar.getInstance();
		t.setTime(time);

		return (yesterday.get(Calendar.YEAR) == t.get(Calendar.YEAR)
				&& yesterday.get(Calendar.DAY_OF_YEAR) == t.get(Calendar.DAY_OF_YEAR));
	}
	
	public static String extractTime(Timestamp time) {
		SimpleDateFormat df = new SimpleDateFormat("HH:mm");
		return df.format(time);
	}
	
	public static String format(Timestamp time, Locale locale){
		Calendar today = Calendar.getInstance();
		Calendar t = Calendar.getInstance();
		t.setTime(time);
		
		SimpleDateFormat df = null;
		if(today.get(Calendar.YEAR) == t.get(Calendar.YEAR)){
			df = new SimpleDateFormat("d MMM HH:mm", locale);
		}else{
			df = new SimpleDateFormat("d MMM yyyy HH:mm", locale);
		}
		return df.format(time);
	}
	
	public static String showSmartTime(Timestamp time, String locale){
		Timestamp now = new Timestamp(System.currentTimeMillis());
		long diff = Math.abs(now.getTime()-time.getTime());
		long diffInSeconds = diff/1000;
		long diffInMinutes = diff/(1000*60);
		long diffInHours = diff / (60 * 60 * 1000);
		
		if("en_US".equals(locale)) {
			if(diffInSeconds<60){
				return diffInSeconds+" seconds ago";
			}else if(diffInMinutes<60){
				if(diffInMinutes==1){
					return diffInMinutes+" minute ago";
				}else{
					return diffInMinutes+" minutes ago";
				}
			}else if(diffInHours<5){
				if(diffInHours==1){
					return diffInHours+" hour ago";
				}else{
					return diffInHours+" hours ago";
				}
			}else if(diffInHours<48){
				if(TimestampUtil.isToday(time)){
					return "Today in "+TimestampUtil.extractTime(time);
				}else /*if(TimestampUtil.isYesterday(time))*/{
					return "Yesterday in "+TimestampUtil.extractTime(time);
				}
			}else{
				return TimestampUtil.format(time, Locale.ENGLISH);
			}
		} else if("uk_UA".equals(locale)){
			if(diffInSeconds<60){
				return diffInSeconds+" секунд тому";
			}else if(diffInMinutes<60){
				String minute = null;
				int mod = (int)diffInMinutes%10;
				if(mod==1){
					minute = "хвилина";
				}else if(mod==2 || mod==3 || mod==4){
					minute = "хвилини";
				}else{
					minute = "хвилин";
				}
				return diffInMinutes+" " + minute + " тому";
			}else if(diffInHours<5){
				if(diffInHours==1){
					return diffInHours+" година тому";
				}else{
					return diffInHours+" години тому";
				}
			}else if(diffInHours<48){
				if(TimestampUtil.isToday(time)){
					return "Сьогодні у "+TimestampUtil.extractTime(time);
				}else /*if(TimestampUtil.isYesterday(time))*/{
					return "Вчора у "+TimestampUtil.extractTime(time);
				}
			}else{
				return TimestampUtil.format(time, new Locale("uk"));
			}
		}
		String language = locale.split("_")[0];
		return TimestampUtil.format(time, new Locale(language));
	}
}
