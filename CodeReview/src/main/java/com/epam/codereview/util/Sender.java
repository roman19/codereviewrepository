package com.epam.codereview.util;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Sender {
	

	public void send(String subject, String text, String toEmail) {

		try {
			MimeMessage message = new MimeMessage(SenderAuthenticator.session);

			message.setFrom(new InternetAddress(SenderAuthenticator.username));

			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(toEmail));

			message.setSubject(subject);

			message.setContent(text, "text/html; charset=utf-8");

			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}