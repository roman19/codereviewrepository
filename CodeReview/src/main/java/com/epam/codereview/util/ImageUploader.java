package com.epam.codereview.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletOutputStream;

import org.apache.commons.io.IOUtils;

import com.epam.codereview.manager.servlet.View;

public class ImageUploader {
	public static void upload(View view, String what) throws IOException{
		String path = view.getRequest().getServletContext()
				.getInitParameter("file-upload")
				+ "/";

		String imageUrl = PathGetter.getLast(view.getRequest().getPathInfo());

		path += what + "/" + imageUrl;

		view.getResponse().addHeader("Content-Type",
				"image/jpeg, image/jpg, image/png, image/gif");

		FileInputStream fileInputStream = new FileInputStream(path);
		byte[] bytes = IOUtils.toByteArray(fileInputStream);

		ServletOutputStream s = view.getResponse().getOutputStream();
		s.write(bytes);
		s.close();
	}
}
