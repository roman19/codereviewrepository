package com.epam.codereview.service;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.dto.PlainOnlineNotificationDto;
import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.exception.TransactionRollbackException;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.Branch;
import com.epam.codereview.model.CodeReview;
import com.epam.codereview.model.CodeReviewUser;
import com.epam.codereview.model.Project;
import com.epam.codereview.project.ProjectHandler;
import com.epam.codereview.project.ProjectValidator;
import com.google.gson.Gson;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CodeReviewService {

    private final static Logger LOG = Logger.getLogger(CodeReviewService.class);

    private DaoManager daoManager;

    public CodeReviewService() {
        daoManager = new DaoManager();
    }

    public CodeReviewService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public CodeReview get(Integer id) {
        return daoManager.executeAndClose(() -> daoManager.getCodeReviewDao().get(id));
    }

    public Integer insert(CodeReview codeReview) {
        return daoManager.executeAndClose(() -> daoManager.getCodeReviewDao().insert(codeReview));
    }

    public void setReviewActive(Integer reviewId, Boolean isActive) {
        Criteria criteria = new Criteria();
        criteria.add(new Condition("id", "=", reviewId));
        daoManager.executeVoidAndClose(() -> {
            List<CodeReview> temp = daoManager.getCodeReviewDao().get(criteria);
            CodeReview codeReview = temp.size() == 0 ? null : temp.get(0);
            if (codeReview != null) codeReview.setActive(isActive);
            daoManager.getCodeReviewDao().update(codeReview);
        });
    }

    public List<CodeReview> getAllReviewsByGroupId(Integer id, Boolean isReviewsActive) throws InstantiationException,
            IllegalAccessException, SQLException {
        return daoManager.executeAndClose(() -> {
            try {
                return daoManager.getCodeReviewDao().getAllReviewsByGroupId(id, isReviewsActive);
            } catch (SQLException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        });
    }

    public List<CodeReview> getMyReviewsByGroupId(Integer groupId, Boolean isReviewsActive, Integer userId)
            throws SQLException, InstantiationException, IllegalAccessException {
        return daoManager.executeAndClose(() -> {
            try {
                return daoManager.getCodeReviewDao().getMyReviewsByGroupId(groupId, isReviewsActive, userId);
            } catch (SQLException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        });
    }

    public List<CodeReview> getOtherReviewsByGroupId(Integer groupId, Boolean isReviewsActive, Integer userId)
            throws SQLException, InstantiationException, IllegalAccessException {
        return daoManager.executeAndClose(() -> {
            try {
                return daoManager.getCodeReviewDao().getOtherReviewsByGroupId(groupId, isReviewsActive, userId);
            } catch (SQLException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        });
    }

    public CodeReview getByFileId(Integer fileId) {
        return daoManager.executeAndClose(() -> daoManager.getCodeReviewDao().getByFileId(fileId));
    }

    //TODO test after changes
    public void createCodeReview(View view, String projectsPath) {
        try {
            String responseMessage = daoManager.transactionAndClose(() -> {
                try {
                    String projectName = view.getParameter("projectName");
                    String codeReviewName = view.getParameter("codeReviewName");
                    String[] pathes = (view.getParameter("pathes") == null ? null : view.getParameter("pathes").split(","));
                    String[] reviewers = (view.getParameter("reviewers") == null ? null : view.getParameter("reviewers").split(","));
                    String branchName = view.getParameter("branch");
                    view.getResponse().setCharacterEncoding("UTF-8");
                    view.getResponse().setContentType("application/text");

                    if (pathes.length == 1 && pathes[0].equals("")) {
                        return "no pathes";
                    } else if (reviewers.length == 1 && reviewers[0].equals("")) {
                        return "no reviewers";
                    } else {
                        ProjectValidator.validate(new File(projectsPath + view.getSessionUserID() + "/"
                                + pathes[0].split("\\\\")[1]));

                        CodeReview codeReview = new CodeReview();
                        if (codeReviewName.length() > 45)
                            codeReviewName = codeReviewName.substring(0, 44);
                        codeReview.setName(codeReviewName);
                        codeReview.setGroupId(Integer.valueOf(view.getParameter("group_id")));
                        codeReview.setOwnerId(view.getSessionUserID());
                        Integer codeReviewID = daoManager.getCodeReviewDao().insert(codeReview);
                        if (codeReviewID == null) {
                            return "failed";
                        } else {
                            ProjectHandler projectHandler = new ProjectHandler(pathes, projectsPath);
                            projectHandler.removeUncheckedFiles(view.getSessionUserID() + "/" + pathes[0].split("\\\\")[1]);
                            Project project = new Project();
                            project.setCodeReviewId(codeReviewID);
                            if (projectName.length() > 45)
                                projectName = projectName.substring(0, 44);
                            project.setName(projectName);
                            String fullName = pathes[0].split("\\\\")[1];
                            if (fullName.length() > 100)
                                fullName = fullName.substring(0, 99);
                            project.setFullName(fullName);
                            project.setGitUrl(view.getParameter("git_url"));
                            if (project.getGitUrl() != null && project.getGitUrl().equals("")) {
                                project.setGitUrl(null);
                            }
                            project.setSize(projectHandler.getProjectSize());
                            Integer projectID = daoManager.getProjectDao().insert(project);

                            // for e-mail and online notification
                            PlainOnlineNotificationDto notificationDto = new PlainOnlineNotificationDto();
                            List<Integer> usersTo = new ArrayList<>();
                            // end

                            UserService userService = new UserService(daoManager);
                            CodeReviewUser codeReviewUser = new CodeReviewUser();
                            codeReviewUser.setCodeReviewId(codeReviewID);
                            for (String reviewer : reviewers) {
                                Integer userID = userService.getByLogin(getLogin(reviewer)).getId();
                                codeReviewUser.setUserId(userID);

                                new CodeReviewUserService(daoManager).insert(codeReviewUser);

                                // send notification on e-mail
                                new NotificationService(daoManager)
                                        .sendReviewNotification(view.getSessionUserID(), userID, codeReviewID, codeReviewName);
                                // add user to notify online
                                usersTo.add(userID);
                            }
                            // build online notification
                            notificationDto.setNotificationType("REVIEW");
                            notificationDto.setOwner(userService.get(view.getSessionUserID()));
                            notificationDto.setItemId(codeReviewID);
                            notificationDto.setItemName(codeReviewName);
                            notificationDto.setUsers(usersTo);
                            // end

                            Branch branch = new Branch();
                            branch.setName(branchName);
                            branch.setProjectId(projectID);
                            Integer branchID = new BranchService(daoManager).insert(branch);

                            com.epam.codereview.model.File file = new com.epam.codereview.model.File();
                            file.setBranchId(branchID);
                            for (String path : pathes) {
                                String tempPath = path.replaceAll("\\\\", "/");
                                if (new File(projectsPath + tempPath).isDirectory())
                                    continue;
                                file.setUrl(tempPath);
                                String temp = FilenameUtils.getExtension(path);
                                try{
                                    file.setLanguageId(new LanguageService(daoManager).getLanguageByFormat(temp).getId());
                                }catch(Exception e){
                                	continue;
                                }
                                file.setSize(new File(projectsPath + tempPath).length());
                                new FileService(daoManager).insert(file);
                            }
                            String json = new Gson().toJson(notificationDto);
                            view.getResponse().setContentType("application/json");
                            return json;
                        }
                    }
                } catch (SessionIsNotAvailableException e) {
                    LOG.error("Failed while review creation.", e);
                    throw new RuntimeException(e);
                }
            });

            view.getResponse().getWriter().write(responseMessage);

        } catch (TransactionRollbackException e) {
            LOG.error("Failed while review creation.", e);
        } catch (IOException e) {
            LOG.error("Failed while send response.", e);
        }
    }

    private String getLogin(String reviewer) {
        return reviewer.substring(reviewer.indexOf("(") + 1, reviewer.length() - 1);
    }

}
