package com.epam.codereview.service;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.model.Issue;

public class IssueService {

    private DaoManager daoManager = new DaoManager();

	public void setNotified(Integer issueId){
		Issue issue = daoManager.executeAndClose(() -> daoManager.getIssueDao().get(issueId));

		issue.setNotified(true);
        daoManager.executeVoidAndClose(() -> daoManager.getIssueDao().update(issue));
	}
}
