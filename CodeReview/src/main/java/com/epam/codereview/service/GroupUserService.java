package com.epam.codereview.service;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.dto.UserGroupDto;
import com.epam.codereview.exception.TransactionRollbackException;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.manager.db.Operator;
import com.epam.codereview.model.GroupUser;
import org.apache.log4j.Logger;

import java.util.List;

public class GroupUserService {

    private final static Logger LOG = Logger.getLogger(GroupUserService.class);

    private DaoManager daoManager;

    public GroupUserService() {
        daoManager = new DaoManager();
    }

    public GroupUserService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public GroupUser get(Integer id) {
        return daoManager.executeAndClose(() -> daoManager.getGroupUserDao().get(id));
	}

    public GroupUser getByUser(Integer id) {
    	Criteria criteria = new Criteria();
		criteria.add(new Condition("user_id", "=", id));
		List<GroupUser> groupUsers = get(criteria);
		return groupUsers.size() == 0 ? null : groupUsers.get(0);
	}

	public List<GroupUser> get(Criteria criteria) {
        return daoManager.executeAndClose(() -> daoManager.getGroupUserDao().get(criteria));
	}
	
	
	
	
	public void insertUserIntoGroup(GroupUser groupUser){
        daoManager.executeAndClose(() -> daoManager.getGroupDao().insert(groupUser));
	}

	public List<GroupUser> getUsersByGroup(Integer id) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("group_id", "=", id));
		criteria.add(new Operator("AND"));
		criteria.add(new Condition("status", "=", "CONFIRMED"));
		return get(criteria);
	}

	public List<GroupUser> getUserWithAllStatus(int groupId){
		 return daoManager.executeAndClose(() -> daoManager.getGroupUserDao().getUserWithAllStatus(groupId));
	}
	public List<GroupUser> getUserWithLeftStatus(int groupId, int userId){
		 return daoManager.executeAndClose(() -> daoManager.getGroupUserDao().getUserWithLeftStatus(groupId,userId));
	}
	
	public void update(GroupUser groupUser) {
        daoManager.executeVoidAndClose(() -> daoManager.getGroupUserDao().update(groupUser));
	}
	
	public void deleteUserFromGroup(GroupUser groupUser){
		groupUser.setStatus("DELETED");
		update(groupUser);
	}

	//TODO check that works fine after changes
    public Integer createGroup(String groupName, boolean isImage, int ownerId, List<Integer> usersId) {
        try {
            return daoManager.transactionAndClose(() -> {
                Integer groupId = null;

                groupId = daoManager.getGroupDao().insertUserGroupReview(groupName, isImage, ownerId);
                for (Integer currentUserID : usersId) {
                    daoManager.getGroupUserDao().insertUserGroupReview(groupId, currentUserID);

                    // send notification on e-mail
                    new NotificationService(daoManager).sendGroupNotification(ownerId, groupId, groupName, currentUserID);
                }
                return groupId;
            });
        } catch (TransactionRollbackException e) {
            LOG.error("Rollback while group creation.", e);
        }
        return null;
	}

	/*
	 * @author Vitalii Horban to edit group
	 */
	public void insertUserGroupReview(int groupId, int userId) {
        daoManager.executeAndClose(() -> daoManager.getGroupUserDao().insertUserGroupReview(groupId, userId));
	}

	public GroupUser getByGroupIdAndUserId(Integer groupId, Integer userId) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("user_id", "=", userId));
		criteria.add(new Operator("AND"));
		criteria.add(new Condition("group_id", "=", groupId));

		List<GroupUser> groupUsers = get(criteria);

		return groupUsers.size() == 0 ? null : groupUsers.get(0);
	}

    public UserGroupDto getUserGroupDto(Integer userId) {
        return daoManager.executeAndClose(() -> daoManager.getUserDao().getUser(userId));
    }

    public List<UserGroupDto> getAllUsersForGroupAutocompleter(Integer userId) {
        return daoManager.executeAndClose(() -> daoManager.getUserDao().getAllUsersGroup(userId));
    }
}
