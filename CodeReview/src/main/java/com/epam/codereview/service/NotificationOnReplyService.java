package com.epam.codereview.service;

import com.epam.codereview.dao.manager.DaoManager;

public class NotificationOnReplyService {

    private DaoManager daoManager;

    public NotificationOnReplyService() {
        daoManager = new DaoManager();
    }

    public NotificationOnReplyService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public void setNotified(Integer userId, Integer replyId) {
        daoManager.executeVoidAndClose(() -> daoManager.getNotificationOnReplyDao().setNotified(userId, replyId));
	}
}
