package com.epam.codereview.service;

import java.util.List;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.dto.ReviewDto;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.manager.db.Operator;
import com.epam.codereview.model.CodeReviewUser;

public class CodeReviewUserService {

    private DaoManager daoManager;

    public CodeReviewUserService() {
        daoManager = new DaoManager();
    }

    public CodeReviewUserService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public List<CodeReviewUser> get(Criteria criteria) {
		return daoManager.executeAndClose(() -> daoManager.getCodeReviewUserDao().get(criteria));
	}
	
	public void leaveReview(Integer reviewId, Integer currentUserId){
		Criteria criteria = new Criteria();
		criteria.add(new Condition("code_review_id", "=", reviewId));
		criteria.add(new Operator("AND"));
		criteria.add(new Condition("user_id", "=", currentUserId));
		List<CodeReviewUser> user = get(criteria);
		CodeReviewUser codeReviewUser = user.size() == 0 ? null : user.get(0);
        daoManager.executeVoidAndClose(() -> daoManager.getCodeReviewUserDao().update(codeReviewUser));
	}
	
	public List<CodeReviewUser> getAllReviewersByCodeReviewId(Integer codeReviewId){
		Criteria criteria = new Criteria();
		criteria.add(new Condition("code_review_id", "=", codeReviewId));
		return get(criteria);
	}
	
	public void setStatusToAllReviewers(String status, Integer codeReviewId){
		List<CodeReviewUser> codeReviewUsers = getAllReviewersByCodeReviewId(codeReviewId);
        daoManager.executeVoidAndClose(() -> {
            for (CodeReviewUser codeReviewUser : codeReviewUsers) {
                codeReviewUser.setStatus(status);
                daoManager.getCodeReviewUserDao().update(codeReviewUser);
            }
        });
	}
	
	public int getNoOfRecords() {
		return daoManager.executeAndClose(() -> daoManager.getCodeReviewUserDao().getNoOfRecords());
	}

	public void setNoOfRecords(int noOfRecords) {
        daoManager.executeVoidAndClose(() -> daoManager.getCodeReviewUserDao().setNoOfRecords(noOfRecords));
	}

	public CodeReviewUser get(Integer id){
		return daoManager.executeAndClose(() -> daoManager.getCodeReviewUserDao().get(id));
	}

	public Integer insert(CodeReviewUser codeReviewUser) {
		return daoManager.executeAndClose(() -> daoManager.getCodeReviewUserDao().insert(codeReviewUser));
	}
	
	public void update(CodeReviewUser codeReviewUser) {
        daoManager.executeVoidAndClose(() -> daoManager.getCodeReviewUserDao().update(codeReviewUser));
	}
	
	public List<ReviewDto> getAllMyActiveReviwByUserId(Integer userID, int offset, int noOfRecords) {
		return  daoManager.executeAndClose(() ->
                daoManager.getCodeReviewUserDao().getAllMyActiveReviwByUserId(userID, offset, noOfRecords));
	}
	
	public List<ReviewDto> getAllMyHistoryReviwByUserId(Integer userID, int offset, int noOfRecords) {
        return  daoManager.executeAndClose(() ->
                daoManager.getCodeReviewUserDao().getAllMyHistoryReviwByUserId(userID, offset, noOfRecords));
	}
	
	public List<ReviewDto> getAllOtherActiveReviwByUserId(Integer userID, int offset, int noOfRecords) {
        return  daoManager.executeAndClose(() ->
                daoManager.getCodeReviewUserDao().getAllOtherActiveReviwByUserId(userID, offset, noOfRecords));
	}
	
	public List<ReviewDto> getAllOtherHistoryReviwByUserId(Integer userID, int offset, int noOfRecords) {
        return  daoManager.executeAndClose(() ->
                daoManager.getCodeReviewUserDao().getAllOtherHistoryReviwByUserId(userID, offset, noOfRecords));
	}
	
	public List<ReviewDto> getAllActiveReviwByUserId(Integer userID, int offset, int noOfRecords) {
        return  daoManager.executeAndClose(() ->
                daoManager.getCodeReviewUserDao().getAllActiveReviwByUserId(userID, offset, noOfRecords));
	}
	
	public List<ReviewDto> getAllHistoryReviwByUserId(Integer userID, int offset, int noOfRecords) {
        return  daoManager.executeAndClose(() ->
                daoManager.getCodeReviewUserDao().getAllHistoryReviwByUserId(userID, offset, noOfRecords));
	}
	
	public CodeReviewUser getByReviewIdAndUserId(Integer reviewId, Integer userId) {

		Criteria criteria = new Criteria();
		criteria.add(new Condition("user_id", "=", userId));
		criteria.add(new Operator("AND"));
		criteria.add(new Condition("code_review_id", "=", reviewId));

		List<CodeReviewUser> reviewUsers = get(criteria);

		return reviewUsers.size() == 0 ? null : reviewUsers.get(0);
	}
	
	public List<Integer> getUsersByReviewId(Integer reviewId) {
		return daoManager.executeAndClose(() -> daoManager.getCodeReviewUserDao().getUsersByReviewId(reviewId));
	}
	
}
