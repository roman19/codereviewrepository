package com.epam.codereview.service;

import java.sql.SQLException;
import java.util.List;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.model.Project;

public class ProjectService {

    private DaoManager daoManager;

    public ProjectService() {
        daoManager = new DaoManager();
    }

    public ProjectService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public Integer insert(Project project) {
		return daoManager.executeAndClose(() -> daoManager.getProjectDao().insert(project));
	}

	public void update(Project project) {
        daoManager.executeVoidAndClose(() -> daoManager.getProjectDao().update(project));
	}

    public Long getUsedMemoryByUser(Integer userId) throws SQLException{
		return daoManager.executeAndClose(() -> daoManager.getProjectDao().getUsedMemoryByUser(userId));
    }
	
	public Project getByReviewId(Integer codeReviewId){
		Criteria criteria = new Criteria();
		criteria.add(new Condition("code_review_id", "=", codeReviewId));
        List<Project> project = daoManager.executeAndClose(() -> daoManager.getProjectDao().get(criteria));
		return project.size() == 0 ? null : project.get(0);
	}
}
