package com.epam.codereview.service;

import com.epam.codereview.exception.ValidationException;
import com.epam.codereview.model.User;
import com.epam.codereview.oauth2.GitHubOAuth2Details;
import com.google.common.io.CharStreams;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GitHubService {

	public User getUserByAccessToken(String accessToken) throws IOException, ParseException,
			ValidationException {

		User user = new User();
		String getUsetInfo = GitHubOAuth2Details.methodUri + "?access_token="
				+ accessToken;

		URL myurl = new URL(getUsetInfo);
		HttpsURLConnection con = (HttpsURLConnection) myurl.openConnection();
		InputStream ins = con.getInputStream();
		InputStreamReader isr = new InputStreamReader(ins, "UTF-8");
		JSONParser parser = new JSONParser();

		JSONObject jObject = (JSONObject) parser.parse(isr);

		user.setSocial("GITHUB");
		user.setSocialId(jObject.get("id").toString());
		String login = jObject.get("login").toString();
		user.setLogin(login);
		user.setFirstName(login);
		user.setLastName(login);
		user.setImageUrl(jObject.get("avatar_url").toString());
		user.setImageFromServer(false);
		user.setEmail(getEmailByAccessToken(accessToken));
		
		getEmailByAccessToken(accessToken);

		return user;

	}

	private String getEmailByAccessToken(String accessToken) throws IOException, ParseException,
			ValidationException {

		String email = null;
		String getUsetInfo = GitHubOAuth2Details.methodUri + "/emails?access_token="
				+ accessToken;

		URL myurl = new URL(getUsetInfo);
		HttpsURLConnection con = (HttpsURLConnection) myurl.openConnection();
		InputStream ins = con.getInputStream();
		InputStreamReader isr = new InputStreamReader(ins, "UTF-8");
		JSONParser parser = new JSONParser();

		JSONArray emails = (JSONArray) parser.parse(isr);
		email = ((JSONObject)emails.get(0)).get("email").toString();
		
		return email;
	}

	private String getPlainToken(String response) {
		Pattern p = Pattern.compile("(?<=(access_token=)).*(?=\\&scope)");
		Matcher m = p.matcher(response);

		if (m.find()) {
			return m.group(0);
		} else {
			return null;
		}
	}

	public String getAccessTokenByCode(String code) {
		String accessToken = null;
		try {

			String accessTokenString = GitHubOAuth2Details.accessTokenUri
					+ "?client_id=" + GitHubOAuth2Details.clientId
					+ "&client_secret=" + GitHubOAuth2Details.clientSecret
					+ "&code=" + code;

			URL myurl = new URL(accessTokenString);
			HttpsURLConnection con = (HttpsURLConnection) myurl
					.openConnection();
			InputStream ins = con.getInputStream();
			InputStreamReader isr = new InputStreamReader(ins, "UTF-8");

			accessToken = getPlainToken(CharStreams.toString(isr));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return accessToken;
	}
}
