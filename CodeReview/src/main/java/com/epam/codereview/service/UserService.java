package com.epam.codereview.service;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.net.ssl.HttpsURLConnection;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.manager.db.Operator;
import com.epam.codereview.model.ForgottenUser;
import com.epam.codereview.model.User;
import com.epam.codereview.util.RandomString;
import com.epam.codereview.util.Sender;

public class UserService {

    private DaoManager daoManager;

    public UserService() {
        daoManager = new DaoManager();
    }

    public UserService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public User get(Integer id) {
        return daoManager.executeAndClose(() -> daoManager.getUserDao().get(id));
	}

	public List<User> get(Criteria criteria) {
        return daoManager.executeAndClose(() -> daoManager.getUserDao().get(criteria));
	}

	public List<User> getAll() {
        return daoManager.executeAndClose(() -> daoManager.getUserDao().getAll());
	}

	public List<User> getByGroup(Integer id) {
		return daoManager.executeAndClose(() -> {
            try {
                List<User> list = daoManager.getUserDao().getUsersByGroup(id);
                if (list != null) {
                    list.add(getByID(new GroupService(daoManager).getGroupByID(id).getUserId()));
                }
                return list;
            } catch (SQLException | IOException | InstantiationException | PropertyVetoException | IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        });
	}

	public User getByID(Integer id) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("id", "=", id));
		List<User> user = daoManager.executeAndClose(() -> daoManager.getUserDao().get(criteria));
		return user.size() == 0 ? null : user.get(0);
	}

	public User getByConfirmCode(String code) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("confirm_code", "LIKE", code));
		List<User> user = daoManager.executeAndClose(() -> daoManager.getUserDao().get(criteria));
		return user.size() == 0 ? null : user.get(0);
	}

	public User getByLogin(String login) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("login", "LIKE", login));
		List<User> user = daoManager.executeAndClose(() -> daoManager.getUserDao().get(criteria));
		return user.size() == 0 ? null : user.get(0);
	}

	public User getByEmail(String email) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("email", "LIKE", email));
		List<User> user = daoManager.executeAndClose(() -> daoManager.getUserDao().get(criteria));
		return user.size() == 0 ? null : user.get(0);
	}

	public User getBySocial(String social, String socialId) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("social_id", "LIKE", socialId));
		criteria.add(new Operator("AND"));
		criteria.add(new Condition("social", "LIKE", social));

		List<User> user = daoManager.executeAndClose(() -> daoManager.getUserDao().get(criteria));
		return user.size() == 0 ? null : user.get(0);
	}
	
	public Integer getUserIdByFileId(Integer fileId){
		return daoManager.executeAndClose(() -> daoManager.getCodeReviewDao().getByFileId(fileId).getOwnerId());
	}

	public boolean isForgotten(String email, int expireTime) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("email", "LIKE", email));
		List<ForgottenUser> fusers = daoManager.executeAndClose(() -> daoManager.getForgottenUserDao().get(criteria));
		if (fusers.size() > 0) {
			for (ForgottenUser fuser : fusers) {
				if (System.currentTimeMillis() - fuser.getForgetDate().getTime() < expireTime * 3600000) {
					return true;
				}
			}
			return false;

		} else {
			return false;
		}
	}

	public ForgottenUser getForgottenUserByCode(String code) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("code", "LIKE", code));
		List<ForgottenUser> fusers = daoManager.executeAndClose(() -> daoManager.getForgottenUserDao().get(criteria));
		return fusers.size() == 0 ? null : fusers.get(0);
	}

	public void rememberedPassword(String email) {
		User user = getByEmail(email);
		user.setStatus("CONFIRMED");
        daoManager.executeVoidAndClose(() -> daoManager.getUserDao().update(user));

		Criteria criteria = new Criteria();
		criteria.add(new Condition("email", "LIKE", user.getEmail()));
		List<ForgottenUser> fusers = daoManager.executeAndClose(() -> daoManager.getForgottenUserDao().get(criteria));

        for (ForgottenUser fuser : fusers) {
            daoManager.executeVoidAndClose(() -> daoManager.getForgottenUserDao().delete(fuser));
		}
	}

	public void clickedOnNewPasswordLink(String code) {
		ForgottenUser fuser = getForgottenUserByCode(code);

		Criteria criteria = new Criteria();
		criteria.add(new Condition("email", "LIKE", fuser.getEmail()));
        List<ForgottenUser> fusers = daoManager.executeAndClose(() -> daoManager.getForgottenUserDao().get(criteria));

		for (ForgottenUser u : fusers) {
            daoManager.executeVoidAndClose(() -> daoManager.getForgottenUserDao().delete(u));
		}
	}

	public Integer insertFromSocNet(User user) {
		return daoManager.executeAndClose(() -> daoManager.getUserDao().insert(user));
	}

	public Integer insert(User user) {

		Integer id = daoManager.executeAndClose(() -> daoManager.getUserDao().insert(user));
		user.setConfirmCode(user.getConfirmCode() + id);
		user.setId(id);

        daoManager.executeVoidAndClose(() -> daoManager.getUserDao().update(user));

        sendConfirmation(user);

		return id;
	}

	private void sendConfirmation(User user) {
		Sender sender = new Sender();
		String subject = "Confirm registration";
		String link = "http://localhost:8080/CodeReview/signup_confirmation?token=" + user.getConfirmCode();

		String text = "<body style='background-color: #f5f5f0'>"
				+

				"<div style='width: 100%; position: relative'>"
				+ "<div style='top: 10px; background-color: white; margin:100px; padding:20px'>"
				+ "<div><h1 style='color: green;'> Hi,"
				+ user.getFirstName()
				+ "</h1>"
				+ "<p style='border-bottom: 1px solid grey; border-bottom-width: 80%; padding-bottom: 10px'>To confirm registration click on the link:</p>"
				+ "</div><div id='one'><p>" + link + "<p></div></div></div></body>";

		sender.send(subject, text, user.getEmail());
	}

	public void update(User user) {
        daoManager.executeVoidAndClose(() -> daoManager.getUserDao().update(user));
	}

	public Integer setNewPassword(String email, String hash) {
		User user = getByEmail(email);

		user.setPassword(hash);
		user.setStatus("CONFIRMED");

        daoManager.executeVoidAndClose(() -> daoManager.getUserDao().update(user));

        return user.getId();
	}

	public void forgotPassword(User user) {
		sendForgotPassword(user);

		user.setStatus("FORGOTTEN");
        daoManager.executeVoidAndClose(() -> daoManager.getUserDao().update(user));
    }

	private void sendForgotPassword(User user) {
		String code = RandomString.generate(40) + user.getId();

		ForgottenUser forgottenUser = new ForgottenUser();
		forgottenUser.setEmail(user.getEmail());
		forgottenUser.setCode(code);

        daoManager.executeAndClose(() -> daoManager.getForgottenUserDao().insert(forgottenUser));

		Sender sender = new Sender();
		String subject = "Forgotten password";

		String link = "http://localhost:8080/CodeReview/goToPasswordResettig?token=" + code;

		String text = "<body style='background-color: #f5f5f0'>"
				+

				"<div style='width: 100%; position: relative'>"
				+ "<div style='top: 10px; background-color: white; margin:100px; padding:20px'>"
				+ "<div><h1 style='color: green;'> Hi,"
				+ user.getFirstName()
				+ "</h1>"
				+ "<p style='border-bottom: 1px solid grey; border-bottom-width: 80%; padding-bottom: 10px'>To reset the password, click on the link:</p>"
				+ "</div><div id='one'><p>" + link + "<p></div></div></div></body>";

		sender.send(subject, text, user.getEmail());
	}

	public Integer getMaxId() throws SQLException {
		return daoManager.executeAndClose(() -> {
            try {
                return daoManager.getUserDao().getMaxId();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        });
	}

	public void setOffline(Integer id) {
		User user = daoManager.executeAndClose(() -> daoManager.getUserDao().get(id));

		user.setLastOnline(new Timestamp(new Date().getTime()));

        daoManager.executeVoidAndClose(() -> daoManager.getUserDao().update(user));
	}

    public List<User> getAllActiveReviewUsers(Integer reviewId) {
        return daoManager.executeAndClose(() -> daoManager.getUserDao().getAllActiveByReviewId(reviewId));
    }
	
	public List<String> getUsersToReplyByIssyeId(Integer issueId){
        List<User> users = daoManager.executeAndClose(() -> daoManager.getUserDao().getUsersToReplyByIssueId(issueId));
        return users.stream().map(User::getLogin).collect(Collectors.toList());
	}
	
	public String getGenderByFirstName(String firstName) throws IOException, ParseException{
		String url = "https://gender-api.com/get?key=RwZBlQJwcnHzgFdotw&name="+URLEncoder.encode(firstName, "UTF-8");

		
		
		URL myurl = new URL(url);
		HttpsURLConnection conn = (HttpsURLConnection) myurl.openConnection();
		conn.setRequestProperty("content-type", "application/json;  charset=utf-8");
		conn.setRequestProperty("Accept-Charset", "utf-8");
		

		InputStream ins = conn.getInputStream();
	    
		InputStreamReader isr = new InputStreamReader(ins, "UTF-8");
		JSONParser parser = new JSONParser();

		JSONObject jObject = (JSONObject) parser.parse(isr);
		
		conn.disconnect();
		
		return jObject.get("gender").toString();
	}
}
