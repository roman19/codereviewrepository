package com.epam.codereview.service;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.dto.EditGroupWithUsersDto;
import com.epam.codereview.dto.GroupDto;
import com.epam.codereview.exception.TransactionRollbackException;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.model.CodeReview;
import com.epam.codereview.model.CodeReviewUser;
import com.epam.codereview.model.Group;


public class GroupService {

    private final static Logger LOG = Logger.getLogger(GroupService.class);

    private DaoManager daoManager;

    public GroupService() {
        daoManager = new DaoManager();
    }

    public GroupService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public int getNoOfRecords() {
		return daoManager.executeAndClose(() ->
                daoManager.getGroupDao().getNoOfRecords());
	}

	public void setNoOfRecords(int noOfRecords) {
        daoManager.executeVoidAndClose(() ->
                daoManager.getGroupDao().setNoOfRecords(noOfRecords));
	}

	public List<Group> get(Criteria criteria) {
        return daoManager.executeAndClose(() ->
                daoManager.getGroupDao().get(criteria));
	}
	
	
	public List<Group> getByUserID(Integer id)
			throws InstantiationException, IllegalAccessException, SQLException, IOException, PropertyVetoException {
		return daoManager.executeAndClose(() -> {
            try {
                return daoManager.getGroupDao().getGroupsByClientID(id);
            } catch (SQLException | IOException | PropertyVetoException | IllegalAccessException | InstantiationException e) {
                LOG.error("Get user groups failed.", e);
                throw new RuntimeException(e);
            }
        });
	}
	
	public Group getById(Integer id){
		Criteria criteria = new Criteria();
		criteria.add(new Condition("id", "=", id));
		List<Group> group = get(criteria);
		return group.size() == 0 ? null : group.get(0);
	}
	
	public void update(Group group){
		daoManager.executeVoidAndClose(() -> daoManager.getGroupDao().update(group));
	}
	
	public void editGroupName(int groupId , String groupName) {
        daoManager.executeVoidAndClose(() ->
                daoManager.getGroupDao().editGroupName(groupId, groupName));
	}

	/*
	 * @author Vitalii Horban to edit group
	 */
	public void deleteUserFromGroup(int groupId , int userId) {
        daoManager.executeVoidAndClose(() ->
                daoManager.getGroupDao().deleteUserFromGroup(groupId, userId));
	}
	
	/*
	 * @author Vitalii Horban to edit group
	 */

	public List<Integer> getOldUsers(List<Integer> oldList, List<Integer> newList) {
		List<Integer> listOld = new ArrayList<>();
		List<Integer> copy = new ArrayList<>();

		listOld.addAll(oldList);
		copy.addAll(listOld);

		List<Integer> listNew = new ArrayList<>();
		listNew.addAll(newList);

		copy.retainAll(listNew);
		listOld.removeAll(copy);

		return listOld;
	}

	/*
	 * @author Vitalii Horban to edit group
	 */
	public List<Integer> getNewUsers(List<Integer> oldList, List<Integer> newList) {

		List<Integer> copy = new ArrayList<>();
		copy.addAll(oldList);
		
		List<Integer> listNew = new ArrayList<>();
		List<Integer> listNewCopy = new ArrayList<>();
		
		listNewCopy.addAll(newList);
		listNew.addAll(newList);
		
		copy.retainAll(listNew);
		listNewCopy.removeAll(copy);

		return listNewCopy;
	}

	/*
	 * @author Vitalii Horban to edit group
	 */
	public List<Integer> getAllUsersFromGroup(Integer groupID) {
        return daoManager.executeAndClose(() ->
                daoManager.getGroupDao().getAllUsersFromGroup(groupID));
	}

	public Group getGroupByID(Integer id){
		Criteria criteria = new Criteria();
		criteria.add(new Condition("id", "=", id));
		List<Group> group = get(criteria);
		return group.size() == 0 ? null : group.get(0);
	}

	/*
	 * @author Vitalii Horban to delete all users with group
	 */
	public void deleteAllUsersAndGroup(Integer groupId) {
        try {
            daoManager.transactionAndClose(() -> {
                daoManager.getGroupDao().deleteUsersFromGroup(groupId);
                daoManager.getGroupDao().deleteGroup(groupId);
                return null;
            });
        } catch (TransactionRollbackException e) {
            LOG.error("Delete users and group failed.", e);
        }
    }

	/*
	 * @author Vitalii Horban for filters 'My' in groups
	 */

	public List<GroupDto> getAllMyGroups(Integer userID, int offset, int noOfRecords) {
		List<GroupDto> list = daoManager.executeAndClose(() ->
                daoManager.getGroupDao().getAllMYGroupsByUserId(userID,offset,noOfRecords));
		Collections.sort(list, (o1, o2) -> (-1) * o1.getGroupId().compareTo(o2.getGroupId()));
		return list;
	}

	/*
	 * @author Vitalii Horban for filters 'Other' in groups
	 */

	public List<GroupDto> getAllOtherGroups(Integer userID, int offset, int noOfRecords) {
		List<GroupDto> list = daoManager.executeAndClose(() ->
                daoManager.getGroupDao().getAllOtherGroupsByUserId(userID, offset, noOfRecords));
		Collections.sort(list, (o1, o2) -> (-1) * o1.getGroupId().compareTo(o2.getGroupId()));
		return list;
	}
	
	public void removeUser(Integer groupId, Integer userId, String projectsPath){
		try {
			daoManager.transactionAndClose(() -> {
			    try {
				    daoManager.getGroupDao().deleteUserFromGroup(groupId, userId);
				    List<CodeReviewUser> codeReviewUsers = daoManager.getCodeReviewUserDao().
				    		getReviewerInAllReviewsByGroup(groupId, userId);
				    for(CodeReviewUser codeReviewUser : codeReviewUsers){
				    	if(codeReviewUser.getStatus().equals("CONFIRMED") 
				    			|| codeReviewUser.getStatus().equals("PENDING"))
				    		codeReviewUser.setStatus("DELETED_BY_ADMIN");
				    	daoManager.getCodeReviewUserDao().update(codeReviewUser);
				    }
				    List<CodeReview> codeReviews = daoManager.getCodeReviewDao().
				    		getMyReviewsByUserAndGroup(userId, groupId);
				    ReviewService reviewService = new ReviewService(daoManager);
				    for(CodeReview codeReview : codeReviews){
				    	reviewService.deleteReview(codeReview.getId(), projectsPath);
				    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			    return null;
			});
		} catch (TransactionRollbackException e) {
			LOG.error("Can`t leave group.", e);
		}
	}

	/*
	 * @author Vitalii Horban to delete groups
	 */
	public void deleteGroup(int groupId, String projectsPath) {
		try {
			daoManager.transactionAndClose(() -> {
                daoManager.getGroupDao().deleteGroup(groupId);
				try {
	                daoManager.getGroupDao().deleteGroup(groupId);
					List<CodeReview> codeReviews = daoManager.getCodeReviewDao().getAllReviewsByGroupId(groupId, true);
					codeReviews.addAll(daoManager.getCodeReviewDao().getAllReviewsByGroupId(groupId, false));
				    ReviewService reviewService = new ReviewService(daoManager);
				    for(CodeReview codeReview : codeReviews){
				    	reviewService.deleteReview(codeReview.getId(), projectsPath);
				    }
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			});
		} catch (TransactionRollbackException e) {
			LOG.error("Can`t delete group.", e);
		}
	}

	/*
	 * @author Vitalii Horban to exit group
	 */
	public void exitGroup(int groupId, int userId, String projectsPath) {
		try {
			daoManager.transactionAndClose(() -> {
			    try {
				    daoManager.getGroupDao().exitGroup(groupId, userId);
				    List<CodeReviewUser> codeReviewUsers = daoManager.getCodeReviewUserDao().
				    		getReviewerInAllReviewsByGroup(groupId, userId);
				    for(CodeReviewUser codeReviewUser : codeReviewUsers){
				    	codeReviewUser.setStatus("LEFT");
				    	daoManager.getCodeReviewUserDao().update(codeReviewUser);
				    }
				    List<CodeReview> codeReviews = daoManager.getCodeReviewDao().
				    		getMyReviewsByUserAndGroup(userId, groupId);
				    ReviewService reviewService = new ReviewService(daoManager);
				    for(CodeReview codeReview : codeReviews){
				    	reviewService.deleteReview(codeReview.getId(), projectsPath);
				    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			    return null;
			});
		} catch (TransactionRollbackException e) {
			LOG.error("Can`t leave group.", e);
		}
	}

	/*
	 * @author Vitalii Horban for filters 'All' in groups
	 */
	public List<GroupDto> getAllGroups(Integer userID, int offset, int noOfRecords) {
		List<GroupDto> groupsList = daoManager.executeAndClose(() ->
                daoManager.getGroupDao().getAllGroupsByUserId(userID, offset, noOfRecords));
		Collections.sort(groupsList, (o1, o2) -> (-1) * o1.getGroupId().compareTo(o2.getGroupId()));
		return groupsList;
	}

	
	
	public Integer countUsersInGroup(Integer groupID) {
		return daoManager.executeAndClose(() ->
                daoManager.getGroupDao().countUsersInGroup(groupID));
	}
	
	
	public EditGroupWithUsersDto getGroupWithUsersByGroupId(Integer groupID) {
		return daoManager.executeAndClose(() ->
                daoManager.getGroupDao().getGroupWithUsersByGroupId(groupID));
	}

}
