package com.epam.codereview.service;

import java.util.List;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.model.NotificationSetting;

public class NotificationSettingsService {

	private DaoManager daoManager;

    public NotificationSettingsService() {
        daoManager = new DaoManager();
    }

    public NotificationSettingsService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public List<NotificationSetting> get(Criteria criteria) {
		return  daoManager.executeAndClose(() -> daoManager.getNotificationSettingDao().get(criteria));
	}

	public NotificationSetting getNotificationSettingsByUserId(Integer userId) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("user_id", "=", userId));
		List<NotificationSetting> notificationSetting = daoManager.executeAndClose(() -> daoManager.getNotificationSettingDao().get(criteria));
		return notificationSetting.size() == 0 ? null : notificationSetting.get(0);
	}
	
	public void update(NotificationSetting notificationSetting) {
        daoManager.executeVoidAndClose(() -> daoManager.getNotificationSettingDao().update(notificationSetting));
	}
}
