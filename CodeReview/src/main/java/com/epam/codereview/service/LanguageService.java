package com.epam.codereview.service;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.manager.db.Operator;
import com.epam.codereview.model.Language;

import java.util.List;

public class LanguageService {

    private DaoManager daoManager = new DaoManager();

    public LanguageService() {
    }

    public LanguageService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public List<Language> get(Criteria criteria) {
		return daoManager.executeAndClose(() -> daoManager.getLanguageDao().get(criteria));
	}
	
	public List<Language> getAllLanguages(){
        return daoManager.executeAndClose(() -> daoManager.getLanguageDao().getAll());
	}
	
	public Language getLanguageByFormat(String format) throws Exception{
		Criteria criteria = new Criteria();
		criteria.add(new Condition("format", "LIKE", format));
		List<Language> language = daoManager.executeAndClose(() -> daoManager.getLanguageDao().get(criteria));
		Language temp = (language.size() == 0 ? null : language.get(0));
		if(temp == null) throw new Exception();
		return temp;
	}
	
	public boolean exists(String format){
		if(format == null || format.equals("")) return false;
		Criteria criteria = new Criteria();
		criteria.add(new Condition("format", "LIKE", format));
		List<Language> language = daoManager.executeAndClose(() -> daoManager.getLanguageDao().get(criteria));
		if (language.size() == 0) return false;
		criteria.add(new Operator("AND"));
		criteria.add(new Condition("name", "!=", "null"));
		language = daoManager.executeAndClose(() -> daoManager.getLanguageDao().get(criteria));
		return language.size() != 0;
	}
	
	public Integer insert(Language language) {
		return daoManager.executeAndClose(() -> daoManager.getLanguageDao().insert(language));
	}
}
