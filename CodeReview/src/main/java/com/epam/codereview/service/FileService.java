package com.epam.codereview.service;

import java.util.List;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.exception.TransactionRollbackException;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.model.File;
import com.epam.codereview.model.FileUser;

public class FileService {

    private DaoManager daoManager;

    public FileService() {
        daoManager = new DaoManager();
    }

    public FileService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public File get(Integer id){
		return daoManager.executeAndClose(() -> daoManager.getFileDao().get(id));
	}
	
	public Integer insert(File file) {
        return daoManager.executeAndClose(() -> daoManager.getFileDao().insert(file));
	}
	
	public File getByUrl(String url){
		Criteria criteria = new Criteria();
		criteria.add(new Condition("url", "like", url));
		List<File> file = daoManager.executeAndClose(() -> daoManager.getFileDao().get(criteria));
		return file.size() == 0 ? null : file.get(0);
	}

	//inserting with filling file_user table
	public Integer insert(File file, Integer reviewId) {
        try {
            return daoManager.transactionAndClose(() -> {
                Integer newId =  daoManager.getFileDao().insert(file);

                List<Integer> reviewMembers = daoManager.getCodeReviewUserDao().getUsersByReviewId(reviewId);

                FileUser fileUser = new FileUser();
                for(Integer member : reviewMembers){
                    fileUser.setUserId(member);
                    fileUser.setFileId(newId);
                    fileUser.setNotified(false);

                    daoManager.getFileUserDao().insert(fileUser);
                }
                return newId;
            });
        } catch (TransactionRollbackException e) {
            e.printStackTrace();
            throw new RuntimeException("Rollback while insert files", e);  //TODO use for show user alert
        }
	}

	public void update(File file) {
        daoManager.executeVoidAndClose(() -> daoManager.getFileDao().update(file));
	}
	
	public void delete(File file){
        daoManager.executeVoidAndClose(() -> daoManager.getFileDao().delete(file));
	}
}
