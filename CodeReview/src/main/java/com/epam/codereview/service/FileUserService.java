package com.epam.codereview.service;

import com.epam.codereview.dao.manager.DaoManager;

public class FileUserService {

    private DaoManager daoManager;

    public FileUserService() {
        daoManager = new DaoManager();
    }

    public FileUserService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public void setNotified(Integer userId, Integer reviewId){
        daoManager.executeVoidAndClose(() -> daoManager.getFileUserDao().setNotified(userId, reviewId));
	}
}
