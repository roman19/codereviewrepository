package com.epam.codereview.service;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.manager.db.Operator;
import com.epam.codereview.model.CodeReviewUser;
import com.epam.codereview.model.DeletedReviewUser;

import java.util.List;

public class DeletedReviewUserService {

    private DaoManager daoManager;

    public DeletedReviewUserService() {
        daoManager = new DaoManager();
    }

    public DeletedReviewUserService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public List<DeletedReviewUser> get(Criteria criteria) {
		return daoManager.executeAndClose(() -> daoManager.getDeletedReviewUserDao().get(criteria));
	}

	public Integer insert(DeletedReviewUser deletedReviewUser) {
		return daoManager.executeAndClose(() -> daoManager.getDeletedReviewUserDao().insert(deletedReviewUser));
	}

	public void update(DeletedReviewUser deletedReviewUser) {
        daoManager.executeVoidAndClose(() -> daoManager.getDeletedReviewUserDao().update(deletedReviewUser));
	}

	public void notifyReviewersAboutReviewDeleting(Integer deletedReviewId, Integer reviewId) {
		List<CodeReviewUser> reviewers = new CodeReviewUserService(daoManager).getAllReviewersByCodeReviewId(reviewId);
		for (CodeReviewUser codeReviewUser : reviewers) {
			if (codeReviewUser.getStatus().equals("CONFIRMED") || codeReviewUser.getStatus().equals("FINISHED_CONFIRMED")) {
				DeletedReviewUser deletedReviewUser = new DeletedReviewUser();
				deletedReviewUser.setDeletedReviewId(deletedReviewId);
				deletedReviewUser.setReviewerId(codeReviewUser.getUserId());
				insert(deletedReviewUser);
			}
		}
	}

	public DeletedReviewUser getDeletedReviewUser(Integer userId, Integer deletedReviewId) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("reviewer_id", "=", userId));
		criteria.add(new Operator("AND"));
		criteria.add(new Condition("deleted_review_id", "=", deletedReviewId));
		List<DeletedReviewUser> user = daoManager.executeAndClose(() -> daoManager.getDeletedReviewUserDao().get(criteria));
		return user.size() == 0 ? null : user.get(0);
	}

	public void setDeletedReviewConfirmed(Integer userId, Integer deletedReviewId) {
		DeletedReviewUser deletedReviewUser = getDeletedReviewUser(userId, deletedReviewId);
		if (deletedReviewUser != null) {
			deletedReviewUser.setStatus("REVIEW_DELETED_CONFIRMED");
			update(deletedReviewUser);
		}
	}
}
