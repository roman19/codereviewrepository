package com.epam.codereview.service;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.model.DeletedReview;

import java.sql.SQLException;
import java.util.List;

public class DeletedReviewService {

    private DaoManager daoManager;

    public DeletedReviewService() {
        daoManager = new DaoManager();
    }

    public DeletedReviewService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public List<DeletedReview> get(Criteria criteria) {
		return daoManager.executeAndClose(() -> daoManager.getDeletedReviewDao().get(criteria));
	}

	public Integer insert(DeletedReview deletedReview) {
		return daoManager.executeAndClose(() -> daoManager.getDeletedReviewDao().insert(deletedReview));
	}
	
	public List<DeletedReview> getAllActiveUnconfirmedDeletedReviewsByUserId(Integer userId)
			throws InstantiationException, IllegalAccessException, SQLException{
		return daoManager.executeAndClose(() -> {
            try {
                return daoManager.getDeletedReviewDao().getAllUnconfirmedDeletedReviewsByUserId(userId, true);
            } catch (SQLException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        });
	}
	
	public List<DeletedReview> getAllUnconfirmedDeletedReviewsFromHistoryByUserId(Integer userId)
			throws InstantiationException, IllegalAccessException, SQLException{
        return daoManager.executeAndClose(() -> {
            try {
                return daoManager.getDeletedReviewDao().getAllUnconfirmedDeletedReviewsByUserId(userId, false);
            } catch (SQLException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        });
	}
}
