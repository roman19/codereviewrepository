package com.epam.codereview.service;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.model.CodeReviewChat;

import java.util.List;

public class ReviewChatService {

    private DaoManager daoManager;

    public ReviewChatService() {
        daoManager = new DaoManager();
    }

    public ReviewChatService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

	public CodeReviewChat get(Integer id) {
		return daoManager.executeAndClose(() -> {
            CodeReviewChat message = daoManager.getCodeReviewChatDao().get(id);
            message.setUser(daoManager.getUserDao().get(message.getUserIdFrom()));
            return message;
        });
	}

	public List<CodeReviewChat> getChatByReviewId(Integer id) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("code_review_id", "=", id));
		return  daoManager.executeAndClose(() -> {
            List<CodeReviewChat> chat = daoManager.getCodeReviewChatDao().get(criteria);
            for (CodeReviewChat message : chat) {
                message.setUser(daoManager.getUserDao().get(message.getUserIdFrom()));
            }
            return chat;
        });
	}

	public Integer insert(CodeReviewChat message) {
        return daoManager.executeAndClose(() -> daoManager.getCodeReviewChatDao().insert(message));
	}
	
	public Integer getCountOfNewMessagesForUser(Integer userId, Integer reviewId) {
		return daoManager.executeAndClose(() -> daoManager.getCodeReviewChatDao().getCountOfNewMessagesForUser(userId, reviewId));
	}
	
	public Integer getCountOfNewMessagesForOwner(Integer userId, Integer reviewId) {
        return daoManager.executeAndClose(() -> daoManager.getCodeReviewChatDao().getCountOfNewMessagesForOwner(userId, reviewId));
	}
}
