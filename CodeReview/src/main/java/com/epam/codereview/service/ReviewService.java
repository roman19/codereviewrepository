package com.epam.codereview.service;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.dto.*;
import com.epam.codereview.exception.TransactionRollbackException;
import com.epam.codereview.exception.UnexpectedServerException;
import com.epam.codereview.model.*;
import com.epam.codereview.project.ProjectHandler;
import com.epam.codereview.util.FileUtil;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.stream.IntStream;

public class ReviewService {

    private final static Logger LOG = Logger.getLogger(ReviewService.class);

    private DaoManager daoManager;

    public ReviewService() {
        daoManager = new DaoManager();
    }

    public ReviewService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public CodeReview get(Integer id) {
        return daoManager.executeAndClose(() -> daoManager.getCodeReviewDao().get(id));
    }

    public File getFileByUrl(String fileUrl) {
        return daoManager.executeAndClose(() -> daoManager.getFileDao().getByUrl(fileUrl));
    }

    public CodeReviewDto getCodeReviewDto(int reviewId, String reviewsServerPath) {
        try {
            return daoManager.transactionAndClose(() -> {
                CodeReviewDto codeReviewDto = null;
                CodeReview codeReview = daoManager.getCodeReviewDao().get(reviewId);
                Project project = daoManager.getProjectDao().getByReviewId(reviewId);
                int issuesTotalCount = daoManager.getIssueDao().getByReviewId(reviewId).size();
                double coverageTotalPercent = getReviewPercentTotal(reviewId);  //TODO this not in transaction
                if (project != null && project.getId() != null) {
                    Branch branch = daoManager.getBranchDao().getByProjectId(project.getId());
                    if (branch != null && branch.getId() != null) {
                        List<File> files = daoManager.getFileDao().getByBranchId(branch.getId());
                        if (files != null && !files.isEmpty()) {
                            List<FileDto> fileDtos = new ArrayList<>();
                            for (File file : files) {
                                FileDto fileDto = new FileDto();
                                fileDto.setFile(file);
                                fileDto.setFullPath(reviewsServerPath + file.getUrl());
                                fileDtos.add(fileDto);
                            }
                            codeReviewDto = new CodeReviewDto();
                            codeReviewDto.setCodeReview(codeReview);
                            codeReviewDto.setProject(project);
                            codeReviewDto.setFileDtos(fileDtos);
                            codeReviewDto.setBranch(branch);
                            codeReviewDto.setIssuesCount(issuesTotalCount);
                            codeReviewDto.setCoveragePercentTotal(coverageTotalPercent);
                        }
                    }
                }
                return codeReviewDto;
            });
        } catch (TransactionRollbackException e) {
            LOG.error("Can't get code review with id = " + reviewId, e);
            throw new UnexpectedServerException("Can't get code review with id = " + reviewId, e);
        }
    }

    public Issue addIssue(Issue issue) {
        try {
            return daoManager.transactionAndClose(() -> {
                Integer issueId = daoManager.getIssueDao().insert(issue);
                return daoManager.getIssueDao().get(issueId);
            });
        } catch (TransactionRollbackException e) {
            LOG.error("Cannot save issue.", e);
            throw new UnexpectedServerException("Cannot save issue.", e);
        }
    }

    public List<IssueDto> getFileIssues(Integer fileId, Integer sessionUserID) {
        List<IssueDto> issueDtos = daoManager.executeAndClose(() -> daoManager.getIssueDao().getByFileIdDtoOrderedByDate(fileId));
        issueDtos.forEach(issue -> issue.setRequiredFields(sessionUserID));
        //set count of replies
        issueDtos.forEach(issue -> issue.setCountOfReplies(new ReplyService(daoManager).getCountOfRepliesForIssue(issue.getId())));

        return issueDtos;
    }

    public Language getLanguage(Integer languageId) {
        return languageId != null
                ? daoManager.executeAndClose(() -> daoManager.getLanguageDao().get(languageId))
                : null;
    }

    public Map<String, Integer> getLanguageFileReviewStatistics(Integer reviewId) {
        return reviewId != null ?
                daoManager.executeAndClose(() -> daoManager.getLanguageDao().getCountFilesByLanguage(reviewId))
                : null;
    }

    public void removeIssue(Integer issueId) {
        daoManager.executeVoidAndClose(() -> daoManager.getIssueDao().delete(issueId));
    }

    public Issue getIssue(Integer issueId) {
        return daoManager.executeAndClose(() -> daoManager.getIssueDao().get(issueId));
    }

    public boolean isReviewOwner(Integer reviewId, Integer currentUserId) {
        try {
            CodeReview codeReview = daoManager.executeAndClose(() -> daoManager.getCodeReviewDao().get(reviewId));
            if (codeReview.getOwnerId().equals(currentUserId)) {
                return true;
            }
        }
        catch (NullPointerException e) {
            LOG.error(e);
        }
        return false;
    }

    public boolean isReviewer(Integer reviewId, Integer currentUserId) {
        try {
            List<CodeReviewUser> codeReviewUsers = daoManager.executeAndClose(
                    () -> daoManager.getCodeReviewUserDao().getByReviewId(reviewId));
            return codeReviewUsers.stream()
                    .filter(o -> o.getUserId().equals(currentUserId))
                    .filter(o -> o.getStatus().equals("CONFIRMED"))
                    .findAny().isPresent();
        } catch (NullPointerException e) {
            LOG.error(e);
        }
        return false;
    }

    public boolean hasReadOnlyRightsForReview(Integer reviewId, Integer currentUserId) {
        try {
            List<CodeReviewUser> codeReviewUsers = daoManager.executeAndClose(
                    () -> daoManager.getCodeReviewUserDao().getByReviewId(reviewId));  //search in review
            boolean readRightsFromInvitedForReview = codeReviewUsers.stream()
                    .filter(o -> o.getUserId().equals(currentUserId))
                    .filter(o -> !o.getStatus().equals("CONFIRMED"))
                    .findAny().isPresent();
            if (readRightsFromInvitedForReview) {
                return true;  //If user invited for review, but not confirmed
            } else {
                List<GroupUser> reviewGroupUsers = daoManager.executeAndClose(
                        () -> daoManager.getGroupUserDao().getByReviewId(reviewId));  //TODO check that group is steal active
                boolean readRightsFromGroup = reviewGroupUsers.stream()
                        .filter(o -> o.getUserId().equals(currentUserId))
                        .filter(o -> o.getStatus().equals("CONFIRMED"))
                        .findAny().isPresent();
                return readRightsFromGroup;
            }
        } catch (NullPointerException e) {
            LOG.error(e);
        }
        return false;
    }

    public boolean hasRightsForReview(Integer reviewId, Integer currentUserId) {  //TODO check for all if group is active and their status in group confirmed or is group owner
        return isReviewOwner(reviewId, currentUserId) ||
                isReviewer(reviewId, currentUserId) ||
                hasReadOnlyRightsForReview(reviewId, currentUserId);
    }

    public void updateIssue(Issue issue) {
        daoManager.executeVoidAndClose(() -> daoManager.getIssueDao().update(issue));
    }

    public void markFileAsReviewed(Integer fileId, Integer sessionUserID) {
        FileUser fileUser = daoManager.executeAndClose(
                () -> daoManager.getFileUserDao().getByFileIdAndUserId(fileId, sessionUserID));
        if (fileUser != null && !fileUser.isReviewed()) {
            fileUser.setReviewed(true);
            daoManager.executeVoidAndClose(() -> daoManager.getFileUserDao().update(fileUser));
        }
    }

    public int getCountIssuesByFile(Integer fileId) {
        return daoManager.executeAndClose(() -> daoManager.getIssueDao().getCountIssuesByFileId(fileId));
    }

    public int getCountFilesByReview(Integer reviewId) {
        return daoManager.executeAndClose(() -> daoManager.getFileDao().getCountByReviewId(reviewId));
    }

    public double getReviewPercentForUser(Integer reviewId, Integer userId) {
        return daoManager.executeAndClose(() -> {
            long reviewedFilesTotalSize = daoManager.getFileUserDao().getTotalReviewedFileSize(reviewId, userId);
            long projectSize = daoManager.getProjectDao().getByReviewId(reviewId).getSize();
            if (reviewedFilesTotalSize == 0 || projectSize == 0) return 0d;
            double percent = (double) reviewedFilesTotalSize / projectSize * 100;
            return percent;
        });
    }

    public double getReviewPercentTotal(Integer reviewId) {
        return daoManager.executeAndClose(() -> {
            long reviewedFilesTotalSize = daoManager.getFileUserDao().getTotalReviewedFileSize(reviewId);
            long projectSize = daoManager.getProjectDao().getByReviewId(reviewId).getSize();
            int usersCount = daoManager.getFileUserDao().getCountDistinctUser(reviewId);
            if (reviewedFilesTotalSize == 0 || projectSize == 0 || usersCount == 0) return 0d;
            double percent = (double) reviewedFilesTotalSize / (projectSize * usersCount) * 100;
            return percent;
        });
    }

    public List<Issue> getTotalReviewIssues(Integer reviewId) {
        return daoManager.executeAndClose(() -> daoManager.getIssueDao().getByReviewId(reviewId));
    }

    public List<Issue> getUserReviewIssues(Integer reviewId, Integer userId) {
        return daoManager.executeAndClose(() -> daoManager.getIssueDao().getByReviewIdAndUserId(reviewId, userId));
    }

    public List<Issue> getReviewIssuesByType(Integer reviewId, String issueType) {
        return daoManager.executeAndClose(() -> daoManager.getIssueDao().getByReviewIdAndIssueType(reviewId, issueType));
    }

    public List<Issue> getReviewIssuesByFile(Integer fileId) {
        return daoManager.executeAndClose(() -> daoManager.getIssueDao().getByFileId(fileId));
    }

    public List<CodeReviewUserDto> getCodeReviewUsers(Integer reviewId) {
        List<CodeReviewUserDto> codeReviewUserDtoList = daoManager.executeAndClose(() -> daoManager.getCodeReviewUserDao().getDtoByReviewId(reviewId));
        Collections.sort(codeReviewUserDtoList);
        for (CodeReviewUserDto codeReviewUserDto : codeReviewUserDtoList) {
            codeReviewUserDto.setCoveragePercent(getReviewPercentForUser(reviewId, codeReviewUserDto.getUserId()));
        }
        return codeReviewUserDtoList;
    }

    public void finishReview(Integer reviewId) {
        CodeReview review = get(reviewId);
        if (review != null && review.isActive()) {
            review.setActive(false);
            daoManager.executeVoidAndClose(() -> daoManager.getCodeReviewDao().update(review));
        }
    }

    /**
     * @author Andriy Palamar
     */
    public void deleteReview(Integer reviewId, String projectsPath) {
        CodeReview review = get(reviewId);
        if (review != null) {
            Project project = new ProjectService(daoManager).getByReviewId(reviewId);

            User user = new UserService(daoManager).getByID(review.getOwnerId());

            DeletedReview deletedReview = new DeletedReview();	//save review that will be deleted physycally from db and on server
            deletedReview.setOwnerId(review.getOwnerId());
            deletedReview.setGroupId(review.getGroupId());
            deletedReview.setName(review.getName());
            deletedReview.setUserName(user.getFirstName() + " " + user.getLastName());
            deletedReview.setIsGitProject(project.getGitUrl() != null);
            deletedReview.setIsActive(review.isActive());
            deletedReview.setCreationDate(review.getCreationDate());
            deletedReview.setCoverageAvgPercent(getReviewPercentTotal(review.getId()));
            Integer deletedReviewId = new DeletedReviewService(daoManager).insert(deletedReview);

            new DeletedReviewUserService(daoManager).notifyReviewersAboutReviewDeleting(deletedReviewId, review.getId());

            ProjectHandler projectHandler = new ProjectHandler(null, projectsPath);
            projectHandler.removeUncheckedFiles(review.getOwnerId() + "/" + project.getFullName());
            new java.io.File(projectsPath + review.getOwnerId() + "/" + project.getFullName()).delete();
            daoManager.executeVoidAndClose(() -> daoManager.getCodeReviewDao().delete(review));
        }
    }

    public void removeUserFormReview(Integer reviewId, Integer userIdForRemove) {
        daoManager.executeVoidAndClose(() -> {
            CodeReviewUser codeReviewUser = daoManager.getCodeReviewUserDao().getByReviewIdAndUserId(reviewId, userIdForRemove);
            if (codeReviewUser != null) {
                codeReviewUser.setStatus("DELETED");
                daoManager.getCodeReviewUserDao().update(codeReviewUser);
            }
        });
    }

    public void addUsersToCodeReview(Integer reviewId, int[] usersIdForAdd) {
        daoManager.executeVoidAndClose(() -> daoManager.getCodeReviewUserDao().addUsersById(reviewId, usersIdForAdd));
    }

    public List<IssueForTabDto> getIssuesForIssuesTab(Integer reviewId, Integer currentUserId, String reviewsServerPath, Integer langId, String issueType) {
        List<IssueForTabDto> issueForTabDtoList = daoManager.executeAndClose(() -> daoManager.getIssueDao().getDtoByReviewId(reviewId, langId, issueType));
        issueForTabDtoList.sort(IssueForTabDto.Sort.byFilePathAndDateDESC());

        Map<String, List<String>> fileContentMap = new HashMap<>();
        for (IssueForTabDto issueForTabDto : issueForTabDtoList) {
            String fileUrl = issueForTabDto.getFilePath();
            if (fileContentMap.get(fileUrl) == null ){
                fileContentMap.put(fileUrl, FileUtil.readFileInLines(reviewsServerPath + fileUrl));
            }
            String issueLinesContent = String.join("\n", fileContentMap.get(fileUrl).subList(issueForTabDto.getLineFrom() - 1, issueForTabDto.getLineTo()));
            issueForTabDto.setFileLinesContent(issueLinesContent.replaceAll("<", "&lt;"));
            issueForTabDto.setIsEditable(Objects.equals(currentUserId, issueForTabDto.getUserId()));
            issueForTabDto.setLines(IntStream.range(issueForTabDto.getLineFrom(), issueForTabDto.getLineTo() + 1).toArray());

            fileUrl = fileUrl.replaceFirst("/", "");
            issueForTabDto.setFilePath(fileUrl.substring(fileUrl.indexOf('/') + 1, fileUrl.length()));

            //for replies
            issueForTabDto.setReplies(new ReplyService(daoManager).getRepliesByIssueId(issueForTabDto.getId()));
        }
        return issueForTabDtoList;
    }

    public List<Language> getLanguagesWithIssues(Integer reviewId) {
        return daoManager.executeAndClose(() -> daoManager.getLanguageDao().getAllWithIssuesByReviewId(reviewId));
    }

    public void setOwnerIsNotified(Integer reviewId, Integer userId) {
        daoManager.executeVoidAndClose(() -> {
            CodeReviewUser codeReviewUser = daoManager.getCodeReviewUserDao().getByReviewIdAndUserId(reviewId, userId);
            codeReviewUser.setIsNotifiedOwner(true);
            daoManager.getCodeReviewUserDao().update(codeReviewUser);
        });
    }
}
