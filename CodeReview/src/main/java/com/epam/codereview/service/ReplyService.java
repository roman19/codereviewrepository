package com.epam.codereview.service;

import com.epam.codereview.controller.MainController;
import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.dto.ReplyNotificationDto;
import com.epam.codereview.model.NotificationOnReply;
import com.epam.codereview.model.Reply;
import com.epam.codereview.model.User;
import com.epam.codereview.util.LoginFetcher;
import org.apache.log4j.Logger;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;

public class ReplyService {

	private final static Logger LOG = Logger.getLogger(MainController.class);

    private DaoManager daoManager;

    public ReplyService() {
        daoManager = new DaoManager();
    }

    public ReplyService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public Reply get(Integer id) {
		return daoManager.executeAndClose(() -> {
            Reply reply = daoManager.getReplyDao().get(id);
            User user = daoManager.getUserDao().get(reply.getOwnerId());
            reply.setUser(user);
            return reply;
        });
	}

	public List<Reply> getRepliesByIssueId(Integer issueId) {
		return daoManager.executeAndClose(() -> {
            List<Reply> replies = daoManager.getReplyDao().getRepliesByIssueId(issueId);
            for (Reply reply : replies) {
                User user = daoManager.getUserDao().get(reply.getOwnerId());
                reply.setUser(user);
            }
            return replies;
        });
	}
	
	public Integer getCountOfRepliesForIssue(Integer issueId) {
		return daoManager.executeAndClose(() -> daoManager.getReplyDao().getCountOfRepliesForIssue(issueId));
	}

	public ReplyNotificationDto insert(Reply reply) {
		UserService userService = new UserService(daoManager);

		Integer newReplyId = daoManager.executeAndClose(() -> daoManager.getReplyDao().insert(reply));

		List<User> usersTo = new ArrayList<>();
		List<Integer> usersIdTo = new ArrayList<>();
		List<String> logins = LoginFetcher.getLoginsFromMessage(reply
				.getMessage());

        for (String login : logins) {
            User user = userService.getByLogin(login);

            if (user != null && !user.getId().equals(reply.getOwnerId())) {
                NotificationOnReply onReply = new NotificationOnReply();
                onReply.setReplyId(newReplyId);
                onReply.setUserId(user.getId());
                daoManager.executeVoidAndClose(() -> daoManager.getNotificationOnReplyDao().insert(onReply));

                usersTo.add(user);
                usersIdTo.add(user.getId());
            }
        }

		ReplyNotificationDto replyNotificationDto = new ReplyService(daoManager).getReplyNotificationDto(newReplyId);
		replyNotificationDto.setUsers(usersIdTo);
		
		try {
			new NotificationService(daoManager).sendReplyNotification(replyNotificationDto, usersTo);
		} catch (MessagingException e) {
			LOG.error("Error during sending on email");
		}

		return replyNotificationDto;
	}
	
	public ReplyNotificationDto getReplyNotificationDto(Integer replyId) {
		return daoManager.executeAndClose(() -> {
            ReplyNotificationDto notificationDto = daoManager.getReplyDao().getReplyNotificationDto(replyId);
            notificationDto.setOwner(daoManager.getUserDao().get(notificationDto.getOwnerId()));
            return notificationDto;
        });
	}

	public void delete(Integer id) {
        daoManager.executeVoidAndClose(() -> daoManager.getReplyDao().delete(id));
	}

	public void update(Reply reply) {
        daoManager.executeVoidAndClose(() -> daoManager.getReplyDao().update(reply));
	}

}
