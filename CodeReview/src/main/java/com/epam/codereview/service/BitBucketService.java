package com.epam.codereview.service;

import com.epam.codereview.exception.ValidationException;
import com.epam.codereview.model.User;
import com.epam.codereview.oauth2.BitbucketOAuth2Details;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class BitBucketService {

	public User getUserByToken(String accessToken) throws IOException,
			ParseException, ValidationException {
		User user = new User();
		String getUsetInfo = BitbucketOAuth2Details.methodUri
				+ "?access_token=" + accessToken;

		URL myurl = new URL(getUsetInfo);
		HttpsURLConnection con = (HttpsURLConnection) myurl.openConnection();
		InputStream ins = con.getInputStream();
		InputStreamReader isr = new InputStreamReader(ins, "UTF-8");
		JSONParser parser = new JSONParser();

		JSONObject jObject = (JSONObject) parser.parse(isr);

		user.setSocial("BITBUCKET");
		user.setSocialId(jObject.get("uuid").toString());
		user.setLogin(jObject.get("username").toString());
		user.setEmail(getEmailByToken(accessToken));

		String[] fullname = jObject.get("display_name").toString().split(" ");

		if (fullname.length == 2) {
			user.setFirstName(fullname[0]);
			user.setLastName(fullname[1]);
		} else {
			user.setFirstName(fullname[0]);
			user.setLastName(fullname[0]);
		}

		JSONObject links = (JSONObject) jObject.get("links");
		JSONObject imageObj = (JSONObject)links.get("avatar");
		String imageUrl = imageObj.get("href").toString();
		imageUrl = imageUrl.replaceAll("\\\\", "");
		user.setImageUrl(imageUrl);
		user.setImageFromServer(false);

		return user;

	}

	private String getEmailByToken(String accessToken) throws IOException,
			ParseException {
		String email = null;

		String getUsetInfo = BitbucketOAuth2Details.methodUri + "emails"
				+ "?access_token=" + accessToken;

		URL myurl = new URL(getUsetInfo);
		HttpsURLConnection con = (HttpsURLConnection) myurl.openConnection();
		InputStream ins = con.getInputStream();
		InputStreamReader isr = new InputStreamReader(ins, "UTF-8");
		JSONParser parser = new JSONParser();

		JSONObject jObject = (JSONObject) parser.parse(isr);

		JSONArray values = (JSONArray) jObject.get("values");
		JSONObject value = (JSONObject) values.get(0);
		email = value.get("email").toString();

		return email;
	}

}
