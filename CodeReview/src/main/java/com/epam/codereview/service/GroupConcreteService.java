package com.epam.codereview.service;


import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.dto.ReviewDto;
import org.apache.log4j.Logger;

import java.util.List;

public class GroupConcreteService {

    private final static Logger LOG = Logger.getLogger(GroupConcreteService.class);

    private DaoManager daoManager;

    public GroupConcreteService() {
        daoManager = new DaoManager();
    }

    public GroupConcreteService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public List<ReviewDto> getAllActiveReviewsByUserId(Integer groupId,Integer is_active, Integer userID, int offset, int noOfRecords) {
        return daoManager.executeAndClose(() ->
                daoManager.getGroupConcreteDao().getAllActiveReviwByUserId(groupId,is_active, userID, offset, noOfRecords));
    }

    public int getNoOfRecords() {
        return daoManager.executeAndClose(() ->
                daoManager.getGroupConcreteDao().getNoOfRecords());
    }

    public List<ReviewDto> getOtherActiveReviewsByUserId(Integer groupId,Integer is_active, Integer userID, int offset, int noOfRecords) {
        return daoManager.executeAndClose(() ->
                daoManager.getGroupConcreteDao().getOtherActiveReviwByUserId(groupId,is_active, userID, offset, noOfRecords));
    }
    
    public List<ReviewDto> getMyActiveReviewsByUserId(Integer groupId,Integer is_active, Integer userID, int offset, int noOfRecords) {
        return daoManager.executeAndClose(() ->
                daoManager.getGroupConcreteDao().getMyActiveReviwByUserId(groupId,is_active, userID, offset, noOfRecords));
    }
    
    
    
    public List<ReviewDto> getHistoryReviwByUserId(Integer groupId, Integer userID, int offset, int noOfRecords) {
        return daoManager.executeAndClose(() ->
                daoManager.getGroupConcreteDao().getHistoryReviwByUserId(groupId, userID, offset, noOfRecords));
    }
    
    
    // TYpeahead for reviews
    public List<ReviewDto> getAllReviwByUserIdTH( Integer is_active, Integer userID) {
        return daoManager.executeAndClose(() ->
                daoManager.getGroupConcreteDao().getAllReviwByUserIdTH( is_active,userID));
    }
    
    
    
}

