package com.epam.codereview.service;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.dto.*;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.manager.db.Operator;
import com.epam.codereview.model.CodeReview;
import com.epam.codereview.model.CodeReviewUser;
import com.epam.codereview.model.Group;
import com.epam.codereview.model.GroupUser;
import com.epam.codereview.model.User;
import com.epam.codereview.util.AsyncSender;
import com.epam.codereview.model.*;
import com.epam.codereview.util.Sender;
import org.apache.log4j.Logger;

import javax.mail.MessagingException;
import java.util.*;
import java.util.stream.Collectors;

public class NotificationService {

	private final static Logger LOG = Logger.getLogger(NotificationService.class);

    private DaoManager daoManager;

    public NotificationService() {
        daoManager = new DaoManager();
    }

    public NotificationService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

	public List<GroupNotificationDto> getGroupNotificationsByUserId(Integer userId, int count, int offset) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("user_id", "=", userId));
		criteria.add(new Operator("AND"));
		criteria.add(new Condition("status", "LIKE", "PENDING"));
		criteria.add(new Operator("ORDER BY notification_date DESC LIMIT " + count + " OFFSET " + offset));

        return daoManager.executeAndClose(() -> {
            List<GroupUser> groupUsers = daoManager.getGroupUserDao().get(criteria);
            List<GroupNotificationDto> notifications = new ArrayList<>();

            for (GroupUser groupUser : groupUsers) {
                Group group = daoManager.getGroupDao().get(groupUser.getGroupId());
                User user = daoManager.getUserDao().get(group.getUserId());
                String groupName = group.getName();

                Criteria criteriaCount = new Criteria();
                criteriaCount.add(new Condition("group_id", "=", groupUser.getGroupId()));
                criteriaCount.add(new Operator("AND"));
                criteriaCount.add(new Condition("status", "LIKE", "CONFIRMED"));

                Integer countMembers = daoManager.getGroupUserDao().get(criteriaCount).size();

                GroupNotificationDto notification = new GroupNotificationDto();
                notification.setGroupId(group.getId());
                notification.setUser(user);
                notification.setGroupName(groupName);
                notification.setCountMembers(countMembers + 1);
                notification.setDate(groupUser.getNotificationDate());

                notifications.add(notification);
            }
            return notifications;
        });

	}

	public Integer getCountOfGroupNotificationsByUserId(Integer userId) {
		Criteria criteria = new Criteria();
		criteria.add(new Condition("user_id", "=", userId));
		criteria.add(new Operator("AND"));
		criteria.add(new Condition("status", "LIKE", "PENDING"));

		return daoManager.executeAndClose(() -> daoManager.getGroupUserDao().get(criteria)).size();
	}

	public List<ReviewNotificationDto> getReviewNotificationsByUserId(Integer userId, int count, int offset) {
		return daoManager.executeAndClose(() -> {
            List<CodeReviewUser> reviewUsers = daoManager.getCodeReviewUserDao().getReviewNotificationsByUserId(userId, count, offset);

            List<ReviewNotificationDto> notifications = new ArrayList<>();

            for (CodeReviewUser reviewUser : reviewUsers) {
                CodeReview review = daoManager.getCodeReviewDao().get(reviewUser.getCodeReviewId());
                User user = daoManager.getUserDao().get(review.getOwnerId());
                String reviewName = review.getName();

                ReviewNotificationDto notification = new ReviewNotificationDto();
                notification.setReviewId(review.getId());
                notification.setUser(user);
                notification.setReviewName(reviewName);
                notification.setDate(reviewUser.getNotificationDate());

                notifications.add(notification);
            }
            return notifications;
        });
	}

	public Integer getCountOfReviewNotificationsByUserId(Integer userId) {
		return daoManager.executeAndClose(() -> daoManager.getCodeReviewUserDao().getCountOfReviewNotificationsByUserId(userId));
	}

	public List<IssueNotificationDto> getIssueNotificationsByUserId(Integer userId, int count, int offset) {
        return daoManager.executeAndClose(() -> {
            List<IssueNotificationDto> notifications = daoManager.getIssueNotificationDao()
                    .getIssueNotificationsByUserId(userId, count, offset);

            for (IssueNotificationDto issue : notifications) {
                issue.setUser(daoManager.getUserDao().get(issue.getUserId()));

                String[] parts = issue.getFileName().split("/");
                String fileName = parts[parts.length - 1];

                issue.setFileName(fileName);
            }
            return notifications;
        });
	}

	public Integer getCountOfIssueNotificationsByUserId(Integer userId) {
		return daoManager.executeAndClose(() -> daoManager.getIssueNotificationDao().getCountOfIssueNotificationsByUserId(userId));
	}

	public List<FileNotificationDto> getFileNotificationsByUserId(Integer userId, int count, int offset) {
		List<FilePlainNotificationDto> notifications = daoManager.executeAndClose(() ->
                daoManager.getFileUserDao().getNewFilesForUser(userId));

		for (FilePlainNotificationDto file : notifications) {
			String[] parts = file.getName().split("/");
			String fileName = parts[parts.length - 1];
			file.setName(fileName);
		}

		Map<Integer, List<FilePlainNotificationDto>> notificationsByReviews = notifications
				.stream().collect(Collectors.groupingBy(FilePlainNotificationDto::getReviewId));

		List<FileNotificationDto> fileNotifications = new ArrayList<>();
		for (Map.Entry<Integer, List<FilePlainNotificationDto>> entry : notificationsByReviews
				.entrySet()) {
			FileNotificationDto notification = new FileNotificationDto(
					entry.getValue());
			fileNotifications.add(notification);
		}

		Collections.sort(fileNotifications,
				(n1, n2) -> n2.getDate().compareTo(n1.getDate()));

		if (offset > fileNotifications.size())
			return null;
		else
			return fileNotifications.subList(offset, offset+count>fileNotifications.size()? fileNotifications.size() : offset+count);
	}

	public Integer getCountOfFileNotificationsByUserId(Integer userId) {
        List<FilePlainNotificationDto> notifications = daoManager.executeAndClose(() ->
                daoManager.getFileUserDao().getNewFilesForUser(userId));

		Map<Integer, List<FilePlainNotificationDto>> notificationsByReviews = notifications
				.stream().collect(Collectors.groupingBy(FilePlainNotificationDto::getReviewId));

		return notificationsByReviews.keySet().size();
	}

	public List<ReplyNotificationDto> getReplyNotificationsByUserId(Integer userId, int count, int offset) {
		return daoManager.executeAndClose(() -> {
            List<ReplyNotificationDto> notifications = daoManager.getNotificationOnReplyDao()
                    .getReplyNotificationsByUserId(userId, count, offset);

            for (ReplyNotificationDto n : notifications) {
                n.setOwner(daoManager.getUserDao().get(n.getOwnerId()));
            }

            return notifications;
        });
	}

	public Integer getCountOfReplyNotificationsByUserId(Integer userId) {
		return daoManager.executeAndClose(() ->
                daoManager.getNotificationOnReplyDao().getCountOfReplyNotificationsByUserId(userId));
	}

	public List<NotificationDto> getAllByUserId(Integer userId, int count, int offset) {
		List<NotificationDto> notifications = new ArrayList<>();

		List<GroupNotificationDto> groupNotifications = getGroupNotificationsByUserId(userId, offset+count, 0);
		List<ReviewNotificationDto> reviewNotifications = getReviewNotificationsByUserId(userId, offset+count, 0);
		List<IssueNotificationDto> issueNotifications = getIssueNotificationsByUserId(userId, offset+count, 0);
		List<FileNotificationDto> fileNotifications = getFileNotificationsByUserId(userId, offset+count, 0);
		List<ReplyNotificationDto> replyNotifications = getReplyNotificationsByUserId(userId, offset+count, 0);

		for (GroupNotificationDto n : groupNotifications) {
			NotificationDto notificationDto = new NotificationDto(
					NotificationType.GROUP, n.getDate(), n);
			notifications.add(notificationDto);
		}

		for (ReviewNotificationDto n : reviewNotifications) {
			NotificationDto notificationDto = new NotificationDto(
					NotificationType.REVIEW, n.getDate(), n);
			notifications.add(notificationDto);
		}

		for (IssueNotificationDto n : issueNotifications) {
			NotificationDto notificationDto = new NotificationDto(
					NotificationType.ISSUE, n.getDate(), n);
			notifications.add(notificationDto);
		}

		for (FileNotificationDto n : fileNotifications) {
			NotificationDto notificationDto = new NotificationDto(
					NotificationType.FILE_ADDED, n.getDate(), n);
			notifications.add(notificationDto);
		}

		for (ReplyNotificationDto n : replyNotifications) {
			NotificationDto notificationDto = new NotificationDto(
					NotificationType.REPLY, n.getDate(), n);
			notifications.add(notificationDto);
		}
		
		Collections.sort(notifications,	(n1, n2) -> n2.getDate().compareTo(n1.getDate()));
		
		if (offset > notifications.size())
			return null;
		else
			return notifications.subList(offset, offset+count>notifications.size()? notifications.size() : offset+count);
	}

	public Integer getCountOfAllByUserId(Integer userId) {
		Integer count = 0;
		count += getCountOfGroupNotificationsByUserId(userId);
		count += getCountOfReviewNotificationsByUserId(userId);
		count += getCountOfIssueNotificationsByUserId(userId);
		count += getCountOfFileNotificationsByUserId(userId);
		count += getCountOfReplyNotificationsByUserId(userId);

		return count;
	}

	public void sendGroupNotification(Integer ownerId, Integer groupId,	String groupName, Integer userToId) {
		daoManager.executeVoidAndClose(() -> {
            if (daoManager.getNotificationSettingDao().getSettingsForUser(userToId).isIssue()) {
                User user = daoManager.getUserDao().get(ownerId);
                String subject = "New notification";
                String link = "http://localhost:8080/CodeReview/cabinet?newGroup=" + groupId;
                User userTo = daoManager.getUserDao().get(userToId);

                String html = "<!DOCTYPE html>"
                        + "<html><head><title>Code Review</title>"
                        + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">"
                        + "</head><body style=\"background-color: #f5f5f0\"><div style=\"width: 100%; position: relative\">"
                        + "<div style=\"width: 35%; margin: 0 auto\">"
                        + "<h1><span><img src=\"https://www.codementor.io/assets/icon_003_4.png\" style=\"width: 50px; height: 50px; margin-right: 15px; margin-bottom:-15px;\" /></span>Code Review</h1>"
                        + "</div><div style=\"width: 50%; top: 10px; background-color: white; margin: 0 auto; padding:10px\">"
                        + "<div><h1 style=\"color: green;\">Hi, "
                        + userTo.getFirstName()
                        + "</h1>"
                        + "<p style=\"border-bottom: 1px solid grey; border-bottom-width: 80%; padding-bottom: 10px\"><b>You have new invitation to group</b></p>"
                        + "</div><div style=\"padding-bottom:10px\">"
                        + "<p><b>"
                        + user.getFirstName()
                        + " "
                        + user.getLastName()
                        + "</b> invite you to group <br/><br/><a href="
                        + link
                        + "><span style=\"font-size:18px\"><b>"
                        + groupName
                        + "</b></span></a><br/><br/> <span style=\"font-size:small; color: rgba(0, 0, 0, 0.4);\">"
                        + new Date() + "</span><p>"
                        + "</div></div></div></body></html>";

                try {
                    new Thread(new AsyncSender(subject, html, userTo.getEmail())).start();
                } catch (Exception e) {
                    LOG.error("ERROR during send notification on e-mail", e);
                }
            }
        });
	}

	public void sendReviewNotification(Integer ownerId, Integer userToId, Integer reviewId, String reviewName) {
        daoManager.executeVoidAndClose(() -> {
            if (daoManager.getNotificationSettingDao().getSettingsForUser(userToId).isIssue()) {
                User user = daoManager.getUserDao().get(ownerId);
                String subject = "New notification";
                String link = "http://localhost:8080/CodeReview/cabinet?newReview=" + reviewId;
                User userTo = daoManager.getUserDao().get(userToId);

                String html = "<!DOCTYPE html>"
                        + "<html><head><title>Code Review</title>"
                        + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">"
                        + "</head><body style=\"background-color: #f5f5f0\"><div style=\"width: 100%; position: relative\">"
                        + "<div style=\"width: 35%; margin: 0 auto\">"
                        + "<h1><span><img src=\"https://www.codementor.io/assets/icon_003_4.png\" style=\"width: 50px; height: 50px; margin-right: 15px; margin-bottom:-15px;\" /></span>Code Review</h1>"
                        + "</div><div style=\"width: 50%; top: 10px; background-color: white; margin: 0 auto; padding:10px\">"
                        + "<div><h1 style=\"color: green;\">Hi, "
                        + userTo.getFirstName()
                        + "</h1>"
                        + "<p style=\"border-bottom: 1px solid grey; border-bottom-width: 80%; padding-bottom: 10px\"><b>You have new invitation to review</b></p>"
                        + "</div><div style=\"padding-bottom:10px\">"
                        + "<p><b>"
                        + user.getFirstName()
                        + " "
                        + user.getLastName()
                        + "</b> invite you to review <br/><br/> <a href="
                        + link
                        + "><span style=\"font-size:18px\"><b>"
                        + reviewName
                        + "</b></span></a> <br/><br/> <span style=\"font-size:small; color: rgba(0, 0, 0, 0.4);\">"
                        + new Date() + "</span><p>"
                        + "</div></div></div></body></html>";

                try {
                    new Thread(new AsyncSender(subject, html, userTo.getEmail())).start();
                } catch (Exception e) {
                    LOG.error("ERROR during send notification on e-mail", e);
                }
            }
        });
	}

	public void sendIssueNotification(Integer userToId,	IssueNotificationDto issue) {
        daoManager.executeVoidAndClose(() -> {
            if (daoManager.getNotificationSettingDao().getSettingsForUser(userToId).isIssue()) {
                String subject = "New notification";
                String link = "http://localhost:8080/CodeReview/review/"
                        + issue.getReviewId() + "?file=" + issue.getFileId()
                        + "&issue=" + issue.getIssueId();
                User userTo = daoManager.getUserDao().get(userToId);

                String html = "<!DOCTYPE html><html><head><title>slack</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"> </head>"
                        + "<body style=\"background-color: #f5f5f0\"><div style=\"width: 100%; position: relative\"><div style=\"width: 35%; margin: 0 auto\">"
                        + "<h1><span><img src=\"https://www.codementor.io/assets/icon_003_4.png\" style=\"width: 50px; height: 50px; margin-right: 15px; margin-bottom:-15px;\" /></span>Code Review</h1>"
                        + "</div><div style=\"width: 75%; top: 10px; background-color: white; margin: 0 auto; padding:10px;\">"
                        + "<div><h1 style=\"color: green;\">Hi, "
                        + userTo.getFirstName()
                        + "</h1>"
                        + "<p style=\"border-bottom: 1px solid grey; border-bottom-width: 80%; padding-bottom: 10px\"><b>You have new issue</b><span><img style=\"width: 45px; height: 45px; margin-bottom:-15px; margin-left:10px\" src=\"http://www.silverskiff.org/wp-content/uploads/2015/10/Warning.png\" alt=\"ERROR ISSUE\"></span></p>"
                        + "</div><div style=\"padding-bottom:10px\">"
                        + "<p><b>"
                        + issue.getUser().getFirstName()
                        + " "
                        + issue.getUser().getLastName()
                        + "</b> left new error issue: <a href="
                        + link
                        + " ><b>"
                        + issue.getIssueTitle()
                        + "</b></a><br/><br/> <span style=\"font-size:small; color: rgba(0, 0, 0, 0.4);\">"
                        + issue.getDate() + "</span><p>" + "</div>";

                if (issue.getIssueBody() != null) {
                    html += "<div><i>description of problem:</i></div><div style=\"border:1px solid black; padding:10px; border-radius:3px; font-size:12px\">"
                            + issue.getIssueBody() + "</div>";
                }
                html += "</div></div></body></html>";

                try {
                    new Thread(new AsyncSender(subject, html, userTo.getEmail())).start();
                } catch (Exception e) {
                    LOG.error("ERROR during send notification on e-mail", e);
                }
            }
        });
	}

	public void sendFileNotification(FileOnlineNotificationDto notification) {
		String subject = "New notification";
		String linkToReview = "http://localhost:8080/CodeReview/review/"
				+ notification.getReviewId();
		String linkToNotification = "http://localhost:8080/CodeReview/cabinet?newFiles="
				+ notification.getReviewId();

        daoManager.executeVoidAndClose(() -> {
            for (Integer userToId : notification.getUsers()) {
                if (daoManager.getNotificationSettingDao().getSettingsForUser(userToId).isFileAdded()) {
                    User userTo = daoManager.getUserDao().get(userToId);

                    String html = "<!DOCTYPE html><html><head><title>slack</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"> </head>"
                            + "<body style=\"background-color: #f5f5f0\"><div style=\"width: 100%; position: relative\"><div style=\"width: 35%; margin: 0 auto\">"
                            + "<h1><span><img src=\"https://www.codementor.io/assets/icon_003_4.png\" style=\"width: 50px; height: 50px; margin-right: 15px; margin-bottom:-15px;\" /></span>Code Review</h1>"
                            + "</div><div style=\"width: 75%; top: 10px; background-color: white; margin: 0 auto; padding:10px;\">"
                            + "<div><h1 style=\"color: green;\">Hi, "
                            + userTo.getFirstName()
                            + "</h1>"
                            + "<p><b>"
                            + notification.getOwner().getFirstName()
                            + " "
                            + notification.getOwner().getLastName()
                            + "</b> added <a href='"
                            + linkToNotification
                            + "' >"
                            + notification.getCountFiles()
                            + " files</a><br/><br/> to <a style=\"font-size:18px\" href="
                            + linkToReview
                            + "><b>"
                            + notification.getReviewName()
                            + "</b></a><br/><br/> <span style=\"font-size:small; color: rgba(0, 0, 0, 0.4);\">"
                            + new Date()
                            + "</span><p>"
                            + "</div>"
                            + "</div></div></body></html>";

                    try {
                        new Thread(new AsyncSender(subject, html, userTo.getEmail())).start();
                    } catch (Exception e) {
                        LOG.error("ERROR during send notification on e-mail", e);
                    }
                }
            }
        });
	}

	public void sendReplyNotification(ReplyNotificationDto notification, List<User> usersTo) throws MessagingException {
		String subject = "New notification";
		String linkToReview = "http://localhost:8080/CodeReview/review/"
				+ notification.getReviewId();
		String linkToIssue = "http://localhost:8080/CodeReview/review/"
				+ notification.getReviewId() + "?file="
				+ notification.getFileId() + "&issue="
				+ notification.getIssueId();
		String linkToReply = "http://localhost:8080/CodeReview/review/"
				+ notification.getReviewId() + "?file="
				+ notification.getFileId() + "&issue="
				+ notification.getIssueId() + "&reply="
				+ notification.getReplyId();

        daoManager.executeVoidAndClose(() -> {
            for (User userTo : usersTo) {
                if (daoManager.getNotificationSettingDao().getSettingsForUser(userTo.getId()).isReply()) {

                    String html = "<!DOCTYPE html><html><head><title>slack</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'>"
                            + "</head>"
                            + "<body style=\"background-color: #f5f5f0\">"
                            + "<div style=\"width: 100%; position: relative\"><div style=\"width: 35%; margin: 0 auto\">"
                            + "<h1><span><img src='https://www.codementor.io/assets/icon_003_4.png' style=\"width: 50px; height: 50px; margin-right: 15px; margin-bottom:-15px;\" /></span>Code Review</h1>"
                            + "</div><div style=\"width: 60%; top: 10px; background-color: white; margin: 0 auto; padding:10px;\">"
                            + "<div><h1 style=\"color: green;\">Hi "
                            + userTo.getFirstName()
                            + "</h1>"
                            + "<p style=\"border-bottom: 1px solid grey; border-bottom-width: 80%; padding-bottom: 10px\"></p></div>"
                            + "<div style=\"padding-bottom:10px\">"
                            + "<p><b>"
                            + notification.getOwner().getFirstName()
                            + " "
                            + notification.getOwner().getLastName()
                            + "</b> replied you <span style=\"font-size:small; color: rgba(0, 0, 0, 0.4);\">"
                            + notification.getDate()
                            + "</span><p></div><div style=\"background: #f9f9f9; border-left: 10px solid #ccc; padding: 0.5em 10px; margin-bottom: 10px; quotes: '\201C''\201D''\2018''\2019';\">"
                            + "<p style=\"display: inline;\">"
                            + notification.getMessage()
                            + "</p>"
                            + "</div><div style=\"font-size:14px; color:blue; float:right; margin:5px\">"
                            + "<a href='"
                            + linkToReply
                            + "'><i>go to reply</i></a></div>"
                            + "<div style=\"margin:5px; width: 100%; margin: 0 0 2em 0; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;\">"
                            + "issue: <a href='"
                            + linkToIssue
                            + "'>"
                            + notification.getIssueTitle()
                            + "</a>"
                            + "</div><div style=\"margin:5px\">review: <a href='"
                            + linkToReview
                            + "'><b>"
                            + notification.getReviewName()
                            + "</b></a>" + "</div></div></div></body></html>";

                    try {
                        new Thread(new AsyncSender(subject, html, userTo.getEmail())).start();
                    } catch (Exception e) {
                        LOG.error("ERROR during send notification on e-mail", e);
                    }
                }
            }
        });
	}

}
