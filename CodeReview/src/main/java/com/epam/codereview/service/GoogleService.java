package com.epam.codereview.service;

import com.epam.codereview.model.User;
import com.epam.codereview.oauth2.GoogleOAuth2Details;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class GoogleService {

	public User getUserByToken(String token) throws IOException, ParseException {

		User user = new User();
		String getUsetInfo = GoogleOAuth2Details.methodUri + "me"
				+ "?access_token=" + token;

		URL myurl = new URL(getUsetInfo);
		HttpsURLConnection con = (HttpsURLConnection) myurl.openConnection();
		InputStream ins = con.getInputStream();
		InputStreamReader isr = new InputStreamReader(ins, "UTF-8");
		JSONParser parser = new JSONParser();
		JSONObject jObject = (JSONObject) parser.parse(isr);
		user.setSocial("GOOGLE");
		user.setSocialId(jObject.get("id").toString());
		user.setEmail(((JSONObject) ((JSONArray) jObject.get("emails")).get(0))
				.get("value").toString());
		JSONObject jsonName = (JSONObject) jObject.get("name");
		user.setLastName(jsonName.get("familyName").toString());
		user.setFirstName(jsonName.get("givenName").toString());
		user.setEmail(((JSONObject) ((JSONArray) jObject.get("emails")).get(0))
				.get("value").toString());

		JSONObject jsonImage = (JSONObject) jObject.get("image");

		String fullUrl = jsonImage.get("url").toString();

		String url = fullUrl.substring(0, fullUrl.lastIndexOf("?"));

		user.setImageUrl(url);
		user.setImageFromServer(false);

		return user;
	}
}
