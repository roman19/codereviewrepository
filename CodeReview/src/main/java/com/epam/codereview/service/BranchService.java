package com.epam.codereview.service;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.model.Branch;

public class BranchService {

    private DaoManager daoManager;

    public BranchService() {
        daoManager = new DaoManager();
    }

    public BranchService(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    public Integer insert(Branch branch) {
		return daoManager.executeAndClose(() -> daoManager.getBranchDao().insert(branch));
	}
}
