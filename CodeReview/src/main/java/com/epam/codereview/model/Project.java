package com.epam.codereview.model;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="project")
public class Project {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;

	@Column(name="name")
	private String name;
	
	@Column(name="full_name")
	private String fullName;
	
	@Column(name="git_url")
	private String gitUrl;
	
	@Column(name="code_review_id")
	private Integer codeReviewId;
	
	@DefaultValue
	@Column(name="avg_grade")
	private Integer avgGrade;
	
	@DefaultValue
	@Column(name="count_issues")
	private Integer countIssues;
	
	@Column(name="size")
	private Long size;
	
	public Project(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCodeReviewId() {
		return codeReviewId;
	}

	public void setCodeReviewId(Integer codeReviewId) {
		this.codeReviewId = codeReviewId;
	}

	public Integer getAvgGrade() {
		return avgGrade;
	}

	public void setAvgGrade(Integer avgGrade) {
		this.avgGrade = avgGrade;
	}

	public Integer getCountIssues() {
		return countIssues;
	}

	public void setCountIssues(Integer countIssues) {
		this.countIssues = countIssues;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGitUrl() {
		return gitUrl;
	}

	public void setGitUrl(String gitUrl) {
		this.gitUrl = gitUrl;
	}

}
