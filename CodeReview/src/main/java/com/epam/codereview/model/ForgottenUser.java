package com.epam.codereview.model;

import java.sql.Timestamp;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;


@Table(name="forgotten_user")
public class ForgottenUser {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="email")
	private String email;
	
	@Column(name="code")
	private String code;
	
	@DefaultValue
	@Column(name="forget_date")
	private Timestamp forgetDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Timestamp getForgetDate() {
		return forgetDate;
	}
}