package com.epam.codereview.model;

import java.sql.Timestamp;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="file")
public class File {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="url")
	private String url;
	
	@Column(name="branch_id")
	private Integer branchId;
	
	@Column(name="language_id")
	private Integer languageId;
	
	@DefaultValue
	@Column(name="count_issues")
	private Integer countIssues;
	
	@Column(name="size")
	private Long size;
	
	@DefaultValue
	@Column(name="status")
	private String status;
	
	@Column(name="notification_date")
	private Timestamp notificationDate;
	
	public File(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer projectId) {
		this.branchId = projectId;
	}

	public Integer getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}

	public Integer getCountIssues() {
		return countIssues;
	}

	public void setCountIssues(Integer countIssues) {
		this.countIssues = countIssues;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(Timestamp notificationDate) {
		this.notificationDate = notificationDate;
	}
}
