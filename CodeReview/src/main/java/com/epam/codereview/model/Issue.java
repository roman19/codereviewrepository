package com.epam.codereview.model;

import java.sql.Timestamp;
import java.util.stream.IntStream;

import com.epam.codereview.dto.IssueDto;
import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="issue")
public class Issue {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="user_id")
	private Integer userId;
	
	@Column(name="file_id")
	private Integer fileId;
	
	@Column(name="line_from")
	private Integer lineFrom;
	
	@Column(name="line_to")
	private Integer lineTo;
	
	@Column(name="title")
	private String title;
	
	@Column(name="body")
	private String body;
	
	@DefaultValue
	@Column(name="time")
	private Timestamp time;
	
	@Column(name="type")
	private String type;
	
	@DefaultValue
	@Column(name="is_notified")
	private boolean notified;
	
	public Issue(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	public Integer getLineFrom() {
		return lineFrom;
	}

	public void setLineFrom(Integer lineFrom) {
		this.lineFrom = lineFrom;
	}

	public Integer getLineTo() {
		return lineTo;
	}

	public void setLineTo(Integer lineTo) {
		this.lineTo = lineTo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isNotified() {
		return notified;
	}

	public void setNotified(boolean notified) {
		this.notified = notified;
	}

    public int[] getLines() {
        return IntStream.range(lineFrom, lineTo + 1).toArray();
    }
}
