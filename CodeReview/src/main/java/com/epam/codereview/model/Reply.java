package com.epam.codereview.model;

import java.sql.Timestamp;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="reply")
public class Reply {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="owner_id")
	private Integer ownerId;
	
	@Column(name="issue_id")
	private Integer issueId;
	
	@Column(name="message")
	private String message;
	
	@DefaultValue
	@Column(name="time")
	private Timestamp time;
	
	private User user;
	
	public Reply(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	public Integer getIssueId() {
		return issueId;
	}

	public void setIssueId(Integer issueId) {
		this.issueId = issueId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
