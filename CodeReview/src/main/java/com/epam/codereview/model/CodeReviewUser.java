package com.epam.codereview.model;

import java.sql.Timestamp;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="code_review_user")
public class CodeReviewUser {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="code_review_id")
	private Integer codeReviewId;
	
	@Column(name="user_id")
	private Integer userId;
	
	@DefaultValue
	@Column(name="status")
	private String status;
	
	@DefaultValue
	@Column(name="coverage_percent")
	private Double coveragePercent;
	
	@DefaultValue
	@Column(name="notification_date")
	private Timestamp notificationDate;

    @DefaultValue
    @Column(name = "is_notified_owner")
    private boolean isNotifiedOwner;

	public CodeReviewUser(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodeReviewId() {
		return codeReviewId;
	}

	public void setCodeReviewId(Integer codeReviewId) {
		this.codeReviewId = codeReviewId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getCoveragePercent() {
		return coveragePercent;
	}

	public void setCoveragePercent(Double coveragePercent) {
		this.coveragePercent = coveragePercent;
	}

	public Timestamp getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(Timestamp notificationDate) {
		this.notificationDate = notificationDate;
	}

    public boolean isNotifiedOwner() {
        return isNotifiedOwner;
    }

    public void setIsNotifiedOwner(boolean isNotifiedOwner) {
        this.isNotifiedOwner = isNotifiedOwner;
    }
}
