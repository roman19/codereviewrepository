package com.epam.codereview.model;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="reply_like")
public class ReplyLike {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="user_id")
	private Integer userId;
	
	@Column(name="reply_id")
	private Integer replyId;
	
	@DefaultValue
	@Column(name="value")
	private Integer value;
	
	public ReplyLike(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getReplyId() {
		return replyId;
	}

	public void setReplyId(Integer replyId) {
		this.replyId = replyId;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
