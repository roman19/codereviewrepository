package com.epam.codereview.model;

import java.sql.Timestamp;

import com.epam.codereview.dto.UserDto;
import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="user")
public class User {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="fname")
	private String firstName;
	
	@Column(name="lname")
	private String lastName;
	
	@Column(name="login")
	private String login;
	
	@Column(name="password")
	private String password;
	
	@Column(name="email")
	private String email;
	
	@Column(name="social")
	private String social;
	
	@Column(name="social_id")
	private String socialId;
	
	@Column(name="image_url")
	private String imageUrl;

	@DefaultValue
	@Column(name="status")
	private String status;
	
	@DefaultValue
	@Column(name="last_online")
	private Timestamp lastOnline;
	
	@Column(name="confirm_code")
	private String confirmCode;
	
	@Column(name="locale")
	private String locale;
	
	@DefaultValue
	@Column(name="auth_date")
	private Timestamp authDate;

	@Column(name="is_image_from_server")
	private boolean imageFromServer;

	@DefaultValue
	@Column(name="enabled_memory")
	private Long enabledMemory;
	
	@Column(name="confirm_memory_purchase")
	private String confirmMemoryPurchase;
	
	private NotificationSetting setting;
	
	private String statusInGroup;
	
	
	
	public String getStatusInGroup() {
		return statusInGroup;
	}

	public void setStatusInGroup(String statusInGroup) {
		this.statusInGroup = statusInGroup;
	}

	public User(){}
	
	public User(UserDto userDto){
		this.firstName = userDto.getName();
		this.lastName = userDto.getSurname();
		this.login = userDto.getLogin();
		this.email = userDto.getEmail();
		this.password = userDto.getPassword();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSocial() {
		return social;
	}

	public void setSocial(String social) {
		this.social = social;
	}

	public String getSocialId() {
		return socialId;
	}

	public void setSocialId(String socialId) {
		this.socialId = socialId;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getLastOnline() {
		return lastOnline;
	}

	public void setLastOnline(Timestamp lastOnline) {
		this.lastOnline = lastOnline;
	}

	public String getConfirmCode() {
		return confirmCode;
	}

	public void setConfirmCode(String confirmCode) {
		this.confirmCode = confirmCode;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", login=" + login
				+ ", password=" + password + ", email=" + email + ", social=" + social + ", socialId=" + socialId
				+ ", imageUrl=" + imageUrl + ", status=" + status + ", lastOnline=" + lastOnline + ", confirmCode="
				+ confirmCode + ", locale=" + locale + "]";
	}

	public Timestamp getAuthDate() {
		return authDate;
	}

	public NotificationSetting getSetting() {
		return setting;
	}

	public void setSetting(NotificationSetting setting) {
		this.setting = setting;
	}

	public boolean isImageFromServer() {
		return imageFromServer;
	}

	public void setImageFromServer(boolean imageFromServer) {
		this.imageFromServer = imageFromServer;
	}
	
	public void setAuthDate(Timestamp authDate) {
		this.authDate = authDate;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return id.equals(user.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

	public Long getEnabledMemory() {
		return enabledMemory;
	}

	public void setEnabledMemory(Long enabledMemory) {
		this.enabledMemory = enabledMemory * 1024 * 1024;
	}

	public String getConfirmMemoryPurchase() {
		return confirmMemoryPurchase;
	}

	public void setConfirmMemoryPurchase(String confirmMemoryPurchase) {
		this.confirmMemoryPurchase = confirmMemoryPurchase;
	}
}
