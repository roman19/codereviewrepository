package com.epam.codereview.model;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name = "notification_setting")
public class NotificationSetting {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;

	@Column(name = "user_id")
	private Integer userId;

	@DefaultValue
	@Column(name = "is_group")
	private boolean group;

	@DefaultValue
	@Column(name = "is_review")
	private boolean review;

	@DefaultValue
	@Column(name = "is_file_added")
	private boolean fileAdded;

	@DefaultValue
	@Column(name = "is_issue")
	private boolean issue;

	@DefaultValue
	@Column(name = "is_reply")
	private boolean reply;

	@DefaultValue
	@Column(name = "is_sound")
	private boolean isSound;

	public NotificationSetting() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public boolean isReview() {
		return review;
	}

	public void setReview(boolean review) {
		this.review = review;
	}

	public boolean isGroup() {
		return group;
	}

	public void setGroup(boolean group) {
		this.group = group;
	}

	public boolean isIssue() {
		return issue;
	}

	public void setIssue(boolean issue) {
		this.issue = issue;
	}

	public boolean isReply() {
		return reply;
	}

	public void setReply(boolean reply) {
		this.reply = reply;
	}

	public boolean isFileAdded() {
		return fileAdded;
	}

	public void setFileAdded(boolean fileAdded) {
		this.fileAdded = fileAdded;
	}

	public boolean isSound() {
		return isSound;
	}

	public void setSound(boolean isSound) {
		this.isSound = isSound;
	}

	public void setItemValue(int item, boolean value) {
		switch (item) {
		case 0:
			group = value;
			review = value;
			fileAdded = value;
			issue = value;
			reply = value;
			break;
		case 1:
			group = value;
			break;
		case 2:
			review = value;
			break;
		case 3:
			fileAdded = value;
			break;
		case 4:
			issue = value;
			break;
		case 5:
			reply = value;
			break;
		case 6:
			isSound = value;
			break;
		}
	}

}
