package com.epam.codereview.model;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="branch")
public class Branch {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="project_id")
	private Integer projectId;
	
	public Branch(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
        if (name != null && name.contains("/")) {
            name =  name.substring(name.lastIndexOf('/') + 1, name.length());
        }
        return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
}
