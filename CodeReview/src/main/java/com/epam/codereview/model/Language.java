package com.epam.codereview.model;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="language")
public class Language {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="format")
	private String format;

    @Column(name="brush")
    private String brush;
	
	public Language(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

    public String getBrush() {
        return brush;
    }

    public void setBrush(String brush) {
        this.brush = brush;
    }
}
