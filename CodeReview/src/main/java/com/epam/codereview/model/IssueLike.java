package com.epam.codereview.model;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="issue_like")
public class IssueLike {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="user_id")
	private Integer userId;
	
	@Column(name="issue_id")
	private Integer issueId;
	
	@DefaultValue
	@Column(name="value")
	private Integer value;
	
	public IssueLike(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getIssueId() {
		return issueId;
	}

	public void setIssueId(Integer issueId) {
		this.issueId = issueId;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
}
