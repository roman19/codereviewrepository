package com.epam.codereview.model;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name = "deleted_review_user")
public class DeletedReviewUser {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;

	@Column(name = "deleted_review_id")
	private Integer deletedReviewId;

	@Column(name = "reviewer_id")
	private Integer reviewerId;

	@DefaultValue
	@Column(name = "status")
	private String status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDeletedReviewId() {
		return deletedReviewId;
	}

	public void setDeletedReviewId(Integer deletedReviewId) {
		this.deletedReviewId = deletedReviewId;
	}

	public Integer getReviewerId() {
		return reviewerId;
	}

	public void setReviewerId(Integer reviewerId) {
		this.reviewerId = reviewerId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
