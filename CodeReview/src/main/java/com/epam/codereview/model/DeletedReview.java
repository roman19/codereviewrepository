package com.epam.codereview.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.epam.codereview.dto.ReviewDto;
import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.service.CodeReviewUserService;
import com.epam.codereview.service.DeletedReviewService;
import com.epam.codereview.service.ReviewService;

@Table(name = "deleted_review")
public class DeletedReview implements Comparable<DeletedReview> {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;

	@Column(name = "owner_id")
	private Integer ownerId;

	@Column(name = "group_id")
	private Integer groupId;

	@Column(name = "name")
	private String name;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_git_project")
	private Boolean isGitProject;

	@Column(name = "creation_date")
	private Timestamp creationDate;

	@Column(name = "coverage_avg_percent")
	private Double coverageAvgPercent;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsGitProject() {
		return isGitProject;
	}

	public void setIsGitProject(Boolean isGitProject) {
		this.isGitProject = isGitProject;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Double getCoverageAvgPercent() {
		return coverageAvgPercent;
	}

	public void setCoverageAvgPercent(Double coverageAvgPercent) {
		this.coverageAvgPercent = coverageAvgPercent;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public int compareTo(DeletedReview o) {
		return (-1) * creationDate.compareTo(o.getCreationDate());
	}

	/**
	 * @return number of deleted reviews
	 */
	public static Integer getAllReviews(View view, int page, Integer recordsPerPage, Integer userId, String what) {
		List<DeletedReview> deletedReviews = null;
		List<DeletedReview> neededReviews = null;
		int offset = 0;
		try {
			deletedReviews = new DeletedReviewService().getAllActiveUnconfirmedDeletedReviewsByUserId(userId);
			neededReviews = new ArrayList<>();
			int i = (page - 1) * recordsPerPage;
			if (i >= deletedReviews.size())
				offset = i - deletedReviews.size();
			for (; i < (page - 1) * recordsPerPage + recordsPerPage; i++) {
				if (i >= deletedReviews.size()) {
					break;
				}
				neededReviews.add(deletedReviews.get(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		int temp = (neededReviews == null ? 0 : neededReviews.size());
		temp = recordsPerPage - (temp > recordsPerPage ? recordsPerPage : temp);
		List<ReviewDto> list;
		CodeReviewUserService codereviewService = new CodeReviewUserService();
		if(what.equals("all")) list = codereviewService.getAllActiveReviwByUserId(userId, offset, temp);
		else list = codereviewService.getAllOtherActiveReviwByUserId(userId, offset, temp);

		for (int i = 0; i < list.size(); i++) {
			ReviewDto reviewDto = list.get(i);
			ReviewService reviewService = new ReviewService();
			if (userId.equals(reviewDto.getUserId())) {
				list.get(i).setCoverageAvgPercent(reviewService.getReviewPercentTotal(reviewDto.getCodeReviewId()));
			} else {
				list.get(i).setCoverageAvgPercent(
						reviewService.getReviewPercentForUser(reviewDto.getCodeReviewId(), userId));
			}
		}
		view.getRequest().setAttribute("deletedReviews", neededReviews);
		view.getRequest().setAttribute("listReviews", list);
		return (neededReviews.size() == 0 && deletedReviews.size() != 0 ?
				deletedReviews.size():neededReviews.size()) + codereviewService.getNoOfRecords();
	}
}
