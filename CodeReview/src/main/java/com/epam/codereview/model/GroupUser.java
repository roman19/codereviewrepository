package com.epam.codereview.model;

import java.sql.Timestamp;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="group_user")
public class GroupUser {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="group_id")
	private Integer groupId;
	
	@Column(name="user_id")
	private Integer userId;
	
	@DefaultValue
	@Column(name="status")
	private String status;
	
	@DefaultValue
	@Column(name="notification_date")
	private Timestamp notificationDate;
	
	
	public GroupUser(){}

	@Override
	public String toString() {
		return "GroupUser [id=" + id + ", groupId=" + groupId + ", userId=" + userId + ", status=" + status
				+ ", notificationDate=" + notificationDate + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(Timestamp notificationDate) {
		this.notificationDate = notificationDate;
	}
}
