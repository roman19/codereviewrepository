package com.epam.codereview.model;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="notification_on_reply")
public class NotificationOnReply {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="reply_id")
	private Integer replyId;
	
	@Column(name="user_id")
	private Integer userId;
	
	@DefaultValue
	@Column(name="is_notified")
	private boolean notified;
	
	public NotificationOnReply(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getReplyId() {
		return replyId;
	}

	public void setReplyId(Integer replyId) {
		this.replyId = replyId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public boolean isNotified() {
		return notified;
	}

	public void setNotified(boolean notified) {
		this.notified = notified;
	}
	
}
