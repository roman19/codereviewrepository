package com.epam.codereview.model;

import java.sql.Timestamp;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="code_review")
public class CodeReview {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="name")
	private String name;

	@Column(name="group_id")
	private Integer groupId;
	
	@Column(name="user_id")
	private Integer ownerId;
	
	@DefaultValue
	@Column(name="is_active")
	private boolean active = true;
	
	@DefaultValue
	@Column(name="coverage_avg_percent")
	private Double coverageAvgPercent;
	
	@DefaultValue
	@Column(name="creation_date")
	private Timestamp creationDate;
	
	public CodeReview(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Double getCoverageAvgPercent() {
		return coverageAvgPercent;
	}

	public void setCoverageAvgPercent(Double coverageAvgPercent) {
		this.coverageAvgPercent = coverageAvgPercent;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

}
