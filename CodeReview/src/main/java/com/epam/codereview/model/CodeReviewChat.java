package com.epam.codereview.model;

import java.sql.Timestamp;

import com.epam.codereview.manager.db.Column;
import com.epam.codereview.manager.db.DefaultValue;
import com.epam.codereview.manager.db.GeneratedValue;
import com.epam.codereview.manager.db.Id;
import com.epam.codereview.manager.db.Table;

@Table(name="chat_code_review")
public class CodeReviewChat {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	
	@Column(name="code_review_id")
	private Integer codeReviewId;
	
	@Column(name="user_id_from")
	private Integer userIdFrom;
	
	@DefaultValue
	@Column(name="time")
	private Timestamp time;
	
	@Column(name="text")
	private String text;
	
	private User user;
	
	public CodeReviewChat(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodeReviewId() {
		return codeReviewId;
	}

	public void setCodeReviewId(Integer codeReviewId) {
		this.codeReviewId = codeReviewId;
	}

	public Integer getUserIdFrom() {
		return userIdFrom;
	}

	public void setUserIdFrom(Integer userIdFrom) {
		this.userIdFrom = userIdFrom;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
