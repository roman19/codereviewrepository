package com.epam.codereview.manager.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.reflections.Reflections;

import com.epam.codereview.util.SenderAuthenticator;

/**
 * Servlet implementation class FrontServlet
 */

@WebServlet("/frontservlet/*")
@MultipartConfig(maxFileSize = 16177215)
public class FrontServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FrontServlet() {
		super();
	}

	@Override
	public void init() throws ServletException {
		super.init();
		
		SenderAuthenticator.initializeSession();

		String controllerPackage = getServletContext().getInitParameter(
				"package_for_controller");

		Reflections reflections = new Reflections(controllerPackage);

		Set<Class<?>> controllers = reflections
				.getTypesAnnotatedWith(Controller.class);

		for (Class<?> clazz : controllers) {
			String parentKey = "";

			if (clazz.isAnnotationPresent(RequestMapping.class))
				parentKey = clazz.getAnnotation(RequestMapping.class).value();

			Set<Method> methods = getAnnotatedMethods(clazz);

			for (Method method : methods) {
				RequestMapping rq = method.getAnnotation(RequestMapping.class);
				String key = parentKey + rq.value() + rq.method().name();

				MethodFactory.addMethod(key, method);
			}
		}

	}

	private Set<Method> getAnnotatedMethods(Class<?> clazz) {
		Set<Method> methods = new HashSet<>();

		for (Method method : clazz.getMethods()) {
			if (method.isAnnotationPresent(RequestMapping.class))
				methods.add(method);
		}
		return methods;
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		View view = new View(request, response);
		Method method = MethodFactory.getMethod(request);

		try {
			if (method != null) {
				method.invoke(method.getDeclaringClass().newInstance(), view);
			} else {
				response.sendError(404);
			}
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | InstantiationException e) {
			e.printStackTrace();
			response.sendError(404);
		}
	}

}
