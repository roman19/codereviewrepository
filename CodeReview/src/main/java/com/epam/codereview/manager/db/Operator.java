package com.epam.codereview.manager.db;

// e.g. OR AND
public class Operator extends Expression {
	private String operator;

	public Operator(String operator) {
		this.operator = operator;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

}
