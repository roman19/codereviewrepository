package com.epam.codereview.manager.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.util.PathGetter;

public class View {
	private HttpServletRequest request;
	private HttpServletResponse response;
	
	public View(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}
	
	public void setAttribute(String arg, Object obj){
		request.setAttribute(arg, obj);
	}
	
	public String getParameter(String param){
		return request.getParameter(param);
	}
	
	public Integer getSessionUserID() throws SessionIsNotAvailableException{
		Object id = request.getSession().getAttribute("userID");
		
		if(id==null)
			throw new SessionIsNotAvailableException("userID is not avaliable");
			
		return (Integer)id;
	}
	
	public String getLocale(){
		return (String) request.getSession().getAttribute("locale");
	}
	public String getLastUrlParam(){
		return PathGetter.getLast(request.getPathInfo());
	}
	
	public void forward(String page) throws ServletException, IOException{
		request.getRequestDispatcher(page).forward(request, response);
	}
	
	
}
