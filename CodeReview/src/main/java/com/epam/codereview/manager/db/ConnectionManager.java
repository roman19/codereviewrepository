package com.epam.codereview.manager.db;

import com.epam.codereview.exception.UnexpectedServerException;
import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionManager {

    private final static Logger LOG = Logger.getLogger(ConnectionManager.class);

    public static Connection getConnection() {
        try {
            return ConnectionManagerHelper.DATA_SOURCE_INSTANCE.getConnection();
        } catch (SQLException e) {
            LOG.error("Fail while create connection.", e);
            throw new UnexpectedServerException("Fail while create connection.");
        }
    }

    private static class ConnectionManagerHelper {
        private static final DataSource DATA_SOURCE_INSTANCE = initDataSource();

        private static DataSource initDataSource() {
            try {
                Context initialContext = new InitialContext();
                Context environmentContext = (Context) initialContext.lookup("java:comp/env");
                String dataResourceName = "jdbc/CodeReview";
                return (DataSource) environmentContext.lookup(dataResourceName);
            } catch (NamingException e) {
                LOG.error("Fail while data source initialization.", e);
                throw new UnexpectedServerException("Fail while data source initialization.");
            }
        }
    }
}
