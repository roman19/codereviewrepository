package com.epam.codereview.manager.db;

//e.g. id=10
public class Condition extends Expression {
	private String column;
	private String operator;
	private Object value;
	
	public Condition(String column, String operator, Object value){
		this.column = column;
		this.operator = operator;
		this.value = value;
	}

	public String getColumn() {
		return column;
	}

	public String getOperator() {
		return operator;
	}

	public Object getValue() {
		return value;
	}
}
