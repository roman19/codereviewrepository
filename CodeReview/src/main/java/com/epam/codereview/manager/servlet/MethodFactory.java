package com.epam.codereview.manager.servlet;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.epam.codereview.exception.BuildApplicationException;

public class MethodFactory {
	private static Map<String, Method> methods = new HashMap<>();

	public static Method getMethod(HttpServletRequest request) {
		Method method = methods
				.get(request.getPathInfo() + request.getMethod());

		if (method == null) {
			Integer i = request.getPathInfo().lastIndexOf("/");
			String key = request.getPathInfo().substring(0, i + 1) + '*'
					+ request.getMethod();
			method = methods.get(key);
		}

		return method;
	}

	static void addMethod(String key, Method value) {

		if (methods.put(key, value) != null) {
			throw new BuildApplicationException("Such URL already exist!");
		}
	}

	public static void print() {
		for (Map.Entry<String, Method> pair : methods.entrySet()) {
			System.out.println(pair.getKey() + "|" + pair.getValue());
		}
	}
}
