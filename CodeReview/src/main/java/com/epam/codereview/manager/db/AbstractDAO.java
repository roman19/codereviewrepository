package com.epam.codereview.manager.db;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

public class AbstractDAO<T> {

    protected Connection connection;

    private EntityManager entityManager = EntityManager.getEntityManager();
	private Class<T> clazz;

    @Deprecated
    protected AbstractDAO(Class<T> clazz) {  //TODO remove
		this.clazz = clazz;
        connection = ConnectionManager.getConnection();
	}

	public AbstractDAO(Class<T> clazz, Connection connection) {
        this.connection = connection;
        this.clazz = clazz;
	}

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public T get(Serializable id) {
		return entityManager.get(connection, clazz, id);
	}
	
	public List<T> get(Criteria criteria) {
		return entityManager.get(connection, clazz, criteria);
	}

	public List<T> getAll() {
		return entityManager.getAll(connection, clazz);
	}

	public Integer insert(Object object) {
		return (Integer)entityManager.save(connection, object);
	}

	public void delete(Object object) {
		entityManager.delete(connection, object);
	}

	public void delete(Serializable id) {
		entityManager.delete(connection, clazz, id);
	}

	public void update(Object object) {
		entityManager.update(connection, object);
	}

}
