package com.epam.codereview.manager.db;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Transformer {

	public static <T> T transformToObject(ResultSet rs, Class<T> clazz)
			throws SQLException, InstantiationException, IllegalAccessException {
		T object = clazz.newInstance();

		Map<Field, String> fieldsColumns = initPairsFieldColumn(clazz);

		if (rs.next()) {
            for (Map.Entry<Field, String> pair : fieldsColumns.entrySet()) {
                pair.getKey().setAccessible(true);
                Object value = rs.getObject(pair.getValue());
                pair.getKey().set(object, value);
            }

		} else {
			return null;
		}

		return object;

	}

	public static <T> List<T> transformToList(ResultSet rs, Class<T> clazz)
			throws SQLException, InstantiationException, IllegalAccessException {
		List<T> objects = new ArrayList<>();

		Map<Field, String> fieldsColumns = initPairsFieldColumn(clazz);

		while (rs.next()) {
			T object = clazz.newInstance();

			for (Map.Entry<Field, String> pair : fieldsColumns.entrySet()) {
				pair.getKey().setAccessible(true);
                Object value = rs.getObject(pair.getValue());
                pair.getKey().set(object, value);
			}
			objects.add(object);
		}
		return objects;

	}
	
	private static <T> Map<Field, String> initPairsFieldColumn(Class<T> clazz) {
		Field[] fields = clazz.getDeclaredFields();

		Map<Field, String> fieldsColumns = new HashMap<>();

		for (Field field : fields) {
			if (field.isAnnotationPresent(Column.class)) {
				Column column = field.getAnnotation(Column.class);
				fieldsColumns.put(field, column.name());
			}

		}

		return fieldsColumns;
	}

}
