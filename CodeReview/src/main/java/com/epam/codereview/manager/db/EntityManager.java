package com.epam.codereview.manager.db;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.epam.codereview.exception.EntityException;

public class EntityManager {
	private static EntityManager instance;

	public static synchronized EntityManager getEntityManager() {
		if (instance == null) {
			instance = new EntityManager();
		}
		return instance;
	}

	private EntityManager() {
	}

	public <T> T get(Connection connection, Class<T> clazz, Serializable id) {
		T entity = null;

		try {

			String tableName = getTableName(clazz);

			if (!getIdField(clazz).getType().isAssignableFrom(id.getClass()))
				throw new EntityException("Bad id argument");

			String columnNameWithId = getIdField(clazz).getAnnotation(
					Column.class).name();
			String sql;

			sql = "SELECT * FROM " + tableName + " WHERE " + columnNameWithId
					+ " = " + id;

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			entity = Transformer.transformToObject(rs, clazz);

		} catch (SQLException | InstantiationException | IllegalAccessException e) {
			throw new EntityException("There is problem with retrieving", e);
		}

		return entity;

	}

	public <T> List<T> get(Connection connection, Class<T> clazz, Criteria criteria) {
		List<T> entities = null;

		try {

			String tableName = getTableName(clazz);

			String sql = "SELECT * FROM " + tableName
					+ buildWhereClause(criteria);

			PreparedStatement pstmt = connection.prepareStatement(sql);

			int i = 1;
			for (Expression expr : criteria.getWhereClause()) {
				if (expr.getClass() == Condition.class) {
					Condition cond = (Condition) expr;
					if (cond.getValue() != null)
						pstmt.setObject(i++, cond.getValue());
				}
			}

			ResultSet rs = pstmt.executeQuery();
			entities = Transformer.transformToList(rs, clazz);

		} catch (SQLException | InstantiationException | IllegalAccessException e) {
			throw new EntityException("There is problem with retrieving", e);
		}

		return entities;
	}

	public <T> List<T> getAll(Connection connection, Class<T> clazz) {
		List<T> entities = null;

		try {

			String tableName = getTableName(clazz);
			String sql;

			sql = "SELECT * FROM " + tableName;

			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			entities = Transformer.transformToList(rs, clazz);

		} catch (SQLException | InstantiationException | IllegalAccessException e) {
			throw new EntityException("There is problem with retrieving", e);
		}

		return entities;
	}

	public Serializable save(Connection connection, Object object) {
		Serializable id = null;

		Class<?> clazz = object.getClass();
		String tableName = getTableName(clazz);
		String sql;

		try {
			Map<String, Object> columnsValues = createMapColumnsValues(object);

			sql = createQueryForInsert(tableName, columnsValues);

			PreparedStatement pstmt = connection.prepareStatement(sql,
					Statement.RETURN_GENERATED_KEYS);

			int i = 1;
			for (Object o : columnsValues.values()) {
				pstmt.setObject(i++, o);
			}

			pstmt.executeUpdate();

			ResultSet rs = pstmt.getGeneratedKeys();
			rs.next();
			id = rs.getInt(1);

		} catch (SQLException | IllegalAccessException e) {
			e.printStackTrace();

			throw new EntityException("There is problem with saving", e);

		}

		return id;

	}

	public void delete(Connection connection, Object object) {
		Class<?> clazz = object.getClass();
		String tableName = getTableName(clazz);
		String sql;

		try {
			Field idField = getIdField(clazz);
			String idColumnName = idField.getAnnotation(Column.class).name();

			idField.setAccessible(true);
			Object id = idField.get(object);

			sql = "DELETE FROM " + tableName + " WHERE " + idColumnName
					+ " = ?";

			PreparedStatement pstmt = connection.prepareStatement(sql);
			pstmt.setObject(1, id);
			pstmt.executeUpdate();

		} catch (SQLException | IllegalArgumentException
				| IllegalAccessException e) {
			throw new EntityException("There is problem with deleting", e);

		}
	}

	public <T> void delete(Connection connection, Class<T> clazz, Serializable id) {
		String tableName = getTableName(clazz);
		String sql;
		Field idField = getIdField(clazz);
		String idColumnName = idField.getAnnotation(Column.class).name();

		sql = "DELETE FROM " + tableName + " WHERE " + idColumnName + " = ?";

		try {
			PreparedStatement pstmt = connection.prepareStatement(sql);
			pstmt.setObject(1, id);
			pstmt.executeUpdate();

		} catch (SQLException e) {
			throw new EntityException("There is problem with deleting", e);

		}

	}

	private String buildWhereClause(Criteria criteria) {
		String whereClause = " WHERE ";

		for (Expression expr : criteria.getWhereClause()) {
			if (expr.getClass() == Condition.class) {
				Condition cond = (Condition) expr;
				if (cond.getValue() == null) {
					whereClause += cond.getColumn() + " " + cond.getOperator()
							+ " NULL";
				} else {
					whereClause += cond.getColumn() + " " + cond.getOperator()
							+ " " + "? ";
				}

			} else if (expr.getClass() == Operator.class) {
				whereClause += ((Operator) expr).getOperator() + " ";
			}
		}

		return whereClause;

	}

	private <T> String getTableName(Class<T> clazz) {
		String tableName;

		if (clazz.isAnnotationPresent(Table.class)) {
			tableName = clazz.getAnnotation(Table.class).name();
		} else {
			throw new EntityException("Such table doesn't exist");
		}
		return tableName;

	}

	private <T> Field getIdField(Class<T> clazz) {

		List<Field> fieldId = Arrays.asList(clazz.getDeclaredFields()).stream()
				.filter(w -> w.isAnnotationPresent(Id.class))
				.collect(Collectors.toList());

		if (fieldId.size() > 1)
			throw new EntityException("Such entity has more then one id");
		else if (fieldId.size() == 0)
			throw new EntityException("Such entity doesn't have id");

		return fieldId.get(0);
	}

	private Map<String, Object> createMapColumnsValues(Object object)
			throws IllegalArgumentException, IllegalAccessException {
		Map<String, Object> columnsValues = new HashMap<>();
		Class<?> clazz = object.getClass();

		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);

			if (field.isAnnotationPresent(Column.class)
					&& (!field.isAnnotationPresent(GeneratedValue.class))) {
				if (field.isAnnotationPresent(DefaultValue.class)) {
					if (field.get(object) != null) {
						columnsValues.put(field.getAnnotation(Column.class)
								.name(), field.get(object));
					}
				} else {
					columnsValues.put(field.getAnnotation(Column.class).name(),
							field.get(object));
				}
			}
		}

		return columnsValues;
	}

	private String createQueryForInsert(String tableName,
			Map<String, Object> columnsValues) throws IllegalArgumentException,
			IllegalAccessException {

		StringBuilder sql = new StringBuilder("INSERT INTO " + tableName + "(");

		for (String s : columnsValues.keySet()) {
			sql.append(s + ",");
		}
		sql.delete(sql.length() - 1, sql.length());
		sql.append(") VALUES(");

		for (int i = 0; i < columnsValues.size(); i++) {
			sql.append("?" + ",");
		}
		sql.delete(sql.length() - 1, sql.length());
		sql.append(")");

		return sql.toString();
	}

	private String createQueryForUpdate(String idColumnName, String tableName,
			Map<String, Object> columnsValues) throws IllegalArgumentException,
			IllegalAccessException {
		StringBuilder sql = new StringBuilder("UPDATE " + tableName + " SET ");

		for (String s : columnsValues.keySet()) {
			sql.append(s + "= ?,");
		}
		sql.delete(sql.length() - 1, sql.length());
		sql.append(" WHERE ");

		sql.append(idColumnName + " = ?");

		return sql.toString();
	}

	public void update(Connection connection, Object object) {
		Class<?> clazz = object.getClass();
		String tableName = getTableName(clazz);

		Field idField = getIdField(clazz);
		String idColumnName = idField.getAnnotation(Column.class).name();

		String sql;

		try {
			Map<String, Object> columnsValues = createMapColumnsValues(object);

			sql = createQueryForUpdate(idColumnName, tableName, columnsValues);

			PreparedStatement pstmt = connection.prepareStatement(sql);

			int i = 1;
			for (Object o : columnsValues.values()) {
				pstmt.setObject(i++, o);
			}

			idField.setAccessible(true);
			Object id = idField.get(object);
			pstmt.setObject(i, id);

			pstmt.executeUpdate();

		} catch (SQLException | IllegalAccessException e) {
			throw new EntityException("There is problem with updating", e);
		}
	}

}
