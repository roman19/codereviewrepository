package com.epam.codereview.manager.db;

import java.util.ArrayList;
import java.util.List;

//e.g. where List: id=10 AND price<11
public class Criteria {
	private List<Expression> whereClause;

	public Criteria() {
		whereClause = new ArrayList<>();
	}

	public void add(Expression expr) {
		whereClause.add(expr);
	}
	
	public List<Expression> getWhereClause(){
		return whereClause;
	}

}
