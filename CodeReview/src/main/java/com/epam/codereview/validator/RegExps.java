package com.epam.codereview.validator;

public class RegExps {

	public static final String PASSWORD = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])\\w{6,}$";
	public static final String ONLY_DIGITS = "[0-9]*";
	public static final String ONLY_LETTERS = "^([a-zA-Zа-яА-ЯіІьїЇєЄґҐ'`’]*)$";
	public static final String EMAIL = "^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$";
	public static final String ANY_CHARACTERS = ".*";
	public static final String LETTERS_AND_DIGITS = "^([0-9a-zA-Zа-яА-ЯіІьїЇєЄґҐ'`’]*)$";
	public static final String LOGIN = "^([0-9a-zA-Z_\\.]*)$";
}
