package com.epam.codereview.validator;

import java.lang.reflect.Field;
import java.text.ParseException;

import com.epam.codereview.exception.ValidationException;
import com.epam.codereview.exception.WrongInputDataException;
import com.epam.codereview.util.DateConverter;

public class FormValidator {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> void validate(T object) throws ValidationException {
		Class<?> classT = object.getClass();

		try {

			for (Field field : classT.getDeclaredFields()) {
				if (field.isAnnotationPresent(Validation.class)) {
					field.setAccessible(true);
					if (field.get(object) == null) {
						throw new WrongInputDataException("Wrong value of "
								+ field.getName());
					}
					String fieldValue = (String) field.get(object);

					if (fieldValue.trim().length() < field.getAnnotation(
							Validation.class).minLength()
							|| fieldValue.length() > field.getAnnotation(
									Validation.class).maxLength()
							|| !fieldValue.matches(field.getAnnotation(
									Validation.class).regexp())) {
						throw new WrongInputDataException("Wrong value of "
								+ field.getName());
					}

				}
				if (field.isAnnotationPresent(EnumField.class)) {
					field.setAccessible(true);
					if (field.get(object) == null) {
						throw new WrongInputDataException("Wrong value of "
								+ field.getName());
					}
					String fieldValue = ((String) field.get(object))
							.toUpperCase();
					try {
						Enum.valueOf(
								(Class<Enum>) field.getAnnotation(
										EnumField.class).enumClass(),
								fieldValue);
					} catch (IllegalArgumentException e) {
						throw new WrongInputDataException("Wrong type of enum");
					}
				}

				if (field.isAnnotationPresent(DateField.class)) {
					field.setAccessible(true);
					if (field.get(object) == null) {
						throw new WrongInputDataException("Wrong value of "
								+ field.getName());
					}
					String fieldValue = (String) field.get(object);
					try {
						DateConverter.convertIntoTimestamp(fieldValue, field
								.getAnnotation(DateField.class).format());
					} catch (ParseException e) {
						throw new WrongInputDataException("Wrong date format");
					}
				}
			}
		} catch (IllegalArgumentException | IllegalAccessException
				| WrongInputDataException e) {
			throw new ValidationException(e);
		}

	}
}
