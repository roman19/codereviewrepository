package com.epam.codereview.tag;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.epam.codereview.util.TimestampUtil;

public class ShowTime extends SimpleTagSupport {
	private String locale;
	private Timestamp time;
	
	public void setLocale(String locale) {
		this.locale = locale;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		
		out.write(TimestampUtil.showSmartTime(time, locale));
	}
	
}
