package com.epam.codereview.tag;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.epam.codereview.model.User;
import com.epam.codereview.service.UserService;
import com.epam.codereview.util.LoginFetcher;

public class ShowReply extends SimpleTagSupport {
	private UserService userService = new UserService();
	private String message;

	public void setMessage(String message) {
		this.message = message;
	}

	public void doTag() throws JspException, IOException {
		List<String> logins = LoginFetcher.getLoginsFromMessage(message);

		for (String login : logins) {
			message = message.replaceAll("@" + login, templateForUser(login));
		}
		JspWriter out = getJspContext().getOut();
		out.println(message);
	}

	private String templateForUser(String login) {
		User user = userService.getByLogin(login);

		return "<a style='color:black !important; text-decoration:underline' href='javascript:void(0);' title='" + user.getFirstName() + " "
				+ user.getLastName()
				+ "' data-toggle='popover' data-placement='top' data-content='"
				+ user.getEmail() + "'>@" + user.getLogin() + "</a>";
	}
}