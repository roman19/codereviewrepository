package com.epam.codereview.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.log4j.Logger;

public class ShowImageUrl extends SimpleTagSupport {

	private final static Logger LOG = Logger.getLogger(ShowImageUrl.class);

	private String url;
	private boolean fromServer;

	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setFromServer(boolean fromServer) {
		this.fromServer = fromServer;
	}



	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();

		if (fromServer == true) {
			out.println("/CodeReview/get_user_image/" + url);
		} else {
			out.println(url);
		}
	}
}
