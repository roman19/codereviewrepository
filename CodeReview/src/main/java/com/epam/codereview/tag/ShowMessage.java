package com.epam.codereview.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.log4j.Logger;

import com.epam.codereview.util.Messager;

public class ShowMessage extends SimpleTagSupport {

	private final static Logger LOG = Logger.getLogger(ShowMessage.class);

	private String key;
	private String locale;

	public void setKey(String key) {
		this.key = key;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		String message = Messager.getMessage(key, locale);
		if (message != null) {
			out.print(message);
		} else {
			LOG.error("wrong locale: " + locale);
		}
	}
}