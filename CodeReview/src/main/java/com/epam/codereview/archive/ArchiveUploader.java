package com.epam.codereview.archive;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.epam.codereview.controller.CodeReviewCreateController;
import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.manager.servlet.View;

public class ArchiveUploader {
	private final static Logger LOG = Logger.getLogger(CodeReviewCreateController.class);
	private View view;
	private final String UPLOAD_PATH;
	private String projectName;

	public ArchiveUploader(View view) throws SessionIsNotAvailableException {
		this.view = view;
		UPLOAD_PATH = view.getRequest().getServletContext().getInitParameter("review-project-upload")
		 + view.getSessionUserID();
	}

	public String uploadProject() throws Exception {
		File archive = uploadArchiveFile();
		try{
			String parentFolderName = extractFromArchive(archive);
			return parentFolderName;
		}finally{
			archive.delete();
		}
	}

	@SuppressWarnings("rawtypes")
	private File uploadArchiveFile() throws Exception {
		if (!ServletFileUpload.isMultipartContent(view.getRequest())) {
			throw new NullPointerException();
		}
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		ServletFileUpload upload = new ServletFileUpload(factory);
		File uploadDir = new File(UPLOAD_PATH);
		if (!uploadDir.exists()) {
			uploadDir.mkdir();
		}
		try {
			List formItems = upload.parseRequest(view.getRequest());
			Iterator iter = formItems.iterator();
			FileItem item = (FileItem) iter.next();
			if (!item.isFormField()) {
				String fileName = new File(item.getName()).getName();
				if(!FilenameUtils.getExtension(fileName).equals("zip")) throw new FileNotFoundException();
				String filePath = UPLOAD_PATH + File.separator + fileName;
				File storeFile = new File(filePath);
				item.write(storeFile);
				return new File(UPLOAD_PATH + "/" + fileName);
			}
		} catch (Exception ex) {
			LOG.warn(ex);
			throw ex;
		}
		return null;
	}

	private String extractFromArchive(File file) throws IOException {
		String zipFilePath = file.getAbsolutePath();
		return unzip(zipFilePath, UPLOAD_PATH);
	}

	private String unzip(String zipFilePath, String destDirectory) {
		try {
			File destDir;
			String[] arrayTemp = zipFilePath.split("\\\\");
			String zipName = arrayTemp[arrayTemp.length - 1];
			zipName = zipName.substring(0, zipName.length() - FilenameUtils.getExtension(zipName).length() - 1);
			projectName = zipName;
			while(true){
				destDir = new File(destDirectory + "/" + zipName + new Random().nextInt(10000000));
				if(destDir.exists()) continue;
				destDir.mkdir();
				break;
			}
			ZipFile zipFile = new ZipFile(zipFilePath);
			zipFile.extractAll(destDir.getAbsolutePath());
			return destDir.getName();
		} catch (ZipException e) {
			return null;
		}
	}

	public String getProjectName() {
		return projectName;
	}

}
