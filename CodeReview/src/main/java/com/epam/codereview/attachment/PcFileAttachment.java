package com.epam.codereview.attachment;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.epam.codereview.dto.CodeReviewDto;
import com.epam.codereview.model.Project;
import com.epam.codereview.project.ProjectHandler;
import com.epam.codereview.service.FileService;
import com.epam.codereview.service.LanguageService;
import com.epam.codereview.service.ProjectService;

public class PcFileAttachment extends FileAttachment{
	protected final static Logger LOG = Logger.getLogger(PcFileAttachment.class);
	private String tempFolder;
	
	public PcFileAttachment(String tempFolder, String[] newFiles, CodeReviewDto codeReviewDto, String reviewsServerPath){
		super(newFiles, codeReviewDto, reviewsServerPath);
		this.tempFolder = tempFolder;
	}

	@Override
	public int addNewFiles(){
		int counter = 0;
		for(String newFile : newFiles){
			try{
				if(addFile(newFile , getFile(newFile))) counter++;
			}catch(Exception e){
				LOG.error(e);
			}
		}
		ProjectHandler projectHandler = new ProjectHandler(null, reviewsServerPath);
		Project project = codeReviewDto.getProject();
		project.setSize(
				projectHandler.getSizeOfProject(
						codeReviewDto.getCodeReview().getOwnerId() + "/" + project.getFullName()));
		new ProjectService().update(project);
		Integer id = codeReviewDto.getCodeReview().getOwnerId();
		projectHandler.removeUncheckedFiles(id + "/temp" + id);
		return counter;
	}

	private boolean addFile(String path, com.epam.codereview.model.File modelFile)
			throws Exception {
		FileService fileService = new FileService();
		File oldFile = new File(reviewsServerPath + path);
		if (oldFile.exists()) {
			try {
				if (oldFile.delete()) {
					fileService.delete(modelFile);
				} else
					throw new Exception();
			} catch (Exception e) {
				return false;
			}
		}
		oldFile.getParentFile().mkdirs();
		File newFile = new File(reviewsServerPath + codeReviewDto.getCodeReview().getOwnerId() + tempFolder + "/"
																										+ oldFile.getName());
		FileUtils.copyFileToDirectory(newFile, oldFile.getParentFile());
		Integer h = oldFile.getAbsolutePath().indexOf(codeReviewDto.getCodeReview().getOwnerId() + "\\"
				+ codeReviewDto.getProject().getFullName());
		com.epam.codereview.model.File tempFile = modelFile;
		if(modelFile == null){
			modelFile = codeReviewDto.getFileDtos().get(0).getFile();
			tempFile = new com.epam.codereview.model.File();
			tempFile.setCountIssues(0);
			tempFile.setLanguageId(new LanguageService().getLanguageByFormat(FilenameUtils.getExtension(path)).getId());
			tempFile.setSize(newFile.length());
			tempFile.setBranchId(modelFile.getBranchId());
			tempFile.setId(modelFile.getId());
		}
		tempFile.setNotificationDate(new Timestamp(System.currentTimeMillis()));
		tempFile.setStatus("NEW");
		tempFile.setUrl(oldFile.getAbsolutePath().substring(h).replaceAll("\\\\", "/"));
		fileService.insert(tempFile, codeReviewDto.getCodeReview().getId());
		
		return true;
	}
}
