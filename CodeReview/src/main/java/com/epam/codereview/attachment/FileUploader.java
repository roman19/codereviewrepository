package com.epam.codereview.attachment;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.service.LanguageService;

public class FileUploader {
	private final static Logger LOG = Logger.getLogger(FileUploader.class);
	private View view;
	private final String UPLOAD_PATH;
	private String unsupportedExtensions = "";
	private List<AttachmentFile> downloadedFiles = new ArrayList<>();

	public FileUploader(View view) throws SessionIsNotAvailableException {
		this.view = view;
		UPLOAD_PATH = view.getRequest().getServletContext().getInitParameter("review-project-upload")
				+ view.getSessionUserID();
	}

	@SuppressWarnings("rawtypes")
	public String uploadFiles() throws Exception {
		if (!ServletFileUpload.isMultipartContent(view.getRequest())) {
			throw new NullPointerException();
		}
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		ServletFileUpload upload = new ServletFileUpload(factory);
		File uploadDir;
		while (true) {
			uploadDir = new File(UPLOAD_PATH + "/temp" + view.getSessionUserID() + "/temp folder" + new Random().nextInt(10000000));
			if (uploadDir.exists())
				continue;
			uploadDir.mkdirs();
			break;
		}
		try {
			List formItems = upload.parseRequest(view.getRequest());
			Iterator iter = formItems.iterator();
			FileItem item = (FileItem) iter.next();
			Set<String> setOfExtensions = new LinkedHashSet<>();
			while (!item.isFormField()) {
				String fileName = new File(item.getName()).getName();
				String filePath = uploadDir.getAbsolutePath() + "/" + fileName;
				File storeFile = new File(filePath);
				if (!new LanguageService().exists(FilenameUtils.getExtension(storeFile.getName()))) {
					setOfExtensions.add("." + FilenameUtils.getExtension(storeFile.getName()));
				} else {
					item.write(storeFile);
					AttachmentFile aFile = new AttachmentFile(storeFile.getName(), storeFile.length());
					downloadedFiles.add(aFile);
				}
				if (iter.hasNext())
					item = (FileItem) iter.next();
				else
					break;
			}
			for (String extension : setOfExtensions) {
				unsupportedExtensions += extension + ", ";
			}
		} catch (Exception ex) {
			LOG.warn(ex);
			throw ex;
		}
		return "/temp" + view.getSessionUserID() + "/" + uploadDir.getName();
	}

	public String getUnsupportedExtensions() {
		return unsupportedExtensions.equals("") ? "":unsupportedExtensions.substring(0, unsupportedExtensions.length() - 2);
	}

	public List<AttachmentFile> getDownloadedFiles() {
		return downloadedFiles;
	}

	public class AttachmentFile {
		private String name;
		private Long size;
		
		public AttachmentFile(String name, Long size){
			this.name = name;
			this.size = size;
		}

		public String getName() {
			return name;
		}

		public Long getSize() {
			return size;
		}
	}
}
