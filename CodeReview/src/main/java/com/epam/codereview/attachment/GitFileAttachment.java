package com.epam.codereview.attachment;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.epam.codereview.dto.CodeReviewDto;
import com.epam.codereview.model.Project;
import com.epam.codereview.project.ProjectHandler;
import com.epam.codereview.service.FileService;
import com.epam.codereview.service.LanguageService;
import com.epam.codereview.service.ProjectService;

public class GitFileAttachment extends FileAttachment{
	private final static Logger LOG = Logger.getLogger(GitFileAttachment.class);
	private String tempProjectFolderName;

	public GitFileAttachment(String[] newFiles, CodeReviewDto codeReviewDto, String reviewsServerPath) {
		super(newFiles, codeReviewDto, reviewsServerPath);
		try{
			this.tempProjectFolderName = newFiles[0].split("/")[1];
		}catch(Exception e){
			this.tempProjectFolderName = newFiles[0].split("\\\\")[1];
		}
	}

	@Override
	public int addNewFiles() {
		int count = 0;
		for (String relativeNewPath : getAllNewPathes()) {
			try {
				if(addFile(relativeNewPath, codeReviewDto.getCodeReview().getOwnerId(),
						codeReviewDto.getProject().getFullName(), getFile(relativeNewPath))) count++;
       			} catch (Exception e) {
				LOG.error(e);
			}
		}
		ProjectHandler projectHandler = new ProjectHandler(null, reviewsServerPath);
		Project project = codeReviewDto.getProject();
		project.setSize(
				projectHandler.getSizeOfProject(
						codeReviewDto.getCodeReview().getOwnerId() + "/" + project.getFullName()));
		new ProjectService().update(project);
		projectHandler.removeUncheckedFiles(codeReviewDto.getCodeReview().getOwnerId() + "/" + tempProjectFolderName);
		return count;
	}

	private boolean addFile(String path, Integer ownerId, String projectName, com.epam.codereview.model.File modelFile)
			throws Exception {
		FileService fileService = new FileService();
		File oldFile = new File(reviewsServerPath + ownerId + "/" + projectName + "/" + path);
		if (oldFile.exists()) {
			try {
				if (oldFile.delete()) {
					fileService.delete(modelFile);
				} else
					throw new Exception();
			} catch (Exception e) {
				return false;
			}
		}
		oldFile.getParentFile().mkdirs();
		File newFile = new File(reviewsServerPath + ownerId + "/" + tempProjectFolderName + "/" + path);
		FileUtils.copyFileToDirectory(newFile, oldFile.getParentFile());
		Integer h = oldFile.getAbsolutePath().indexOf(ownerId + "\\" + projectName);
		com.epam.codereview.model.File tempFile = modelFile;
		if(modelFile == null){
			modelFile = codeReviewDto.getFileDtos().get(0).getFile();
			tempFile = new com.epam.codereview.model.File();
			tempFile.setCountIssues(0);
			tempFile.setLanguageId(new LanguageService().getLanguageByFormat(FilenameUtils.getExtension(path)).getId());
			tempFile.setSize(newFile.length());
			tempFile.setBranchId(modelFile.getBranchId());
			tempFile.setId(modelFile.getId());
		}
		tempFile.setNotificationDate(new Timestamp(System.currentTimeMillis()));
		tempFile.setStatus("NEW");
		tempFile.setUrl(oldFile.getAbsolutePath().substring(h).replaceAll("\\\\", "/"));
		fileService.insert(tempFile, codeReviewDto.getCodeReview().getId());
		
		return true;
	}

	private String[] getAllNewPathes() {
		String[] pathes = new String[this.newFiles.length];
		for (int i = 0; i < this.newFiles.length; i++) {
			pathes[i] = getPathAfterProjectFolder(this.newFiles[i]);
		}
		return pathes;
	}

	private String getPathAfterProjectFolder(String path) {
		String[] parts = path.split("/");
		String pathAfterProjectFolder = "";
		for (int i = 2; i < parts.length; i++) {
			if (i != parts.length - 1)
				pathAfterProjectFolder += parts[i] + "/";
			else
				pathAfterProjectFolder += parts[i];
		}
		return pathAfterProjectFolder;
	}
}
