package com.epam.codereview.attachment;

import com.epam.codereview.dto.CodeReviewDto;
import com.epam.codereview.dto.FileDto;

public abstract class FileAttachment {
	protected String[] newFiles;
	protected CodeReviewDto codeReviewDto;
	protected String reviewsServerPath;
	
	public FileAttachment(String[] newFiles, CodeReviewDto codeReviewDto, String reviewsServerPath){
		this.newFiles = changeAllSlashes(newFiles);
		this.codeReviewDto = codeReviewDto;
		this.reviewsServerPath = reviewsServerPath;
	}
	
	private String[] changeAllSlashes(String[] newFiles){
		for(int i = 0; i < newFiles.length; i++){
			newFiles[i] = newFiles[i].replaceAll("\\\\", "/");
		}
		return newFiles;
	}

	public abstract int addNewFiles();
	
	public com.epam.codereview.model.File getFile(String path){
		for(FileDto fileDto : codeReviewDto.getFileDtos()){
			if(fileDto.getFullPath().contains(path)) return fileDto.getFile();
		}
		return null;
	}
}
