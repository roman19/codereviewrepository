package com.epam.codereview.websocket;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.epam.codereview.model.CodeReviewChat;
import com.epam.codereview.service.ReviewChatService;

@ServerEndpoint(value = "/websocket/reviewchat")
public class ReviewChatServerEndpoint {
	private final static Logger LOG = Logger.getLogger(ReviewChatServerEndpoint.class);

	private ReviewChatService chatService = new ReviewChatService();

	private static Map<Integer, Set<Session>> reviewChats = Collections
			.synchronizedMap(new HashMap<Integer,Set<Session>>());

	@OnOpen
	public void onOpen(Session userSession) {
		LOG.debug("OPEN!");

		Integer reviewId = Integer.parseInt(userSession
				.getRequestParameterMap().get("reviewId").get(0));
		LOG.debug(reviewId);
		
		
		if (reviewChats.get(reviewId) == null) {
			reviewChats.put(reviewId,
					Collections.synchronizedSet(new HashSet<Session>()));
		}
		reviewChats.get(reviewId).add(userSession);
	}

	@OnClose
	public void onClose(Session userSession) {
		LOG.debug("close chat!");

		for (Set<Session> sessions : reviewChats.values()) {
			sessions.remove(userSession);
		}
	}

	@OnMessage
	public void onMessage(String message, Session userSession) {
		

		try {

			JSONObject jsonMessage = null;

			jsonMessage = (JSONObject) new JSONParser().parse(message);

			String reviewId = jsonMessage.get("reviewId").toString();
			String userId = jsonMessage.get("userId").toString();
			String messageText = jsonMessage.get("message").toString();

			System.out.println(reviewId + messageText);

			CodeReviewChat chatMessage = new CodeReviewChat();
			chatMessage.setCodeReviewId(Integer.parseInt(reviewId));
			chatMessage.setUserIdFrom(Integer.parseInt(userId));
			chatMessage.setText(messageText);

			Integer newMessageId = chatService.insert(chatMessage);

			for (Session session : reviewChats.get(Integer.parseInt(reviewId))) {
				session.getAsyncRemote().sendText(newMessageId.toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@OnError
	public void onError(Throwable t) {
		t.printStackTrace();
	}

}