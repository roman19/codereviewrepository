package com.epam.codereview.websocket;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.epam.codereview.dto.FileOnlineNotificationDto;
import com.epam.codereview.dto.IssueDto;
import com.epam.codereview.dto.PlainOnlineNotificationDto;
import com.epam.codereview.dto.ReplyNotificationDto;
import com.epam.codereview.service.NotificationSettingsService;
import com.google.gson.Gson;

@ServerEndpoint(value = "/websocket/notification")
public class NotificationServerEndpoint {

	private final static Logger LOG = Logger
			.getLogger(NotificationServerEndpoint.class);

	NotificationSettingsService settingsService = new NotificationSettingsService();

	private static Map<Integer, Session> users = Collections
			.synchronizedMap(new HashMap<Integer, Session>());

	@OnOpen
	public void onOpen(Session userSession) {
		System.out.println("notification opened");

		Integer userId = Integer.parseInt(userSession.getRequestParameterMap()
				.get("userId").get(0));

		LOG.debug(userId);

		users.put(userId, userSession);

	}

	@OnClose
	public void onClose(Session userSession) {
		System.out.println("close notification");

		Iterator<Entry<Integer, Session>> iterator = users.entrySet()
				.iterator();

		while (iterator.hasNext()) {
			Entry<Integer, Session> entry = iterator.next();
			if (entry.getValue() == userSession) {
				System.out.println("user " + entry.getKey() + " deleted");
				iterator.remove();
			}

		}

	}

	@OnMessage
	public void onMessage(String message, Session userSession) {

		System.out.println("On message: " + message);

		try {
			JSONObject jsonMessage = (JSONObject) new JSONParser()
					.parse(message);

			String type = jsonMessage.get("notificationType").toString();

			if ("GROUP".equals(type) || "REVIEW".equals(type)) {
				PlainOnlineNotificationDto notification = new Gson().fromJson(
						message, PlainOnlineNotificationDto.class);
				sendPlainOnlineNotification(notification);

			} else if ("REVIEW".equals(type)) {
				PlainOnlineNotificationDto notification = new Gson().fromJson(
						message, PlainOnlineNotificationDto.class);
				sendPlainOnlineNotification(notification);
			} else if ("ISSUE".equals(type)) {
				IssueDto notification = new Gson().fromJson(message,
						IssueDto.class);
				sendIssueOnlineNotification(notification);
			} else if ("FILE_ADDED".equals(type)) {
				FileOnlineNotificationDto notification = new Gson().fromJson(
						message, FileOnlineNotificationDto.class);
				sendFileOnlineNotification(notification);
			} else if ("REPLY".equals(type)) {
				ReplyNotificationDto notification = new Gson().fromJson(
						message, ReplyNotificationDto.class);
				sendReplyOnlineNotification(notification);
			}

		} catch (ParseException e) {
			LOG.error("Error occured during parsing input message!", e);
		}

	}

	@OnError
	public void onError(Throwable t) {
		t.printStackTrace();
	}

	private void sendPlainOnlineNotification(
			PlainOnlineNotificationDto notification) {
		Map<String, Object> map = new HashMap<>();
		map.put("type", notification.getNotificationType());
		map.put("itemId", notification.getItemId());
		map.put("itemName", notification.getItemName());
		map.put("ownerName", notification.getOwner().getFirstName() + " "
				+ notification.getOwner().getLastName());
		map.put("ownerImageUrl", notification.getOwner().getImageUrl());
		map.put("imageFromServer",notification.getOwner().isImageFromServer());

		for (Integer userId : notification.getUsers()) {
			if (users.get(userId) != null) {
				map.put("isSound", settingsService.getNotificationSettingsByUserId(userId).isSound());
				String responseJson = new Gson().toJson(map);
				users.get(userId).getAsyncRemote().sendText(responseJson);
			}
		}
	}

	private void sendIssueOnlineNotification(IssueDto notification) {
		Map<String, Object> map = new HashMap<>();
		map.put("type", notification.getNotificationType());
		map.put("level", notification.getType());
		map.put("issueId", notification.getId());
		map.put("title", notification.getTitle());
		map.put("body", notification.getBody());
		map.put("reviewId", notification.getReviewId());
		map.put("reviewName", notification.getReviewName());
		map.put("fileId", notification.getFileId());
		map.put("ownerName", notification.getUserFullName());
		map.put("ownerImageUrl", notification.getImageUrl());
		map.put("imageFromServer", notification.isImageFromServer());
		map.put("isSound", settingsService.getNotificationSettingsByUserId(notification.getUserTo()).isSound());

		String responseJson = new Gson().toJson(map);

		if (users.get(notification.getUserTo()) != null) {
			users.get(notification.getUserTo()).getAsyncRemote()
					.sendText(responseJson);
		}
	}

	private void sendFileOnlineNotification(
			FileOnlineNotificationDto notification) {
		Map<String, Object> map = new HashMap<>();
		map.put("type", notification.getNotificationType());
		map.put("reviewId", notification.getReviewId());
		map.put("reviewName", notification.getReviewName());
		map.put("ownerName", notification.getOwner().getFirstName() + " "
				+ notification.getOwner().getLastName());
		map.put("ownerImageUrl", notification.getOwner().getImageUrl());
		map.put("imageFromServer",notification.getOwner().isImageFromServer());
		map.put("countFiles", notification.getCountFiles());

		for (Integer userId : notification.getUsers()) {
			if (users.get(userId) != null) {
				map.put("isSound", settingsService.getNotificationSettingsByUserId(userId).isSound());
				String responseJson = new Gson().toJson(map);
				users.get(userId).getAsyncRemote().sendText(responseJson);
			}
		}
	}

	private void sendReplyOnlineNotification(ReplyNotificationDto notification) {
		Map<String, Object> map = new HashMap<>();
		map.put("type", notification.getNotificationType());
		map.put("reviewId", notification.getReviewId());
		map.put("reviewName", notification.getReviewName());
		map.put("ownerName", notification.getOwner().getFirstName() + " "
				+ notification.getOwner().getLastName());
		map.put("ownerImageUrl", notification.getOwner().getImageUrl());
		map.put("imageFromServer", notification.getOwner().isImageFromServer());
		map.put("replyId", notification.getReplyId());
		map.put("message", notification.getMessage());
		map.put("fileId", notification.getFileId());
		map.put("issueId", notification.getIssueId());

		for (Integer userId : notification.getUsers()) {
			if (users.get(userId) != null) {
				map.put("isSound", settingsService.getNotificationSettingsByUserId(userId).isSound());
				String responseJson = new Gson().toJson(map);
				users.get(userId).getAsyncRemote().sendText(responseJson);
			}
		}
	}

}
