package com.epam.codereview.settings;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.epam.codereview.attachment.FileUploader;
import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.manager.servlet.View;

public class ImageSaver {

	private final static Logger LOG = Logger.getLogger(FileUploader.class);
	private View view;
	private final String UPLOAD_PATH;

	public ImageSaver(View view) throws SessionIsNotAvailableException {
		this.view = view;
		UPLOAD_PATH = view.getRequest().getServletContext().getInitParameter("file-upload") + "user/";
	}
	
	
	public ImageSaver(View view, String destination) throws SessionIsNotAvailableException {
		this.view = view;
		UPLOAD_PATH = view.getRequest().getServletContext().getInitParameter("file-upload") + destination+"/";
	}
	
	
	
	
	
	public static void main(String[] args) {
		System.out.println("sfjgldjl");
	}
	@SuppressWarnings("rawtypes")
	public String uploadImage() throws Exception {

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		ServletFileUpload upload = new ServletFileUpload(factory);
		File uploadDir = new File(UPLOAD_PATH);
		if (!uploadDir.exists()) {
			uploadDir.mkdir();
		}
		try {
			List formItems = upload.parseRequest(view.getRequest());
			Iterator iter = formItems.iterator();
			FileItem item = (FileItem) iter.next();
			if (!item.isFormField()) {
				String fileName = new File(item.getName()).getName();
				String extension = FilenameUtils.getExtension(fileName);
				if (!extension.equalsIgnoreCase("jpg") && !extension.equalsIgnoreCase("jpeg")
						&& !extension.equalsIgnoreCase("png") && !extension.equalsIgnoreCase("raw")
						 && !extension.equalsIgnoreCase("tiff") && !extension.equalsIgnoreCase("wmf")
						 && !extension.equalsIgnoreCase("jp2") && !extension.equalsIgnoreCase("gif")
						 && !extension.equalsIgnoreCase("psd")) throw new Exception();
				File storeFile;
				while(true){
					storeFile = new File(UPLOAD_PATH + "/" + new Random().nextInt(10000000) + fileName);
					if(!storeFile.exists()) break;
				}
				item.write(storeFile);
				return storeFile.getName();
			}
		} catch (Exception ex) {
			LOG.warn(ex);
			throw ex;
		}
		return null;
	}
}
