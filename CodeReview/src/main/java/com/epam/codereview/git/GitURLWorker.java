package com.epam.codereview.git;

import java.io.IOException;

public class GitURLWorker {
	private String url;
	private String login;
	private String projectName;
	
	public GitURLWorker(String url) throws IOException{
		if(isURLValid(url)){
			this.url = new String(url.substring(url.indexOf("https://"), url.length()));
			try{
				this.login = this.url.substring(8, this.url.indexOf("@"));
				if(this.login.equals("")) throw new IOException();
			}catch(Exception e){
				this.login = null;
			}
			this.projectName = getProjectNameFromUrl();
		}else throw new IOException();
	}
	private String getProjectNameFromUrl(){
		String[] temp = url.split("/");
		return temp[temp.length - 1].substring(0, temp[temp.length - 1].length() - 4);
	}
	
	private boolean isURLValid(String url){
		if(url.contains("https://") && url.endsWith(".git")) return true;
		else return false;
	}

	public String getUrl() {
		return url;
	}

	public String getLogin() {
		return login;
	}

	public String getProjectName() {
		return projectName;
	}
}
