package com.epam.codereview.git;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

public class GitHub {
	private String projectsPath;
	private GitURLWorker gitUrl;
	private Integer userID;

	public GitHub(String url, String projectsPath, Integer userID) throws IOException {
		gitUrl = new GitURLWorker(url);
		this.projectsPath = projectsPath;
		this.userID = userID;
	}
	
	public GitHub(GitURLWorker gitUrl, String projectsPath, Integer userID) throws IOException {
		this.gitUrl = gitUrl;
		this.projectsPath = projectsPath;
		this.userID = userID;
	}

	public String cloneFromPublicRepository(String branch) throws IOException, InvalidRemoteException, TransportException,
			GitAPIException {
		File parentFolder = new File(projectsPath + userID);
		if(!parentFolder.exists()) parentFolder.mkdirs();
		File localPath = File.createTempFile(gitUrl.getProjectName(), "", parentFolder);
		localPath.delete();
		if(branch != null){
			try (Git result = Git.cloneRepository().setURI(gitUrl.getUrl()).setDirectory(localPath).setBranch(branch).call()) {
				return getFolderWithProjectName(localPath.getAbsolutePath());
			}
		}else{
			try (Git result = Git.cloneRepository().setURI(gitUrl.getUrl()).setDirectory(localPath).call()) {
				result.getRepository().close();
				return getFolderWithProjectName(localPath.getAbsolutePath());
			}
		}
	}

	public String cloneFromPrivateRepository(String branch, String password) throws InvalidRemoteException, TransportException,
			GitAPIException {
		CredentialsProvider credentials = new UsernamePasswordCredentialsProvider(gitUrl.getLogin(), password);
		boolean isUnigue = false;
		File dir = null;
		do{
			File parentFolder = new File(projectsPath + userID);
			if(!parentFolder.exists()) parentFolder.mkdirs();
			dir = new File(parentFolder.getAbsolutePath() + "/" + gitUrl.getProjectName() + new Random().nextInt(1000000));
			try {
				CloneCommand cloneCommand;
				if(branch != null){
					cloneCommand = new CloneCommand().setCredentialsProvider(credentials)
							.setDirectory(dir).setURI(gitUrl.getUrl()).setBranch(branch);
				}else{
					cloneCommand = new CloneCommand().setCredentialsProvider(credentials)
							.setDirectory(dir).setURI(gitUrl.getUrl());
				}
				cloneCommand.call();
				isUnigue = true;
			}catch(JGitInternalException e){
				isUnigue = false;
			}
		}while(!isUnigue);
		return getFolderWithProjectName(dir.getAbsolutePath());
	}
	
	public static String getFolderWithProjectName(String absolutePath){
		String[] temp = absolutePath.substring(0, absolutePath.length()).split("\\\\");
		return temp[temp.length - 1];
	}
	
	public static List<String> getBranches(String url, String password) 
			throws IOException, InvalidRemoteException, TransportException, GitAPIException{
		GitURLWorker gitWorker = new GitURLWorker(url);
		CredentialsProvider cp = new UsernamePasswordCredentialsProvider(gitWorker.getLogin(), password);
		Collection<Ref> refs = Git.lsRemoteRepository().setHeads(true).setTags(true).setRemote(gitWorker.getUrl())
				.setCredentialsProvider(cp).call();
		List<String> branches = new ArrayList<>();
		for (Ref ref : refs) {
			branches.add(ref.getName());
		}
		return branches;
	}
}
