package com.epam.codereview.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.CodeReview;
import com.epam.codereview.model.CodeReviewChat;
import com.epam.codereview.service.ReviewChatService;
import com.epam.codereview.service.ReviewService;
import com.epam.codereview.service.UserService;
import com.epam.codereview.util.PathGetter;

@Controller
@RequestMapping(value = "/review")
public class ReviewChatController {
	private ReviewChatService chatService = new ReviewChatService();
	private UserService userService = new UserService();

	@RequestMapping(value = "/chat/*")
	public void goToChat(View view) throws IOException, ServletException,
			SessionIsNotAvailableException {

		ReviewService reviewService = new ReviewService();

		Integer reviewId = Integer.parseInt(PathGetter.getLast(view
				.getRequest().getPathInfo()));

		CodeReview codeReview = reviewService.get(reviewId);

		if (codeReview == null || view.getSessionUserID() == null) {
			view.getResponse().sendError(404);
			return;
		}

		view.setAttribute("review", codeReview);
		view.getRequest().getRequestDispatcher("/pages/review/chat/chat.jsp")
				.forward(view.getRequest(), view.getResponse());
	}

	@RequestMapping(value = "/chat/sendMessage")
	public void sendMessage(View view) throws IOException, ServletException {

		try {

			Integer messageId = Integer
					.parseInt(view.getParameter("messageId"));
			Integer userID = view.getSessionUserID();

			CodeReviewChat message = chatService.get(messageId);
			view.setAttribute("message", message);

			if (userID.equals(message.getUserIdFrom())) {

				view.getRequest()
						.getRequestDispatcher(
								"/pages/review/chat/myMessage.jsp")
						.forward(view.getRequest(), view.getResponse());
			} else {
				view.getRequest()
						.getRequestDispatcher(
								"/pages/review/chat/userMessage.jsp")
						.forward(view.getRequest(), view.getResponse());
			}

		} catch (Exception e) {
			e.printStackTrace();
			
			view.getResponse().sendError(403);
		}

	}

	@RequestMapping(value = "/chat/openReviewChat")
	public void loadReviewChat(View view) throws IOException, ServletException {

		try {

			Integer reviewId = Integer.parseInt(view.getParameter("reviewId"));

			List<CodeReviewChat> messages = chatService
					.getChatByReviewId(reviewId);

			view.setAttribute("messages", messages);
			view.setAttribute("userID", view.getSessionUserID());
			view.setAttribute("userTime",
					userService.get(view.getSessionUserID()).getLastOnline());
			view.setAttribute("currentDate", new Date());
			view.getRequest()
					.getRequestDispatcher("/pages/review/chat/allMessages.jsp")
					.forward(view.getRequest(), view.getResponse());

		} catch (Exception e) {
			view.getResponse().sendError(403);
		}

	}
	
	@RequestMapping(value = "/chat/closeReviewChat")
	public void closeReviewChat(View view) throws IOException, ServletException {
		try {
			Integer userId = view.getSessionUserID();
			userService.setOffline(userId);
		} catch (Exception e) {
			view.getResponse().sendError(403);
		}
	}
}
