package com.epam.codereview.controller;

import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.RequestMethod;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.CodeReview;
import com.epam.codereview.model.Group;
import com.epam.codereview.model.User;
import com.epam.codereview.service.GroupService;
import com.epam.codereview.service.ReviewService;
import com.epam.codereview.service.UserService;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/review/dashboard")
public class ReviewDashboardController {

    private final static Logger LOG = Logger.getLogger(ReviewDashboardController.class);

    private ReviewService reviewService = new ReviewService();
    private UserService userService = new UserService();
    private GroupService groupService = new GroupService();

    @RequestMapping(value = "", method = RequestMethod.POST)
    public void showDashboard(View view) throws ServletException, IOException {
        try {
            Integer reviewId = Integer.valueOf(view.getParameter("reviewId"));
            Integer currentUserId = view.getSessionUserID();
            if (reviewService.hasRightsForReview(reviewId, currentUserId)) {
                sendOnDashboardPage(view, reviewId, currentUserId);
            } else {
                view.getResponse().sendError(403);
            }
        } catch (NullPointerException | NumberFormatException | SessionIsNotAvailableException e) {
            LOG.error(e);
            view.getResponse().sendError(404);
        }
    }

    @RequestMapping(value = "/remove_user", method = RequestMethod.POST)
    public void removeUserFromReview(View view) throws ServletException, IOException {
        try {
            Integer reviewId = Integer.valueOf(view.getParameter("reviewId"));
            Integer userIdForRemove = Integer.valueOf(view.getParameter("userId"));
            Integer currentUserId = view.getSessionUserID();
            if (reviewService.isReviewOwner(reviewId, currentUserId)) {
                reviewService.removeUserFormReview(reviewId, userIdForRemove);
                sendOnDashboardPage(view, reviewId, currentUserId);
            } else {
                view.getResponse().sendError(403);
            }
        } catch (NullPointerException | NumberFormatException | SessionIsNotAvailableException e) {
            LOG.error(e);
            view.getResponse().sendError(404);
        }
    }

    @RequestMapping(value = "/send_is_notified", method = RequestMethod.POST)
    public void sendIsNotifiedByOwner(View view) throws ServletException, IOException {
        try {
            Integer reviewId = Integer.valueOf(view.getParameter("reviewId"));
            Integer userId = Integer.valueOf(view.getParameter("userId"));
            Integer currentUserId = view.getSessionUserID();
            if (reviewService.isReviewOwner(reviewId, currentUserId)) {
                reviewService.setOwnerIsNotified(reviewId, userId);
                view.setAttribute("reviewId", reviewId);
                CodeReview codeReview = reviewService.get(reviewId);
                view.setAttribute("isActiveReview", codeReview.isActive());
                view.setAttribute("isOwnerReview", codeReview.getOwnerId().equals(currentUserId));
                view.setAttribute("reviewUsers", reviewService.getCodeReviewUsers(reviewId));
                view.getRequest().getRequestDispatcher("/pages/review/dashboard/user_table.jsp")
                        .forward(view.getRequest(), view.getResponse());
            } else {
                view.getResponse().sendError(403);
            }
        } catch (NullPointerException | NumberFormatException | SessionIsNotAvailableException e) {
            LOG.error(e);
            view.getResponse().sendError(404);
        }
    }



    @RequestMapping(value = "/load_users", method = RequestMethod.POST)
    public void addUserToReview(View view) throws ServletException, IOException {
        try {
            Integer reviewId = Integer.valueOf(view.getParameter("reviewId"));
            Integer currentUserId = view.getSessionUserID();
            if (reviewService.isReviewOwner(reviewId, currentUserId)) {
                Integer reviewGroupId = reviewService.get(reviewId).getGroupId();
                Group group = groupService.getById(reviewGroupId);
                List<User> usersFromReviewGroupForAdd;
                if (group.isActive()) {
                    usersFromReviewGroupForAdd = userService.getByGroup(reviewGroupId);
                    usersFromReviewGroupForAdd.removeIf(o -> o.getId().equals(currentUserId));
                    List<User> activeReviewUsers = userService.getAllActiveReviewUsers(reviewId) ;
                    usersFromReviewGroupForAdd.removeAll(activeReviewUsers);
                    view.setAttribute("usersFromReviewGroup", usersFromReviewGroupForAdd);
                    view.getRequest().getRequestDispatcher("/pages/review/dashboard/users_selector.jsp")
                            .forward(view.getRequest(), view.getResponse());
                }
            } else {
                view.getResponse().sendError(403);
            }
        } catch (NullPointerException | NumberFormatException | SessionIsNotAvailableException e) {
            LOG.error(e);
            view.getResponse().sendError(404);
        }
    }

    @RequestMapping(value = "/add_users", method = RequestMethod.POST)
    public void addUsersToReview(View view) throws ServletException, IOException, SQLException, PropertyVetoException, InstantiationException, IllegalAccessException {
        try {
            Integer reviewId = Integer.valueOf(view.getParameter("reviewId"));
            int[] usersIdForAdd = new Gson().fromJson(String.valueOf(view.getParameter("users")), int[].class);
            Integer currentUserId = view.getSessionUserID();
            if (reviewService.isReviewOwner(reviewId, currentUserId)) {
                Integer reviewGroupId = reviewService.get(reviewId).getGroupId();
                Group group = groupService.getById(reviewGroupId);
                List<User> usersFromReviewGroupForAdd;
                if (group.isActive()) {
                    usersFromReviewGroupForAdd = userService.getByGroup(reviewGroupId);
                    usersFromReviewGroupForAdd.removeIf(o -> o.getId().equals(currentUserId));
                    List<User> activeReviewUsers = userService.getAllActiveReviewUsers(reviewId) ;
                    usersFromReviewGroupForAdd.removeAll(activeReviewUsers);
                    reviewService.addUsersToCodeReview(reviewId, usersIdForAdd);  //TODO before add check that users are still in list
                }
                view.setAttribute("reviewId", reviewId);
                CodeReview codeReview = reviewService.get(reviewId);
                view.setAttribute("isActiveReview", codeReview.isActive());
                view.setAttribute("isOwnerReview", codeReview.getOwnerId().equals(currentUserId));
                view.setAttribute("reviewUsers", reviewService.getCodeReviewUsers(reviewId));
                view.getRequest().getRequestDispatcher("/pages/review/dashboard/user_table.jsp")
                        .forward(view.getRequest(), view.getResponse());
            } else {
                view.getResponse().sendError(403);
            }
        } catch (NullPointerException | NumberFormatException | JsonSyntaxException | SessionIsNotAvailableException e) {
            LOG.error(e);
            view.getResponse().sendError(404);
        }
    }

    private void sendOnDashboardPage(View view, Integer reviewId, Integer currentUserId) throws ServletException, IOException {
        view.setAttribute("reviewId", reviewId);
        CodeReview codeReview = reviewService.get(reviewId);
        view.setAttribute("isActiveReview", codeReview.isActive());
        view.setAttribute("isOwnerReview", codeReview.getOwnerId().equals(currentUserId));
        view.setAttribute("reviewOwner", userService.get(codeReview.getOwnerId()));
        view.setAttribute("totalReviewPercent", reviewService.getReviewPercentTotal(reviewId));
        view.setAttribute("errorIssuesCount", reviewService.getReviewIssuesByType(reviewId, "ERROR").size());
        view.setAttribute("warningIssuesCount", reviewService.getReviewIssuesByType(reviewId, "WARNING").size());
        view.setAttribute("infoIssuesCount", reviewService.getReviewIssuesByType(reviewId, "INFO").size());
        view.setAttribute("totalIssuesCount", reviewService.getTotalReviewIssues(reviewId).size());
        view.setAttribute("totalFilesCount", reviewService.getCountFilesByReview(reviewId));
        Map<String, Integer> languageFileStatistics = reviewService.getLanguageFileReviewStatistics(reviewId);
        if (languageFileStatistics != null) {
            String json = new Gson().toJson(languageFileStatistics);
            view.setAttribute("langData", json);
        }
        view.setAttribute("reviewUsers", reviewService.getCodeReviewUsers(reviewId));
        view.getRequest().getRequestDispatcher("/pages/review/dashboard/dashboard.jsp")
                .forward(view.getRequest(), view.getResponse());
    }
}
