package com.epam.codereview.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.epam.codereview.dto.EditGroupWithUsersDto;
import com.epam.codereview.dto.GroupDto;
import com.epam.codereview.dto.GroupWithUsersDto;
import com.epam.codereview.dto.PlainOnlineNotificationDto;
import com.epam.codereview.dto.ReviewDto;
import com.epam.codereview.dto.SaveEditedGroupWithUsersDto;
import com.epam.codereview.dto.UserGroupDto;
import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.manager.db.Operator;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.RequestMethod;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.CodeReviewUser;
import com.epam.codereview.model.DeletedReview;

import com.epam.codereview.model.Group;
import com.epam.codereview.model.GroupUser;
import com.epam.codereview.model.User;
import com.epam.codereview.service.CodeReviewUserService;
import com.epam.codereview.service.DeletedReviewUserService;
import com.epam.codereview.service.GroupConcreteService;
import com.epam.codereview.service.GroupService;
import com.epam.codereview.service.GroupUserService;
import com.epam.codereview.service.NotificationService;
import com.epam.codereview.service.ReviewService;
import com.epam.codereview.service.UserService;
import com.epam.codereview.settings.ImageSaver;
import com.google.gson.Gson;

@Controller
@RequestMapping(value = "/cabinet")
public class CabinetController {
	private final static Logger LOG = Logger.getLogger(MainController.class);

	private GroupService groupService;
	private GroupUserService groupUserService;
	private CodeReviewUserService codereviewService;
	private NotificationService notificationService = new NotificationService();
	private GroupConcreteService groupConcreteService=new GroupConcreteService();
	private UserService userService;
	public static int recordsPerPage = 3;
	public static int groupRecordsPerPage = 3;

	public CabinetController() {
		userService=new UserService();
		groupService = new GroupService();
		groupUserService = new GroupUserService();
		codereviewService = new CodeReviewUserService();
	}

	@RequestMapping(value = "")
	public void start(View view) throws ServletException, IOException {
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		view.getRequest().getSession().setAttribute("login", userService.get(userId).getLogin());
		view.getRequest().getSession().setAttribute("user", userService.get(userId));
		
		int page = 1;
		int recordsPerPage = 3;
		if ( session.getAttribute("recordsOnPage")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPage");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPage") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPage"));
			session.setAttribute("recordsOnPage",recordsPerPage);

		}
		
		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsPerPage", recordsPerPage);
		view.setAttribute("countOfNotifications", notificationService.getCountOfAllByUserId(userId));
		view.getResponse().setCharacterEncoding("UTF-8");

		view.getRequest().getRequestDispatcher("/pages/cabinet/cabinet_review.jsp")
				.forward(view.getRequest(), view.getResponse());

	}
	
	
	
	@RequestMapping(value = "/changeImageGroup", method = RequestMethod.POST)
	public void changeImage(View view)
			throws ServletException, IOException, NumberFormatException, SessionIsNotAvailableException {
		
		try {
			
			HttpSession session = view.getRequest().getSession();
			Integer groupId = (Integer) session.getAttribute("GROUP_IDGroup");

			Group group = groupService.getById(groupId);
			String oldPhotoPath = view.getRequest().getServletContext().getInitParameter("file-upload") + "group/"
					+ group.getImageUrl();
			new File(oldPhotoPath).delete();
			group.setImageUrl(new ImageSaver(view,"group").uploadImage());
			
			groupService.update(group);
			
		} catch (Exception e) {
			e.printStackTrace();
			view.getResponse().setContentType("application/text");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write("error");
			
		}
		
		
		
		
	}
	
	

	@RequestMapping(value = "/hidereview", method = RequestMethod.POST)
	public void hideDeletedReview(View view)
			throws ServletException, IOException, NumberFormatException, SessionIsNotAvailableException {
		DeletedReviewUserService deletedReviewUserService = new DeletedReviewUserService();
		deletedReviewUserService.setDeletedReviewConfirmed(view.getSessionUserID(),
				Integer.valueOf(view.getParameter("deletedReviewId")));
	}

	@RequestMapping(value = "/hide_review_from_reviewer", method = RequestMethod.POST)
	public void hideReviewFromReviewer(View view)
			throws ServletException, IOException, NumberFormatException, SessionIsNotAvailableException {
		CodeReviewUserService codeReviewUserService = new CodeReviewUserService();
		CodeReviewUser codeReviewUser = codeReviewUserService.
				getByReviewIdAndUserId(Integer.valueOf(view.getParameter("codeReviewId")), 
						Integer.valueOf(view.getSessionUserID()));
		String status = view.getParameter("reviewerStatus");
		if(status.equals("DELETED")) codeReviewUser.setStatus("DELETED_CONFIRMED");
		else codeReviewUser.setStatus("OWNER_DELETED_OR_LEFT_GROUP_CONFIRMED");
		codeReviewUserService.update(codeReviewUser);
	}

	@RequestMapping(value = "/set_status_left", method = RequestMethod.POST)
	public void setAwayYourselfFromReview(View view)
			throws ServletException, IOException, NumberFormatException, SessionIsNotAvailableException {
		CodeReviewUserService codeReviewUserService = new CodeReviewUserService();
		CodeReviewUser codeReviewUser = codeReviewUserService.
				getByReviewIdAndUserId(Integer.valueOf(view.getParameter("codeReviewId")), 
						Integer.valueOf(view.getSessionUserID()));
		codeReviewUser.setStatus("LEFT");
		codeReviewUserService.update(codeReviewUser);
	}

	@RequestMapping(value = "/finish_confirm", method = RequestMethod.POST)
	public void confirmReviewFinish(View view)
			throws ServletException, IOException, NumberFormatException, SessionIsNotAvailableException {
		CodeReviewUserService codeReviewUserService = new CodeReviewUserService();
		CodeReviewUser codeReviewUser = codeReviewUserService.
				getByReviewIdAndUserId(Integer.valueOf(view.getParameter("reviewId")), 
						Integer.valueOf(view.getSessionUserID()));
		codeReviewUser.setStatus("FINISHED_CONFIRMED");
		codeReviewUserService.update(codeReviewUser);
	}
	

	@RequestMapping(value = "/review")
	public void review(View view) throws ServletException, IOException {
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		int page = 1;
		int recordsPerPage = 3;
		
		if ( session.getAttribute("recordsOnPage")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPage");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPage") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPage"));
			session.setAttribute("recordsOnPage",recordsPerPage);

		}
		session.setAttribute("recordsPerPage", recordsPerPage);
		int noOfRecords = DeletedReview.getAllReviews(view, page, recordsPerPage, userId, "all");

		System.out.println("noOfRecordsv - " + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);

		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsPerPage", recordsPerPage);
		view.getResponse().setCharacterEncoding("UTF-8");

		view.getRequest().getRequestDispatcher("/pages/cabinet/content_review.jsp")
				.forward(view.getRequest(), view.getResponse());

	}
	
	@RequestMapping(value = "/review/active")
	public void reviewActive(View view) throws ServletException, IOException {

		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		int page = 1;
		int recordsPerPage = 3;
		
		if ( session.getAttribute("recordsOnPage")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPage");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPage") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPage"));
			session.setAttribute("recordsOnPage",recordsPerPage);

		}
		session.setAttribute("recordsPerPage", recordsPerPage);
		int noOfRecords = DeletedReview.getAllReviews(view, page, recordsPerPage, userId, "all");
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);

		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsPerPage", recordsPerPage);
		view.getResponse().setCharacterEncoding("UTF-8");

		view.getRequest().getRequestDispatcher("/pages/cabinet/content_review_all.jsp")
				.forward(view.getRequest(), view.getResponse());

	}

	@RequestMapping(value = "/review/history")
	public void reviewHistory(View view) throws ServletException, IOException {

		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		int page = 1;
		int recordsPerPage = 3;
		
		if ( session.getAttribute("recordsOnPage")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPage");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPage") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPage"));
			session.setAttribute("recordsOnPage",recordsPerPage);

		}
		session.setAttribute("recordsPerPage", recordsPerPage);
		List<ReviewDto> list = codereviewService.getAllHistoryReviwByUserId(userId, (page - 1) * recordsPerPage,
				recordsPerPage);

		int noOfRecords = codereviewService.getNoOfRecords();
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);
		
		for(int i = 0; i < list.size(); i++){
			ReviewDto reviewDto = list.get(i);
			ReviewService reviewService = new ReviewService();
			if(userId.equals(reviewDto.getUserId())) 
				list.get(i).setCoverageAvgPercent(reviewService.getReviewPercentTotal(reviewDto.getCodeReviewId()));
			else 
				list.get(i).setCoverageAvgPercent(reviewService.getReviewPercentForUser(reviewDto.getCodeReviewId(), userId));
		}

		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("listReviews", list);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsPerPage", recordsPerPage);
		view.getResponse().setCharacterEncoding("UTF-8");
		System.out.println("History");
		view.getRequest().getRequestDispatcher("/pages/cabinet/content_review_history.jsp")
				.forward(view.getRequest(), view.getResponse());

	}
	
	
	@RequestMapping(value = "/review/history_all")
	public void reviewHistoryAll(View view) throws ServletException, IOException {

		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		int page = 1;
		int recordsPerPage = 3;
		
		if ( session.getAttribute("recordsOnPage")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPage");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPage") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPage"));
			session.setAttribute("recordsOnPage",recordsPerPage);

		}
		session.setAttribute("recordsPerPage", recordsPerPage);
		List<ReviewDto> list = codereviewService.getAllHistoryReviwByUserId(userId, (page - 1) * recordsPerPage,
				recordsPerPage);

		int noOfRecords = codereviewService.getNoOfRecords();
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);
		
		for(int i = 0; i < list.size(); i++){
			ReviewDto reviewDto = list.get(i);
			ReviewService reviewService = new ReviewService();
			if(userId.equals(reviewDto.getUserId())) 
				list.get(i).setCoverageAvgPercent(reviewService.getReviewPercentTotal(reviewDto.getCodeReviewId()));
			else 
				list.get(i).setCoverageAvgPercent(reviewService.getReviewPercentForUser(reviewDto.getCodeReviewId(), userId));
		}

		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("listReviews", list);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsPerPage", recordsPerPage);
		view.getResponse().setCharacterEncoding("UTF-8");
		System.out.println("History");
		view.getRequest().getRequestDispatcher("/pages/cabinet/content_review_history_all.jsp")
				.forward(view.getRequest(), view.getResponse());

	}
	
	
	

	@RequestMapping(value = "/review/all")
	public void reviewAll(View view) throws ServletException, IOException {

		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		int page = 1;
		int recordsPerPage = 3;
		
		if ( session.getAttribute("recordsOnPage")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPage");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPage") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPage"));
			session.setAttribute("recordsOnPage",recordsPerPage);

		}
		session.setAttribute("recordsPerPage", recordsPerPage);
		int noOfRecords = DeletedReview.getAllReviews(view, page, recordsPerPage, userId, "all");
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);

		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsPerPage", recordsPerPage);
		view.getResponse().setCharacterEncoding("UTF-8");

		view.getRequest().getRequestDispatcher("/pages/cabinet/content_review_all.jsp")
				.forward(view.getRequest(), view.getResponse());

	}

	@RequestMapping(value = "/review/my")
	public void reviewMy(View view) throws ServletException, IOException {
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		int page = 1;
		int recordsPerPage = 3;
		
		if ( session.getAttribute("recordsOnPage")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPage");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPage") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPage"));
			session.setAttribute("recordsOnPage",recordsPerPage);

		}
		session.setAttribute("recordsPerPage", recordsPerPage);

		List<ReviewDto> list = codereviewService.getAllMyActiveReviwByUserId(userId, (page - 1) * recordsPerPage,
				recordsPerPage);
		int noOfRecords = codereviewService.getNoOfRecords();
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);
		
		for(int i = 0; i < list.size(); i++){
			ReviewDto reviewDto = list.get(i);
			ReviewService reviewService = new ReviewService();
			if(userId.equals(reviewDto.getUserId())) 
				list.get(i).setCoverageAvgPercent(reviewService.getReviewPercentTotal(reviewDto.getCodeReviewId()));
			else 
				list.get(i).setCoverageAvgPercent(reviewService.getReviewPercentForUser(reviewDto.getCodeReviewId(), userId));
		}

		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("listReviews", list);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getRequest().setAttribute("recordsPerPage", recordsPerPage);
		view.getRequest().getRequestDispatcher("/pages/cabinet/content_review_my.jsp")
				.forward(view.getRequest(), view.getResponse());

	}

	@RequestMapping(value = "/review/other")
	public void reviewOther(View view) throws ServletException, IOException {
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		int page = 1;
		int recordsPerPage = 3;
		
		if ( session.getAttribute("recordsOnPage")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPage");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPage") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPage"));
			session.setAttribute("recordsOnPage",recordsPerPage);

		}
		session.setAttribute("recordsPerPage", recordsPerPage);
		
		int noOfRecords = DeletedReview.getAllReviews(view, page, recordsPerPage, userId, "other");
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);
		
		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsPerPage", recordsPerPage);
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getRequest().getRequestDispatcher("/pages/cabinet/content_review_other.jsp")
				.forward(view.getRequest(), view.getResponse());

	}

	@RequestMapping(value = "/groups")
	public void groups(View view) throws ServletException, IOException {
		System.out.println("group all");
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		int page = 1;
		int recordsPerPage = 3;
		
		if ( session.getAttribute("recordsOnPage")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPage");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPage") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPage"));
			session.setAttribute("recordsOnPage",recordsPerPage);

		}

		List<GroupDto> list = groupService.getAllGroups(userId, (page - 1) * recordsPerPage, recordsPerPage);

		int noOfRecords = groupService.getNoOfRecords();
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);

		
		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("listGroups", list);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsOnPage", recordsPerPage);
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getRequest().getRequestDispatcher("/pages/cabinet/content_groups.jsp")
				.forward(view.getRequest(), view.getResponse());

	}

	@RequestMapping(value = "/groups/all")
	public void groupsAll(View view) throws ServletException, IOException {
		System.out.println("group all");
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		int page = 1;
		int recordsPerPage = 3;
		
		if ( session.getAttribute("recordsOnPage")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPage");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPage") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPage"));
			session.setAttribute("recordsOnPage",recordsPerPage);

		}

		List<GroupDto> list = groupService.getAllGroups(userId, (page - 1) * recordsPerPage, recordsPerPage);

		int noOfRecords = groupService.getNoOfRecords();
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);

		
		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("listGroups", list);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsOnPage", recordsPerPage);

		view.getResponse().setCharacterEncoding("UTF-8");
		view.getRequest().getRequestDispatcher("/pages/cabinet/content_groups_all.jsp")
				.forward(view.getRequest(), view.getResponse());

	}

	@RequestMapping(value = "/groups/my")
	public void groupsMy(View view) throws ServletException, IOException {

		System.out.println("group my");

		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		int page = 1;
		int recordsPerPage = 3;
		
		if ( session.getAttribute("recordsOnPage")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPage");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPage") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPage"));
			session.setAttribute("recordsOnPage",recordsPerPage);

		}
		System.out.println("UserID " + userId);
		List<GroupDto> list = groupService
				.getAllMyGroups(userId, (page - 1) * recordsPerPage, recordsPerPage);

		int noOfRecords = groupService.getNoOfRecords();
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);

		view.getRequest().setAttribute("listGroups", list);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsOnPage", recordsPerPage);

		view.getResponse().setCharacterEncoding("UTF-8");
		view.getRequest().getRequestDispatcher("/pages/cabinet/content_groups_my.jsp")
				.forward(view.getRequest(), view.getResponse());

	}

	@RequestMapping(value = "/groups/other")
	public void groupsOther(View view) throws ServletException, IOException {
		System.out.println("group other");

		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		int page = 1;
		int recordsPerPage = 3;
		
		if ( session.getAttribute("recordsOnPage")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPage");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPage") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPage"));
			session.setAttribute("recordsOnPage",recordsPerPage);

		}
		List<GroupDto> list = groupService.getAllOtherGroups(userId, (page - 1) * recordsPerPage,
				recordsPerPage);

		int noOfRecords = groupService.getNoOfRecords();
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);

		
		view.getRequest().setAttribute("listGroups", list);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsOnPage", recordsPerPage);
		view.getResponse().setCharacterEncoding("UTF-8");

		view.getRequest().getRequestDispatcher("/pages/cabinet/content_groups_other.jsp")
				.forward(view.getRequest(), view.getResponse());

	}

	
	@RequestMapping(value = "/addgroup")
	public void addGroup(View view) throws ServletException, IOException {

		view.getRequest().getRequestDispatcher("/pages/cabinet/addGroup.jsp")
				.forward(view.getRequest(), view.getResponse());
	}

	@RequestMapping(value = "/addgroup/SearchController")
	public void search(View view) throws ServletException, IOException {
		view.getResponse().setContentType("application/json");
		try {
			HttpSession session = view.getRequest().getSession();
			Integer userId = (Integer) session.getAttribute("userID");

            List<UserGroupDto> list = groupUserService.getAllUsersForGroupAutocompleter(userId);

			String json = new Gson().toJson(list);

			view.getResponse().setContentType("application/json");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write(json);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@RequestMapping(value = "/addgroup/AddToTable")
	public void addToTable(View view) throws ServletException, IOException {
		String userId = view.getRequest().getParameter("id");

		UserGroupDto user = groupUserService.getUserGroupDto(Integer.valueOf(userId));

		System.out.println(user);
		String json = new Gson().toJson(user);
		view.getResponse().setContentType("application/json");
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getResponse().getWriter().write(json);
	}

	@RequestMapping(value = "/addgroup/SaveToDB")
	public void saveToDb(View view) throws ServletException, IOException {
		
		try {

			GroupWithUsersDto allgroup = new Gson().fromJson(view.getRequest()
					.getParameter("group"), GroupWithUsersDto.class);

			System.out.println(allgroup);

			HttpSession session = view.getRequest().getSession();
			Integer userId = (Integer) session.getAttribute("userID");
			String groupName = allgroup.getName();
			boolean isImage = allgroup.isImage();
			List<Integer> list = allgroup.getUsers();

			Integer groupId = groupUserService.createGroup(groupName, isImage, userId,
					list);

			// for online notification
			UserService userService = new UserService();
			User user = userService.get(userId);

			PlainOnlineNotificationDto notificationDto = new PlainOnlineNotificationDto();
			notificationDto.setNotificationType("GROUP");
			notificationDto.setOwner(user);
			notificationDto.setItemId(groupId);
			notificationDto.setItemName(groupName);
			notificationDto.setUsers(list);

			view.getResponse().setContentType("application/json");
			view.getResponse().setCharacterEncoding("UTF-8");

			String json = new Gson().toJson(notificationDto);

			view.getResponse().getWriter().write(json);
		
		} catch (Exception e) {
			LOG.error("Error during saving group", e);
		}
	}
	
	@RequestMapping(value = "/addgroup/saveImage", method=RequestMethod.POST)
	public void saveGroupImage(View view) throws ServletException, IOException {
		
		try {
			GroupService groupService = new GroupService();
			
			String imageUrl = com.epam.codereview.util.ImageSaver.saveWithAjax(view.getRequest(), "group");
			Integer groupId = Integer.parseInt(imageUrl.split("\\.")[0]);
			
			Group group = groupService.getById(groupId);
			group.setImageUrl(imageUrl);
			groupService.update(group);
		} catch (Exception e) {
			LOG.error("Error during saving group image", e);
		}
	}

	@RequestMapping(value = "/groups/editgroup")
	public void editGroup(View view) throws ServletException, IOException {

		String groupId = view.getRequest().getParameter("groupId");

		EditGroupWithUsersDto dto = groupService.getGroupWithUsersByGroupId(Integer.valueOf(groupId));
		String name = dto.getName();
		List<UserGroupDto> list = dto.getUsers();
		List<Integer> userIds = new ArrayList<>();

		for (UserGroupDto i : list) {
			userIds.add(i.getId());
		}

		int[] ids = new int[userIds.size()];
		for (int i = 0; i < userIds.size(); i++) {
			ids[i] = userIds.get(i);
			System.out.println(ids[i]);
		}
		view.getRequest().setAttribute("arrayIds", ids);
		view.getRequest().setAttribute("groupID", groupId);

		view.getResponse().setCharacterEncoding("UTF-8");
		view.getRequest().setAttribute("groupname", name);
		view.getRequest().setAttribute("groupMembers", list);
		view.getRequest().getRequestDispatcher("/pages/cabinet/editGroup.jsp")
				.forward(view.getRequest(), view.getResponse());
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/addgroup/SaveToDBedit")
	public void saveToDBedit(View view) throws ServletException, IOException {

		SaveEditedGroupWithUsersDto allgroup = new Gson().fromJson(view.getRequest().getParameter("group"),
				SaveEditedGroupWithUsersDto.class);

		String groupName = allgroup.getName();
		Integer groupId = allgroup.getGroupId();

		
		
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		User user = userService.get(userId);
		
		
		List<Integer> listOfUsersNew = allgroup.getUsers();
		System.out.println("New users " + listOfUsersNew);

		List<Integer> listOfUsersOLD = groupService.getAllUsersFromGroup(groupId);
		System.out.println("Old users " + listOfUsersOLD);

		List<Integer> usersToRemove = groupService.getOldUsers(listOfUsersOLD, listOfUsersNew);
		System.out.println("Users to remove" + usersToRemove);

		List<Integer> usersToSendNotifications = groupService.getNewUsers(listOfUsersOLD, listOfUsersNew);
		System.out.println("Users to send notifications" + usersToSendNotifications);

		groupService.editGroupName(groupId, groupName);

		
		
		// add all new users to current group
		for (int i = 0; i < usersToSendNotifications.size(); i++) {
			
			List<GroupUser> guList=groupUserService.getUserWithLeftStatus(groupId,usersToSendNotifications.get(i));
			
			GroupUser gu=null;
				if(guList.size()>0){	
					gu=guList.get(0);
				}
			
			if(gu !=null){
				if(gu.getStatus().equals("LEFT")){
						gu.setStatus("PENDING");					
					groupUserService.update(gu);
				}else{
					groupUserService.insertUserGroupReview(groupId, usersToSendNotifications.get(i));
				}
			}else{
				groupUserService.insertUserGroupReview(groupId, usersToSendNotifications.get(i));
			}
			
		}

		
		
		PlainOnlineNotificationDto notificationDto = new PlainOnlineNotificationDto();
		notificationDto.setNotificationType("GROUP");
		notificationDto.setOwner(user);
		notificationDto.setItemId(groupId);
		notificationDto.setItemName(groupName);
		notificationDto.setUsers(usersToSendNotifications);

		view.getResponse().setContentType("application/json");
		view.getResponse().setCharacterEncoding("UTF-8");

		String json = new Gson().toJson(notificationDto);
		view.getResponse().getWriter().write(json);
		
		
		

	}

	
	
	
	@RequestMapping(value = "/searchReviews")
	public void searchReviews(View view) throws ServletException, IOException {
		
		view.getResponse().setContentType("application/json");
		
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		
		List<ReviewDto> list=groupConcreteService.getAllReviwByUserIdTH(1, userId);
		
		String json = new Gson().toJson(list);

		view.getResponse().setContentType("application/json");
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getResponse().getWriter().write(json);
	
		
	
	
	
	}
	
	
	
	
	
	
}
