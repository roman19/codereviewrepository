package com.epam.codereview.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.codereview.dto.UserDto;
import com.epam.codereview.exception.ValidationException;
import com.epam.codereview.exception.WrongInputDataException;
import com.epam.codereview.model.User;
import com.epam.codereview.service.UserService;
import com.epam.codereview.util.Hasher;
import com.epam.codereview.util.ImageSaver;
import com.epam.codereview.util.ObjectFiller;
import com.epam.codereview.validator.FormValidator;

/**
 * Servlet implementation class SignUpController
 */
@WebServlet("/signup")
@MultipartConfig(maxFileSize = 1049000)
public class SignUpController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(SignUpController.class);

	public SignUpController() {
		super();
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			UserDto form = new UserDto();
			ObjectFiller.fill(form, request);

			FormValidator.validate(form);

			if (!form.getPassword().equals(form.getCpassword()))
				throw new ValidationException(
						"Password and confirm password don't match");

			User user = new User(form);
			user.setPassword(Hasher.hash(user.getPassword()));
			user.setConfirmCode(Hasher.hash(user.getLogin()));
			user.setLocale((String) request.getSession().getAttribute("locale"));
			user.setImageFromServer(true);
			user.setLastOnline(new Timestamp(System.currentTimeMillis()));
			user.setAuthDate(new Timestamp( System.currentTimeMillis()));
			
			UserService userService = new UserService();
			if ("application/octet-stream".equals(request.getPart("image")
					.getContentType())) {
				
				/*String sex = userService.getGenderByFirstName(user.getFirstName());
				if("male".equals(sex)){
					user.setImageUrl("male.png");
				}else if("female".equals(sex)){
					user.setImageUrl("female.png");
				}else{
					user.setImageUrl("profile.png");
				}*/
				
				user.setImageUrl("profile.png");
				userService.insert(user);
			} else {
				Integer newId = userService.insert(user);
				user.setImageUrl(ImageSaver.save(request, "user", newId));
				user.setId(newId);
				userService.update(user);
			}
			
			request.getSession().setAttribute("registered", true);
			response.sendRedirect(
					request.getContextPath() + "/");

		} catch (NoSuchMethodException | SecurityException
				| IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | ValidationException
				| NoSuchAlgorithmException | ServletException
				| WrongInputDataException e) {
			LOG.error("REGISTRATION ERROR: ", e);
			response.sendRedirect(request.getHeader("referer"));
		}

	}

}
