package com.epam.codereview.controller;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;

import javax.servlet.ServletException;

import com.epam.codereview.dto.UserPasswordDto;
import com.epam.codereview.dto.UserPersonalDataDto;
import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.RequestMethod;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.NotificationSetting;
import com.epam.codereview.model.User;
import com.epam.codereview.project.ProjectHandler;
import com.epam.codereview.service.NotificationSettingsService;
import com.epam.codereview.service.ProjectService;
import com.epam.codereview.service.UserService;
import com.epam.codereview.settings.ImageSaver;
import com.epam.codereview.util.AsyncSender;
import com.epam.codereview.util.Hasher;
import com.epam.codereview.util.ObjectFiller;
import com.epam.codereview.validator.FormValidator;
import com.google.gson.Gson;

@Controller
@RequestMapping(value = "/cabinet")
public class SettingsController {

	@RequestMapping(value = "/settings")
	public void getNotifications(View view) throws ServletException, IOException, SQLException {
		try {
			Integer userId = view.getSessionUserID();
			UserService userService = new UserService();
			User user = userService.getByID(userId);
			NotificationSettingsService settings = new NotificationSettingsService();
			NotificationSetting setting = settings.getNotificationSettingsByUserId(userId);
			view.setAttribute("review", setting.isReview());
			view.setAttribute("group", setting.isGroup());
			view.setAttribute("fileAdded", setting.isFileAdded());
			view.setAttribute("issue", setting.isIssue());
			view.setAttribute("reply", setting.isReply());
			view.setAttribute("isSound", setting.isSound());
			view.setAttribute("user", user);
			Long usedMemory = new ProjectService().getUsedMemoryByUser(user.getId());
			Long enabledMemory = user.getEnabledMemory();
			String memory = ProjectHandler.convertBytesInMegaBytes(usedMemory) + " / " +
					ProjectHandler.convertBytesInMegaBytes(enabledMemory) + " MB";
			view.setAttribute("memory", memory);
			view.setAttribute("memoryPercentage", ((double) usedMemory / (double) enabledMemory) * 100);
			view.getRequest().getRequestDispatcher("/pages/cabinet/content_settings.jsp")
					.forward(view.getRequest(), view.getResponse());
		} catch (SessionIsNotAvailableException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/settings/personal", method = RequestMethod.POST)
	public void editPersonalData(View view) throws ServletException, IOException {
		try {
			UserPersonalDataDto userPersonalData = new UserPersonalDataDto();
			ObjectFiller.fill(userPersonalData, view.getRequest());
			Integer userId = view.getSessionUserID();
			UserService userService = new UserService();
			User user = userService.getByID(userId);
			if (userPersonalData.getName() != null && !userPersonalData.getName().equals(""))
				user.setFirstName(userPersonalData.getName());
			if (userPersonalData.getSurname() != null && !userPersonalData.getSurname().equals(""))
				user.setLastName(userPersonalData.getSurname());
			if (userPersonalData.getLogin() != null && !userPersonalData.getLogin().equals(""))
				user.setLogin(userPersonalData.getLogin());
			userService.update(user);
			String json = new Gson().toJson(userPersonalData);
			view.getResponse().setContentType("application/json");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/settings/confirm", method = RequestMethod.POST)
	public void confirmMemoryPurchase(View view) throws ServletException, IOException {
		try {
			Integer temp;
			do{
				temp = new Random().nextInt(10000000);
				if(temp >= 1000000) break;
			}while(true);
			Integer userId = view.getSessionUserID();
			Integer code = Integer.valueOf(userId.toString() + temp.toString());
			UserService userService = new UserService();
			User user = userService.getByID(userId);
			user.setConfirmMemoryPurchase(code.toString());
			userService.update(user);
            new Thread(new AsyncSender("Memory Purchase Confirming", "To confirm memory purchase enter this code - " + code, user.getEmail())).start();
			view.getResponse().setContentType("application/text");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write("succes");
		} catch (Exception e) {
			view.getResponse().setContentType("application/text");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write("error");
		}
	}

	@RequestMapping(value = "/settings/confirm_memory_purchase", method = RequestMethod.POST)
	public void saveMemoryPurchase(View view) throws ServletException, IOException {
		try {
			Integer userId = view.getSessionUserID();
			UserService userService = new UserService();
			User user = userService.getByID(userId);
			long userCode = Long.valueOf(view.getParameter("code"));
			long dbCode = Long.valueOf(user.getConfirmMemoryPurchase());
			if(userCode != dbCode) throw new Exception();
			user.setEnabledMemory(Long.valueOf(view.getParameter("size")) + (((user.getEnabledMemory() / 1024) / 1024)));
			userService.update(user);
			view.getResponse().setContentType("application/text");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write("succes");
		} catch (Exception e) {
			view.getResponse().setContentType("application/text");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write("error");
		}
	}

	@RequestMapping(value = "/settings/photo", method = RequestMethod.POST)
	public void savePhoto(View view) throws ServletException, IOException {
		try {
			Integer userId = view.getSessionUserID();
			UserService userService = new UserService();
			User user = userService.getByID(userId);
			String oldPhotoPath = view.getRequest().getServletContext().getInitParameter("file-upload") + "user/"
					+ user.getImageUrl();
			if(!user.getImageUrl().equals("male.png") && !user.getImageUrl().equals("female.png") &&
					!user.getImageUrl().equals("profile.png"))new File(oldPhotoPath).delete();
			user.setImageUrl(new ImageSaver(view).uploadImage());
			user.setImageFromServer(true);
			userService.update(user);
		} catch (Exception e) {
			view.getResponse().setContentType("application/text");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write("error");
		}
	}

	@RequestMapping(value = "/settings/password", method = RequestMethod.POST)
	public void editPassword(View view) throws ServletException, IOException {
		try {
			UserPasswordDto userPasswordDto = new UserPasswordDto();
			ObjectFiller.fill(userPasswordDto, view.getRequest());
			Integer userId = view.getSessionUserID();
			UserService userService = new UserService();
			User user = userService.getByID(userId);

			if (userPasswordDto.getCurrentPassword() != null && !userPasswordDto.getCurrentPassword().equals("")) {
				if (!Hasher.hash(userPasswordDto.getCurrentPassword()).equals(user.getPassword())) {
					userPasswordDto.setCurrentPassword(null);
					userPasswordDto.setNewPassword("");
					userPasswordDto.setRepeatPassword("");
				}else{
					try {
						if (userPasswordDto.getNewPassword() != null && !userPasswordDto.getNewPassword().equals("")) {
							FormValidator.validate(userPasswordDto);
						}
					} catch (Exception e) {
						userPasswordDto.setNewPassword(null);
						userPasswordDto.setRepeatPassword("");
					}
					if (userPasswordDto.getNewPassword() != null && !userPasswordDto.getNewPassword().equals("")
							&& userPasswordDto.getRepeatPassword() != null && !userPasswordDto.getRepeatPassword().equals("")) {
						if (userPasswordDto.getNewPassword().equals(userPasswordDto.getRepeatPassword())) {
							user.setPassword(Hasher.hash(userPasswordDto.getNewPassword()));
						} else {
							userPasswordDto.setRepeatPassword(null);
						}
					}
				}
			}
			if(userPasswordDto.getCurrentPassword() != null && userPasswordDto.getNewPassword() != null
					&& userPasswordDto.getRepeatPassword() != null){
				if(!userPasswordDto.getCurrentPassword().equals("") && !userPasswordDto.getNewPassword().equals("")
						&& !userPasswordDto.getRepeatPassword().equals("")){
					userPasswordDto.setCurrentPassword("succes");
					userPasswordDto.setNewPassword("succes");
					userPasswordDto.setRepeatPassword("succes");
				}
			}
			userService.update(user);
			String json = new Gson().toJson(userPasswordDto);
			view.getResponse().setContentType("application/json");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/settings/notifications/item", method = RequestMethod.POST)
	public void editNotificationItem(View view) throws ServletException, IOException {
		try {
			int item = Integer.valueOf(view.getParameter("item"));
			boolean value = Boolean.valueOf(view.getParameter("value"));
			Integer userId = view.getSessionUserID();
			NotificationSettingsService notificationSettingService = new NotificationSettingsService();
			NotificationSetting notificationSettings = notificationSettingService
					.getNotificationSettingsByUserId(userId);
			notificationSettings.setItemValue(item, value);
			notificationSettingService.update(notificationSettings);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
