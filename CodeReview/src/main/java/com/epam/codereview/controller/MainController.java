package com.epam.codereview.controller;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import com.epam.codereview.dto.LoginDto;
import com.epam.codereview.dto.NewPasswordDto;
import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.exception.ValidationException;
import com.epam.codereview.exception.WrongInputDataException;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.RequestMethod;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.ForgottenUser;
import com.epam.codereview.model.User;
import com.epam.codereview.service.UserService;
import com.epam.codereview.util.Hasher;
import com.epam.codereview.util.ImageUploader;
import com.epam.codereview.util.ObjectFiller;
import com.epam.codereview.validator.FormValidator;

@Controller
public class MainController {
    private final static Logger LOG = Logger.getLogger(MainController.class);

    @RequestMapping(value = "/")
    public void index(View view) throws ServletException, IOException {

        Boolean isAfterRefistration = (Boolean) view.getRequest().getSession()
                .getAttribute("registered");
        if ((isAfterRefistration != null && isAfterRefistration)) {
            view.setAttribute("isReg", true);
            view.getRequest().getSession().removeAttribute("registered");
        }

        view.getRequest().getRequestDispatcher("/pages/index/index.jsp")
                .forward(view.getRequest(), view.getResponse());
    }

    @RequestMapping(value = "/changeLanguageToUA")
    public void changeLanguageToUA(View view) throws IOException {
    	String tab = view.getParameter("tab");
        view.getRequest().getSession().setAttribute("locale", "uk_UA");
        view.getResponse().sendRedirect(view.getRequest().getHeader("referer")+tab);
    }

    @RequestMapping(value = "/changeLanguageToUS")
    public void changeLanguageToUS(View view) throws IOException {
    	String tab = view.getParameter("tab");
        view.getRequest().getSession().setAttribute("locale", "en_US");
        view.getResponse().sendRedirect(view.getRequest().getHeader("referer")+tab);
    }

    @RequestMapping(value = "/signin", method = RequestMethod.POST)
    public void signin(View view) throws IOException {

        try {

            LoginDto loginForm = new LoginDto();
            ObjectFiller.fill(loginForm, view.getRequest());
            FormValidator.validate(loginForm);

            UserService userService = new UserService();
            User user = userService.getByLogin((loginForm.getLogin()));

            if (user == null)
                throw new WrongInputDataException();

            view.getRequest().getSession().setAttribute("userID", user.getId());

            view.getResponse().sendRedirect(
                    view.getRequest().getContextPath() + "/cabinet");

        } catch (Exception e) {
            e.printStackTrace();
            view.getResponse().sendError(404);
        }

    }

    @RequestMapping(value = "/logout")
    public void logout(View view) throws IOException {

        try {
            UserService userService = new UserService();
            User user = userService.get(view.getSessionUserID());

            if (view.getLocale() != null)
                user.setLocale(view.getLocale());

            userService.update(user);

            view.getRequest().getSession().invalidate();

            view.getResponse().sendRedirect(
                    view.getRequest().getContextPath() + "/");

        } catch (SessionIsNotAvailableException e) {
            LOG.error("Session is not available before logout", e);

            view.getRequest().getSession().invalidate();
            view.getResponse().sendRedirect(
                    view.getRequest().getContextPath() + "/");
        }catch(Exception e){
            LOG.error("Some errors occured during logout", e);

            view.getRequest().getSession().invalidate();
            view.getResponse().sendRedirect(
                    view.getRequest().getContextPath() + "/");
        }

    }

    @RequestMapping(value = "/checkSigninForm", method = RequestMethod.POST)
    public void checkLoginAndPassword(View view) throws IOException {

        view.getResponse().setContentType("text/plain");
        view.getResponse().setCharacterEncoding("UTF-8");

        try {
            LoginDto loginForm = new LoginDto();
            ObjectFiller.fill(loginForm, view.getRequest());
            FormValidator.validate(loginForm);

            UserService userService = new UserService();
            User user = userService.getByLogin(loginForm.getLogin());

            String message = null;

            if (user == null
                    || !user.getPassword().equals(
                    Hasher.hash(loginForm.getPassword()))) {
                message = "Invalid login or password";
            } else if ("PENDING".equals(user.getStatus())) {
                message = "Your application is pending";
            } else if ("FORGOTTEN".equals(user.getStatus())) {
                userService.rememberedPassword(user.getEmail());
                message = "good";
            } else {
                message = "good";
            }

            view.getResponse().getWriter().write(message);

        } catch (Exception e) {
            view.getResponse().getWriter().write("Invalid login or password");
        }

    }

    @RequestMapping(value = "/checklogin")
    public void checkLogin(View view) throws IOException {
        UserService userService = new UserService();

        Criteria criteria = new Criteria();
        criteria.add(new Condition("login", "=", view.getParameter("login")));

        Boolean isExist = userService.get(criteria).size() == 1;

        LOG.debug("is exist: " + isExist);

        view.getResponse().setContentType("text/plain");
        view.getResponse().setCharacterEncoding("UTF-8");
        view.getResponse().getWriter().write(isExist.toString());
    }

    @RequestMapping(value = "/checkemail")
    public void checkEmail(View view) throws IOException {

        try {

            UserService userService = new UserService();

            User user = userService.getByEmail(view.getParameter("email"));

            view.getResponse().setContentType("text/plain");
            view.getResponse().setCharacterEncoding("UTF-8");
            view.getResponse().getWriter().write(String.valueOf(user != null));
        } catch (Exception e) {
            view.getResponse().sendError(500);
        }
    }

    @RequestMapping(value = "/checkForForgot")
    public void checkForForgotPassword(View view) throws IOException {
        UserService userService = new UserService();

        try {

            User user = userService.getByEmail(view.getParameter("email"));

            boolean exist = user != null;

            Integer expireTime = Integer.parseInt(view.getRequest()
                    .getServletContext()
                    .getInitParameter("expire-time-password"));

            boolean isForgotten = userService.isForgotten(
                    view.getParameter("email"), expireTime);

            String message;
            if (isForgotten) {
                message = "You already sent request";
            } else if (!exist) {
                message = "Such email doesn't exist";
            } else {
                message = "good";
            }

            view.getResponse().setContentType("text/plain");
            view.getResponse().setCharacterEncoding("UTF-8");
            view.getResponse().getWriter().write(message);
        } catch (Exception e) {
            LOG.error(
                    "Error uccured when we checked for exist email and forgotten user",
                    e);

            view.getResponse().sendError(500);
        }
    }

    @RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
    public void forgotPassword(View view) throws IOException {
        try {

            String email = view.getParameter("email");

            if (email == null)
                throw new WrongInputDataException();

            UserService userService = new UserService();

            User user = userService.getByEmail(email);

            userService.forgotPassword(user);

            view.getResponse().sendRedirect(
                    view.getRequest().getHeader("referer"));

        } catch (Exception e) {
            view.getResponse().sendError(404);

        }

    }

    @RequestMapping(value = "/goToPasswordResettig")
    public void forgotAcceptTeacher(View view) throws ServletException,
            IOException {

        try {

            String code = view.getParameter("token");

            UserService userService = new UserService();
            ForgottenUser fuser = userService.getForgottenUserByCode(code);

            if (fuser == null)
                throw new WrongInputDataException(
                        "Such user doesn't exist with such code");

            Integer expireTime = Integer.parseInt(view.getRequest()
                    .getServletContext()
                    .getInitParameter("expire-time-password"));
            if (System.currentTimeMillis() - fuser.getForgetDate().getTime() > expireTime * 3600000) {
                throw new WrongInputDataException(
                        "Time for resetting password has expired");
            }

            userService.clickedOnNewPasswordLink(code);

            view.setAttribute("email", fuser.getEmail());

            view.getRequest()
                    .getRequestDispatcher("/pages/index/newPassword.jsp")
                    .forward(view.getRequest(), view.getResponse());

        } catch (Exception e) {
            LOG.error(e);

            view.getResponse().sendError(404);
        }

    }

    @RequestMapping(value = "/newPassword", method = RequestMethod.POST)
    public void newPassword(View view) throws ServletException, IOException {

        try {
            NewPasswordDto form = new NewPasswordDto();
            ObjectFiller.fill(form, view.getRequest());
            FormValidator.validate(form);

            String email = form.getEmail();
            String password = form.getPassword1();
            String cpassword = form.getPassword2();

            if (!password.equals(cpassword)) {
                throw new ValidationException("passwords doesn't match");
            }

            UserService userService = new UserService();
            Integer userID = userService.setNewPassword(email,
                    Hasher.hash(password));

            view.getRequest().getSession().setAttribute("userID", userID);
            view.getResponse().sendRedirect(
                    view.getRequest().getContextPath() + "/cabinet");

        } catch (Exception e) {
            LOG.error("Resetting password", e);
            view.getResponse().sendError(404);
        }
    }

    @RequestMapping(value = "/signup_confirmation")
    public void signUpConfirmation(View view) throws IOException {
        try {

            Integer expireTime = Integer.parseInt(view.getRequest()
                    .getServletContext()
                    .getInitParameter("expire-time-confirmation"));

            String code = view.getParameter("token");

            UserService userService = new UserService();
            User user = userService.getByConfirmCode(code);
            if (user != null
                    && user.getStatus().equals("PENDING")
                    && System.currentTimeMillis()
                    - user.getAuthDate().getTime() <= expireTime * 3600000) {
                user.setStatus("CONFIRMED");
                userService.update(user);
                view.getRequest().getSession(true)
                        .setAttribute("userID", user.getId());
                view.getRequest().getSession()
                        .setAttribute("message", "Registration completed");

                view.getResponse().sendRedirect(
                        view.getRequest().getContextPath() + "/cabinet");

            } else {
                view.getResponse().sendError(404);
            }
        } catch (Exception e) {
            e.printStackTrace();
            view.getResponse().sendError(404);
        }
    }

    @RequestMapping(value = "/get_user_image/*")
    public void getUserImage(View view) throws IOException, ServletException {

        System.out.println("user image");

        ImageUploader.upload(view, "user");
    }

}
