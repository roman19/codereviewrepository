package com.epam.codereview.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import com.epam.codereview.dao.IssueLikeDao;
import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.dto.LikeDto;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.View;
import com.google.gson.Gson;

@Controller
public class IssueLikeController {

	private DaoManager daoManager = new DaoManager(); //TODO remove, use only services

	@RequestMapping("/decrementLikeUpIssue")
	public void decrementLikeUp(View view) throws IOException{		
		
		int issueId=Integer.valueOf(view.getRequest().getParameter("IssueId"));
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		
		daoManager.executeVoidAndClose(() -> daoManager.getIssueLikeDao().decrementLikeOnIssue(userId, issueId));

		Integer totalLikes = daoManager.executeAndClose(() ->
                daoManager.getIssueLikeDao().countTotalLike(issueId));

		view.getResponse().setContentType("application/text");
		view.getResponse().getWriter().write(String.valueOf(totalLikes.toString()));
	}
	
	
	@RequestMapping("/incrementLikeUpIssue")
	public void incrementLikeUp(View view) throws IOException{	

		int issueId=Integer.valueOf(view.getRequest().getParameter("IssueId"));
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
				
        daoManager.executeVoidAndClose(() -> daoManager.getIssueLikeDao().likeOnIssue(userId, issueId));

        Integer totalLikes = daoManager.executeAndClose(() ->
                daoManager.getIssueLikeDao().countTotalLike(issueId));
		
		
		view.getResponse().setContentType("application/text");
		view.getResponse().getWriter().write(String.valueOf(totalLikes.toString()));
	}
	
	
	@RequestMapping("/incrementLikeDownIssue")
	public void incrementLikeDown(View view) throws IOException{		
	
		int issueId=Integer.valueOf(view.getRequest().getParameter("IssueId"));
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");

        daoManager.executeVoidAndClose(() -> daoManager.getIssueLikeDao().disLikeOnIssue(userId, issueId));

        Integer totalLikes = daoManager.executeAndClose(() ->
                daoManager.getIssueLikeDao().countTotalDisLike(issueId));
		
		view.getResponse().setContentType("application/text");
		view.getResponse().getWriter().write(String.valueOf(totalLikes.toString()));
		
	}
	@RequestMapping("/decrementLikeDownIssue")
	public void decrementLikeDown(View view) throws IOException{		
	
		int issueId=Integer.valueOf(view.getRequest().getParameter("IssueId"));
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");

        daoManager.executeVoidAndClose(() -> daoManager.getIssueLikeDao().decrementLikeOnIssue(userId, issueId));

        Integer totalLikes = daoManager.executeAndClose(() ->
                daoManager.getIssueLikeDao().countTotalDisLike(issueId));
		
		view.getResponse().setContentType("application/text");
		view.getResponse().getWriter().write(String.valueOf(totalLikes.toString()));
	
	}
	
	
	@RequestMapping("/loadLikes")
	public void loadLikes(View view) throws IOException{		
	
		int issueId=Integer.valueOf(view.getRequest().getParameter("IssueId"));
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		
		Integer totalLikes = daoManager.executeAndClose(() ->
                daoManager.getIssueLikeDao().countTotalLike(issueId));
		Integer totalDisLikes = daoManager.executeAndClose(() ->
                daoManager.getIssueLikeDao().countTotalDisLike(issueId));
		Integer likeValue = daoManager.executeAndClose(() ->
                daoManager.getIssueLikeDao().getValueOfLike(userId, issueId));

		LikeDto like=new LikeDto();
		like.setTotalLikes(totalLikes);
		like.setTotalDisLikes(totalDisLikes);
		like.setLikeValue(likeValue);
		
		Gson gson=new Gson();
		String likesInfo=gson.toJson(like);
		
		
		view.getResponse().setContentType("application/json");
		view.getResponse().getWriter().write(String.valueOf(likesInfo));
	
	}

}
