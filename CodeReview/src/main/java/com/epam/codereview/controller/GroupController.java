package com.epam.codereview.controller;

import com.epam.codereview.dto.EditGroupWithUsersDto;
import com.epam.codereview.dto.ReviewDto;
import com.epam.codereview.dto.UserGroupDto;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.RequestMethod;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.Group;
import com.epam.codereview.model.GroupUser;
import com.epam.codereview.model.User;
import com.epam.codereview.service.*;
import com.epam.codereview.settings.ImageSaver;
import com.epam.codereview.util.ImageUploader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@Controller
public class GroupController {

	GroupService groupService = new GroupService();
	GroupUserService groupUserService=new GroupUserService();
	UserService userService=new UserService();
	CodeReviewUserService codeReviewUserService=new CodeReviewUserService();

    private GroupConcreteService groupConcreteService = new GroupConcreteService();

	@RequestMapping(value = "/get_group_image/*")
	public void getGroupImage(View view) throws IOException, ServletException {
		  System.out.println("group  image");
		ImageUploader.upload(view, "group");

	}
	
	
	@RequestMapping(value = "/deletecurrentuserfromgroup")
	public void deleteUserConGroup(View view) throws IOException, ServletException {
		
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		 Integer groupId=Integer.valueOf(view.getRequest().getParameter("groupId"));
		
		
		int totalMembers=groupService.countUsersInGroup(groupId)+1;
		int  totalActiveReviews=groupConcreteService.getAllActiveReviewsByUserId(groupId,1,userId,0,100).size();
		int totalMyActiveReviews= groupConcreteService.getMyActiveReviewsByUserId(groupId,1,userId,0,100).size();	
		int totalHistoryReviews=groupConcreteService.getHistoryReviwByUserId(groupId,userId,0,100).size();
		
		view.getRequest().setAttribute("totalGroupMembers", totalMembers);
		view.getRequest().setAttribute("totalActiveReviews", totalActiveReviews);
		view.getRequest().setAttribute("totalMyActiveReviews", totalMyActiveReviews);
		view.getRequest().setAttribute("totalHistoryReviews", totalHistoryReviews);
		
		
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getRequest().getRequestDispatcher("/pages/cabinet/group_navbar.jsp").forward(view.getRequest(),
				view.getResponse());
		
	}
	
	@RequestMapping(value = "/refreshcurrentfilter")
	public void refreshFilterConGroup(View view) throws IOException, ServletException {
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getRequest().getRequestDispatcher("/pages/cabinet/group_review_filter.jsp").forward(view.getRequest(),
				view.getResponse());
	}
	
	
	

	
	
	
	@RequestMapping(value = "/group/*")
	public void getReviewAll_concrete(View view) throws IOException, ServletException {

		int groupId = Integer.valueOf(view.getLastUrlParam());		
		Group group = groupService.getById(groupId);
		boolean userInCurrentGroup=false;
		
		User groupOwner=userService.get(group.getUserId());	
		
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		
		
		List<Integer> listWithAllUsers= groupService.getAllUsersFromGroup(groupId);
		if(groupOwner.getId().equals(userId)){
			userInCurrentGroup=true;
		}else{
			for (Integer i : listWithAllUsers) {
				if(i.equals(userId)){
					userInCurrentGroup=true;
				}
			}
		}
		
		if(!userInCurrentGroup){
			view.getResponse().sendError(404);
			return ;
		}
		
		
		User user=userService.get(userId);
		session.setAttribute("user", user);
		session.setAttribute("GROUP_IDGroup", groupId);
//		List<GroupUser>  listWithUsersID=	groupUserService.getUsersByGroup(groupId);
		List<GroupUser>  listWithUsersID=groupUserService.getUserWithAllStatus(groupId);
		
		
		
		List<User> listWithUsers=new ArrayList<>();
		
		for (GroupUser gu : listWithUsersID) {
			
			User userInGroup=userService.get(gu.getUserId());
			userInGroup.setStatusInGroup(gu.getStatus());
			 listWithUsers.add(userInGroup);
		}
		
		
		int page = 1;
		int recordsPerPage = 4;
		if ( session.getAttribute("recordsOnPagecg")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPagecg");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPagecg") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPagecg"));
			session.setAttribute("recordsOnPagecg",recordsPerPage);

		}
		session.setAttribute("recordsOnPagecg", recordsPerPage);
		List<ReviewDto> list = groupConcreteService.getAllActiveReviewsByUserId(groupId,1, userId, (page - 1) * recordsPerPage, recordsPerPage);

		
//		for (int i = 0; i < list.size(); i++) {
//			ReviewDto reviewDto = list.get(i);
//			ReviewService reviewService = new ReviewService();
//			if (userId.equals(reviewDto.getUserId())) {
//				list.get(i).setCoverageAvgPercent(reviewService.getReviewPercentTotal(reviewDto.getCodeReviewId()));
//			} else {
//				list.get(i).setCoverageAvgPercent(
//						reviewService.getReviewPercentForUser(reviewDto.getCodeReviewId(), userId));
//			}
//		}
		
		
		int noOfRecords = groupConcreteService.getNoOfRecords();
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);
		
		
		
		EditGroupWithUsersDto dto = groupService.getGroupWithUsersByGroupId(Integer.valueOf(groupId));
		List<UserGroupDto> listWithUsersForId = dto.getUsers();
		
		int[] ids = new int[listWithUsersForId.size()];
		for (int i = 0; i < listWithUsersForId.size(); i++) {
			ids[i] = listWithUsersForId.get(i).getId();		
		}
		
		int totalMembers=groupService.countUsersInGroup(groupId)+1;
		System.out.println("total members"+totalMembers);
		int  totalActiveReviews=groupConcreteService.getAllActiveReviewsByUserId(groupId,1,userId,0,100).size();

		int totalMyActiveReviews= groupConcreteService.getMyActiveReviewsByUserId(groupId,1,userId,0,100).size();
	
		int totalHistoryReviews=groupConcreteService.getHistoryReviwByUserId(groupId,userId,0,100).size();
		
		view.getRequest().setAttribute("arrayIds", ids);
		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("listReviews", list);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsOnPagecg", recordsPerPage);
		view.getRequest().setAttribute("totalGroupMembers", totalMembers);
		view.getRequest().setAttribute("totalActiveReviews", totalActiveReviews);
		view.getRequest().setAttribute("totalMyActiveReviews", totalMyActiveReviews);
		view.getRequest().setAttribute("totalHistoryReviews", totalHistoryReviews);
		
		
		view.setAttribute("groupName", group.getName());
		view.setAttribute("groupId", group.getId());
		view.setAttribute("group", group);
		view.setAttribute("listWithUsers",listWithUsers );
		view.setAttribute("listWithUsersIDS",listWithUsersID );
		view.setAttribute("owner",groupOwner );
		view.setAttribute("ownerId",group.getUserId() );
		
		
		
		
		view.getResponse().setCharacterEncoding("UTF-8");

		view.getRequest().getRequestDispatcher("/pages/cabinet/group.jsp").forward(view.getRequest(),
				view.getResponse());

	}
	

	@RequestMapping(value = "/concretegroup/review")
	public void getReviewGroupConcrete(View view) throws IOException, ServletException {
		System.out.println("server");
			
		int groupId = Integer.parseInt(view.getRequest().getParameter("groupId"));
		Group group = groupService.getById(groupId);
		
		
		User groupOwner=userService.get(group.getUserId());	
		
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		User user=userService.get(userId);
		session.setAttribute("user", user);
		
		List<GroupUser>  listWithUsersID=	groupUserService.getUsersByGroup(groupId);
		List<User> listWithUsers=new ArrayList<>();
		
		for (GroupUser gu : listWithUsersID) {
			 listWithUsers.add(userService.get(gu.getUserId()));
		}
		
		
		int page = 1;
		int recordsPerPage =4;
		if ( session.getAttribute("recordsOnPagecg")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPagecg");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPagecg") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPagecg"));
			session.setAttribute("recordsOnPagecg",recordsPerPage);
		}
		session.setAttribute("recordsOnPagecg", recordsPerPage);
		List<ReviewDto> list = groupConcreteService.getAllActiveReviewsByUserId(groupId,1, userId, (page - 1) * recordsPerPage, recordsPerPage);

		
//		for (int i = 0; i < list.size(); i++) {
//			ReviewDto reviewDto = list.get(i);
//			ReviewService reviewService = new ReviewService();
//			if (userId.equals(reviewDto.getUserId())) {
//				list.get(i).setCoverageAvgPercent(reviewService.getReviewPercentTotal(reviewDto.getCodeReviewId()));
//			} else {
//				list.get(i).setCoverageAvgPercent(
//						reviewService.getReviewPercentForUser(reviewDto.getCodeReviewId(), userId));
//			}
//		}
		
		
		
		int noOfRecords = groupConcreteService.getNoOfRecords();
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);
		
		
		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("listReviews", list);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsPerPage", recordsPerPage);
		
		view.setAttribute("groupName", group.getName());
		view.setAttribute("groupId", group.getId());
		view.setAttribute("listWithUsers",listWithUsers );
		view.setAttribute("owner",groupOwner );

        view.getResponse().setCharacterEncoding("UTF-8");

		view.getRequest().getRequestDispatcher("/pages/cabinet/concrete_group_pagenation_review.jsp").forward(view.getRequest(),
				view.getResponse());
	}
	
	
	@RequestMapping(value = "/concretegroup/reviewmy")
	public void getReviewGroupConcreteMy(View view) throws IOException, ServletException {
		System.out.println("server");
			
		int groupId = Integer.parseInt(view.getRequest().getParameter("groupId"));
		Group group = groupService.getById(groupId);
		
		
		User groupOwner=userService.get(group.getUserId());	
		
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		User user=userService.get(userId);
		session.setAttribute("user", user);
		
		List<GroupUser>  listWithUsersID=	groupUserService.getUsersByGroup(groupId);
		List<User> listWithUsers=new ArrayList<>();
		
		for (GroupUser gu : listWithUsersID) {
			 listWithUsers.add(userService.get(gu.getUserId()));
		}

		int page = 1;
		int recordsPerPage =4;
		if ( session.getAttribute("recordsOnPagecg")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPagecg");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPagecg") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPagecg"));
			session.setAttribute("recordsOnPagecg",recordsPerPage);

		}
		session.setAttribute("recordsOnPagecg", recordsPerPage);
		List<ReviewDto> list=groupConcreteService.getMyActiveReviewsByUserId(groupId,1, userId, (page - 1) * recordsPerPage, recordsPerPage);

		
//		for (int i = 0; i < list.size(); i++) {
//			ReviewDto reviewDto = list.get(i);
//			ReviewService reviewService = new ReviewService();
//			if (userId.equals(reviewDto.getUserId())) {
//				list.get(i).setCoverageAvgPercent(reviewService.getReviewPercentTotal(reviewDto.getCodeReviewId()));
//			} else {
//				list.get(i).setCoverageAvgPercent(
//						reviewService.getReviewPercentForUser(reviewDto.getCodeReviewId(), userId));
//			}
//		}
		
		
		int noOfRecords = groupConcreteService.getNoOfRecords();
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);
		
		
		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("listReviews", list);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsPerPage", recordsPerPage);
		

		view.setAttribute("groupName", group.getName());
		view.setAttribute("groupId", group.getId());
		view.setAttribute("listWithUsers",listWithUsers );
		view.setAttribute("owner",groupOwner );
		
		
		
		view.getResponse().setCharacterEncoding("UTF-8");

		view.getRequest().getRequestDispatcher("/pages/cabinet/concrete_group_pagenation_review_my.jsp").forward(view.getRequest(),
				view.getResponse());
	}
	
	@RequestMapping(value = "/concretegroup/reviewother")
	public void getReviewGroupConcreteOther(View view) throws IOException, ServletException {
		System.out.println("server other");
			
		int groupId = Integer.parseInt(view.getRequest().getParameter("groupId"));
		Group group = groupService.getById(groupId);
		
		
		User groupOwner=userService.get(group.getUserId());	
		
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		User user=userService.get(userId);
		session.setAttribute("user", user);
		
		List<GroupUser>  listWithUsersID=	groupUserService.getUsersByGroup(groupId);
		List<User> listWithUsers=new ArrayList<>();
		
		for (GroupUser gu : listWithUsersID) {
			 listWithUsers.add(userService.get(gu.getUserId()));
		}
		
		
		int page = 1;
		int recordsPerPage =4;
		if ( session.getAttribute("recordsOnPagecg")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPagecg");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPagecg") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPagecg"));
			session.setAttribute("recordsOnPagecg",recordsPerPage);

		}
		session.setAttribute("recordsOnPagecg", recordsPerPage);
		List<ReviewDto> list = groupConcreteService.getOtherActiveReviewsByUserId(groupId,1, userId, (page - 1) * recordsPerPage, recordsPerPage);

		
//		for (int i = 0; i < list.size(); i++) {
//			ReviewDto reviewDto = list.get(i);
//			ReviewService reviewService = new ReviewService();
//			if (userId.equals(reviewDto.getUserId())) {
//				list.get(i).setCoverageAvgPercent(reviewService.getReviewPercentTotal(reviewDto.getCodeReviewId()));
//			} else {
//				list.get(i).setCoverageAvgPercent(
//						reviewService.getReviewPercentForUser(reviewDto.getCodeReviewId(), userId));
//			}
//		}
		
		
		
		
		int noOfRecords = groupConcreteService.getNoOfRecords();
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);
		
		
		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("listReviews", list);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsPerPage", recordsPerPage);
		

		view.setAttribute("groupName", group.getName());
		view.setAttribute("groupId", group.getId());
		view.setAttribute("listWithUsers",listWithUsers );
		view.setAttribute("owner",groupOwner );

		view.getResponse().setCharacterEncoding("UTF-8");

		view.getRequest().getRequestDispatcher("/pages/cabinet/concrete_group_pagenation_review_other.jsp").forward(view.getRequest(),
				view.getResponse());
	
	}
	
	
	@RequestMapping(value = "/concretegroup/reviewhistory")
	public void getReviewGroupConcreteHistory(View view) throws IOException, ServletException {
		System.out.println("server history");
			
		int groupId = Integer.parseInt(view.getRequest().getParameter("groupId"));
		Group group = groupService.getById(groupId);
		
		
		User groupOwner=userService.get(group.getUserId());	
		
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		User user=userService.get(userId);
		session.setAttribute("user", user);
		
		List<GroupUser>  listWithUsersID=	groupUserService.getUsersByGroup(groupId);
		List<User> listWithUsers=new ArrayList<>();
		
		for (GroupUser gu : listWithUsersID) {
			 listWithUsers.add(userService.get(gu.getUserId()));
		}
		
		
		int page = 1;
		int recordsPerPage =4;
		if ( session.getAttribute("recordsOnPagecg")!=null) {
			recordsPerPage=(Integer)session.getAttribute("recordsOnPagecg");
		}

		if (view.getRequest().getParameter("page") != null) {
			page = Integer.parseInt(view.getRequest().getParameter("page"));
		}
		if (view.getRequest().getParameter("recordsOnPagecg") != null) {
			recordsPerPage = Integer.parseInt(view.getRequest().getParameter("recordsOnPagecg"));
			session.setAttribute("recordsOnPagecg",recordsPerPage);

		}
		session.setAttribute("recordsOnPagecg", recordsPerPage);
		List<ReviewDto> list =groupConcreteService.getHistoryReviwByUserId(groupId, userId, (page - 1) * recordsPerPage, recordsPerPage);
		
		
//		for (int i = 0; i < list.size(); i++) {
//			ReviewDto reviewDto = list.get(i);
//			ReviewService reviewService = new ReviewService();
//			if (userId.equals(reviewDto.getUserId())) {
//				list.get(i).setCoverageAvgPercent(reviewService.getReviewPercentTotal(reviewDto.getCodeReviewId()));
//			} else {
//				list.get(i).setCoverageAvgPercent(
//						reviewService.getReviewPercentForUser(reviewDto.getCodeReviewId(), userId));
//			}
//		}
		
		
		int noOfRecords = groupConcreteService.getNoOfRecords();
		System.out.println("noOfRecords" + noOfRecords);
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
		System.out.println("noOfPages" + noOfPages);
		
		
		view.getRequest().setAttribute("userID", userId);
		view.getRequest().setAttribute("listReviews", list);
		view.getRequest().setAttribute("noOfPages", noOfPages);
		view.getRequest().setAttribute("currentPage", page);
		view.getRequest().setAttribute("recordsPerPage", recordsPerPage);
		

		view.setAttribute("groupName", group.getName());
		view.setAttribute("groupId", group.getId());
		view.setAttribute("listWithUsers",listWithUsers );
		view.setAttribute("owner",groupOwner );
		
		
		
		view.getResponse().setCharacterEncoding("UTF-8");

		view.getRequest().getRequestDispatcher("/pages/cabinet/concrete_group_pagenation_review_history.jsp").forward(view.getRequest(),
				view.getResponse());
	
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
