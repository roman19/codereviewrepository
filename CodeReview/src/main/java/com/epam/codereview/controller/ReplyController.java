package com.epam.codereview.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import com.epam.codereview.dto.ReplyNotificationDto;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.RequestMethod;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.Reply;
import com.epam.codereview.service.ReplyService;
import com.epam.codereview.service.UserService;
import com.google.gson.Gson;

@Controller
@RequestMapping(value = "/review")
public class ReplyController {
	private final static Logger LOG = Logger.getLogger(ReplyController.class);

	private ReplyService replyService = new ReplyService();

	@RequestMapping(value = "/uploadUsers")
	public void uploadUsers(View view) throws ServletException, IOException {
		try {
			Integer issueId = Integer.parseInt(view.getParameter("issueId"));

			UserService userService = new UserService();

			List<String> users = userService.getUsersToReplyByIssyeId(issueId);

			for (int i = 0; i < users.size(); i++) {
				users.set(i, "@" + users.get(i));
			}

			String json = new Gson().toJson(users);
			view.getResponse().setContentType("application/json");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write(json);

		} catch (Exception e) {
			LOG.error("Error during fetching new users to reply", e);
		}
	}

	@RequestMapping(value = "/uploadReplies")
	public void uploadReplies(View view) throws ServletException, IOException {
		try {
			Integer issueId = Integer.parseInt(view.getParameter("issueId"));
			Boolean reviewIsActive = new Boolean(view.getParameter("reviewIsActive"));

			view.setAttribute("replies",
					replyService.getRepliesByIssueId(issueId));
			view.setAttribute("reviewIsActive", reviewIsActive);
			view.getRequest()
					.getRequestDispatcher("/pages/review/reply/allReplies.jsp")
					.forward(view.getRequest(), view.getResponse());

		} catch (Exception e) {
			LOG.error("Error during fetching new users to reply", e);
		}
	}

	@RequestMapping(value = "/getReply")
	public void getReply(View view) throws ServletException, IOException {
		Integer replyId = Integer.parseInt(view.getParameter("replyId"));

		view.getResponse().setContentType("text/plain");
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getResponse().getWriter()
				.write(replyService.get(replyId).getMessage());
	}

	@RequestMapping(value = "/saveReply")
	public void saveReply(View view) throws ServletException, IOException {
		try {

			Integer issueId = Integer.parseInt(view.getParameter("issueId"));
			String message = view.getParameter("message");

			Reply reply = new Reply();
			reply.setIssueId(issueId);
			reply.setMessage(message);
			reply.setOwnerId(view.getSessionUserID());

			ReplyNotificationDto replyDto = replyService.insert(reply);
			
			String json = new Gson().toJson(replyDto);
		    view.getResponse().setContentType("application/json");
		    view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write(json);

		} catch (Exception e) {
			LOG.error("Error occured during saving reply", e);
		}
	}
	
	@RequestMapping(value = "/writeReply")
	public void writeReply(View view) throws ServletException, IOException {
		try {
			
			Integer replyId = Integer.parseInt(view.getParameter("replyId"));

			view.setAttribute("reply", replyService.get(replyId));
			view.getRequest()
					.getRequestDispatcher("/pages/review/reply/reply.jsp")
					.forward(view.getRequest(), view.getResponse());

		} catch (Exception e) {
			LOG.error("Error occured during writing reply", e);
		}
	}

	@RequestMapping(value = "/deleteReply", method = RequestMethod.POST)
	public void deleteReply(View view) throws ServletException, IOException {
		try {

			Integer replyId = Integer.parseInt(view.getParameter("replyId"));
			replyService.delete(replyId);

		} catch (Exception e) {
			LOG.error("Error occured during deleting reply", e);
		}
	}

	@RequestMapping(value = "/editReply", method = RequestMethod.POST)
	public void editReply(View view) throws ServletException, IOException {
		try {

			Integer replyId = Integer.parseInt(view.getParameter("replyId"));
			String message = view.getParameter("message");

			Reply reply = replyService.get(replyId);
			reply.setMessage(message);
			replyService.update(reply);
			view.setAttribute("reply", reply);

			view.getRequest()
					.getRequestDispatcher("/pages/review/reply/reply.jsp")
					.forward(view.getRequest(), view.getResponse());

		} catch (Exception e) {
			LOG.error("Error occured during editing reply", e);
		}
	}

}
