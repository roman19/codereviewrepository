package com.epam.codereview.controller;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.epam.codereview.attachment.FileUploader;
import com.epam.codereview.attachment.FileUploader.AttachmentFile;
import com.epam.codereview.attachment.GitFileAttachment;
import com.epam.codereview.attachment.PcFileAttachment;
import com.epam.codereview.dto.CodeReviewDto;
import com.epam.codereview.dto.FileOnlineNotificationDto;
import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.git.GitHub;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.RequestMethod;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.Language;
import com.epam.codereview.model.User;
import com.epam.codereview.project.ProjectHandler;
import com.epam.codereview.service.LanguageService;
import com.epam.codereview.service.NotificationService;
import com.epam.codereview.service.ProjectService;
import com.epam.codereview.service.ReviewService;
import com.epam.codereview.service.UserService;
import com.epam.codereview.tree.TreeBuilder;
import com.google.gson.Gson;

@Controller
@RequestMapping(value = "/review")
public class FileAttachmentController {
	private final static Logger LOG = Logger.getLogger(FileAttachmentController.class);
	ReviewService reviewService = new ReviewService();

	////////////////////////////////////////////////	PC attachment	////////////////////////////////////////////
	
	@RequestMapping(value = "/extensions", method = RequestMethod.POST)
	public void getExtensions(View view)
			throws SessionIsNotAvailableException, IOException {
		LanguageService languageService = new LanguageService();
		List<Language> languages = languageService.getAllLanguages();
		String extensions = "";
		for(Language language : languages){
			extensions += "." + language.getFormat() + ",";
		}
		view.getResponse().setContentType("application/text");
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getResponse().getWriter().write(extensions.substring(0, extensions.length() - 2));
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/pc_attachment", method = RequestMethod.POST )
	public void pcAttach(View view) throws IOException{
		try {
			String reviewsServerPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
			String projectFolder = "temp" + view.getSessionUserID();
			ProjectHandler projectHandler = new ProjectHandler(null, reviewsServerPath);
			projectHandler.removeUncheckedFiles(view.getSessionUserID() + "/"
					+ projectFolder);
			new File(reviewsServerPath + view.getSessionUserID() + "/" + projectFolder)
					.delete();
			
			FileUploader fileUploader = new FileUploader(view);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("tempFolder", fileUploader.uploadFiles());
			JSONArray jsonArray = new JSONArray();
			for(AttachmentFile file : fileUploader.getDownloadedFiles()){
				jsonArray.add(new Gson().toJson(file));
			}
			jsonObject.put("files", jsonArray);
			jsonObject.put("failedExtensions", fileUploader.getUnsupportedExtensions());
			view.getResponse().setContentType("application/json");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write(jsonObject.toString());
		} catch (Exception e) {
			LOG.error(e);
		}
	}
	
	@RequestMapping(value = "/pc_attachment_save_changes", method = RequestMethod.POST )
	public void savePcChanges(View view) throws SessionIsNotAvailableException, IOException{
		String reviewsServerPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
		String tempFolder = view.getParameter("tempFolder");
		String[] newFiles = view.getParameter("newFiles").split(",");
		if (newFiles.length == 1 && newFiles[0].equals("")) {
			view.getResponse().getWriter().write("no files");
			return;
		}
		Integer reviewId = Integer.valueOf(view.getParameter("code_review_id"));
		CodeReviewDto codeReviewDto = reviewService.getCodeReviewDto(reviewId, reviewsServerPath);
		PcFileAttachment pcFileAttachment = new PcFileAttachment(tempFolder, newFiles, codeReviewDto, reviewsServerPath);
		Integer numberOfNewFiles = pcFileAttachment.addNewFiles();
		
		//for notifications
		FileOnlineNotificationDto notificationDto = new FileOnlineNotificationDto(view.getSessionUserID(), reviewId, numberOfNewFiles);
		NotificationService notificationService = new NotificationService();
		notificationService.sendFileNotification(notificationDto);
		
		view.getResponse().setContentType("application/json");
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getResponse().getWriter().write(new Gson().toJson(notificationDto));
		
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/folder_tree", method = RequestMethod.POST )
	public void buildFolderTree(View view) throws IOException, SQLException, SessionIsNotAvailableException{
		int reviewId = Integer.valueOf(view.getParameter("code_review_id"));
		String reviewsServerPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
		CodeReviewDto codeReviewDto = reviewService.getCodeReviewDto(reviewId, reviewsServerPath);
		String project = codeReviewDto.getProject().getFullName();
		String projectName = codeReviewDto.getProject().getName();
		TreeBuilder treeBuilder = new TreeBuilder(
				project, projectName, reviewsServerPath, codeReviewDto.getCodeReview().getOwnerId(), false);
		User user = new UserService().getByID(view.getSessionUserID());
		long usedMemory = new ProjectService().getUsedMemoryByUser(user.getId());
		long enabledMemory = user.getEnabledMemory();
		String memory = " / " + ProjectHandler.convertBytesInMegaBytes(enabledMemory) + " MB";
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("tree", treeBuilder.buildFileAttachmentTree());
		jsonObject.put("memory", memory);
		jsonObject.put("usedMemory", usedMemory);
		jsonObject.put("enabledMemory", enabledMemory);
		jsonObject.put("memoryPercentage", ((double) usedMemory / (double) enabledMemory) * 100);
		view.getResponse().setContentType("application/json");
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getResponse().getWriter().write(jsonObject.toString());
		
	}

	////////////////////////////////////////////////	git attachment	////////////////////////////////////////////
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/git_attachment", method = RequestMethod.POST )
	public void gitAttach(View view) throws IOException{
		String reviewsServerPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
		CodeReviewDto codeReviewDto = 
				reviewService.getCodeReviewDto(Integer.valueOf(view.getParameter("code_review_id")), reviewsServerPath);

		view.setAttribute("codeReviewName", codeReviewDto.getCodeReview().getName());
		
		String password = view.getParameter("password");
		view.getResponse().setContentType("application/json");
		view.getResponse().setCharacterEncoding("UTF-8");
		try {
			JSONObject json = new JSONObject();
			JSONArray jsonBranches = new JSONArray();
			for (String branch : GitHub.getBranches(codeReviewDto.getProject().getGitUrl(), password == null ? "":password)) {
				jsonBranches.add(branch);
			}
			json.put("branches", jsonBranches);
			json.put("url", codeReviewDto.getProject().getGitUrl());
			view.getResponse().getWriter().write(json.toString());
		} catch (Exception e) {
			JSONObject jsonErrorObject = new JSONObject();
			if (e.getLocalizedMessage() != null
					&& e.getLocalizedMessage().contains("not authorized")) {
				jsonErrorObject.put("status", "auth_error");
			} else {
				jsonErrorObject.put("status", "no_repository");
			}
			view.getResponse().getWriter().write(jsonErrorObject.toString());
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/attachment_tree", method = RequestMethod.GET)
	public void buildTree(View view) throws IOException,
			SessionIsNotAvailableException, SQLException {
		String project = view.getRequest().getParameter("project");
		String projectName = view.getRequest().getParameter("projectName");
		TreeBuilder treeBuilder = new TreeBuilder(project, projectName,
				view.getRequest().getServletContext().getInitParameter("review-project-upload"),
				view.getSessionUserID());
		String tree = treeBuilder.buildAttachmentCheckBoxTree(Integer.valueOf(view.getParameter("code_review_id")));
		User user = new UserService().getByID(view.getSessionUserID());
		long usedMemory = new ProjectService().getUsedMemoryByUser(user.getId());
		long enabledMemory = user.getEnabledMemory();
		String memory = " / " + ProjectHandler.convertBytesInMegaBytes(enabledMemory) + " MB";
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("tree", tree);
		jsonObject.put("memory", memory);
		jsonObject.put("usedMemory", usedMemory);
		jsonObject.put("enabledMemory", enabledMemory);
		jsonObject.put("memoryPercentage", ((double) usedMemory / (double) enabledMemory) * 100);
		view.getResponse().setContentType("application/json");
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getResponse().getWriter().write(jsonObject.toString());
	}
	
	@RequestMapping(value = "/git_attachment_save_changes", method = RequestMethod.POST )
	public void saveGitChanges(View view) throws IOException, SessionIsNotAvailableException{
		String[] pathes = (view.getParameter("pathes") == null ? null : view.getParameter("pathes").split(","));
		view.getResponse().setContentType("application/text");
		view.getResponse().setCharacterEncoding("UTF-8");
		if (pathes.length == 1 && pathes[0].equals("")) {
			view.getResponse().getWriter().write("no pathes");
			return;
		}
		String reviewsServerPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
		CodeReviewDto codeReviewDto = 
				reviewService.getCodeReviewDto(Integer.valueOf(view.getParameter("code_review_id")), reviewsServerPath);
		GitFileAttachment fileAttachment = new GitFileAttachment(pathes, codeReviewDto, reviewsServerPath);
		int countOfAddedFiles = fileAttachment.addNewFiles();
		
		//for notifications
		FileOnlineNotificationDto notificationDto = new FileOnlineNotificationDto(view.getSessionUserID(), codeReviewDto.getCodeReview().getId(), countOfAddedFiles);
		NotificationService notificationService = new NotificationService();
		notificationService.sendFileNotification(notificationDto);
				
		view.getResponse().setContentType("application/json");
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getResponse().getWriter().write(new Gson().toJson(notificationDto));
	}

	@RequestMapping(value = "/cancel", method = RequestMethod.GET)
	public void cancelCodeReviewCreatin(View view)
			throws SessionIsNotAvailableException, IOException {
		String reviewsServerPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
		CodeReviewDto codeReviewDto = 
				reviewService.getCodeReviewDto(Integer.valueOf(view.getParameter("code_review_id")), reviewsServerPath);
		String projectFolder = view.getParameter("projectFolder");
		if (projectFolder == null || projectFolder.equals(""))
			return;
		ProjectHandler projectHandler = new ProjectHandler(null, reviewsServerPath);
		projectHandler.removeUncheckedFiles(codeReviewDto.getCodeReview().getOwnerId() + "/"
				+ projectFolder);
		new File(reviewsServerPath + view.getSessionUserID() + "/" + projectFolder)
				.delete();
	}

}
