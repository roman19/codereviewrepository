package com.epam.codereview.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;

import com.epam.codereview.exception.ValidationException;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.RequestMethod;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.User;
import com.epam.codereview.oauth2.BitbucketOAuth2Details;
import com.epam.codereview.oauth2.GitHubOAuth2Details;
import com.epam.codereview.oauth2.GoogleOAuth2Details;
import com.epam.codereview.service.BitBucketService;
import com.epam.codereview.service.GitHubService;
import com.epam.codereview.service.GoogleService;
import com.epam.codereview.service.UserService;

@Controller
public class OAuth2Controller {
	private final static Logger LOG = Logger.getLogger(OAuth2Controller.class);

	@RequestMapping(value = "/googleLogin")
	public void googleLogin(View view) throws IOException {
		String autorizationString = GoogleOAuth2Details.userAuthorizationUri
				+ "?client_id=" + GoogleOAuth2Details.clientId
				+ "&redirect_uri=" + GoogleOAuth2Details.redirectUri
				+ "&scope=" + GoogleOAuth2Details.scope
				+ "&response_type=token";
		view.getResponse().sendRedirect(autorizationString);
	}

	@RequestMapping(value = "/googleAuthentication", method = RequestMethod.POST)
	public void googleLoginAuth(View view) throws IOException {
		GoogleService googleService = new GoogleService();
		UserService userService = new UserService();

		try {

			User user = googleService
					.getUserByToken(view.getParameter("token"));

			User userFromSocNet = userService.getBySocial(user.getSocial(),
					user.getSocialId());

			if (userFromSocNet == null) {

				String emailPart = user.getEmail().substring(0,
						user.getEmail().indexOf("@"));

				user.setLogin(emailPart + userService.getMaxId().toString());
				user.setLocale(view.getLocale());
				user.setStatus("CONFIRMED");

				Integer newID = userService.insertFromSocNet(user);

				view.getRequest().getSession().setAttribute("userID", newID);
			} else {
				view.getRequest().getSession()
						.setAttribute("userID", userFromSocNet.getId());
			}

			view.getResponse().sendRedirect(
					view.getRequest().getContextPath() + "/cabinet");

		} catch (ParseException | SQLException e) {
			LOG.error("Problem with google authentication", e);
		}
	}

	@RequestMapping(value = "/checkUserByToken", method = RequestMethod.POST)
	public void checkUserByToken(View view) throws IOException {
		GoogleService googleService = new GoogleService();
		UserService userService = new UserService();

		try {
			User userFromSocNet = googleService.getUserByToken(view
					.getParameter("token"));

			User user = userService.getBySocial(userFromSocNet.getSocial(),
					userFromSocNet.getSocialId());

			String message = "";
			if (user == null) {
				// check if such email exists
				if (userService.getByEmail(userFromSocNet.getEmail()) == null) {
					message = "good";
				}
			} else {
				message = "good";
			}

			view.getResponse().getWriter().write(message);

		} catch (Exception e) {
			LOG.error("Problem during check bitbucket authentication", e);
		}
	}

	@RequestMapping(value = "/googleLoginCode")
	public void googleLoginCode(View view) throws IOException, ServletException {

		view.setAttribute("authGoogle", true);
		view.getRequest().getRequestDispatcher("/pages/index/index.jsp")
				.forward(view.getRequest(), view.getResponse());
	}

	@RequestMapping(value = "/bitbucketLogin")
	public void bitbucketLogin(View view) throws IOException {
		String autorizationString = BitbucketOAuth2Details.userAuthorizationUri
				+ "?client_id=" + BitbucketOAuth2Details.clientId
				+ "&redirect_uri=" + BitbucketOAuth2Details.redirectUri
				+ "&scope=" + BitbucketOAuth2Details.scope
				+ "&response_type=token";

		view.getResponse().sendRedirect(autorizationString);
	}

	@RequestMapping(value = "/bitbucketLoginCode")
	public void bitbucketLoginCode(View view) throws IOException,
			ServletException {

		view.setAttribute("authBitbucket", true);
		view.getRequest().getRequestDispatcher("/pages/index/index.jsp")
				.forward(view.getRequest(), view.getResponse());

	}

	@RequestMapping(value = "/bitbucketAuthentication", method = RequestMethod.POST)
	public void bitbucketLoginAuth(View view) throws IOException {
		BitBucketService bitBucketService = new BitBucketService();
		UserService userService = new UserService();

		try {
			User user = bitBucketService.getUserByToken(view
					.getParameter("token"));

			User userFromSocNet = userService.getBySocial(user.getSocial(),
					user.getSocialId());

			if (userFromSocNet == null) {

				user.setLocale(view.getLocale());
				user.setStatus("CONFIRMED");

				if (userService.getByLogin(user.getLogin()) != null) {
					Integer addId = userService.getMaxId();
					user.setLogin(user.getLogin() + addId);
				}

				Integer newID = userService.insertFromSocNet(user);

				view.getRequest().getSession().setAttribute("userID", newID);
			} else {
				view.getRequest().getSession().setAttribute("userID", userFromSocNet.getId());
			}

			view.getResponse().sendRedirect(
					view.getRequest().getContextPath() + "/cabinet");

		} catch (ParseException | ValidationException | SQLException e) {
			LOG.error("Problem with bitbucket authentication", e);
		}
	}

	@RequestMapping(value = "/checkUserByBitbucketToken", method = RequestMethod.POST)
	public void checkUserByBitbucketToken(View view) throws IOException {
		BitBucketService bitBucketService = new BitBucketService();
		UserService userService = new UserService();

		try {
			User userFromSocNet = bitBucketService.getUserByToken(view
					.getParameter("token"));

			User user = userService.getBySocial(userFromSocNet.getSocial(),
					userFromSocNet.getSocialId());

			String message = "";
			if (user == null) {
				// check if such email exists
				if (userService.getByEmail(userFromSocNet.getEmail()) == null) {
					message = "good";
				}
			} else {
				message = "good";
			}

			view.getResponse().getWriter().write(message);

		} catch (Exception e) {
			LOG.error("Problem during check bitbucket authentication", e);
		}
	}

	@RequestMapping(value = "/githubLogin")
	public void githubLogin(View view) throws IOException {
		String autorizationString = GitHubOAuth2Details.userAuthorizationUri
				+ "?client_id=" + GitHubOAuth2Details.clientId
				+ "&redirect_uri=" + GitHubOAuth2Details.redirectUri
				+ "&scope=" + GitHubOAuth2Details.scope;

		view.getResponse().sendRedirect(autorizationString);
	}

	@RequestMapping(value = "/githubLoginCode")
	public void githubLoginCode(View view) throws IOException, ServletException {

		view.setAttribute("authGithub", true);
		view.setAttribute("code", view.getParameter("code"));
		view.getRequest().getRequestDispatcher("/pages/index/index.jsp")
				.forward(view.getRequest(), view.getResponse());

	}

	@RequestMapping(value = "/githubAuthentication", method = RequestMethod.POST)
	public void githubLoginAuth(View view) throws IOException {
		GitHubService gitHubService = new GitHubService();
		UserService userService = new UserService();

		try {
			String accessToken = view.getParameter("token");
			
			User user = gitHubService.getUserByAccessToken(accessToken);

			User userFromSocNet = userService.getBySocial(user.getSocial(),
					user.getSocialId());

			if (userFromSocNet == null) {

				user.setLocale(view.getLocale());
				user.setStatus("CONFIRMED");

				if (userService.getByLogin(user.getLogin()) != null) {
					Integer addId = userService.getMaxId();
					user.setLogin(user.getLogin() + addId);
				}

				Integer newID = userService.insertFromSocNet(user);

				view.getRequest().getSession().setAttribute("userID", newID);
			} else {
				view.getRequest().getSession()
						.setAttribute("userID", userFromSocNet.getId());
			}

			view.getResponse().sendRedirect(
					view.getRequest().getContextPath() + "/cabinet");

		} catch (ParseException | ValidationException | SQLException e) {
			LOG.error("Problem with bitbucket authentication", e);
		}
	}

	@RequestMapping(value = "/checkUserByGithubCode", method = RequestMethod.POST)
	public void checkUserByGithubCode(View view) throws IOException {
		GitHubService gitHubService = new GitHubService();
		UserService userService = new UserService();

		try {
			String code = view.getParameter("code");
			String accessToken = gitHubService.getAccessTokenByCode(code);
			User userFromSocNet = gitHubService.getUserByAccessToken(accessToken);

			User user = userService.getBySocial(userFromSocNet.getSocial(),
					userFromSocNet.getSocialId());

			String message = "bad";
			if (user == null) {
				// check if such email exists
				if (userService.getByEmail(userFromSocNet.getEmail()) == null) {
					message = accessToken;
				}
			} else {
				message = accessToken;
			}

			view.getResponse().getWriter().write(message);

		} catch (Exception e) {
			LOG.error("Problem during check github authentication", e);
		}
	}

}
