package com.epam.codereview.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import com.epam.codereview.dto.FileNotificationDto;
import com.epam.codereview.dto.GroupNotificationDto;
import com.epam.codereview.dto.IssueNotificationDto;
import com.epam.codereview.dto.NotificationDto;
import com.epam.codereview.dto.ReplyNotificationDto;
import com.epam.codereview.dto.ReviewNotificationDto;
import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.RequestMethod;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.CodeReviewUser;
import com.epam.codereview.model.GroupUser;
import com.epam.codereview.service.CodeReviewUserService;
import com.epam.codereview.service.FileUserService;
import com.epam.codereview.service.GroupUserService;
import com.epam.codereview.service.IssueService;
import com.epam.codereview.service.NotificationOnReplyService;
import com.epam.codereview.service.NotificationService;

@Controller
@RequestMapping(value = "/cabinet")
public class NotificationController {
	private final static Logger LOG = Logger.getLogger(NotificationController.class);

	@RequestMapping(value = "/notifications")
	public void getNotifications(View view) throws ServletException,
			IOException {
		NotificationService notificationService = new NotificationService();

		try {
			Integer userID = view.getSessionUserID();
			Integer count = Integer.parseInt(view.getParameter("count"));
			Integer offset = Integer.parseInt(view.getParameter("offset"));
			
			List<NotificationDto> notifications = notificationService.getAllByUserId(userID, count, offset);

			view.setAttribute("notifications", notifications);

			view.getRequest()
					.getRequestDispatcher(
							"/pages/cabinet/content_notifications.jsp")
					.forward(view.getRequest(), view.getResponse());
		} catch (SessionIsNotAvailableException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/groupNotifications")
	public void getGroupNotifications(View view) throws ServletException,
			IOException {
		NotificationService notificationService = new NotificationService();

		try {
			Integer userID = view.getSessionUserID();
			
			Integer offset = Integer.parseInt(view.getParameter("offset"));
			Integer count = Integer.parseInt(view.getParameter("count"));

			List<GroupNotificationDto> notifications = notificationService
					.getGroupNotificationsByUserId(userID, count, offset);

			view.setAttribute("notifications", notifications);

			view.getRequest()
					.getRequestDispatcher(
							"/pages/cabinet/notification/group_notifications.jsp")
					.forward(view.getRequest(), view.getResponse());
		} catch (SessionIsNotAvailableException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/reviewNotifications")
	public void getReviewNotifications(View view) throws ServletException,
			IOException {
		NotificationService notificationService = new NotificationService();

		try {
			Integer userID = view.getSessionUserID();
			
			Integer offset = Integer.parseInt(view.getParameter("offset"));
			Integer count = Integer.parseInt(view.getParameter("count"));

			List<ReviewNotificationDto> notifications = notificationService
					.getReviewNotificationsByUserId(userID, count, offset);

			view.setAttribute("notifications", notifications);

			view.getRequest()
					.getRequestDispatcher(
							"/pages/cabinet/notification/review_notifications.jsp")
					.forward(view.getRequest(), view.getResponse());
		} catch (SessionIsNotAvailableException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/issueNotifications")
	public void getIssueNotifications(View view) throws ServletException,
			IOException {
		NotificationService notificationService = new NotificationService();

		try {
			Integer userID = view.getSessionUserID();
			
			Integer offset = Integer.parseInt(view.getParameter("offset"));
			Integer count = Integer.parseInt(view.getParameter("count"));

			List<IssueNotificationDto> notifications = notificationService
					.getIssueNotificationsByUserId(userID, count, offset);

			view.setAttribute("notifications", notifications);

			view.getRequest()
					.getRequestDispatcher(
							"/pages/cabinet/notification/issue_notifications.jsp")
					.forward(view.getRequest(), view.getResponse());
		} catch (SessionIsNotAvailableException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/fileNotifications")
	public void getFileNotifications(View view) throws ServletException,
			IOException {
		NotificationService notificationService = new NotificationService();

		try {
			Integer userID = view.getSessionUserID();
			
			Integer offset = Integer.parseInt(view.getParameter("offset"));
			Integer count = Integer.parseInt(view.getParameter("count"));

			List<FileNotificationDto> notifications = notificationService
					.getFileNotificationsByUserId(userID, count, offset);

			view.setAttribute("notifications", notifications);

			view.getRequest()
					.getRequestDispatcher(
							"/pages/cabinet/notification/file_notifications.jsp")
					.forward(view.getRequest(), view.getResponse());
		} catch (SessionIsNotAvailableException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/allNotifications")
	public void getAllNotifications(View view) throws ServletException,
			IOException {
		NotificationService notificationService = new NotificationService();

		try {
			Integer userID = view.getSessionUserID();
			
			String offsetParam = view.getParameter("offset");
			String countParam = view.getParameter("count");
			
			List<NotificationDto> notifications = null;
			
			if(offsetParam==null || countParam==null){
				notifications = notificationService.getAllByUserId(userID, 10, 10);
			}else{
				int offset = Integer.parseInt(offsetParam);
				int count = Integer.parseInt(countParam);
				notifications = notificationService.getAllByUserId(userID, count, offset);
			}

			view.setAttribute("notifications", notifications);

			view.getRequest()
					.getRequestDispatcher(
							"/pages/cabinet/notification/all_notifications.jsp")
					.forward(view.getRequest(), view.getResponse());
		} catch (SessionIsNotAvailableException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/loadNotifications")
	public void loadNotifications(View view) throws ServletException,
			IOException {
		NotificationService notificationService = new NotificationService();

		try {
			Integer userID = view.getSessionUserID();
			
			Integer offset = Integer.parseInt(view.getParameter("offset"));
			Integer count = Integer.parseInt(view.getParameter("count"));
			String type = view.getParameter("type");
			
			if ("ALL".equals(type)){
				List<NotificationDto> notifications = notificationService.getAllByUserId(userID, count, offset);
				view.setAttribute("notifications", notifications);
				view.forward("/pages/cabinet/notification/all_notifications.jsp");
			}else if("GROUP".equals(type)){
				List<GroupNotificationDto> notifications = notificationService.getGroupNotificationsByUserId(userID, count, offset);
				view.setAttribute("notifications", notifications);
				view.forward("/pages/cabinet/notification/group_notifications.jsp");
			}else if("REVIEW".equals(type)){
				List<ReviewNotificationDto> notifications = notificationService.getReviewNotificationsByUserId(userID, count, offset);
				view.setAttribute("notifications", notifications);
				view.forward("/pages/cabinet/notification/review_notifications.jsp");
			}else if("ISSUE".equals(type)){
				List<IssueNotificationDto> notifications = notificationService.getIssueNotificationsByUserId(userID, count, offset);
				view.setAttribute("notifications", notifications);
				view.forward("/pages/cabinet/notification/issue_notifications.jsp");
			}else if("REPLY".equals(type)){
				List<ReplyNotificationDto> notifications = notificationService.getReplyNotificationsByUserId(userID, count, offset);
				view.setAttribute("notifications", notifications);
				view.forward("/pages/cabinet/notification/reply_notifications.jsp");
			}else if("FILE".equals(type)){
				List<FileNotificationDto> notifications = notificationService.getFileNotificationsByUserId(userID, count, offset);
				view.setAttribute("notifications", notifications);
				view.forward("/pages/cabinet/notification/file_notifications.jsp");
			}
			
		} catch (SessionIsNotAvailableException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/confirmGroup", method = RequestMethod.POST)
	public void confirmGroup(View view) throws ServletException, IOException {
		setGroupUserStatus(view, "CONFIRMED");
	}

	@RequestMapping(value = "/rejectGroup", method = RequestMethod.POST)
	public void rejectGroup(View view) throws ServletException, IOException {
		setGroupUserStatus(view, "REJECTED");
	}

	private void setGroupUserStatus(View view, String status) {
		GroupUserService groupUserService = new GroupUserService();

		try {
			String groupId = view.getParameter("groupId");
			Integer id = Integer.parseInt(groupId);

			GroupUser groupUser = groupUserService.getByGroupIdAndUserId(id,
					view.getSessionUserID());
			groupUser.setStatus(status);
			groupUserService.update(groupUser);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@RequestMapping(value = "/confirmReview", method = RequestMethod.POST)
	public void confirmReview(View view) throws ServletException, IOException {
		setReviewUserStatus(view, "CONFIRMED");
	}

	@RequestMapping(value = "/rejectReview", method = RequestMethod.POST)
	public void rejectReview(View view) throws ServletException, IOException {
		setReviewUserStatus(view, "REJECTED");
	}

	private void setReviewUserStatus(View view, String status) {
		CodeReviewUserService reviewUserService = new CodeReviewUserService();

		try {
			String reviewId = view.getParameter("reviewId");
			Integer id = Integer.parseInt(reviewId);

			CodeReviewUser reviewUser = reviewUserService
					.getByReviewIdAndUserId(id, view.getSessionUserID());
			reviewUser.setStatus(status);
			reviewUserService.update(reviewUser);
		} catch (NullPointerException e) {
			LOG.warn("confirmed or rejected review that was deleted by owner");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/confirmIssue", method = RequestMethod.POST)
	public void confirmIssue(View view) throws ServletException, IOException {
		IssueService issueService = new IssueService();

		try {
			String issueId = view.getParameter("issueId");
			Integer id = Integer.parseInt(issueId);

			issueService.setNotified(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/confirmFile", method = RequestMethod.POST)
	public void confirmFile(View view) throws ServletException, IOException {
		FileUserService fileUserService = new FileUserService();

		try {
			String reviewId = view.getParameter("reviewId");
			Integer id = Integer.parseInt(reviewId);
			fileUserService.setNotified(view.getSessionUserID(), id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/confirmReply", method = RequestMethod.POST)
	public void confirmReply(View view) throws ServletException, IOException {
		NotificationOnReplyService onReplyService = new NotificationOnReplyService();

		try {
			String reviewId = view.getParameter("replyId");
			Integer id = Integer.parseInt(reviewId);
			onReplyService.setNotified(view.getSessionUserID(), id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
