package com.epam.codereview.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.epam.codereview.archive.ArchiveUploader;
import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.git.GitHub;
import com.epam.codereview.git.GitURLWorker;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.RequestMethod;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.Group;
import com.epam.codereview.model.User;
import com.epam.codereview.project.ProjectHandler;
import com.epam.codereview.service.CodeReviewService;
import com.epam.codereview.service.GroupService;
import com.epam.codereview.service.ProjectService;
import com.epam.codereview.service.UserService;
import com.epam.codereview.tree.TreeBuilder;

@Controller
public class CodeReviewCreateController {
	private final static Logger LOG = Logger
			.getLogger(CodeReviewCreateController.class);
	private static String projectsPath;

	@RequestMapping(value = "/review_create")
	public void startCreating(View view) throws ServletException, IOException {
		view.getRequest()
				.getRequestDispatcher(
						"/pages/reviewCreating/reviewCreating.jsp")
				.forward(view.getRequest(), view.getResponse());
	}

	@RequestMapping(value = "/groupcheck", method = RequestMethod.POST )
	public void hasGroup(View view) throws SessionIsNotAvailableException, IOException{
		Integer userId = view.getSessionUserID();
		view.getResponse().setContentType("application/text");
		view.getResponse().setCharacterEncoding("UTF-8");
		if(Integer.valueOf(view.getParameter("groupId")) != 0) view.getResponse().getWriter().write("succes");
		try{
			List<Group> groups = new GroupService().getByUserID(userId);
			if(groups.size() == 0){
				view.getResponse().getWriter().write("error");
			}else{
				GroupService groupService = new GroupService();
				boolean isEmpty = true;
				for(Group group : groups){
					if(groupService.getAllUsersFromGroup(group.getId()).size() != 0){
						isEmpty = false;
						break;
					}
				}
				view.getResponse().getWriter().write(isEmpty ? "error":"succes");
			}
		}catch(Exception e){
			e.printStackTrace();
			view.getResponse().getWriter().write("error");
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/archive", method = RequestMethod.POST )
	public void archive(View view) throws ServletException, IOException, SessionIsNotAvailableException {
		if (projectsPath == null){
			projectsPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
		}
		view.getResponse().setContentType("application/json");
		view.getResponse().setCharacterEncoding("UTF-8");
		JSONObject jsonObject = new JSONObject();
		try{
			ArchiveUploader archiveUploader = new ArchiveUploader(view);
			String project = archiveUploader.uploadProject();
			jsonObject.put("status", "succes");
			jsonObject.put("project", project);
			jsonObject.put("projectName", archiveUploader.getProjectName());
		}catch(FileNotFoundException e){
			jsonObject.put("status", "non_zip");
		}catch(Exception e){
			jsonObject.put("status", "wrong");
		}
		view.getResponse().getWriter().write(jsonObject.toString());
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/git_branches", method = RequestMethod.POST)
	public void getBranches(View view) throws ServletException, IOException {
		if (projectsPath == null)
			projectsPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
		String url = view.getParameter("url");
		String password = view.getParameter("password");
		view.getResponse().setContentType("application/json");
		view.getResponse().setCharacterEncoding("UTF-8");
		try {
			JSONArray jsonBranches = new JSONArray();
			for (String branch : GitHub.getBranches(url, password == null ? ""
					: password)) {
				jsonBranches.add(branch);
			}
			view.getResponse().getWriter().write(jsonBranches.toString());
		} catch (Exception e) {
			JSONObject jsonErrorObject = new JSONObject();
			if (e.getLocalizedMessage() != null
					&& e.getLocalizedMessage().contains("not authorized")) {
				jsonErrorObject.put("status", "auth_error");
			} else {
				jsonErrorObject.put("status", "no_repository");
			}
			view.getResponse().getWriter().write(jsonErrorObject.toString());
		}
	}

	@RequestMapping(value = "/git_downloader", method = RequestMethod.GET)
	public void cloneProjectFromPublicRepo(View view) throws IOException {
		if (projectsPath == null)
			projectsPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
		String url = view.getRequest().getParameter("url");
		String branch = view.getRequest().getParameter("branch");
		view.getResponse().setContentType("application/text");
		view.getResponse().setCharacterEncoding("UTF-8");
		try {
			GitHub git = new GitHub(url, projectsPath, view.getSessionUserID());
			String parentFolderName = git.cloneFromPublicRepository(branch);
			view.getResponse().getWriter().write(parentFolderName);
		} catch (Exception e) {
			LOG.error(e);
		}
	}

	@RequestMapping(value = "/git_downloader", method = RequestMethod.POST)
	public void cloneProjectFromPrivateRepo(View view) throws IOException {
		if (projectsPath == null)
			projectsPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
		String url = view.getRequest().getParameter("url");
		String password = view.getRequest().getParameter("password");
		String branch = view.getRequest().getParameter("branch");
		view.getResponse().setContentType("application/text");
		view.getResponse().setCharacterEncoding("UTF-8");
		try {
			GitURLWorker gitUrl = new GitURLWorker(url);
			GitHub git = new GitHub(gitUrl, projectsPath, view.getSessionUserID());
			String parentFolderName = git.cloneFromPrivateRepository(branch, password);
			parentFolderName += "," + gitUrl.getProjectName();
			view.getResponse().getWriter().write(parentFolderName);
		} catch (Exception e) {
			LOG.error(e);
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/tree", method = RequestMethod.GET)
	public void buildTree(View view) throws IOException,
			SessionIsNotAvailableException, SQLException {
		String project = view.getRequest().getParameter("project");
		String projectName = view.getRequest().getParameter("projectName");
		TreeBuilder treeBuilder = new TreeBuilder(project, projectName,
				projectsPath, view.getSessionUserID());
		String tree = treeBuilder.buildCheckBoxTree();
		User user = new UserService().getByID(view.getSessionUserID());
		long usedMemory = new ProjectService().getUsedMemoryByUser(user.getId());
		long enabledMemory = user.getEnabledMemory();
		String memory = " / " + ProjectHandler.convertBytesInMegaBytes(enabledMemory) + " MB";
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("tree", tree);
		jsonObject.put("memory", memory);
		jsonObject.put("usedMemory", usedMemory);
		jsonObject.put("enabledMemory", enabledMemory);
		jsonObject.put("memoryPercentage", ((double) usedMemory / (double) enabledMemory) * 100);
		view.getResponse().setContentType("application/json");
		view.getResponse().setCharacterEncoding("UTF-8");
		view.getResponse().getWriter().write(jsonObject.toString());
	}

	@RequestMapping(value = "/groups_by_user", method = RequestMethod.GET)
	public void getGroupsByUser(View view) {
		try {
			String groups = "";
			for (Group group : new GroupService().getByUserID(view.getSessionUserID())) {
				groups += "<option id='" + group.getId() + "'>"
						+ group.getName() + "</option>";
			}
			view.getResponse().setContentType("application/text");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write(groups);
		} catch (Exception e) {
			LOG.error("empty group list by code review creating with unknown error");
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/users_by_selected_group", method = RequestMethod.GET)
	public void getUsersBySelectedGroup(View view) {
		try {
			Integer currentUser = new Integer(view.getSessionUserID());
			JSONArray jsonArray = new JSONArray();
			JSONObject jsonObject;
			for (User user : new UserService().getByGroup(new Integer(view
					.getParameter("groupID")))) {
				if (user.getId().equals(currentUser))
					continue;
				jsonObject = new JSONObject();
				jsonObject.put("id", user.getId());
				jsonObject.put("name",
						user.getFirstName() + " " + user.getLastName() + " ("
								+ user.getLogin() + ")");
				jsonArray.add(jsonObject);
			}
			view.getResponse().setContentType("application/json");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write(jsonArray.toString());
		} catch (Exception e) {
			LOG.error("empty group list by code review creating with unknown error");
		}
	}

	@RequestMapping(value = "/create_code_review", method = RequestMethod.POST)
	public void createReview(View view) throws SessionIsNotAvailableException,
			IOException {
		view.setAttribute("threadToExit" + view.getSessionUserID(), Thread.currentThread());
		new CodeReviewService().createCodeReview(view, projectsPath);
	}

	@RequestMapping(value = "/cancel", method = RequestMethod.GET)
	public void cancelCodeReviewCreatin(View view)
			throws SessionIsNotAvailableException, IOException {
		Object thread = view.getParameter("threadToExit" + view.getSessionUserID());
		if(thread != null){
			((Thread) thread).interrupt();
			view.getRequest().getSession().removeAttribute("threadToExit" + view.getSessionUserID());
		}
		String projectFolder = view.getParameter("projectFolder");
		if (projectFolder == null || projectFolder.equals(""))
			return;
		ProjectHandler projectHandler = new ProjectHandler(null, projectsPath);
		projectHandler.removeUncheckedFiles(view.getSessionUserID() + "/"
				+ projectFolder);
		new File(projectsPath + view.getSessionUserID() + "/" + projectFolder)
				.delete();
	}
}
