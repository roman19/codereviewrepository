package com.epam.codereview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.epam.codereview.dao.ReplyLikeDao;
import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.dto.LikeDto;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.View;
import com.google.gson.Gson;

@Controller
public class ReplyLikeController {


	private DaoManager daoManager = new DaoManager();

	@RequestMapping("/decrementLikeUpReply")
	public void decrementLikeUp(View view) throws IOException{		
		int reply=Integer.valueOf(view.getRequest().getParameter("ReplyId"));
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");

        daoManager.executeVoidAndClose(() -> daoManager.getReplyLikeDao().decrementLikeOnReply(userId, reply));

        Integer totalLikes = daoManager.executeAndClose(() ->
                daoManager.getReplyLikeDao().countTotalLike(reply));
		
		view.getResponse().setContentType("application/text");
		view.getResponse().getWriter().write(String.valueOf(totalLikes.toString()));
	}
	
	
	@RequestMapping("/incrementLikeUpReply")
	public void incrementLikeUp(View view) throws IOException{	
		int reply=Integer.valueOf(view.getRequest().getParameter("ReplyId"));
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");

        daoManager.executeVoidAndClose(() -> daoManager.getReplyLikeDao().likeOnReply(userId, reply));

        Integer totalLikes = daoManager.executeAndClose(() ->
                daoManager.getReplyLikeDao().countTotalLike(reply));
				
		view.getResponse().setContentType("application/text");
		view.getResponse().getWriter().write(String.valueOf(totalLikes.toString()));
	}
	
	
	@RequestMapping("/incrementLikeDownReply")
	public void incrementLikeDown(View view) throws IOException{		
		int reply=Integer.valueOf(view.getRequest().getParameter("ReplyId"));
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");

        daoManager.executeVoidAndClose(() -> daoManager.getReplyLikeDao().disLikeOnReply(userId, reply));

        Integer totalLikes = daoManager.executeAndClose(() ->
                daoManager.getReplyLikeDao().countTotalDisLike(reply));

		view.getResponse().setContentType("application/text");
		view.getResponse().getWriter().write(String.valueOf(totalLikes.toString()));
		
	}
	@RequestMapping("/decrementLikeDownReply")
	public void decrementLikeDown(View view) throws IOException{		
		int reply=Integer.valueOf(view.getRequest().getParameter("ReplyId"));
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");

        daoManager.executeVoidAndClose(() -> daoManager.getReplyLikeDao().decrementLikeOnReply(userId, reply));

        Integer totalLikes = daoManager.executeAndClose(() ->
                daoManager.getReplyLikeDao().countTotalDisLike(reply));

		view.getResponse().setContentType("application/text");
		view.getResponse().getWriter().write(String.valueOf(totalLikes.toString()));
	
	}
	
	
	@RequestMapping("/loadLikesReply")
	public void loadLikes(View view) throws IOException{		
		int reply=Integer.valueOf(view.getRequest().getParameter("ReplyId"));
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");

        Integer totalLikes = daoManager.executeAndClose(() ->
                daoManager.getReplyLikeDao().countTotalLike(reply));
        Integer totalDisLikes = daoManager.executeAndClose(() ->
                daoManager.getReplyLikeDao().countTotalDisLike(reply));
        Integer likeValue = daoManager.executeAndClose(() ->
                daoManager.getReplyLikeDao().getValueOfLike(userId, reply));
		
		LikeDto like=new LikeDto();
		like.setTotalLikes(totalLikes);
		like.setTotalDisLikes(totalDisLikes);
		like.setLikeValue(likeValue);
		
		Gson gson=new Gson();
		String likesInfo=gson.toJson(like);
		System.out.println(likesInfo);
		
		view.getResponse().setContentType("application/json");
		view.getResponse().getWriter().write(String.valueOf(likesInfo));
	
	}

	@RequestMapping("/loadReplyIds")
	public void loadReplyIds(View view) throws IOException{		
		int issueId=Integer.valueOf(view.getRequest().getParameter("IssueId"));
		
		List <Integer> list = new ArrayList<>(daoManager.executeAndClose(() ->
                                daoManager.getReplyLikeDao().getAllReplyId(issueId)));

        Object [] array=list.toArray();
		
		System.out.println("array lingth " +array.length);
		if(array.length>0){
		Gson gson=new Gson();
		String likesInfo=gson.toJson(array);
		System.out.println(likesInfo);
		
		view.getResponse().setContentType("application/json");
		view.getResponse().getWriter().write(likesInfo);
		
		}	
		
	}

}
