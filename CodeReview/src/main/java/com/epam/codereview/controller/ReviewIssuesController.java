package com.epam.codereview.controller;

import com.epam.codereview.dto.IssueDto;
import com.epam.codereview.dto.IssueForTabDto;
import com.epam.codereview.dto.IssueNotificationDto;
import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.RequestMethod;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.CodeReview;
import com.epam.codereview.model.Issue;
import com.epam.codereview.service.*;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

@Controller
@RequestMapping(value = "/review/issues")
public class ReviewIssuesController {

    private final static Logger LOG = Logger.getLogger(ReviewIssuesController.class);

    private ReviewService reviewService = new ReviewService();
    private UserService userService = new UserService();
    private ReplyService replyService = new ReplyService();

    @RequestMapping(value = "", method = RequestMethod.POST)
    public void showIssues(View view) throws ServletException, IOException {
        try {
            Integer reviewId = Integer.valueOf(view.getParameter("reviewId"));
            Integer currentUserId = view.getSessionUserID();
            if (reviewService.hasRightsForReview(reviewId, currentUserId)) {
                String reviewsServerPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
                List<IssueForTabDto> issueForTabDtoList = reviewService.getIssuesForIssuesTab(reviewId, currentUserId, reviewsServerPath, null, null);
                CodeReview codeReview = reviewService.get(reviewId);
                view.setAttribute("reviewId", reviewId);
                view.setAttribute("isActiveReview", codeReview.isActive());
                view.setAttribute("issues", issueForTabDtoList);
                view.setAttribute("languages", reviewService.getLanguagesWithIssues(reviewId));
                view.getRequest().getRequestDispatcher("/pages/review/issues/issues.jsp").forward(view.getRequest(), view.getResponse());
            } else {
                view.getResponse().sendError(403);
            }
        } catch (NullPointerException | NumberFormatException | SessionIsNotAvailableException e) {
            LOG.error("Error", e);
            view.getResponse().sendError(404);
        }
    }

    @RequestMapping(value = "/filter", method = RequestMethod.POST)
    public void filterIssues(View view) throws ServletException, IOException {
        try {
            Integer reviewId = Integer.valueOf(view.getParameter("reviewId"));
            Integer currentUserId = view.getSessionUserID();
            Integer langId = Integer.valueOf(view.getParameter("langId"));
            String issueType = view.getParameter("issueType");

            if (reviewService.hasRightsForReview(reviewId, currentUserId)) {
                String reviewsServerPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
                List<IssueForTabDto> issueForTabDtoList = reviewService.getIssuesForIssuesTab(reviewId, currentUserId, reviewsServerPath, langId, issueType);
                CodeReview codeReview = reviewService.get(reviewId);
                view.setAttribute("reviewId", reviewId);
                view.setAttribute("isActiveReview", codeReview.isActive());
                view.setAttribute("issues", issueForTabDtoList);
                view.getRequest().getRequestDispatcher("/pages/review/issues/issues_list.jsp").forward(view.getRequest(), view.getResponse());
            } else {
                view.getResponse().sendError(403);
            }
        } catch (NullPointerException | NumberFormatException | SessionIsNotAvailableException e) {
            LOG.error("Error", e);
            view.getResponse().sendError(404);
        }
    }

    @RequestMapping(value = "/edit_issue", method = RequestMethod.POST)
    public void editIssue(View view) throws ServletException, IOException {
        try {
            Integer reviewId = Integer.valueOf(view.getParameter("reviewId"));
            Integer currentUserId = view.getSessionUserID();
            CodeReview currentCodeReview = reviewService.get(reviewId);
            if (reviewService.hasRightsForReview(reviewId, currentUserId)) {
                String issueIdParameter = view.getParameter("issueId");
                String issueType = view.getParameter("type");
                String issueTitle = view.getParameter("title").replaceAll("&nbsp;", " ").replaceAll("<", "&lt;").trim();
                if (issueTitle.isEmpty()) {
                    view.getResponse().sendError(404);
                    return;
                }
                String issueBody = view.getParameter("body").replaceAll("<p>(\\s*(&nbsp;)+\\s*)+</p>", "").trim();
                if (issueBody.matches("(\\s*(&nbsp;)+\\s*)+")) issueBody = "";
                Issue issue = reviewService.getIssue(Integer.valueOf(issueIdParameter));
                if (issue.getUserId().equals(currentUserId)) {
                    issue.setType(issueType);
                    issue.setTitle(issueTitle);
                    issue.setBody(issueBody.isEmpty() ? null : issueBody);
                    issue.setTime(new Timestamp(System.currentTimeMillis()));
                    reviewService.updateIssue(issue);
                }

//                IssueDto issueDto = new IssueDto(issue, userService.get(currentUserId), currentCodeReview, userService.getUserIdByFileId(issue.getFileId()));
//                issueDto.setIsEditable(true);

                IssueForTabDto issueForTabDto = new IssueForTabDto(issue, userService.get(currentUserId), replyService.getRepliesByIssueId(issue.getId()));
                issueForTabDto.setIsEditable(true);

//                view.setAttribute("issue", issueDto);
                view.setAttribute("issue", issueForTabDto);
                view.setAttribute("isActiveReview", currentCodeReview.isActive());
                view.getRequest().getRequestDispatcher("/pages/review/issues/issue_item.jsp").forward(view.getRequest(), view.getResponse());

                if ("ERROR".equals(issue.getType())) {

                    NotificationService notificationService = new NotificationService();
                    FileService fileService = new FileService();
                    IssueNotificationDto notificationDto = new IssueNotificationDto();
                    notificationDto.setUser(userService.get(currentUserId));
                    notificationDto.setIssueId(issue.getId());
                    notificationDto.setIssueTitle(issue.getTitle());
                    notificationDto.setIssueBody(issue.getBody());
                    notificationDto.setReviewId(currentCodeReview.getId());
                    notificationDto.setReviewName(currentCodeReview.getName());
                    notificationDto.setFileId(issue.getFileId());
                    String[] parts = fileService.get(issue.getFileId()).getUrl().split("/");
                    String fileName = parts[parts.length - 1];
                    notificationDto.setFileName(fileName);
                    notificationDto.setDate(issue.getTime());

                    try {  //if error, we still need to post issue!!!!
                        notificationService.sendIssueNotification(currentCodeReview.getOwnerId(), notificationDto);
                    } catch (Exception e) {
                        LOG.error("Message not sent about ERROR issue creation", e);
                    }
                }
            }
        } catch (NullPointerException | SessionIsNotAvailableException e) {
            LOG.error(e);
            view.getResponse().sendError(404);
        }

    }

}
