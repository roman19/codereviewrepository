package com.epam.codereview.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.RequestMethod;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.service.GroupService;

@Controller
@RequestMapping(value = "/cabinet")
public class DeleteGroupController {
	private static String projectsPath;

	@RequestMapping(value = "/group_delete_user", method=RequestMethod.POST)
	public void getReviewGroupConcreteHistory(View view) throws IOException, ServletException {
		if (projectsPath == null){
			projectsPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
		}
		String groupId = view.getRequest().getParameter("groupId");
		String userId = view.getRequest().getParameter("userId");
		GroupService groupService = new GroupService();
		groupService.removeUser(Integer.valueOf(groupId), Integer.valueOf(userId), projectsPath);
	}

	@RequestMapping(value = "/group_leave", method=RequestMethod.POST)
	public void deleteGroup(View view) throws ServletException, IOException {
		if (projectsPath == null){
			projectsPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
		}
		HttpSession session = view.getRequest().getSession();
		Integer userId = (Integer) session.getAttribute("userID");
		String groupId = view.getRequest().getParameter("groupId");
		GroupService groupService = new GroupService();
		groupService.exitGroup(Integer.valueOf(groupId), userId, projectsPath);
	}

	@RequestMapping(value = "/group_delete", method=RequestMethod.POST)
	public void exitGroup(View view) throws ServletException, IOException {
		if (projectsPath == null){
			projectsPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
		}
		String groupId = view.getRequest().getParameter("groupId");
		GroupService groupService = new GroupService();
		groupService.deleteGroup(Integer.valueOf(groupId), projectsPath);
	}
}
