package com.epam.codereview.controller;

import com.epam.codereview.dto.CodeReviewDto;
import com.epam.codereview.dto.FilePairDto;
import com.epam.codereview.dto.IssueDto;
import com.epam.codereview.dto.IssueNotificationDto;
import com.epam.codereview.exception.SessionIsNotAvailableException;
import com.epam.codereview.manager.servlet.Controller;
import com.epam.codereview.manager.servlet.RequestMapping;
import com.epam.codereview.manager.servlet.RequestMethod;
import com.epam.codereview.manager.servlet.View;
import com.epam.codereview.model.Branch;
import com.epam.codereview.model.CodeReview;
import com.epam.codereview.model.File;
import com.epam.codereview.model.Issue;
import com.epam.codereview.service.*;
import com.epam.codereview.tree.TreeBuilder;
import com.epam.codereview.util.FileUtil;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/review")
public class ReviewController {

    private final static Logger LOG = Logger.getLogger(ReviewController.class);

	private ReviewService reviewService = new ReviewService();
	private UserService userService = new UserService();
	private ReviewChatService chatService = new ReviewChatService();


    @RequestMapping(value = "/*")
    public void review(View view) throws ServletException, IOException {
        try {
            Integer reviewId = Integer.valueOf(view.getLastUrlParam());
            Integer currentUserId = view.getSessionUserID();
            if (reviewService.hasRightsForReview(reviewId, currentUserId)) {
                String reviewsServerPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
                CodeReviewDto codeReviewDto = reviewService.getCodeReviewDto(reviewId, reviewsServerPath);

                //for chat
                if (codeReviewDto.getCodeReview().getOwnerId().equals(view.getSessionUserID())) {
                    Integer countNewMessages = chatService.getCountOfNewMessagesForOwner(currentUserId, reviewId);
                    view.setAttribute("countNewMessages", countNewMessages);
                } else {
                    Integer countNewMessages = chatService.getCountOfNewMessagesForUser(currentUserId, reviewId);
                    view.setAttribute("countNewMessages", countNewMessages);
                }
                //end for chat

                view.setAttribute("reviewId", reviewId);
                view.setAttribute("codeReviewName", codeReviewDto.getCodeReview().getName());
                view.setAttribute("projectName", codeReviewDto.getProject().getName());
                view.setAttribute("isProjectFromGitRepository", codeReviewDto.getProject().getGitUrl());
                Branch branch = codeReviewDto.getBranch();
                view.setAttribute("branchName", branch == null ? null : branch.getName());
                view.setAttribute("isActiveReview", codeReviewDto.getCodeReview().isActive());
                view.setAttribute("isOwnerReview", reviewService.isReviewOwner(reviewId, currentUserId));
                view.setAttribute("isReviewer", reviewService.isReviewer(reviewId, currentUserId));

                // for dashboard params
                view.setAttribute("reviewOwner", userService.get(codeReviewDto.getCodeReview().getOwnerId()));
                view.setAttribute("reviewUsers", reviewService.getCodeReviewUsers(reviewId));
                view.setAttribute("totalReviewPercent", codeReviewDto.getCoveragePercentTotal());
                view.setAttribute("errorIssuesCount", reviewService.getReviewIssuesByType(reviewId, "ERROR").size());
                view.setAttribute("warningIssuesCount", reviewService.getReviewIssuesByType(reviewId, "WARNING").size());
                view.setAttribute("infoIssuesCount", reviewService.getReviewIssuesByType(reviewId, "INFO").size());
                view.setAttribute("totalIssuesCount", codeReviewDto.getIssuesCount());
                view.setAttribute("totalFilesCount", codeReviewDto.getFileDtos().size());
                Map<String, Integer> languageFileStatistics = reviewService.getLanguageFileReviewStatistics(reviewId);
                if (languageFileStatistics != null) {
                    String json = new Gson().toJson(languageFileStatistics);
                    view.setAttribute("langData", json);
                }
                // for dashboard params end
                view.getRequest().getRequestDispatcher("/pages/review/review.jsp")
                        .forward(view.getRequest(), view.getResponse());
            } else {
                view.getResponse().sendError(404);
            }
        } catch (NullPointerException | NumberFormatException | SessionIsNotAvailableException e) {
        	e.printStackTrace();
            LOG.error(e);
            view.getResponse().sendError(404);
        }
    }

    @RequestMapping(value = "/finish")
    public void reviewFinish(View view) throws ServletException, IOException {
        try {
            Integer reviewId = Integer.valueOf(view.getParameter("id"));
            Integer currentUserId = view.getSessionUserID();
            if (reviewService.isReviewOwner(reviewId, currentUserId)) {
                reviewService.finishReview(reviewId);
                view.getResponse().sendRedirect(view.getRequest().getHeader("Referer"));
            } else {
                view.getResponse().sendError(403);
            }
        }catch (NullPointerException | NumberFormatException | SessionIsNotAvailableException e) {
            LOG.error(e);
            view.getResponse().sendError(404);
        }
    }

    @RequestMapping(value = "/delete")
    public void reviewDelete(View view) throws ServletException, IOException {
        try {
            Integer reviewId = Integer.valueOf(view.getParameter("id"));
            Integer currentUserId = view.getSessionUserID();
            if (reviewService.isReviewOwner(reviewId, currentUserId)) {
                reviewService.deleteReview(reviewId, 
                		view.getRequest().getServletContext().getInitParameter("review-project-upload"));
                view.getResponse().sendRedirect(view.getRequest().getHeader("Referer"));
            } else {
                view.getResponse().sendError(403);
            }
        }catch (NullPointerException | NumberFormatException | SessionIsNotAvailableException e) {
            LOG.error(e);
            view.getResponse().sendError(404);
        }
    }

    @RequestMapping(value = "/project", method = RequestMethod.POST)
    public void showProject(View view) throws ServletException, IOException {
        try {
            Integer reviewId = Integer.valueOf(view.getParameter("reviewId"));
            Integer currentUserId = view.getSessionUserID();
            if (reviewService.hasRightsForReview(reviewId, currentUserId)) {
                String reviewsServerPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
                CodeReviewDto codeReviewDto = reviewService.getCodeReviewDto(reviewId, reviewsServerPath);

                String project = codeReviewDto.getProject().getFullName();
                String projectName = codeReviewDto.getProject().getName();

                TreeBuilder treeBuilder = new TreeBuilder(project, projectName, reviewsServerPath, codeReviewDto.getCodeReview().getOwnerId(), currentUserId);

                view.setAttribute("reviewTree", treeBuilder.buildReviewTree());
                view.setAttribute("projectName", codeReviewDto.getProject().getName());
                view.getRequest().getRequestDispatcher("/pages/review/review_files.jsp")
                        .forward(view.getRequest(), view.getResponse());
            }
        } catch (NullPointerException | NumberFormatException | SessionIsNotAvailableException e) {
            LOG.error("Error while load project in review", e);
            view.getResponse().sendError(404);
        }
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/file", method = RequestMethod.POST)
    public void showFileContent(View view) throws ServletException, IOException {  //TODO for all /file/.. check  if is active review and user rights, send review id from client always
        try {
            Integer fileId = null;
            String brush = null;

            String fileUrl = view.getRequest().getParameter("relativePath");
            File file = reviewService.getFileByUrl(fileUrl);
            fileId = file.getId();
            brush = reviewService.getLanguage(file.getLanguageId()).getBrush();

            String fileFullPath = view.getRequest().getParameter("fullPath");
            String fileContent = FileUtil.readFileInString(fileFullPath);
            List<IssueDto> issueDtos = reviewService.getFileIssues(fileId, view.getSessionUserID());
            JSONObject obj = new JSONObject();
            obj.put("id", fileId);
            obj.put("content", fileContent);
            obj.put("brush", brush == null ? "plain" : brush);
            obj.put("issues", new Gson().toJson(issueDtos));
            view.getResponse().setContentType("application/json");
            view.getResponse().setCharacterEncoding("UTF-8");
            view.getResponse().getWriter().write(obj.toJSONString());
        } catch (NullPointerException | SessionIsNotAvailableException e) {
            LOG.error(e);
            view.getResponse().sendError(404);
        }
    }

    @RequestMapping(value = "/file/reviewed", method = RequestMethod.POST)
    public void fileReviewed(View view) throws ServletException, IOException {
        try {
            Integer fileId = Integer.valueOf(view.getParameter("fileId"));
            reviewService.markFileAsReviewed(fileId, view.getSessionUserID());

        } catch (NullPointerException | NumberFormatException | SessionIsNotAvailableException e) {
            LOG.error(e);
        }
    }
    
    @RequestMapping(value = "/file/issue", method = RequestMethod.POST)
    public void saveIssue(View view) throws ServletException, IOException {
        try {
            String issueIdParameter = view.getParameter("issueId");
            String issueType = view.getParameter("type");
            String issueTitle = view.getParameter("title").replaceAll("&nbsp;", " ").replaceAll("<", "&lt;").trim();
            if (issueTitle.isEmpty()) {
                view.getResponse().sendError(404);
                return;
            }
            String issueBody = view.getParameter("body").replaceAll("<p>(\\s*(&nbsp;)+\\s*)+</p>", "").trim();
            if (issueBody.matches("(\\s*(&nbsp;)+\\s*)+")) issueBody = "";
            int currentUserId = view.getSessionUserID();
            Issue issue = null;
            if (issueIdParameter != null) {
                Integer issueId = Integer.valueOf(issueIdParameter);
                issue = reviewService.getIssue(issueId);
                if (issue.getUserId().equals(currentUserId)) {
                    issue.setType(issueType);
                    issue.setTitle(issueTitle);
                    issue.setBody(issueBody.isEmpty() ? null : issueBody);
                    issue.setTime(new Timestamp(System.currentTimeMillis()));
                    reviewService.updateIssue(issue);
                }
            } else {
                issue = new Issue();
                issue.setUserId(currentUserId);
                issue.setFileId(Integer.valueOf(view.getRequest().getParameter("fileId")));
                issue.setLineFrom(Integer.valueOf(view.getRequest().getParameter("lineFrom")));
                issue.setLineTo(Integer.valueOf(view.getRequest().getParameter("lineTo")));
                issue.setType(issueType);
                issue.setTitle(issueTitle);
                issue.setBody(issueBody.isEmpty() ? null : issueBody);
                issue.setTime(new Timestamp(System.currentTimeMillis()));
                issue = reviewService.addIssue(issue);
            }
            view.getResponse().setContentType("application/json");
            view.getResponse().setCharacterEncoding("UTF-8");
            
            if (issue != null) {
            	CodeReviewService codeReviewService = new CodeReviewService();
            	CodeReview currentCodeReview = codeReviewService.getByFileId(issue.getFileId());
            	IssueDto issueDto = new IssueDto(issue, userService.get(currentUserId), currentCodeReview, userService.getUserIdByFileId(issue.getFileId()));
                issueDto.setIsEditable(true);
                JSONObject obj = new JSONObject();
                obj.put("issue", issueDto.toJSONObject());
                obj.put("issuesCount", reviewService.getCountIssuesByFile(issueDto.getFileId()));
                view.getResponse().getWriter().write(obj.toJSONString());

                if ("ERROR".equals(issue.getType())) {

                	NotificationService notificationService = new NotificationService();
                    FileService fileService = new FileService();
                    IssueNotificationDto notificationDto = new IssueNotificationDto();
                    notificationDto.setUser(userService.get(currentUserId));
                    notificationDto.setIssueId(issue.getId());
                    notificationDto.setIssueTitle(issue.getTitle());
                    notificationDto.setIssueBody(issue.getBody());
                    CodeReview codeReview = codeReviewService.getByFileId(issue.getFileId());
                    notificationDto.setReviewId(codeReview.getId());
                    notificationDto.setReviewName(codeReview.getName());
                    notificationDto.setFileId(issue.getFileId());
                    String[] parts = fileService.get(issue.getFileId()).getUrl().split("/");
                    String fileName = parts[parts.length - 1];
                    notificationDto.setFileName(fileName);
                    notificationDto.setDate(issue.getTime());
                    
                    //if error, we still need to post issue (Exception handling inside NotificationService)!!!!
                    notificationService.sendIssueNotification(codeReview.getOwnerId(), notificationDto);
                }
            } else {
                LOG.error("Cannot find saved issue.");
                view.getResponse().sendError(500);
            }
        } catch (NullPointerException | SessionIsNotAvailableException e) {
            LOG.error(e);
            view.getResponse().sendError(404);
        }
    }

    

    @RequestMapping(value = "/file/issue/delete", method = RequestMethod.POST)
    public void deleteIssue(View view) throws ServletException, IOException {
        try {
            Integer issueId = Integer.valueOf(view.getParameter("issueId"));
            Issue issueForRemove = reviewService.getIssue(issueId);
            if (issueForRemove.getUserId().equals(view.getSessionUserID())) {
                reviewService.removeIssue(issueId);
                view.getResponse().setContentType("application/json");
                view.getResponse().setCharacterEncoding("UTF-8");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("isRemoved", reviewService.getIssue(issueId) == null);
                jsonObject.put("issuesCount", reviewService.getCountIssuesByFile(issueForRemove.getFileId()));
                jsonObject.put("fileId", issueForRemove.getFileId());
                view.getResponse().getWriter().write(jsonObject.toJSONString());
            } else {
                view.getResponse().sendError(403);
            }
        } catch (NullPointerException | SessionIsNotAvailableException e) {
            LOG.error(e);
            view.getResponse().sendError(404);
        }
    }

    @RequestMapping(value = "/file/issue/edit", method = RequestMethod.POST)
    public void editIssue(View view) throws ServletException, IOException {
        try {
            Integer issueId = Integer.valueOf(view.getParameter("issueId"));
            Issue issue = reviewService.getIssue(issueId);

            if (issue.getUserId().equals(view.getSessionUserID())) {
                view.getResponse().setContentType("application/json");
                view.getResponse().setCharacterEncoding("UTF-8");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("title", issue.getTitle());
                jsonObject.put("body", issue.getBody());
                jsonObject.put("lines", new Gson().toJson(issue.getLines()));
                view.getResponse().getWriter().write(jsonObject.toJSONString());
            } else {
                view.getResponse().sendError(403);
            }
        } catch (NullPointerException | SessionIsNotAvailableException e) {
            LOG.error(e);
            view.getResponse().sendError(404);
        }
    }
    
    @RequestMapping(value = "/filePath", method = RequestMethod.POST)
    public void getFilePath(View view) throws ServletException, IOException {
        try {
        	Integer fileId = Integer.parseInt(view.getParameter("fileId"));
        	
        	String reviewsServerPath = view.getRequest().getServletContext().getInitParameter("review-project-upload");
        	
			FileService fileService = new FileService();
			File file = fileService.get(fileId);
			FilePairDto filePairDto = new FilePairDto(reviewsServerPath+file.getUrl(), file.getUrl());
			
			String json = new Gson().toJson(filePairDto);

			view.getResponse().setContentType("application/json");
			view.getResponse().setCharacterEncoding("UTF-8");
			view.getResponse().getWriter().write(json);
			
		} catch (Exception e) {
			LOG.error("Error during getting file path by fileId", e);
		}
    }

}