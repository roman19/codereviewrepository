package com.epam.codereview.dao;

import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.CodeReview;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class CodeReviewDao extends AbstractDAO<CodeReview>{

    private final static Logger LOG = Logger.getLogger(CodeReviewDao.class);

    private final String GET_ALL_REVIEWS_BY_GROUP_ID = "SELECT * FROM code_review "
            + "WHERE group_id = ? AND is_active = ? AND is_deleted = 0;";
    private final String GET_MY_REVIEWS_BY_GROUP_ID = "SELECT * FROM code_review "
            + "WHERE group_id = ? AND is_active = ?"
            + " AND user_id = ? AND is_deleted = 0;";
    private final String GET_OTHER_REVIEWS_BY_GROUP_ID = "SELECT * FROM code_review "
            + "WHERE group_id = ? AND is_active = ?"
            + " AND user_id != ? AND is_deleted = 0;";
    private final String GET_MY_REVIEWS_BY_USER_AND_GROUP = "SELECT * FROM code_review " + 
    		"WHERE user_id = ? AND group_id = ?";

    public CodeReviewDao(Connection connection) {
        super(CodeReview.class, connection);
    }
    
    public List<CodeReview> getMyReviewsByUserAndGroup(Integer userId, Integer groupId)
    		throws SQLException, InstantiationException, IllegalAccessException{
        PreparedStatement stmt = connection.prepareStatement(GET_MY_REVIEWS_BY_USER_AND_GROUP);
        stmt.setInt(1, userId);
        stmt.setInt(2, groupId);
        ResultSet rs = stmt.executeQuery();
        List<CodeReview> list = new ArrayList<>();
        CodeReview user;
        do {
            user = (CodeReview) Transformer.transformToObject(rs, CodeReview.class);
            if (user != null)
                list.add(user);
        } while (user != null);
        rs.close();
        stmt.close();
        return list;
    }

    public List<CodeReview> getAllReviewsByGroupId(Integer id, Boolean isReviewsActive)
            throws SQLException, InstantiationException, IllegalAccessException{
        PreparedStatement stmt = connection.prepareStatement(GET_ALL_REVIEWS_BY_GROUP_ID);
        stmt.setInt(1, id);
        stmt.setBoolean(2, isReviewsActive);
        ResultSet rs = stmt.executeQuery();
        List<CodeReview> list = new ArrayList<>();
        CodeReview user;
        do {
            user = (CodeReview) Transformer.transformToObject(rs, CodeReview.class);
            if (user != null)
                list.add(user);
        } while (user != null);
        rs.close();
        stmt.close();
        return list;
    }

    public List<CodeReview> getMyReviewsByGroupId(Integer groupId, Boolean isReviewsActive, Integer userId)
            throws SQLException, InstantiationException, IllegalAccessException{
        PreparedStatement stmt = connection.prepareStatement(GET_MY_REVIEWS_BY_GROUP_ID);
        stmt.setInt(1, groupId);
        stmt.setBoolean(2, isReviewsActive);
        stmt.setInt(3, userId);
        ResultSet rs = stmt.executeQuery();
        List<CodeReview> list = new ArrayList<>();
        CodeReview user;
        do {
            user = (CodeReview) Transformer.transformToObject(rs, CodeReview.class);
            if (user != null)
                list.add(user);
        } while (user != null);
        rs.close();
        stmt.close();
        return list;
    }

    public List<CodeReview> getOtherReviewsByGroupId(Integer groupId, Boolean isReviewsActive, Integer userId)
            throws SQLException, InstantiationException, IllegalAccessException{
        PreparedStatement stmt = connection.prepareStatement(GET_OTHER_REVIEWS_BY_GROUP_ID);
        stmt.setInt(1, groupId);
        stmt.setBoolean(2, isReviewsActive);
        stmt.setInt(3, userId);
        ResultSet rs = stmt.executeQuery();
        List<CodeReview> list = new ArrayList<>();
        CodeReview user;
        do {
            user = (CodeReview) Transformer.transformToObject(rs, CodeReview.class);
            if (user != null)
                list.add(user);
        } while (user != null);
        rs.close();
        stmt.close();
        return list;
    }

    public CodeReview getByFileId(Integer fileId) {
        CodeReview codeReview = null;

        String sqlQuery = "SELECT cr.* FROM file f JOIN branch b ON f.branch_id=b.id "
                + "JOIN project p ON p.id=b.project_id "
                + "JOIN code_review cr ON cr.id=p.code_review_id WHERE f.id=?";

        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setInt(1, fileId);
            ResultSet rs = pstmt.executeQuery();
            codeReview = Transformer.transformToObject(rs, CodeReview.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Sql query: " + sqlQuery + " didn't pass!", e);
        }

        return codeReview;
    }
}
