package com.epam.codereview.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.Project;

public class ProjectDao extends AbstractDAO<Project> {

    private final static Logger LOG = Logger.getLogger(ProjectDao.class);
    private final static String GET_USED_MEMORY_BY_USER = "SELECT sum(size) used_memory FROM project p " + 
																"JOIN code_review cr ON p.code_review_id = cr.id " + 
																	"WHERE cr.user_id = ?;";

    public ProjectDao(Connection connection) {
        super(Project.class, connection);
    }
    
    public Long getUsedMemoryByUser(Integer userId){
		try {
			PreparedStatement stmt = connection.prepareStatement(GET_USED_MEMORY_BY_USER);
	        stmt.setInt(1, userId);
	        ResultSet rs = stmt.executeQuery();
	        BigDecimal memory = null;
	        if(rs.next()){
	        	memory = (BigDecimal) rs.getObject("used_memory");
	        }
	        rs.close();
	        stmt.close();
	        return memory == null ? 0l:memory.longValue();
		} catch (SQLException e) {
			return 0l;
		}
    }
    
    public static void main(String[] args) throws SQLException {
    	DaoManager daoManager = new DaoManager();
		System.out.println(daoManager.getProjectDao().getUsedMemoryByUser(57));
	}

    public Project getByReviewId(int reviewId) {
		Project project = null;
		String sql = "SELECT * FROM project WHERE code_review_id = " + reviewId;
		try(Statement stmt = connection.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			project = Transformer.transformToObject(rs, Project.class);
		} catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
		}
		return project;
	}

}
