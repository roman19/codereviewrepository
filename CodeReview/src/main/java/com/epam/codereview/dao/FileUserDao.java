package com.epam.codereview.dao;

import com.epam.codereview.dto.FilePlainNotificationDto;
import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.FileUser;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FileUserDao extends AbstractDAO<FileUser> {

    private final static Logger LOG = Logger.getLogger(FileUserDao.class);

    private final static String COUNT_REVIEWED_FILES_SIZE_BY_REVIEW =
            "SELECT SUM(f.size) AS total_files_size FROM file_user fu " +
                    "JOIN `file` f ON fu.file_id = f.id " +
                    "JOIN branch b ON f.branch_id = b.id " +
                    "JOIN project p ON b.project_id = p.id " +
                    "WHERE fu.is_reviewed = b'1' AND p.code_review_id = ?";
    private final static String COUNT_REVIEWED_FILES_SIZE_BY_REVIEW_AND_USER = COUNT_REVIEWED_FILES_SIZE_BY_REVIEW + " AND fu.user_id = ?";

    public FileUserDao(Connection connection) {
        super(FileUser.class, connection);
    }

    public FileUser getByFileIdAndUserId(Integer fileId, Integer userId) {
        FileUser fileUser = null;
        String sql = "SELECT * FROM file_user WHERE file_id = " + fileId + " AND user_id = " + userId;
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            fileUser = Transformer.transformToObject(rs, FileUser.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return fileUser;
    }

    public FileUser getByFileUrlAndUserId(String fileUrl, Integer userId) {
        FileUser fileUser = null;
        String sql = "SELECT fu.* FROM file_user fu JOIN `file` f ON fu.file_id = f.id WHERE f.url LIKE '" + fileUrl + "' AND fu.user_id = " + userId;
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            fileUser = Transformer.transformToObject(rs, FileUser.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return fileUser;
    }

    public int getCountDistinctUser(Integer reviewId) {
        String sql = "SELECT COUNT(DISTINCT fu.user_id) as users_count FROM file_user fu " +
                "JOIN `file` f ON fu.file_id = f.id " +
                "JOIN branch b ON f.branch_id = b.id " +
                "JOIN project p ON b.project_id = p.id " +
                "WHERE p.code_review_id = " + reviewId;
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                int usersCount = rs.getInt("users_count");
                return usersCount == 0 ? 1 : usersCount;
            }
        } catch (SQLException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return 1;
    }

    public long getTotalReviewedFileSize(Integer reviewId, Integer userId) {
        try(PreparedStatement pstmt = connection.prepareStatement(COUNT_REVIEWED_FILES_SIZE_BY_REVIEW_AND_USER)) {
            pstmt.setInt(1, reviewId);
            pstmt.setInt(2, userId);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getLong("total_files_size");
            }
        } catch (SQLException e) {
            LOG.error("Query: " + COUNT_REVIEWED_FILES_SIZE_BY_REVIEW_AND_USER + " with code_review_id = " + reviewId + ", user_id = " + userId + " didn't pass.", e);
        }
        return 0;
    }

    public long getTotalReviewedFileSize(Integer reviewId) {
        try(PreparedStatement pstmt = connection.prepareStatement(COUNT_REVIEWED_FILES_SIZE_BY_REVIEW)) {
            pstmt.setInt(1, reviewId);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getLong("total_files_size");
            }
        } catch (SQLException e) {
            LOG.error("Query: " + COUNT_REVIEWED_FILES_SIZE_BY_REVIEW + " with reviewId = " + reviewId + " didn't pass.", e);
        }
        return 0;
    }

    public List<FilePlainNotificationDto> getNewFilesForUser(Integer userId) {
        List<FilePlainNotificationDto> files = new ArrayList<>();

        String sqlQuery = "SELECT cr.user_id, f.url, f.id, f.notification_date, cr.id AS reviewId, cr.name AS reviewName FROM file f "
                + "JOIN file_user fu ON f.id=fu.file_id "
                + "JOIN branch b ON b.id=f.branch_id "
                + "JOIN project p ON p.id=b.project_id "
                + "JOIN code_review cr ON cr.id=p.code_review_id "
                + "WHERE fu.user_id=? AND fu.is_notified=0";

        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setInt(1, userId);
            ResultSet rs = pstmt.executeQuery();
            files = Transformer.transformToList(rs,
                    FilePlainNotificationDto.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sqlQuery + " has aborted.", e);
        }

        return files;
    }

    public void setNotified(Integer userId, Integer reviewId) {

        String sqlQuery = "UPDATE file_user SET file_user.is_notified=1 WHERE file_user.user_id=? AND file_user.file_id IN "
                + "(SELECT f.id FROM code_review cr JOIN project p ON cr.id=p.code_review_id "
                + "JOIN branch b ON b.project_id=p.id JOIN file f ON f.branch_id=b.id WHERE cr.id=? AND f.status LIKE 'NEW')";

        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setInt(1, userId);
            pstmt.setInt(2, reviewId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error("Query: " + sqlQuery + " has aborted.", e);
        }
    }
}
