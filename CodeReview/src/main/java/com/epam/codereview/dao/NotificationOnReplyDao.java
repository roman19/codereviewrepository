package com.epam.codereview.dao;

import com.epam.codereview.dto.ReplyNotificationDto;
import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.NotificationOnReply;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class NotificationOnReplyDao extends AbstractDAO<NotificationOnReply> {

    private final static Logger LOG = Logger.getLogger(NotificationOnReplyDao.class);

    public NotificationOnReplyDao(Connection connection) {
        super(NotificationOnReply.class, connection);
    }

    public List<ReplyNotificationDto> getReplyNotificationsByUserId(
            Integer userId, int count, int offset) {
        List<ReplyNotificationDto> notifications= null;

        String sqlQuery = "SELECT r.owner_id, r.id AS reply_id, r.message, r.issue_id, i.title, i.file_id, cr.id AS review_id, cr.name AS review_name, r.time "
                + "FROM notification_on_reply nor JOIN reply r ON nor.reply_id=r.id "
                + "JOIN issue i ON i.id=r.issue_id "
                + "JOIN file f ON f.id=i.file_id "
                + "JOIN branch b ON b.id=f.branch_id "
                + "JOIN project p ON p.id=b.project_id "
                + "JOIN code_review cr ON cr.id=p.code_review_id "
                + "WHERE nor.user_id=? AND r.owner_id<>? AND nor.is_notified=0 ORDER BY r.time DESC LIMIT ? OFFSET ?";

        try (PreparedStatement pstmt = connection
                .prepareStatement(sqlQuery)) {
            pstmt.setInt(1, userId);
            pstmt.setInt(2, userId);
            pstmt.setInt(3, count);
            pstmt.setInt(4, offset);
            ResultSet rs = pstmt.executeQuery();
            notifications = Transformer.transformToList(rs,
                    ReplyNotificationDto.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sqlQuery + " has aborted.", e);
        }

        return notifications;
    }

    public Integer getCountOfReplyNotificationsByUserId(Integer userId) {
        Integer count = null;

        String sqlQuery = "SELECT COUNT(*) FROM notification_on_reply nor WHERE nor.user_id=? AND nor.is_notified=0";

        try (PreparedStatement pstmt = connection
                .prepareStatement(sqlQuery)) {
            pstmt.setInt(1, userId);

            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOG.error("Query: " + sqlQuery + " has aborted.", e);
        }

        return count;
    }

    public void setNotified(Integer userId, Integer replyId) {

        String sqlQuery = "UPDATE notification_on_reply SET is_notified=1 WHERE reply_id=? AND user_id=?";

        try (PreparedStatement pstmt = connection
                .prepareStatement(sqlQuery)) {
            pstmt.setInt(1, replyId);
            pstmt.setInt(2, userId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error("Query: " + sqlQuery + " has aborted.", e);
        }
    }

}
