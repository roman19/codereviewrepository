package com.epam.codereview.dao;

import com.epam.codereview.dto.UserGroupDto;
import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.User;
import org.apache.log4j.Logger;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDao extends AbstractDAO<User> {

    private final static Logger LOG = Logger.getLogger(UserDao.class);

    private final String GET_USERS_BY_GROUP = "SELECT * FROM user"
            + " JOIN group_user ON user.id = group_user.user_id"
            + " WHERE group_user.group_id = ? AND group_user.status like 'CONFIRMED';";

    private final String GET_ALL_USERS_BY_GROUP = "SELECT * FROM user"
            + " JOIN group_user ON user.id = group_user.user_id"
            + " WHERE group_user.group_id = ?;";

    public UserDao(Connection connection) {
        super(User.class, connection);
    }

    public List<User> getAllUsersByGroup(Integer id)
            throws SQLException, IOException, PropertyVetoException, InstantiationException, IllegalAccessException {
        PreparedStatement stmt = connection.prepareStatement(GET_ALL_USERS_BY_GROUP);
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        List<User> list = new ArrayList<>();
        User user;
        do {
            user = (User) Transformer.transformToObject(rs, User.class);
            if (user != null)
                list.add(user);
        } while (user != null);
        rs.close();
        stmt.close();
        return list;
    }

    public List<User> getUsersByGroup(Integer id)
            throws SQLException, IOException, PropertyVetoException, InstantiationException, IllegalAccessException {
        PreparedStatement stmt = connection.prepareStatement(GET_USERS_BY_GROUP);
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        List<User> list = new ArrayList<>();
        User user;
        do {
            user = (User) Transformer.transformToObject(rs, User.class);
            if (user != null)
                list.add(user);
        } while (user != null);
        rs.close();
        stmt.close();
        return list;
    }

    public Integer getMaxId() throws SQLException {
        Integer maxId = null;
        Statement statement = connection.createStatement();
        statement.execute("SELECT MAX(id) FROM user");
        ResultSet rs = statement.getResultSet();
        if (rs.next()) {
            maxId = rs.getInt(1);
        }
        return maxId;
    }

    public UserGroupDto getUser(int userId){
        UserGroupDto user = null;
        PreparedStatement ps = null;
        try {
            connection.prepareStatement("set names utf8").executeQuery();
            ps = connection.prepareStatement("SELECT * FROM user where id=?");
            ps.setInt(1, userId);

            ResultSet rs = ps.executeQuery();
            rs.next();

            user=new UserGroupDto();
            user.setFname(rs.getString("fname"));
            user.setLname(rs.getString("lname"));
            user.setId(rs.getInt("id"));
            user.setLogin(rs.getString("login"));
            user.setPhoto(rs.getString("image_url"));
            user.setImageFromServer(rs.getBoolean("is_image_from_server"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * @author Vitalii Horban
     * Search users in typehead autocomplete
     * */

    public ArrayList<UserGroupDto> getAllUsersGroup(int userId) {
        ArrayList<UserGroupDto> list = new ArrayList<>();
        PreparedStatement ps = null;
        String data;
        try {
            connection.prepareStatement("set names utf8").executeQuery();
            ps = connection.prepareStatement("SELECT * FROM user where id <> ?");
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            UserGroupDto p = null;
            while (rs.next()) {
                p = new UserGroupDto();
                p.setId(rs.getInt("id"));
                data = rs.getString("fname");
                p.setFname(data);
                data=rs.getString("lname");
                p.setLname(data);
                data=rs.getString("login");
                p.setLogin(data);
                data=rs.getString("image_url");
                p.setPhoto(data);
                p.setImageFromServer(rs.getBoolean("is_image_from_server"));
                p.setTotalSearchedName(rs.getString("fname")+" "+rs.getString("lname")+" "+rs.getString("login"));

                list.add(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<User> getAllActiveByReviewId(Integer reviewId) {
        List<User> activeReviewUsers = null;
        String sql = "SELECT u.* FROM code_review_user cru " +
                "JOIN `user` u ON cru.user_id = u.id " +
                "WHERE cru.code_review_id = " + reviewId + " AND cru.status IN ('CONFIRMED', 'PENDING')";
        try (Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            activeReviewUsers = Transformer.transformToList(rs, User.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return activeReviewUsers;
    }

    public List<User> getUsersToReplyByIssueId(Integer issueId){
        List<User> users = null;
        String sql = "SELECT DISTINCT * FROM (SELECT u.* FROM reply r JOIN user u ON u.id=r.owner_id WHERE r.issue_id = ? "
                + "UNION ALL SELECT us.* FROM user us JOIN issue i ON i.user_id=us.id WHERE i.id = ?) AS users";
        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setInt(1, issueId);
            pstmt.setInt(2, issueId);
            ResultSet rs = pstmt.executeQuery();
            users = Transformer.transformToList(rs, User.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return users;
    }
}
