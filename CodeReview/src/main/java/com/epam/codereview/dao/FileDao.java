package com.epam.codereview.dao;

import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.File;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class FileDao extends AbstractDAO<File> {

    private final static Logger LOG = Logger.getLogger(FileDao.class);

    public FileDao(Connection connection) {
        super(File.class, connection);
    }

    public List<File> getByBranchId(Integer branchId) {
        List<File> files = null;
        String sql = "SELECT * FROM file WHERE branch_id = " + branchId;
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            files = Transformer.transformToList(rs, File.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return files;
    }

    public File getByUrl(String fileUrl) {
        File file = null;
        String sql = "SELECT * FROM file WHERE url LIKE '" + fileUrl + "'";
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            file = Transformer.transformToObject(rs, File.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return file;
    }

    public int getCountByReviewId(Integer reviewId) {
        String sql = "SELECT count(*) AS files_count FROM `file` f " +
                "JOIN branch b ON f.branch_id = b.id " +
                "JOIN project p on b.project_id = p.id " +
                "WHERE p.code_review_id = " + reviewId;
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return rs.getInt("files_count");
            }
        } catch (SQLException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return 0;
    }
}
