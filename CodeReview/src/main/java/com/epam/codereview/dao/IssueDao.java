package com.epam.codereview.dao;

import com.epam.codereview.dto.IssueDto;
import com.epam.codereview.dto.IssueForTabDto;
import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.Issue;
import javafx.util.Pair;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class IssueDao extends AbstractDAO<Issue> {

    private final static Logger LOG = Logger.getLogger(IssueDao.class);

    public IssueDao(Connection connection) {
        super(Issue.class, connection);
    }

    public List<IssueDto> getByFileIdDtoOrderedByDate(Integer fileId) {
        List<IssueDto> issues = null;
        String sql = "SELECT * FROM issue i " +
                "JOIN `user` u ON i.user_id = u.id  " +
                "WHERE i.file_id = " + fileId + " ORDER BY time ASC";
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            issues = Transformer.transformToList(rs, IssueDto.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return issues;
    }

    /** Pair (file id, count issues) */
    public Pair<Integer, Integer> getCountIssuesByFileUrl(String url) {
        String sql = "SELECT count(i.id) AS issues_count, f.id FROM issue i " +
                "RIGHT JOIN `file` f ON i.file_id = f.id " +
                "WHERE f.url like '" + url + "'";
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return new Pair<>(rs.getInt("id"), rs.getInt("issues_count"));
            }
        } catch (SQLException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return null;
    }

    public int getCountIssuesByFileId(Integer fileId) {
        String sql = "SELECT count(*) AS issues_count FROM issue i " +
                "WHERE i.file_id = " + fileId;
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return rs.getInt("issues_count");
            }
        } catch (SQLException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return 0;
    }


    public List<Issue> getByFileId(Integer fileId) {
        List<Issue> issues = null;
        String sql = "SELECT i.* FROM issue i WHERE i.file_id = " + fileId;
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            issues = Transformer.transformToList(rs, Issue.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return issues;
    }

    public List<Issue> getByReviewId(Integer reviewId) {
        List<Issue> issues = null;
        String sql = "SELECT i.* FROM issue i " +
                "JOIN `file` f ON i.file_id = f.id " +
                "JOIN branch b ON f.branch_id = b.id " +
                "JOIN project p ON b.project_id = p.id " +
                "WHERE p.code_review_id = " + reviewId;
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            issues = Transformer.transformToList(rs, Issue.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return issues;
    }

    public List<Issue> getByReviewIdAndUserId(Integer reviewId, Integer userId) {
        List<Issue> issues = null;
        String sql = "SELECT i.* FROM issue i " +
                "JOIN `file` f ON i.file_id = f.id " +
                "JOIN branch b ON f.branch_id = b.id " +
                "JOIN project p ON b.project_id = p.id " +
                "WHERE p.code_review_id = " + reviewId + " AND i.user_id =" + userId;
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            issues = Transformer.transformToList(rs, Issue.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return issues;
    }

    public List<Issue> getByReviewIdAndIssueType(Integer reviewId, String type) {
        List<Issue> issues = null;
        String sql = "SELECT i.* FROM issue i " +
                "JOIN `file` f ON i.file_id = f.id " +
                "JOIN branch b ON f.branch_id = b.id " +
                "JOIN project p ON b.project_id = p.id " +
                "WHERE p.code_review_id = " + reviewId + " AND i.type LIKE '" + type + "'";
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            issues = Transformer.transformToList(rs, Issue.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return issues;
    }

    public List<IssueForTabDto> getDtoByReviewId(Integer reviewId, Integer langId, String issueType) {
        List<IssueForTabDto> issues = null;
        String sql = "SELECT i.*, u.login, u.fname, u.lname, f.url, l.brush FROM issue i " +
                "JOIN `file` f ON i.file_id = f.id " +
                "JOIN `language` l ON f.language_id = l.id " +
                "JOIN `user` u ON i.user_id = u.id " +
                "JOIN branch b ON f.branch_id = b.id " +
                "JOIN project p ON b.project_id = p.id " +
                "WHERE p.code_review_id = " + reviewId;
        if (langId != null && langId > 0) sql = sql + " AND l.id = " + langId;
        if (issueType != null && !issueType.equalsIgnoreCase("all")) sql = sql + " AND i.type LIKE '" + issueType.toUpperCase() + "'";
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            issues = Transformer.transformToList(rs, IssueForTabDto.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return issues;
    }
}
