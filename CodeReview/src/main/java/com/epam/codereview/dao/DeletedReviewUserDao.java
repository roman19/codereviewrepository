package com.epam.codereview.dao;

import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.model.DeletedReviewUser;

import java.sql.Connection;

public class DeletedReviewUserDao extends AbstractDAO<DeletedReviewUser> {

    public DeletedReviewUserDao(Connection connection) {
        super(DeletedReviewUser.class, connection);
    }
}
