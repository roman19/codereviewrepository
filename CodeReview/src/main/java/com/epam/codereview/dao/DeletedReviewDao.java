package com.epam.codereview.dao;

import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.DeletedReview;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DeletedReviewDao extends AbstractDAO<DeletedReview> {
	private final String GET_ALL_ACTIVE_UNCONFIRMED_DELETED_REVIEWS = "SELECT * FROM deleted_review "
			+ "JOIN deleted_review_user ON deleted_review.id = deleted_review_user.deleted_review_id "
			+ "WHERE reviewer_id = ? AND status LIKE 'REVIEW_DELETED' AND deleted_review.is_active = ?";

    public DeletedReviewDao(Connection connection) {
        super(DeletedReview.class, connection);
    }

    public List<DeletedReview> getAllUnconfirmedDeletedReviewsByUserId(Integer userId, Boolean isActive)
			throws SQLException, InstantiationException, IllegalAccessException{
		PreparedStatement stmt = connection.prepareStatement(GET_ALL_ACTIVE_UNCONFIRMED_DELETED_REVIEWS);
		stmt.setInt(1, userId);
		stmt.setBoolean(2, isActive);
		ResultSet rs = stmt.executeQuery();
		List<DeletedReview> list = new ArrayList<>();
		DeletedReview deletedReview;
		do {
			deletedReview = (DeletedReview) Transformer.transformToObject(rs, DeletedReview.class);
			if (deletedReview != null)
				list.add(deletedReview);
		} while (deletedReview != null);
		rs.close();
		stmt.close();
		return list;
	}
}
