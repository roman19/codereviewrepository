package com.epam.codereview.dao.manager;

public interface DaoCommand<T> {

    T execute();

}
