package com.epam.codereview.dao;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.epam.codereview.dto.EditGroupWithUsersDto;
import com.epam.codereview.dto.GroupDto;
import com.epam.codereview.dto.UserGroupDto;
import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.Group;

public class GroupDao extends AbstractDAO<Group> {
    private final String GET_GROUPS_BY_USER_ID = "SELECT DISTINCT r.id, r.name, r.user_id, r.is_active, r.image_url FROM review_group r"
            + " JOIN group_user ON r.id = group_user.group_id"
            + " WHERE (r.user_id = ?"
            + " OR (group_user.user_id = ? AND group_user.status LIKE 'CONFIRMED')) AND r.is_active = 1;";

    private static String INSERT_GROUP_AND_OWNER = "INSERT INTO review_group (`name`,`user_id`,`is_active`) VALUES (?,?,1)";

    private static String COUNT_USERS_IN_GROUP = "SELECT COUNT(user_id) FROM group_user where group_id=? and status ='CONFIRMED'";

    private static String DELETE_GROUP = "update review_group  SET is_active=0 WHERE id=?";
    private static String DELETE_ALL_USERS_FROM_CURRENT_GROUP = "UPDATE group_user gu  SET gu.status='DELETED' WHERE gu.group_id=?";
    private static String EXIT_GROUP = "UPDATE group_user SET status='LEFT' WHERE user_id=? AND group_id=?";
    private static String GET_GROUP_WITH_USERS = "SELECT * FROM review_group rg INNER JOIN group_user gu ON rg.id=gu.group_id INNER JOIN user u ON u.id=gu.user_id WHERE  rg.is_active=1 and rg.id=? and gu.status='CONFIRMED' or gu.status='PENDING'";
    private static String GET_ALL_USERS_BY_GROUP_ID = "SELECT * FROM group_user WHERE group_id=? AND status = 'CONFIRMED'";
    private static String DELETE_USER_FROM_GROUP = "UPDATE group_user SET status='DELETED' WHERE group_id=? and user_id=?";
    private static String GET_GROUPNAME_BY_ID = "SELECT name FROM review_group WHERE id=?";
    private static String CHANGE_GROUP_NAME = "UPDATE review_group SET name=? WHERE id=?";

    private int noOfRecords;
    private UserDao userDao;

    public GroupDao(Connection connection) {
        super(Group.class, connection);
        userDao = new UserDao(connection); //TODO remove
    }

    @Override
    public void setConnection(Connection connection) {  //TODO remove after remove UserDao
        super.setConnection(connection);
        userDao.setConnection(connection);
    }

    /*
         * @author: Vitalii Horban To change group Name
         */
    public void editGroupName(int groupId, String groupName) {
        try {
            connection.prepareStatement("set names utf8").executeQuery();
            PreparedStatement preparedStatement = connection.prepareStatement(CHANGE_GROUP_NAME);
            preparedStatement.setString(1, groupName);
            preparedStatement.setInt(2, groupId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	/*
	 * @author: Vitalii Horban To get name of group by id
	 */

    public String getNameOfGroup(Integer groupID) {
        String groupName = null;
        try {
            connection.prepareStatement("set names utf8").executeQuery();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_GROUPNAME_BY_ID);
            preparedStatement.setInt(1, groupID);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            groupName = rs.getString(1);

        } catch (Exception e) {

            e.printStackTrace();

        }
        return groupName;

    }

    /*
     * @author: Vitalii Horban To delete one user from group
     */
    public void deleteUserFromGroup(int groupId, int userId) {
        try {
            connection.prepareStatement("set names utf8").executeQuery();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_FROM_GROUP);
            preparedStatement.setInt(1, groupId);
            preparedStatement.setInt(2, userId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

	/*
	 * @author: Vitalii Horban To edit groups
	 */

    public List<Integer> getAllUsersFromGroup(Integer groupID) {
        List<Integer> list = new ArrayList<>();
        try {
            connection.prepareStatement("set names utf8").executeQuery();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_USERS_BY_GROUP_ID);
            preparedStatement.setInt(1, groupID);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {

                list.add(rs.getInt("user_id"));

            }

        } catch (Exception e) {

            e.printStackTrace();

        }
        return list;
    }

    /*
     * @author: Vitalii Horban To delete all users from group
     */
    public void deleteUsersFromGroup(int groupId) {
        try {
            connection.prepareStatement("set names utf8").executeQuery();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ALL_USERS_FROM_CURRENT_GROUP);
            preparedStatement.setInt(1, groupId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

	/*
	 * @author: Vitalii Horban To edit groups
	 */

    public EditGroupWithUsersDto getGroupWithUsersByGroupId(Integer groupID) {
        List<UserGroupDto> list = new ArrayList<>();
        EditGroupWithUsersDto gu = null;
        try {
            connection.prepareStatement("set names utf8").executeQuery();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_GROUP_WITH_USERS);
            preparedStatement.setInt(1, groupID);
            ResultSet rs = preparedStatement.executeQuery();
            gu = new EditGroupWithUsersDto();
            UserGroupDto userDto = null;

            while (rs.next()) {

                gu.setName(rs.getString("name"));
                userDto = new UserGroupDto();

                userDto.setId(rs.getInt(7));
                userDto.setFname(rs.getString("fname"));
                userDto.setLname(rs.getString("lname"));
                userDto.setLogin(rs.getString("login"));
                userDto.setPhoto(rs.getString("image_url"));
                list.add(userDto);

            }

            gu.setUsers(list);

            gu.setName(getNameOfGroup(groupID));

        } catch (Exception e) {

            e.printStackTrace();

        }
        return gu;

    }

    /*
     * @author: Vitalii Horban To exit from groups
     */
    public void exitGroup(int groupId, int userId) {
        try {
            connection.prepareStatement("set names utf8").executeQuery();
            PreparedStatement preparedStatement = connection.prepareStatement(EXIT_GROUP);
            preparedStatement.setInt(1, userId);
            preparedStatement.setInt(2, groupId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

    /*
     * @author: Vitalii Horban To delete groups
     */
    public void deleteGroup(int groupId) {
        try {
            connection.prepareStatement("set names utf8").executeQuery();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_GROUP);
            preparedStatement.setInt(1, groupId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

	/*
	 * @author: Vitalii Horban For filters 'My' in groups ++
	 */

    public int getNoOfRecords() {
        return noOfRecords;
    }

    public void setNoOfRecords(int noOfRecords) {
        this.noOfRecords = noOfRecords;
    }

    public List<GroupDto> getAllMYGroupsByUserId(Integer userID, int offset, int noOfRecords) {
        List<GroupDto> list = new ArrayList<>();

        try {
            connection.prepareStatement("set names utf8").executeQuery();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT  SQL_CALC_FOUND_ROWS * FROM review_group rg   JOIN user u on u.id=rg.user_id"
                            + " WHERE rg.user_id=? and rg.is_active=1   ORDER BY  rg.id DESC  limit " + offset + "," + noOfRecords);
            preparedStatement.setInt(1, userID);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next())

            {
                GroupDto group = new GroupDto();
                group.setGroupId(rs.getInt("id"));
                group.setGroupName(rs.getString("name"));
                group.setUserFirstName(rs.getString("fname"));
                group.setUserLastName(rs.getString("lname"));
                group.setUserId(rs.getInt("user_id"));
                group.setIsActiveGroup(rs.getInt("is_active") == 1 ? true : false);
                int usersNumber = countUsersInGroup(rs.getInt("id"));
                group.setTotalUserNumerous(usersNumber + 1);
                group.setImage_url(rs.getString("image_url"));
                list.add(group);
            }
            rs.close();

            rs = preparedStatement.executeQuery();

            preparedStatement = connection.prepareStatement(
                    "SELECT  COUNT(*)  FROM review_group rg   JOIN user u on u.id=rg.user_id WHERE rg.user_id=? and rg.is_active=1 ");

            preparedStatement.setInt(1, userID);
            rs = preparedStatement.executeQuery();
            rs.next();
            this.noOfRecords = rs.getInt(1);
            System.out.println("Founded rows" + rs.getInt(1));

        } catch (

                Exception e)

        {

            e.printStackTrace();

        }
        return list;

    }

    /*
     * @author: Vitalii Horban For filters 'Other' in groups
     */
    public List<GroupDto> getAllOtherGroupsByUserId(Integer userID, int offset, int noOfRecords) {
        List<GroupDto> list = new ArrayList<>();

        try {
            connection.prepareStatement("set names utf8").executeQuery();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT * FROM  review_group rg JOIN group_user  gu ON rg.id=gu.group_id"
                            + " WHERE gu.user_id=? and rg.is_active=1 and gu.status='CONFIRMED'   ORDER BY  rg.id DESC   limit " + offset + ","
                            + noOfRecords);

            preparedStatement.setInt(1, userID);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {

                GroupDto group = new GroupDto();
                group.setGroupId(rs.getInt("id"));
                group.setGroupName(rs.getString("name"));
                group.setUserId(rs.getInt("user_id"));

                UserGroupDto user = userDao.getUser(rs.getInt("user_id"));
                group.setUserFirstName(user.getFname());
                group.setUserLastName(user.getLname());

                group.setIsActiveGroup(rs.getInt("is_active") == 1 ? true : false);
                int usersNumber = countUsersInGroup(rs.getInt("id"));
                group.setTotalUserNumerous(usersNumber + 1);
                group.setImage_url(rs.getString("image_url"));
                list.add(group);
            }

            rs.close();

            rs = preparedStatement.executeQuery();

            preparedStatement = connection
                    .prepareStatement("SELECT COUNT(*) FROM  review_group rg JOIN group_user  gu ON rg.id=gu.group_id "
                            + "  WHERE gu.user_id=? and rg.is_active=1 and gu.status='CONFIRMED' ");

            preparedStatement.setInt(1, userID);
            rs = preparedStatement.executeQuery();
            rs.next();
            this.noOfRecords = rs.getInt(1);
            System.out.println("Founded other rows" + rs.getInt(1));

        } catch (Exception e) {

            e.printStackTrace();

        }
        return list;

    }

    /*
     * @author: Vitalii Horban For filters 'All' in groups limit " + offset + "
     * ," + noOfRecords+
     */
    public List<GroupDto> getAllGroupsByUserId(Integer userID, int offset, int noOfRecords) {
        List<GroupDto> list = new ArrayList<>();

        try {
            connection.prepareStatement("set names utf8").executeQuery();
            PreparedStatement preparedStatement = connection.prepareStatement(

                    "select * from (SELECT   rg.id , rg.name , u.fname, u.lname, rg.user_id, rg.is_active , rg.image_url FROM review_group rg   JOIN user u on u.id=rg.user_id WHERE rg.user_id=? and rg.is_active=1  "

                            + " UNION    SELECT rg.id , rg.name , u.fname, u.lname, rg.user_id, rg.is_active , rg.image_url FROM  review_group  rg JOIN group_user  gu ON rg.id=gu.group_id JOIN user u on u.id=rg.user_id    WHERE gu.user_id=? and rg.is_active=1 and gu.status='CONFIRMED')a   ORDER BY  id DESC   limit "
                            + offset + "," + noOfRecords);

            preparedStatement.setInt(1, userID);
            preparedStatement.setInt(2, userID);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {

                GroupDto group = new GroupDto();
                group.setGroupId(rs.getInt("id"));
                group.setGroupName(rs.getString("name"));
                group.setUserId(rs.getInt("user_id"));

                UserGroupDto user = userDao.getUser(rs.getInt("user_id"));
                group.setUserFirstName(user.getFname());
                group.setUserLastName(user.getLname());

                group.setIsActiveGroup(rs.getInt("is_active") == 1 ? true : false);
                int usersNumber = countUsersInGroup(rs.getInt("id"));
                group.setTotalUserNumerous(usersNumber + 1);
                group.setImage_url(rs.getString("image_url"));
                list.add(group);
            }

            rs.close();

            rs = preparedStatement.executeQuery();

            preparedStatement = connection
                    .prepareStatement("select count(*) from (SELECT   rg.id , rg.name , u.fname, u.lname, rg.user_id, rg.is_active  FROM review_group rg   JOIN user u on u.id=rg.user_id WHERE rg.user_id=? and rg.is_active=1   UNION    SELECT rg.id , rg.name , u.fname, u.lname, rg.user_id, rg.is_active FROM  review_group  rg JOIN group_user  gu ON rg.id=gu.group_id JOIN user u on u.id=rg.user_id    WHERE gu.user_id=? and rg.is_active=1 and gu.status='CONFIRMED' )a;");
            preparedStatement.setInt(1, userID);
            preparedStatement.setInt(2, userID);
            rs = preparedStatement.executeQuery();
            rs.next();
            this.noOfRecords = rs.getInt(1);
            System.out.println("Founded other rows" + rs.getInt(1));

        } catch (Exception e) {

            e.printStackTrace();

        }
        return list;
    }
    /*
     * @author: Vitalii Horban For counting users in group
     */
    public Integer countUsersInGroup(Integer groupID) {
        int usersNumber = 0;

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(COUNT_USERS_IN_GROUP);
            preparedStatement.setInt(1, groupID);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            usersNumber = rs.getInt(1);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return usersNumber;

    }

    /*
     * @author Vitalii Horban To save users with group name when creating group
     */
    public Integer insertUserGroupReview(String groupName, boolean isImage, int ownerId) {

		String imageUrl = null;
		if (isImage) {
			imageUrl = "group1.png";
		} else {
			Integer groupImageId = new Random().nextInt(10)+1;
			imageUrl = "group" + groupImageId + ".png";
		}
		Group group = new Group();
		group.setName(groupName);
		group.setImageUrl(imageUrl);
		group.setUserId(ownerId);
		group.setActive(true);

        return insert(group);
    }

    public List<Group> getGroupsByClientID(Integer id)
            throws SQLException, IOException, PropertyVetoException, InstantiationException, IllegalAccessException {
        PreparedStatement stmt = connection.prepareStatement(GET_GROUPS_BY_USER_ID);
        stmt.setInt(1, id);
        stmt.setInt(2, id);
        ResultSet rs = stmt.executeQuery();
        List<Group> list = new ArrayList<>();
        Group group;
        do {
            group = (Group) Transformer.transformToObject(rs, Group.class);
            if (group != null)
                list.add(group);
        } while (group != null);
        rs.close();
        stmt.close();
        return list;
    }
}
