package com.epam.codereview.dao.manager;

import com.epam.codereview.dao.*;
import com.epam.codereview.exception.TransactionRollbackException;
import com.epam.codereview.exception.UnexpectedServerException;
import com.epam.codereview.manager.db.ConnectionManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

public class DaoManager {

    private final static Logger LOG = Logger.getLogger(DaoManager.class);

    private Connection connection;

    private UserDao userDao;
    private ForgottenUserDao forgottenUserDao;
    private CodeReviewDao codeReviewDao;
    private CodeReviewUserDao codeReviewUserDao;
    private DeletedReviewDao deletedReviewDao;
    private DeletedReviewUserDao deletedReviewUserDao;
    private BranchDao branchDao;
    private ProjectDao projectDao;
    private FileDao fileDao;
    private FileUserDao fileUserDao;
    private LanguageDao languageDao;
    private IssueDao issueDao;
    private IssueLikeDao issueLikeDao;
    private IssueNotificationDao issueNotificationDao;
    private ReplyDao replyDao;
    private ReplyLikeDao replyLikeDao;
    private GroupConcreteDao groupConcreteDao;
    private GroupDao groupDao;
    private GroupUserDao groupUserDao;
    private CodeReviewChatDao codeReviewChatDao;
    private NotificationOnReplyDao notificationOnReplyDao;
    private NotificationSettingDao notificationSettingDao;

    private boolean isInTransaction = false;

    public DaoManager() {
    }

    private Connection getConnection(){
        try {
            if(this.connection == null || this.connection.isClosed()){
                this.connection = ConnectionManager.getConnection();
                if (connection == null) throw new UnexpectedServerException("Fail while getting db connection.");
            }
        } catch (SQLException e) {
            LOG.error("Fail while getting db connection.", e);
            throw new UnexpectedServerException("Fail while getting db connection.");
        }
        return connection;
    }

    public UserDao getUserDao() {
        if (userDao == null) userDao = new UserDao(getConnection());
        else userDao.setConnection(getConnection());
        return userDao;
    }

    public ForgottenUserDao getForgottenUserDao() {
        if (forgottenUserDao == null) forgottenUserDao = new ForgottenUserDao(getConnection());
        else forgottenUserDao.setConnection(getConnection());
        return forgottenUserDao;
    }

    public CodeReviewDao getCodeReviewDao() {
        if (codeReviewDao == null) codeReviewDao = new CodeReviewDao(getConnection());
        else codeReviewDao.setConnection(getConnection());
        return codeReviewDao;
    }

    public CodeReviewUserDao getCodeReviewUserDao() {
        if (codeReviewUserDao == null) codeReviewUserDao = new CodeReviewUserDao(getConnection());
        else codeReviewUserDao.setConnection(getConnection());
        return codeReviewUserDao;
    }

    public BranchDao getBranchDao() {
        if (branchDao == null) branchDao = new BranchDao(getConnection());
        else branchDao.setConnection(getConnection());
        return branchDao;
    }

    public ProjectDao getProjectDao() {
        if (projectDao == null) projectDao = new ProjectDao(getConnection());
        else projectDao.setConnection(getConnection());
        return projectDao;
    }

    public FileDao getFileDao() {
        if (fileDao == null) fileDao = new FileDao(getConnection());
        else fileDao.setConnection(getConnection());
        return fileDao;
    }

    public FileUserDao getFileUserDao() {
        if (fileUserDao == null) fileUserDao = new FileUserDao(getConnection());
        else fileUserDao.setConnection(getConnection());
        return fileUserDao;
    }

    public LanguageDao getLanguageDao() {
        if (languageDao == null) languageDao = new LanguageDao(getConnection());
        else languageDao.setConnection(getConnection());
        return languageDao;
    }

    public IssueDao getIssueDao() {
        if (issueDao == null) issueDao = new IssueDao(getConnection());
        else issueDao.setConnection(getConnection());
        return issueDao;
    }

    public IssueLikeDao getIssueLikeDao() {
        if (issueLikeDao == null) issueLikeDao = new IssueLikeDao(getConnection());
        else issueLikeDao.setConnection(getConnection());
        return issueLikeDao;
    }

    public ReplyDao getReplyDao() {
        if (replyDao == null) replyDao = new ReplyDao(getConnection());
        else replyDao.setConnection(getConnection());
        return replyDao;
    }

    public ReplyLikeDao getReplyLikeDao() {
        if (replyLikeDao == null) replyLikeDao = new ReplyLikeDao(getConnection());
        else replyLikeDao.setConnection(getConnection());
        return replyLikeDao;
    }

    public GroupDao getGroupDao() {
        if (groupDao == null) groupDao = new GroupDao(getConnection());
        else groupDao.setConnection(getConnection());
        return groupDao;
    }

    public GroupUserDao getGroupUserDao() {
        if (groupUserDao == null) groupUserDao = new GroupUserDao(getConnection());
        else groupUserDao.setConnection(getConnection());
        return groupUserDao;
    }

    public CodeReviewChatDao getCodeReviewChatDao() {
        if (codeReviewChatDao == null) codeReviewChatDao = new CodeReviewChatDao(getConnection());
        else codeReviewChatDao.setConnection(getConnection());
        return codeReviewChatDao;
    }

    public NotificationOnReplyDao getNotificationOnReplyDao() {
        if (notificationOnReplyDao == null) notificationOnReplyDao = new NotificationOnReplyDao(getConnection());
        else notificationOnReplyDao.setConnection(getConnection());
        return notificationOnReplyDao;
    }

    public NotificationSettingDao getNotificationSettingDao() {
        if (notificationSettingDao == null) notificationSettingDao = new NotificationSettingDao(getConnection());
        else notificationSettingDao.setConnection(getConnection());
        return notificationSettingDao;
    }

    public DeletedReviewDao getDeletedReviewDao() {
        if (deletedReviewDao == null) deletedReviewDao = new DeletedReviewDao(getConnection());
        else deletedReviewDao.setConnection(getConnection());
        return deletedReviewDao;
    }

    public DeletedReviewUserDao getDeletedReviewUserDao() {
        if (deletedReviewUserDao == null) deletedReviewUserDao = new DeletedReviewUserDao(getConnection());
        else deletedReviewUserDao.setConnection(getConnection());
        return deletedReviewUserDao;
    }

    public IssueNotificationDao getIssueNotificationDao() {
        if (issueNotificationDao == null) issueNotificationDao = new IssueNotificationDao(getConnection());
        else issueNotificationDao.setConnection(getConnection());
        return issueNotificationDao;
    }

    public GroupConcreteDao getGroupConcreteDao() {
        if (groupConcreteDao == null) groupConcreteDao = new GroupConcreteDao(getConnection());
        else groupConcreteDao.setConnection(getConnection());
        return groupConcreteDao;
    }

    /**
     * T - type of object, that will be returned after command execution.
     * Example:
     *
     * DaoManager daoManager = new DaoManager();
     * User user = daoManager.executeAndClose(new DaoCommand<User>(){
     *      public User execute(){
     *          return manager.getUserDao().get(1);
     *      }
     * });
     * */
    public <T> T executeAndClose(DaoCommand<T> command){

        if (isInTransaction) return command.execute();  // not close connection if use in transaction

        try{
            return command.execute();
        } finally {
            try {
                getConnection().close();
            } catch (SQLException e) {
                LOG.error("Error while closing a connection.", e);
            }
        }
    }

    /** For execute without return, such as delete and update */
    public void executeVoidAndClose(DaoVoidCommand command){

        if (isInTransaction) command.execute(); // not close connection if use in transaction

        try{
            command.execute();
        } finally {
            try {
                getConnection().close();
            } catch (SQLException e) {
                LOG.error("Error while closing a connection.", e);
            }
        }
    }

    /** @see DaoManager#executeAndClose(DaoCommand), same usage. */
    public <T> T transactionAndClose(DaoCommand<T> command) throws TransactionRollbackException {
        try {
            return this.transaction(command);
        } finally {
            try {
                getConnection().close();
            } catch (SQLException e) {
                LOG.error("Error while closing a connection.", e);
            }
        }
    }

    private  <T> T transaction(DaoCommand<T> command) throws TransactionRollbackException {
        try {
            isInTransaction = true;
            getConnection().setAutoCommit(false);

            T returnValue = command.execute();

            getConnection().commit();
            isInTransaction = false;

            return returnValue;
        } catch(Exception e){
            LOG.error("Transaction fail.", e);
            try {
                getConnection().rollback();
            } catch (SQLException e1) {
                LOG.error("Rollback transaction fail.", e1);
            }
            throw new TransactionRollbackException("Transaction fail", e);
        } finally {
            isInTransaction = false;
            try {
                getConnection().setAutoCommit(true);
            } catch (SQLException e) {
                LOG.error("Transaction, set setAutoCommit(true) fail.", e);
            }
        }
    }

}
