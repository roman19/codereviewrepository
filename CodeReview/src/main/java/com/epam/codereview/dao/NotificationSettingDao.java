package com.epam.codereview.dao;

import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.model.NotificationSetting;

import java.sql.Connection;

public class NotificationSettingDao extends AbstractDAO<NotificationSetting> {

    public NotificationSettingDao(Connection connection) {
        super(NotificationSetting.class, connection);
    }
	
	public NotificationSetting getSettingsForUser(Integer userId){
		Criteria criteria = new Criteria();
		criteria.add(new Condition("user_id", "=", userId));
		return get(criteria).get(0);
	}

}
