package com.epam.codereview.dao;

import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.model.IssueLike;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class IssueLikeDao extends AbstractDAO<IssueLike> {

    public IssueLikeDao(Connection connection) {
        super(IssueLike.class, connection);
    }

    private static String CHECK_CURRENT_LIKE = "SELECT COUNT(*) FROM issue_like i WHERE i.issue_id=? AND i.user_id=?";
    private static String COUNT_ALL_LIKES = "SELECT COUNT(*) FROM issue_like i WHERE i.issue_id=? AND i.value = 1";
    private static String COUNT_ALL_DISLIKES = "SELECT COUNT(*) FROM issue_like i WHERE i.issue_id=? AND i.value = -1";
    private static String UPDATE_LIKE = "UPDATE issue_like i SET i.value=? WHERE i.issue_id=? AND i.user_id=?";
    private static String GET_LIKE_VALUE="SELECT i.value FROM issue_like i WHERE i.issue_id=? AND i.user_id=?";

    /*
     * @author: Vitalii Horban For counting users in group
     */
    public Integer checkCurrentLike(Integer userID, Integer issueID) {
        int issueNumber = 0;

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CHECK_CURRENT_LIKE);
            preparedStatement.setInt(1, issueID);
            preparedStatement.setInt(2, userID);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                issueNumber = rs.getInt(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return issueNumber;

    }

    public Integer getValueOfLike(Integer userID, Integer issueID) {
        int issueNumber = 0;

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_LIKE_VALUE);
            preparedStatement.setInt(1, issueID);
            preparedStatement.setInt(2, userID);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                issueNumber = rs.getInt(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return issueNumber;

    }

    public Integer countTotalLike( Integer issueID) {
        int issueNumber = 0;

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(COUNT_ALL_LIKES);
            preparedStatement.setInt(1, issueID);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                issueNumber = rs.getInt(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return issueNumber;

    }

    public Integer countTotalDisLike(Integer issueID) {
        int likesNumber = 0;

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(COUNT_ALL_DISLIKES);
            preparedStatement.setInt(1, issueID);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                likesNumber = rs.getInt(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return likesNumber;
    }

    public Integer likeOnIssue(Integer userID, Integer issueID) {
        if (checkCurrentLike(userID, issueID) == 0) {

            IssueLike like = new IssueLike();
            like.setIssueId(issueID);
            like.setUserId(userID);
            like.setValue(1);
            return insert(like);
        } else {

            try {
                connection.prepareStatement("set names utf8").executeQuery();
                PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement(UPDATE_LIKE);
                preparedStatement.setInt(1, 1);
                preparedStatement.setInt(2, issueID);
                preparedStatement.setInt(3, userID);
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return -1;
        }
    }

    public Integer disLikeOnIssue(Integer userID, Integer issueID) {
        if (checkCurrentLike(userID, issueID) == 0) {

            IssueLike like = new IssueLike();
            like.setIssueId(issueID);
            like.setUserId(userID);
            like.setValue(-1);
            return insert(like);
        } else {

            try {
                connection.prepareStatement("set names utf8").executeQuery();
                PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement(UPDATE_LIKE);
                preparedStatement.setInt(1, -1);
                preparedStatement.setInt(2, issueID);
                preparedStatement.setInt(3, userID);
                preparedStatement.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return -1;
        }

    }

    public Integer decrementLikeOnIssue(Integer userID, Integer issueID) {

        if (checkCurrentLike(userID, issueID) == 0) {

            IssueLike like = new IssueLike();
            like.setIssueId(issueID);
            like.setUserId(userID);
            like.setValue(0);
            return insert(like);
        } else {

            try {
                connection.prepareStatement("set names utf8").executeQuery();
                PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement(UPDATE_LIKE);
                preparedStatement.setInt(1, 0);
                preparedStatement.setInt(2, issueID);
                preparedStatement.setInt(3, userID);
                preparedStatement.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return -1;
        }

    }

}