package com.epam.codereview.dao;

import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.GroupUser;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class GroupUserDao extends AbstractDAO<GroupUser> {

    private final static Logger LOG = Logger.getLogger(GroupUserDao.class);

    public GroupUserDao(Connection connection) {
        super(GroupUser.class, connection);
    }

    public Integer insertUserGroupReview(int groupId, int userId) {
        GroupUser groupUser = new GroupUser();
        groupUser.setGroupId(groupId);
        groupUser.setUserId(userId);
        return insert(groupUser);
    }

    
    public List<GroupUser> getUserWithAllStatus(int groupId){
    	  List<GroupUser> groupUsers = null;
          String sql = "SELECT * from group_user gu WHERE gu.group_id="+groupId+"  AND status IN ('CONFIRMED','PENDING','LEFT')";
          try(Statement stmt = connection.createStatement()) {
              ResultSet rs = stmt.executeQuery(sql);
              groupUsers = Transformer.transformToList(rs, GroupUser.class);
          } catch (SQLException | InstantiationException | IllegalAccessException e) {
              LOG.error("Query: " + sql + " didn't pass.", e);
          }
          return groupUsers;
    }
    
    
    
    public List<GroupUser> getUserWithLeftStatus(int groupId, int userId){
  	  List<GroupUser> groupUsers = null;
        String sql = "SELECT * FROM group_user gu WHERE gu.group_id="+groupId+" and gu.user_id="+userId+" AND gu.status='LEFT'";
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            groupUsers = Transformer.transformToList(rs, GroupUser.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return groupUsers;
  }
    
    
    
    public List<GroupUser> getByReviewId(Integer reviewId) {
        List<GroupUser> groupUsers = null;
        String sql = "SELECT gu.* FROM group_user gu JOIN code_review cr ON gu.group_id = cr.group_id WHERE cr.id = " + reviewId;
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            groupUsers = Transformer.transformToList(rs, GroupUser.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return groupUsers;
    }

}
