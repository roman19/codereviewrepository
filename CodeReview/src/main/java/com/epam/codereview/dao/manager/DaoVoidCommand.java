package com.epam.codereview.dao.manager;

public interface DaoVoidCommand {

    void execute();
}
