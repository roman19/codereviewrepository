package com.epam.codereview.dao;

import com.epam.codereview.dto.ReviewDto;
import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.CodeReviewUser;
import com.epam.codereview.model.GroupUser;
import com.epam.codereview.service.UserService;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GroupConcreteDao extends AbstractDAO<GroupUser> {

	private final static Logger LOG = Logger.getLogger(GroupConcreteDao.class);

	private UserService userService;
	private final String GET_GIT_URL = "SELECT git_url FROM codereview.project where code_review_id=?";
	private final String GET_TOTAL_ISSUES = "SELECT count_issues FROM project WHERE code_review_id=?";
	private int noOfRecords;

    public GroupConcreteDao(Connection connection) {
        super(GroupUser.class, connection);
        userService = new UserService(); //TODO remove service from here
    }

    public List<ReviewDto> getAllActiveReviwByUserId( Integer groupId ,Integer is_active, Integer userID, int offset,
			int noOfRecords) {
		List<ReviewDto> list = new ArrayList<>();

		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM (     "
					+ " SELECT id,name, user_id, is_active, coverage_avg_percent, creation_date,is_deleted, group_id   FROM code_review cr "
					+ " WHERE cr.is_active=? and cr.user_id=? "

					+ " UNION "

					+ " SELECT   deleted_review.id,   deleted_review.name, deleted_review.owner_id, deleted_review.is_active, deleted_review.coverage_avg_percent, "
					+ " deleted_review.creation_date,  deleted_review.is_deleted , deleted_review.group_id " + " FROM deleted_review  "
					+ " JOIN deleted_review_user ON deleted_review.id = deleted_review_user.deleted_review_id "
					+ " WHERE reviewer_id = ? AND status LIKE 'REVIEW_DELETED' AND deleted_review.is_active = ? "

					+ " UNION "

					+ " SELECT cr.id, cr.name, cr.user_id, cr.is_active, cr.coverage_avg_percent, "
					+ " cr.creation_date,cr.is_deleted , cr.group_id FROM code_review_user cru JOIN code_review cr  ON cru.code_review_id =cr.id  "
					+ " WHERE (cru.status='CONFIRMED' OR cru.status='DELETED' OR cru.status='OWNER_DELETED_OR_LEFT_GROUP') "
					+ " AND cru.user_id=?) a WHERE a.group_id=? ORDER BY a.id DESC limit " + offset + "," + noOfRecords);
			preparedStatement.setInt(1, is_active);
			preparedStatement.setInt(2, userID);
			preparedStatement.setInt(3, userID);
			preparedStatement.setInt(4, is_active);
			preparedStatement.setInt(5, userID);
			preparedStatement.setInt(6, groupId);

			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				ReviewDto review = new ReviewDto();
				review.setCodeReviewId(rs.getInt("id"));
				review.setActive((rs.getInt("is_active") == 1) ? true : false);
				review.setCoverageAvgPercent(rs.getDouble("coverage_avg_percent"));
				review.setCreationDate(rs.getTimestamp("creation_date"));
				review.setName(rs.getString("name"));
				CodeReviewUser user = getByReviewIdAndUserId(review.getCodeReviewId(), userID);
				review.setReviewerStatus(user == null ? null : user.getStatus());
				int userId = rs.getInt("user_id");
				review.setUserName(
						userService.get(userId).getFirstName() + " " + userService.get(userId).getLastName());
				
				review.setUserId(userId);
				review.setDeleted(rs.getBoolean("is_deleted"));
				review.setTotalIssues(getTotalIssues(rs.getInt("id")));
				review.setGitURL(getGitUrl(rs.getInt("id")));
				review.setTotalIssues(getTotalIssues(review.getCodeReviewId()));
				list.add(review);
			}

			rs.close();

			 preparedStatement = connection.prepareStatement(
					 "SELECT count(*) FROM (     "
								+ " SELECT id,name, user_id, is_active, coverage_avg_percent, creation_date,is_deleted , group_id  FROM code_review cr "
								+ " WHERE cr.is_active=? and cr.user_id=? "

								+ " UNION "

								+ " SELECT   deleted_review.id,   deleted_review.name, deleted_review.owner_id, deleted_review.is_active, deleted_review.coverage_avg_percent, "
								+ " deleted_review.creation_date,  deleted_review.is_deleted , deleted_review.group_id " + " FROM deleted_review  "
								+ " JOIN deleted_review_user ON deleted_review.id = deleted_review_user.deleted_review_id "
								+ " WHERE reviewer_id = ? AND status LIKE 'REVIEW_DELETED' AND deleted_review.is_active = ? "

								+ " UNION "

								+ " SELECT cr.id, cr.name, cr.user_id, cr.is_active, cr.coverage_avg_percent, "
								+ " cr.creation_date,cr.is_deleted , cr.group_id FROM code_review_user cru JOIN code_review cr  ON cru.code_review_id =cr.id  "
								+ " WHERE (cru.status='CONFIRMED' OR cru.status='DELETED' OR cru.status='OWNER_DELETED_OR_LEFT_GROUP') "
								+ " AND cru.user_id=?) a WHERE a.group_id=?");
			 preparedStatement.setInt(1, is_active);
			 preparedStatement.setInt(2, userID);
			 preparedStatement.setInt(3, userID);
			 preparedStatement.setInt(4, is_active);
			 preparedStatement.setInt(5, userID);
			 preparedStatement.setInt(6, groupId);

			 rs = preparedStatement.executeQuery();
			 rs.next();
			 this.noOfRecords = rs.getInt(1);
			 System.out.println("Founded rows = " + rs.getInt(1));

		} catch (Exception e) {

			e.printStackTrace();

		}
		return list;
	}

    
    
	public List<ReviewDto> getOtherActiveReviwByUserId(Integer groupId , Integer is_active, Integer userID, int offset,
			int noOfRecords) {
		List<ReviewDto> list = new ArrayList<>();

		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM (     "
					+ " SELECT   deleted_review.id as id,   deleted_review.name as name, deleted_review.owner_id as  user_id, deleted_review.is_active as is_active, deleted_review.coverage_avg_percent as coverage_avg_percent, "
					+ " deleted_review.creation_date as creation_date,  deleted_review.is_deleted as is_deleted , deleted_review.group_id " + " FROM deleted_review  "
					+ " JOIN deleted_review_user ON deleted_review.id = deleted_review_user.deleted_review_id "
					+ " WHERE reviewer_id = ? AND status LIKE 'REVIEW_DELETED' AND deleted_review.is_active = ? "

					+ " UNION "

					+ " SELECT cr.id, cr.name, cr.user_id, cr.is_active, cr.coverage_avg_percent, "
					+ " cr.creation_date,cr.is_deleted , cr.group_id  FROM code_review_user cru JOIN code_review cr  ON cru.code_review_id =cr.id  "
					+ " WHERE (cru.status='CONFIRMED' OR cru.status='DELETED' OR cru.status='OWNER_DELETED_OR_LEFT_GROUP') "
					+ " AND cru.user_id=?) a WHERE a.group_id=? ORDER BY a.id DESC limit " + offset + "," + noOfRecords);
		
			preparedStatement.setInt(1, userID);
			preparedStatement.setInt(2, is_active);
			preparedStatement.setInt(3, userID);
			preparedStatement.setInt(4, groupId);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				ReviewDto review = new ReviewDto();
				review.setCodeReviewId(rs.getInt("id"));
				review.setActive((rs.getInt("is_active") == 1) ? true : false);
				review.setCoverageAvgPercent(rs.getDouble("coverage_avg_percent"));
				review.setCreationDate(rs.getTimestamp("creation_date"));
				review.setName(rs.getString("name"));
				CodeReviewUser user = getByReviewIdAndUserId(review.getCodeReviewId(), userID);
				review.setReviewerStatus(user == null ? null : user.getStatus());
				int userId = rs.getInt("user_id");
				review.setUserName(
						userService.get(userId).getFirstName() + " " + userService.get(userId).getLastName());
				
				review.setUserId(userId);
				review.setDeleted(rs.getBoolean("is_deleted"));
				review.setTotalIssues(getTotalIssues(rs.getInt("id")));
				review.setGitURL(getGitUrl(rs.getInt("id")));
				review.setTotalIssues(getTotalIssues(review.getCodeReviewId()));
				list.add(review);
			}

			rs.close();

			 preparedStatement = connection.prepareStatement(
					 "SELECT count(*) FROM (     "
								+ " SELECT   deleted_review.id,   deleted_review.name, deleted_review.owner_id, deleted_review.is_active, deleted_review.coverage_avg_percent, "
								+ " deleted_review.creation_date,  deleted_review.is_deleted , deleted_review.group_id  " + " FROM deleted_review  "
								+ " JOIN deleted_review_user ON deleted_review.id = deleted_review_user.deleted_review_id "
								+ " WHERE reviewer_id = ? AND status LIKE 'REVIEW_DELETED' AND deleted_review.is_active = ? "

								+ " UNION "

								+ " SELECT cr.id, cr.name, cr.user_id, cr.is_active, cr.coverage_avg_percent, "
								+ " cr.creation_date,cr.is_deleted , cr.group_id   FROM code_review_user cru JOIN code_review cr  ON cru.code_review_id =cr.id  "
								+ " WHERE (cru.status='CONFIRMED' OR cru.status='DELETED' OR cru.status='OWNER_DELETED_OR_LEFT_GROUP') "
								+ " AND cru.user_id=?)  a WHERE a.group_id=? ");
			 	
			 	preparedStatement.setInt(1, userID);
				preparedStatement.setInt(2, is_active);
				preparedStatement.setInt(3, userID);
				preparedStatement.setInt(4, groupId);
				
			 rs = preparedStatement.executeQuery();
			 rs.next();
			 this.noOfRecords = rs.getInt(1);
			 System.out.println("Founded rows = " + rs.getInt(1));

		} catch (Exception e) {

			e.printStackTrace();

		}
		return list;

	}
	
	
	
	
	 public List<ReviewDto> getMyActiveReviwByUserId(Integer groupId , Integer is_active, Integer userID, int offset, int noOfRecords) {
	        List<ReviewDto> list = new ArrayList<>();

	        try {
	            PreparedStatement preparedStatement = connection.prepareStatement(
	                    "SELECT * FROM code_review cr WHERE cr.is_active=? and cr.user_id=? and cr.group_id=?  ORDER BY  cr.id DESC  limit "
	                            + offset + "," + noOfRecords);
	            preparedStatement.setInt(1, is_active);
	            preparedStatement.setInt(2, userID);
	            preparedStatement.setInt(3, groupId);
	            ResultSet rs = preparedStatement.executeQuery();
	            while (rs.next()) {
	                ReviewDto review = new ReviewDto();
	                review.setCodeReviewId(rs.getInt("id"));
	                review.setActive((rs.getInt("is_active") == 1) ? true : false);
	                review.setCoverageAvgPercent(rs.getDouble("coverage_avg_percent"));
	                review.setCreationDate(rs.getTimestamp("creation_date"));
	                review.setName(rs.getString("name"));
	                int userId = rs.getInt("user_id");
	                review.setUserName(
	                        userService.get(userId).getFirstName() + " " + userService.get(userId).getLastName());
	                ;
	                review.setUserId(userId);
	                review.setTotalIssues(getTotalIssues(rs.getInt("id")));
	                review.setGitURL(getGitUrl(rs.getInt("id")));
	                list.add(review);
	            }

	            rs.close();

	            preparedStatement = connection.prepareStatement(
	                    "SELECT COUNT(*)   FROM code_review cr WHERE cr.is_active=? and cr.user_id=? and cr.group_id=? ");

	            preparedStatement.setInt(1, is_active);
	            preparedStatement.setInt(2, userID);
	            preparedStatement.setInt(3, groupId);
	            
	            rs = preparedStatement.executeQuery();
	            rs.next();
	            this.noOfRecords = rs.getInt(1);
	            System.out.println("Founded rows" + rs.getInt(1));

	        } catch (Exception e) {

	            e.printStackTrace();

	        }
	        return list;

	    }
	
	
	
	
	 public List<ReviewDto> getHistoryReviwByUserId(Integer groupId , Integer userID, int offset, int noOfRecords) {
	        List<ReviewDto> list = new ArrayList<>();

	        try {

	            PreparedStatement preparedStatement = connection
	                    .prepareStatement("SELECT a.*, u.fname, u.lname FROM ( SELECT cr.id, cr.name, cr.user_id, cr.is_active, cr.coverage_avg_percent, "
	                            + "cr.creation_date, cr.group_id FROM code_review_user cru JOIN code_review cr  ON cru.code_review_id =cr.id  "
	                            + "WHERE cru.status='FINISHED_CONFIRMED' AND cr.is_active=0 and cru.user_id=? "
	                            + " UNION "
	                            +" SELECT id,name, user_id, is_active, coverage_avg_percent, creation_date, cr.group_id FROM code_review cr "
	                            + "WHERE cr.is_active=0 and cr.user_id=? ) a JOIN `user` u ON u.id = a.user_id WHERE a.group_id=? " +
	                            " GROUP BY id DESC  limit "+ offset + "," + noOfRecords );
	            preparedStatement.setInt(1, userID);
	            preparedStatement.setInt(2, userID);
	            preparedStatement.setInt(3, groupId);
	            ResultSet rs = preparedStatement.executeQuery();
	            while (rs.next()) {
	                ReviewDto review = new ReviewDto();
	                review.setCodeReviewId(rs.getInt("id"));
	                review.setActive((rs.getInt("is_active") == 1) ? true : false);
	                review.setCoverageAvgPercent(rs.getDouble("coverage_avg_percent"));
	                review.setCreationDate(rs.getTimestamp("creation_date"));
	                review.setName(rs.getString("name"));
	                int userId = rs.getInt("user_id");
	                review.setUserName(rs.getString("fname") + " " + rs.getString("lname"));
	                review.setUserId(userId);
	                review.setTotalIssues(getTotalIssues(rs.getInt("id")));
	                review.setGitURL(getGitUrl(rs.getInt("id")));
	                list.add(review);
	            }

	            rs.close();

	            preparedStatement = connection
	                    .prepareStatement("SELECT count(*) FROM  ( SELECT cr.id, cr.name, cr.user_id, cr.is_active, cr.coverage_avg_percent, "
	                            + "cr.creation_date , cr.group_id FROM code_review_user cru JOIN code_review cr  ON cru.code_review_id =cr.id  "
	                            + "WHERE cru.status='FINISHED_CONFIRMED' AND cr.is_active=0 and cru.user_id=? "
	                            + " UNION "
	                            + " SELECT id,name, user_id, is_active, coverage_avg_percent, creation_date, cr.group_id  FROM code_review cr "
	                            + "WHERE cr.is_active=0 and cr.user_id=? ) a JOIN `user` u ON u.id = a.user_id WHERE a.group_id=? ");
	            preparedStatement.setInt(1, userID);
	            preparedStatement.setInt(2, userID);
	            preparedStatement.setInt(3, groupId);
	            rs = preparedStatement.executeQuery();
	            rs.next();
	            this.noOfRecords = rs.getInt(1);
	            System.out.println("Founded rows" + rs.getInt(1));

	        } catch (Exception e) {

	            e.printStackTrace();

	        }
	        return list;

	    }
	
	
	
	
	
	public String getGitUrl(int codeReviewId) throws SQLException {
		PreparedStatement stmt = connection.prepareStatement(GET_GIT_URL);
		stmt.setInt(1, codeReviewId);
		ResultSet rs = stmt.executeQuery();
		String gitUrl = null;

		if (rs.next()) {
			gitUrl = rs.getString(1);
		}

		return gitUrl;
	}

	public CodeReviewUser getByReviewIdAndUserId(Integer reviewId, Integer userIdForRemove) {
		CodeReviewUser codeReviewUser = null;

		String sqlQuery = "SELECT * FROM code_review_user" + " WHERE code_review_id=? AND user_id=?";

		try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
			pstmt.setInt(1, reviewId);
			pstmt.setInt(2, userIdForRemove);
			ResultSet rs = pstmt.executeQuery();
			codeReviewUser = Transformer.transformToObject(rs, CodeReviewUser.class);
		} catch (SQLException | InstantiationException | IllegalAccessException e) {
			LOG.error("Sql query: " + sqlQuery + " didn't pass!", e);
		}
		return codeReviewUser;
	}

	public Integer getTotalIssues(int codeReviewId) throws SQLException {
		PreparedStatement stmt = connection.prepareStatement(GET_TOTAL_ISSUES);
		stmt.setInt(1, codeReviewId);
		ResultSet rs = stmt.executeQuery();

		int totalIssues = 0;
		if (rs.next()) {
			totalIssues = rs.getInt(1);
		}
		return totalIssues;
	}

	public int getNoOfRecords() {
		return noOfRecords;
	}

	public void setNoOfRecords(int noOfRecords) {
		this.noOfRecords = noOfRecords;
	}


	
	//search typehead for reviews
	 public List<ReviewDto> getAllReviwByUserIdTH( Integer is_active, Integer userID) {
			List<ReviewDto> list = new ArrayList<>();

			try {
				PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM (     "
						+ " SELECT id,name, user_id, is_active, coverage_avg_percent, creation_date, group_id   FROM code_review cr "
						+ " WHERE cr.is_active=1 and cr.user_id=? "

						+ " UNION "

						+ " SELECT   deleted_review.id,   deleted_review.name, deleted_review.owner_id, deleted_review.is_active, deleted_review.coverage_avg_percent, "
						+ " deleted_review.creation_date,   deleted_review.group_id " + " FROM deleted_review  "
						+ " JOIN deleted_review_user ON deleted_review.id = deleted_review_user.deleted_review_id "
						+ " WHERE reviewer_id = ? AND status LIKE 'REVIEW_DELETED' AND deleted_review.is_active = 1 "

						+ " UNION "

						+ " SELECT cr.id, cr.name, cr.user_id, cr.is_active, cr.coverage_avg_percent, "
						+ " cr.creation_date, cr.group_id FROM code_review_user cru JOIN code_review cr  ON cru.code_review_id =cr.id  "
						+ " WHERE (cru.status='CONFIRMED' OR cru.status='DELETED' OR cru.status='OWNER_DELETED_OR_LEFT_GROUP') "
						+ " AND cru.user_id=?) a "
						+" UNION "
						+" SELECT * FROM ( SELECT cr.id, cr.name, cr.user_id, cr.is_active, cr.coverage_avg_percent, "
	                            + "cr.creation_date, cr.group_id FROM code_review_user cru JOIN code_review cr  ON cru.code_review_id =cr.id  "
	                            + "WHERE cru.status='FINISHED_CONFIRMED' AND cr.is_active=0 and cru.user_id=? "
	                            + " UNION "
	                            +" SELECT id,name, user_id, is_active, coverage_avg_percent, creation_date, cr.group_id FROM code_review cr "
	                            + "WHERE cr.is_active=0 and cr.user_id=? ) b"
						
						
						);
				
				preparedStatement.setInt(1, userID);
				preparedStatement.setInt(2, userID);
				preparedStatement.setInt(3, userID);
				preparedStatement.setInt(4, userID);
				preparedStatement.setInt(5, userID);

				ResultSet rs = preparedStatement.executeQuery();
				while (rs.next()) {
					ReviewDto review = new ReviewDto();
					review.setCodeReviewId(rs.getInt("id"));
					review.setActive((rs.getInt("is_active") == 1) ? true : false);
					review.setCoverageAvgPercent(rs.getDouble("coverage_avg_percent"));
					review.setCreationDate(rs.getTimestamp("creation_date"));
					review.setName(rs.getString("name"));
					CodeReviewUser user = getByReviewIdAndUserId(review.getCodeReviewId(), userID);
					review.setReviewerStatus(user == null ? null : user.getStatus());
					int userId = rs.getInt("user_id");
					review.setUserName(
							userService.get(userId).getFirstName() + " " + userService.get(userId).getLastName());
					
					review.setUserId(userId);
					review.setTotalSearchName(rs.getString("name")+rs.getTimestamp("creation_date"));
					review.setTotalIssues(getTotalIssues(rs.getInt("id")));
					review.setGitURL(getGitUrl(rs.getInt("id")));
					review.setTotalIssues(getTotalIssues(review.getCodeReviewId()));
					System.out.println("Review TH = "+review);
					list.add(review);
				}

				rs.close();

				
			} catch (Exception e) {

				e.printStackTrace();

			}
			return list;
		}
	
	
	
	
	

}
