package com.epam.codereview.dao;

import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.Language;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class LanguageDao extends AbstractDAO<Language> {

    private final static Logger LOG = Logger.getLogger(LanguageDao.class);

    public LanguageDao(Connection connection) {
        super(Language.class, connection);
    }

    public Map<String, Integer> getCountFilesByLanguage(Integer reviewId) {
        Map<String, Integer> languageMap = new LinkedHashMap<>();
        String sql = "SELECT l.name, COUNT(l.name) AS fcount FROM `language` l " +
                "JOIN `file` f ON l.id = f.language_id " +
                "JOIN `branch` b ON f.branch_id = b.id " +
                "JOIN `project` p ON b.project_id = p.id " +
                "WHERE p.code_review_id = " + reviewId +
                " GROUP BY l.name " +
                "ORDER BY fcount DESC, l.name ASC";
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String langName = rs.getString("name");
                Integer filesCount = rs.getInt("fcount");
                if (langName != null) {
                    languageMap.put(langName, filesCount);
                }
            }
        } catch (SQLException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return languageMap.size() > 0 ? languageMap : null;
    }

    public List<Language> getAllByReviewId(Integer reviewId) {
        List<Language> languages = null;
        String sql = "SELECT l.* FROM `language` l " +
                "JOIN `file` f ON l.id = f.language_id " +
                "JOIN `branch` b ON f.branch_id = b.id " +
                "JOIN `project` p ON b.project_id = p.id " +
                "WHERE p.code_review_id = " + reviewId;
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            languages = Transformer.transformToList(rs, Language.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return languages;
    }

    public List<Language> getAllWithIssuesByReviewId(Integer reviewId) {
        List<Language> languages = null;
        String sql = "SELECT DISTINCT l.* FROM issue i " +
                "JOIN `file` f ON i.file_id = f.id " +
                "JOIN `language` l ON f.language_id = l.id " +
                "JOIN `user` u ON i.user_id = u.id " +
                "JOIN branch b ON f.branch_id = b.id " +
                "JOIN project p ON b.project_id = p.id " +
                "WHERE p.code_review_id = " + reviewId;
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            languages = Transformer.transformToList(rs, Language.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return languages;
    }

}
