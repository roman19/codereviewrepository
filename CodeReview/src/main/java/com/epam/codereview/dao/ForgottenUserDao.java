package com.epam.codereview.dao;

import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.model.ForgottenUser;

import java.sql.Connection;

public class ForgottenUserDao extends AbstractDAO<ForgottenUser> {

	public ForgottenUserDao(Connection connection) {
		super(ForgottenUser.class, connection);
	}
}