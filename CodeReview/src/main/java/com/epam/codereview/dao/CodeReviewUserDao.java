package com.epam.codereview.dao;

import com.epam.codereview.dto.CodeReviewUserDto;
import com.epam.codereview.dto.ReviewDto;
import com.epam.codereview.manager.db.*;
import com.epam.codereview.model.CodeReviewUser;
import com.epam.codereview.service.UserService;

import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CodeReviewUserDao extends AbstractDAO<CodeReviewUser> {

    private final static Logger LOG = Logger.getLogger(CodeReviewUserDao.class);

    private UserService userService;
    private int noOfRecords;
    private final String GET_TOTAL_ISSUES="SELECT count_issues FROM project WHERE code_review_id=?";
    private final String GET_GIT_URL="SELECT git_url FROM codereview.project where code_review_id=?";
    private final String GET_REVIEWER_IN_ALL_REVIEWS_BY_GROUP = 
    		"SELECT r.id, r.code_review_id, r.user_id, r.status, r.coverage_percent, r.notification_date, r.is_notified_owner " +
				"FROM code_review c " + 
					"JOIN code_review_user r ON c.id = r.code_review_id " + 
					"JOIN group_user g ON g.user_id = r.user_id " + 
					"WHERE g.user_id = ? and c.group_id = ? and r.status = 'CONFIRMED' and c.is_active = 1 " + 
						"group by c.id;";

    public CodeReviewUserDao(Connection connection) {
        super(CodeReviewUser.class, connection);
        userService = new UserService();  //TODO remove service
    }
    
    /*
     * @author Andriy Palamar
     */
    public List<CodeReviewUser> getReviewerInAllReviewsByGroup(Integer groupId, Integer userId)
    		throws SQLException, InstantiationException, IllegalAccessException{
        PreparedStatement stmt = connection.prepareStatement(GET_REVIEWER_IN_ALL_REVIEWS_BY_GROUP);
        stmt.setInt(1, userId);
        stmt.setInt(2, groupId);
        ResultSet rs = stmt.executeQuery();
        List<CodeReviewUser> list = new ArrayList<>();
        CodeReviewUser user;
        do {
            user = (CodeReviewUser) Transformer.transformToObject(rs, CodeReviewUser.class);
            if (user != null)
                list.add(user);
        } while (user != null);
        rs.close();
        stmt.close();
        return list;
    }

	/*
	 * @author Vitalii Horban for showing issues of reviews ++
	 */

    public Integer getTotalIssues(int codeReviewId) throws SQLException{
        PreparedStatement stmt = connection.prepareStatement(GET_TOTAL_ISSUES);
        stmt.setInt(1, codeReviewId);
        ResultSet rs = stmt.executeQuery();

        int totalIssues=0;
        if( rs.next()){
            totalIssues=rs.getInt(1);
        }
        return totalIssues;
    }


    public String getGitUrl(int codeReviewId) throws SQLException{
        PreparedStatement stmt = connection.prepareStatement(GET_GIT_URL);
        stmt.setInt(1, codeReviewId);
        ResultSet rs = stmt.executeQuery();
        String gitUrl=null;

        if( rs.next()){
            gitUrl=rs.getString(1);
        }

        return gitUrl;
    }
	
	/*
	 * @author Vitalii Horban for showing active my reviews ++
	 */

    public List<ReviewDto> getAllMyActiveReviwByUserId(Integer userID, int offset, int noOfRecords) {
        List<ReviewDto> list = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT * FROM code_review cr WHERE cr.is_active=1 and cr.user_id=?  ORDER BY  cr.id DESC  limit "
                            + offset + "," + noOfRecords);
            preparedStatement.setInt(1, userID);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ReviewDto review = new ReviewDto();
                review.setCodeReviewId(rs.getInt("id"));
                review.setActive((rs.getInt("is_active") == 1) ? true : false);
                review.setCoverageAvgPercent(rs.getDouble("coverage_avg_percent"));
                review.setCreationDate(rs.getTimestamp("creation_date"));
                review.setName(rs.getString("name"));
                int userId = rs.getInt("user_id");
                review.setUserName(
                        userService.get(userId).getFirstName() + " " + userService.get(userId).getLastName());
                ;
                review.setUserId(userId);
                review.setTotalIssues(getTotalIssues(rs.getInt("id")));
                review.setGitURL(getGitUrl(rs.getInt("id")));
                list.add(review);
            }

            rs.close();

            preparedStatement = connection.prepareStatement(
                    "SELECT COUNT(*)   FROM code_review cr WHERE cr.is_active=1 and cr.user_id=? ");

            preparedStatement.setInt(1, userID);
            rs = preparedStatement.executeQuery();
            rs.next();
            this.noOfRecords = rs.getInt(1);
            System.out.println("Founded rows" + rs.getInt(1));

        } catch (Exception e) {

            e.printStackTrace();

        }
        return list;

    }
    
    
    
    

	/*
	 * @author Vitalii Horban for showing history my reviews ++
	 */

    public List<ReviewDto> getAllMyHistoryReviwByUserId(Integer userID, int offset, int noOfRecords) {
        List<ReviewDto> list = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "select   * FROM code_review cr WHERE cr.is_active=0 and cr.user_id=?  ORDER BY  cr.id DESC  limit "
                            + offset + "," + noOfRecords);
            preparedStatement.setInt(1, userID);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ReviewDto review = new ReviewDto();
                review.setCodeReviewId(rs.getInt("id"));
                review.setActive((rs.getInt("is_active") == 1) ? true : false);
                review.setCoverageAvgPercent(rs.getDouble("coverage_avg_percent"));
                review.setCreationDate(rs.getTimestamp("creation_date"));
                review.setName(rs.getString("name"));
                int userId = rs.getInt("user_id");
                review.setUserName(
                        userService.get(userId).getFirstName() + " " + userService.get(userId).getLastName());
                ;
                review.setUserId(userId);
                review.setTotalIssues(getTotalIssues(rs.getInt("id")));
                review.setGitURL(getGitUrl(rs.getInt("id")));
                list.add(review);
            }
            rs.close();

            preparedStatement = connection.prepareStatement(
                    "SELECT COUNT(*)   FROM code_review cr WHERE cr.is_active=0 and cr.user_id=? ");

            preparedStatement.setInt(1, userID);
            rs = preparedStatement.executeQuery();
            rs.next();
            this.noOfRecords = rs.getInt(1);
            System.out.println("Founded rows" + rs.getInt(1));

        } catch (Exception e) {

            e.printStackTrace();

        }
        return list;

    }

	/*
	 * @author Vitalii Horban for showing active other reviews
	 */

    public List<ReviewDto> getAllOtherActiveReviwByUserId(Integer userID, int offset, int noOfRecords) {
        List<ReviewDto> list = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT * FROM code_review_user cru JOIN code_review cr  ON cru.code_review_id =cr.id  "
                            + "WHERE (cru.status='CONFIRMED' OR cru.status='DELETED' OR cru.status='OWNER_DELETED_OR_LEFT_GROUP' OR cru.status='DELETED_BY_ADMIN') "
                            + "AND cru.user_id=? GROUP BY cr.id DESC limit "
                            + offset + "," + noOfRecords
            );
            preparedStatement.setInt(1, userID);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ReviewDto review = new ReviewDto();
                review.setCodeReviewId(rs.getInt("code_review_id"));
                review.setActive((rs.getInt("is_active") == 1) ? true : false);
                review.setCoverageAvgPercent(rs.getDouble("coverage_avg_percent"));
                review.setCreationDate(rs.getTimestamp("creation_date"));
                review.setName(rs.getString("name"));
                CodeReviewUser user = getByReviewIdAndUserId(review.getCodeReviewId(), userID);
                review.setReviewerStatus(user == null ? null:user.getStatus());
                int userId = rs.getInt(10);
                review.setUserName(
                        userService.get(userId).getFirstName() + " " + userService.get(userId).getLastName());
                review.setUserId(userId);
                review.setTotalIssues(getTotalIssues(rs.getInt("code_review_id")));
                review.setGitURL(getGitUrl(rs.getInt("code_review_id")));
                list.add(review);
            }

            rs.close();
            preparedStatement = connection.prepareStatement(
                    "SELECT COUNT(*) FROM code_review_user cru " +
                            "JOIN code_review cr  ON cru.code_review_id =cr.id  "
                            + "WHERE (cru.status='CONFIRMED' OR cru.status='DELETED' OR cru.status='OWNER_DELETED_OR_LEFT_GROUP' OR cru.status='DELETED_BY_ADMIN') "
                            + "AND cru.user_id=?"

            );

            preparedStatement.setInt(1, userID);
            rs = preparedStatement.executeQuery();
            rs.next();
            this.noOfRecords = rs.getInt(1);
            System.out.println("Founded rows" + rs.getInt(1));


        } catch (Exception e) {

            e.printStackTrace();

        }
        return list;

    }

	

	/*
	 * @author Vitalii Horban for showing history other reviews
	 */

    public List<ReviewDto> getAllOtherHistoryReviwByUserId(Integer userID, int offset, int noOfRecords) {
        List<ReviewDto> list = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT * FROM code_review_user cru " +
                            "JOIN code_review cr  ON cru.code_review_id =cr.id  " +
                            "WHERE cru.status='CONFIRMED'AND cr.is_active=0 and cru.user_id=? " +
                            "GROUP BY cr.id DESC limit " + offset + "," + noOfRecords);
            preparedStatement.setInt(1, userID);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ReviewDto review = new ReviewDto();
                review.setCodeReviewId(rs.getInt("code_review_id"));
                review.setActive((rs.getInt("is_active") == 1) ? true : false);
                review.setCoverageAvgPercent(rs.getDouble("coverage_avg_percent"));
                review.setCreationDate(rs.getTimestamp("creation_date"));
                review.setName(rs.getString("name"));
                int userId = rs.getInt("user_id");
                review.setUserName(userService.get(userId).getFirstName()+" "+userService.get(userId).getLastName()); ;
                review.setUserId(userId);
                review.setTotalIssues(getTotalIssues(rs.getInt("id")));
                review.setGitURL(getGitUrl(rs.getInt("id")));
                list.add(review);
            }

            rs.close();

            preparedStatement = connection.prepareStatement(
                    "SELECT  COUNT(*) FROM code_review_user cru " +
                            "JOIN code_review cr  ON cru.code_review_id =cr.id  " +
                            "WHERE cru.status='CONFIRMED' AND cr.is_active=0 and cru.user_id=? ");
            preparedStatement.setInt(1, userID);
            rs = preparedStatement.executeQuery();
            rs.next();
            this.noOfRecords = rs.getInt(1);
            System.out.println("Founded rows" + rs.getInt(1));

        } catch (Exception e) {

            e.printStackTrace();

        }
        return list;

    }
	
	
	/*
	 * @author Vitalii Horban for showing active all reviews
	 */

    public List<ReviewDto> getAllActiveReviwByUserId(Integer userID, int offset, int noOfRecords) {
        List<ReviewDto> list = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT * FROM  ( SELECT cr.id, cr.name, cr.user_id, cr.is_active, cr.coverage_avg_percent, "
                            + "cr.creation_date FROM code_review_user cru JOIN code_review cr  ON cru.code_review_id =cr.id  "
                            + "WHERE (cru.status='CONFIRMED' OR cru.status='DELETED' OR cru.status='OWNER_DELETED_OR_LEFT_GROUP' OR cru.status='DELETED_BY_ADMIN')"
                            + " AND cru.user_id=? "
                            + " UNION "
                            +" SELECT id,name, user_id, is_active, coverage_avg_percent, creation_date  FROM code_review cr "
                            + "WHERE cr.is_active=1 and cr.user_id=? ) a GROUP BY id DESC  limit "+ offset + "," + noOfRecords );
            preparedStatement.setInt(1, userID);
            preparedStatement.setInt(2, userID);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ReviewDto review = new ReviewDto();
                review.setCodeReviewId(rs.getInt("id"));
                review.setActive((rs.getInt("is_active") == 1) ? true : false);
                review.setCoverageAvgPercent(rs.getDouble("coverage_avg_percent"));
                review.setCreationDate(rs.getTimestamp("creation_date"));
                review.setName(rs.getString("name"));
                CodeReviewUser user = getByReviewIdAndUserId(review.getCodeReviewId(), userID);
                review.setReviewerStatus(user == null ? null:user.getStatus());
                int userId = rs.getInt("user_id");
                review.setUserName(userService.get(userId).getFirstName()+" "+userService.get(userId).getLastName()); ;
                review.setUserId(userId);
                review.setTotalIssues(getTotalIssues(rs.getInt("id")));
                review.setGitURL(getGitUrl(rs.getInt("id")));
                list.add(review);
            }

            rs.close();

            preparedStatement = connection
                    .prepareStatement("SELECT count(*) FROM  ( SELECT cr.id, cr.name, cr.user_id, cr.is_active, cr.coverage_avg_percent, "
                            + "cr.creation_date FROM code_review_user cru JOIN code_review cr  ON cru.code_review_id =cr.id  "
                            + "WHERE (cru.status='CONFIRMED' OR cru.status='DELETED' OR cru.status='OWNER_DELETED_OR_LEFT_GROUP' OR cru.status='DELETED_BY_ADMIN')"
                            + " AND cru.user_id=? "
                            + " UNION "
                            +" SELECT id,name, user_id, is_active, coverage_avg_percent, creation_date  FROM code_review cr "
                            + "WHERE cr.is_active=1 and cr.user_id=? ) a");
            preparedStatement.setInt(1, userID);
            preparedStatement.setInt(2, userID);
            rs = preparedStatement.executeQuery();
            rs.next();
            this.noOfRecords = rs.getInt(1);
            System.out.println("Founded rows" + rs.getInt(1));

        } catch (Exception e) {

            e.printStackTrace();

        }
        return list;
    }
	
	/*
	 * @author Vitalii Horban for showing active all reviews
	 */

    public List<ReviewDto> getAllHistoryReviwByUserId(Integer userID, int offset, int noOfRecords) {
        List<ReviewDto> list = new ArrayList<>();

        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT a.*, u.fname, u.lname FROM ( SELECT cr.id, cr.name, cr.user_id, cr.is_active, cr.coverage_avg_percent, "
                            + "cr.creation_date FROM code_review_user cru JOIN code_review cr  ON cru.code_review_id =cr.id  "
                            + "WHERE cru.status='FINISHED_CONFIRMED' AND cr.is_active=0 and cru.user_id=? "
                            + " UNION "
                            +" SELECT id,name, user_id, is_active, coverage_avg_percent, creation_date  FROM code_review cr "
                            + "WHERE cr.is_active=0 and cr.user_id=? ) a JOIN `user` u ON u.id = a.user_id " +
                            "GROUP BY id DESC  limit "+ offset + "," + noOfRecords );
            preparedStatement.setInt(1, userID);
            preparedStatement.setInt(2, userID);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                ReviewDto review = new ReviewDto();
                review.setCodeReviewId(rs.getInt("id"));
                review.setActive((rs.getInt("is_active") == 1) ? true : false);
                review.setCoverageAvgPercent(rs.getDouble("coverage_avg_percent"));
                review.setCreationDate(rs.getTimestamp("creation_date"));
                review.setName(rs.getString("name"));
                int userId = rs.getInt("user_id");
                review.setUserName(rs.getString("fname") + " " + rs.getString("lname"));
                review.setUserId(userId);
                review.setTotalIssues(getTotalIssues(rs.getInt("id")));
                review.setGitURL(getGitUrl(rs.getInt("id")));
                list.add(review);
            }

            rs.close();

            preparedStatement = connection
                    .prepareStatement("SELECT count(*) FROM  ( SELECT cr.id, cr.name, cr.user_id, cr.is_active, cr.coverage_avg_percent, "
                            + "cr.creation_date FROM code_review_user cru JOIN code_review cr  ON cru.code_review_id =cr.id  "
                            + "WHERE cru.status='FINISHED_CONFIRMED' AND cr.is_active=0 and cru.user_id=? "
                            + " UNION "
                            + " SELECT id,name, user_id, is_active, coverage_avg_percent, creation_date  FROM code_review cr "
                            + "WHERE cr.is_active=0 and cr.user_id=? ) a");
            preparedStatement.setInt(1, userID);
            preparedStatement.setInt(2, userID);
            rs = preparedStatement.executeQuery();
            rs.next();
            this.noOfRecords = rs.getInt(1);
            System.out.println("Founded rows" + rs.getInt(1));

        } catch (Exception e) {

            e.printStackTrace();

        }
        return list;

    }

    public int getNoOfRecords() {
        return noOfRecords;
    }

    public void setNoOfRecords(int noOfRecords) {
        this.noOfRecords = noOfRecords;
    }

    public List<CodeReviewUser> getByReviewId(Integer reviewId) {
        List<CodeReviewUser> codeReviewUsers = null;
        String sql = "SELECT * FROM code_review_user WHERE code_review_id = " + reviewId;
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            codeReviewUsers = Transformer.transformToList(rs, CodeReviewUser.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return codeReviewUsers;
    }

    public List<CodeReviewUserDto> getDtoByReviewId(Integer reviewId) {
        List<CodeReviewUserDto> codeReviewUsers = null;
        String sql = "SELECT cru.user_id, cru.status, u.login, u.fname, u.lname, " +
                "COUNT(i.id) AS issues_count FROM code_review_user cru " +
                "JOIN `user` u ON cru.user_id = u.id " +
                "JOIN project p ON cru.code_review_id = p.code_review_id " +
                "JOIN branch b ON p.id = b.project_id " +
                "JOIN file f ON b.id = f.branch_id " +
                "LEFT JOIN issue i ON f.id = i.file_id AND u.id = i.user_id " +
                "WHERE cru.code_review_id = " + reviewId + " AND cru.status NOT IN ('DELETED', 'DELETED_CONFIRMED', " +
                "'OWNER_DELETED_OR_LEFT_GROUP', 'OWNER_DELETED_OR_LEFT_GROUP_CONFIRMED') AND cru.is_notified_owner = b'0' " +
                "GROUP BY cru.user_id";
        try(Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            codeReviewUsers = Transformer.transformToList(rs, CodeReviewUserDto.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sql + " didn't pass.", e);
        }
        return codeReviewUsers;
    }

    public List<Integer> getUsersByReviewId(Integer reviewId){
        Criteria criteria = new Criteria();
        criteria.add(new Condition("code_review_id", "=", reviewId));
        criteria.add(new Operator("AND"));
        criteria.add(new Condition("status", "LIKE", "CONFIRMED"));

        List<CodeReviewUser> reviewUsers = get(criteria);

        return reviewUsers.stream().map(i->i.getUserId()).collect(Collectors.toList());
    }

    public CodeReviewUser getByReviewIdAndUserId(Integer reviewId, Integer userIdForRemove) {
        CodeReviewUser codeReviewUser = null;

        String sqlQuery = "SELECT * FROM code_review_user" +
                " WHERE code_review_id=? AND user_id=?";

        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setInt(1, reviewId);
            pstmt.setInt(2, userIdForRemove);
            ResultSet rs = pstmt.executeQuery();
            codeReviewUser = Transformer.transformToObject(rs, CodeReviewUser.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Sql query: " + sqlQuery + " didn't pass!", e);
        }
        return codeReviewUser;
    }

    public void addUsersById(Integer reviewId, int[] usersIdForAdd) {
        String insertQuery = "INSERT INTO code_review_user(user_id,code_review_id,status,is_notified_owner) VALUES(?,?,?,b'0')";
        String updateQuery = "UPDATE code_review_user SET status='PENDING', is_notified_owner = b'0' WHERE code_review_id=" + reviewId + " AND user_id=?";
        try {
            PreparedStatement insertPstmt = connection.prepareStatement(insertQuery);
            PreparedStatement updatePstmt = connection.prepareStatement(updateQuery);
            connection.setAutoCommit(false);
            for (int userId : usersIdForAdd) {
                if (getByReviewIdAndUserId(reviewId, userId) == null) {
                    insertPstmt.setInt(1, userId);
                    insertPstmt.setInt(2, reviewId);
                    insertPstmt.setString(3, "PENDING");
                    insertPstmt.addBatch();
                } else {
                    updatePstmt.setInt(1, userId);
                    updatePstmt.addBatch();
                }
            }
            insertPstmt.executeBatch();
            insertPstmt.close();
            updatePstmt.executeBatch();
            updatePstmt.close();
            connection.commit();
        } catch (SQLException e) {
            LOG.error(e);
            try {
                connection.rollback();
            } catch (SQLException e1) {
                LOG.error(e1);
            }
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                LOG.error(e);
            }
        }
    }

    //for retrieving review notifications
    public List<CodeReviewUser> getReviewNotificationsByUserId(
            Integer userId, int count, int offset) {

        List<CodeReviewUser> notifications = new ArrayList<>();

        String sqlQuery = "SELECT cru.* FROM code_review_user cru JOIN code_review cr ON cru.code_review_id=cr.id "
                + "WHERE cru.user_id=? AND cru.status LIKE 'PENDING' AND cr.is_active=1 ORDER BY cru.notification_date DESC LIMIT ? OFFSET ?";

        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setInt(1, userId);
            pstmt.setInt(2, count);
            pstmt.setInt(3, offset);
            ResultSet rs = pstmt.executeQuery();
            notifications = Transformer.transformToList(rs, CodeReviewUser.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sqlQuery + " has aborted.", e);
        }

        return notifications;
    }

    public Integer getCountOfReviewNotificationsByUserId(Integer userId) {
        Integer count = null;

        String sqlQuery = "SELECT COUNT(*) FROM code_review_user cru JOIN code_review cr ON cru.code_review_id=cr.id "
                + "WHERE cru.user_id=? AND cru.status LIKE 'PENDING' AND cr.is_active=1";

        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setInt(1, userId);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOG.error("Query: " + sqlQuery + " has aborted.", e);
        }

        return count;
    }
}
