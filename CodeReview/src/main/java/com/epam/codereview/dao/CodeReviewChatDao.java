package com.epam.codereview.dao;

import com.epam.codereview.controller.MainController;
import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.model.CodeReviewChat;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CodeReviewChatDao extends AbstractDAO<CodeReviewChat> {
    private final static Logger LOG = Logger.getLogger(MainController.class);

    public CodeReviewChatDao(Connection connection) {
        super(CodeReviewChat.class, connection);
    }

    public Integer getCountOfNewMessagesForUser(Integer userId, Integer reviewId) {
        Integer count = null;

        String sqlQuery = "SELECT COUNT(*) FROM user u JOIN code_review_user cu ON u.id=cu.user_id JOIN  chat_code_review c ON c.code_review_id=cu.code_review_id WHERE c.code_review_id=? AND u.id=? AND c.time>u.last_online";
        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setInt(1, reviewId);
            pstmt.setInt(2, userId);

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                count = rs.getInt(1);
            }

        } catch (SQLException e) {
            LOG.error("Query: " + sqlQuery + " has aborted!", e);
        }
        return count;
    }

    public Integer getCountOfNewMessagesForOwner(Integer userId, Integer reviewId) {
        Integer count = null;

        String sqlQuery = "SELECT COUNT(*) FROM user u JOIN code_review cr ON u.id=cr.user_id JOIN  chat_code_review c ON c.code_review_id=cr.id WHERE c.code_review_id=? AND u.id=?  AND c.time>u.last_online";
        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setInt(1, reviewId);
            pstmt.setInt(2, userId);

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                count = rs.getInt(1);
            }

        } catch (SQLException e) {
            LOG.error("Query: " + sqlQuery + " has aborted!", e);
        }
        return count;
    }

}
