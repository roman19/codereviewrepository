package com.epam.codereview.dao;

import com.epam.codereview.dto.IssueNotificationDto;
import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class IssueNotificationDao extends AbstractDAO<IssueNotificationDao> {

	private final static Logger LOG = Logger.getLogger(IssueNotificationDao.class);

    public IssueNotificationDao(Connection connection) {
        super(IssueNotificationDao.class, connection);
    }

    public List<IssueNotificationDto> getIssueNotificationsByUserId(
			Integer userId, int count, int offset) {

		List<IssueNotificationDto> notifications = new ArrayList<>();

		String sqlQuery = "SELECT i.id, i.title, i.body, i.user_id, cr.name, cr.id AS reviewid, f.url, f.id AS fileid, i.time FROM issue i JOIN file f ON i.file_id=f.id "
				+ "JOIN branch b ON b.id=f.branch_id "
				+ "JOIN project p ON p.id=b.project_id "
				+ "JOIN code_review cr ON cr.id=p.code_review_id "
				+ "WHERE cr.user_id=? AND i.user_id<>cr.user_id AND i.is_notified=0 AND i.type LIKE 'ERROR' ORDER BY i.time DESC LIMIT ? OFFSET ?";
		
		try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
			pstmt.setInt(1, userId);
			pstmt.setInt(2, count);
			pstmt.setInt(3, offset);
			ResultSet rs = pstmt.executeQuery();
			notifications = Transformer.transformToList(rs, IssueNotificationDto.class);
		} catch (SQLException | InstantiationException | IllegalAccessException e) {
			LOG.error("Query: " + sqlQuery + " has aborted.", e);
		}

		return notifications;
	}
	
	public Integer getCountOfIssueNotificationsByUserId(Integer userId) {
		Integer count = null;
		
		String sqlQuery = "SELECT COUNT(*) FROM issue i JOIN file f ON i.file_id=f.id "
				+ "JOIN branch b ON b.id=f.branch_id "
				+ "JOIN project p ON p.id=b.project_id "
				+ "JOIN code_review cr ON cr.id=p.code_review_id "
				+ "WHERE cr.user_id=? AND i.user_id<>cr.user_id AND i.is_notified=0 AND i.type LIKE 'ERROR'";
		
		try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
			pstmt.setInt(1, userId);
			
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOG.error("Query: " + sqlQuery + " has aborted.", e);
		}

		return count;
	}

}
