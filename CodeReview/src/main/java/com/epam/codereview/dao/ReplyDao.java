package com.epam.codereview.dao;

import com.epam.codereview.dto.ReplyNotificationDto;
import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Condition;
import com.epam.codereview.manager.db.Criteria;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.Reply;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ReplyDao extends AbstractDAO<Reply> {

    private final static Logger LOG = Logger.getLogger(ReplyDao.class);

    public ReplyDao(Connection connection) {
        super(Reply.class, connection);
    }

    public List<Reply> getRepliesByIssueId(Integer issueId) {
        Criteria criteria = new Criteria();
        criteria.add(new Condition("issue_id", "=", issueId));

        return get(criteria);
    }

    public Integer getCountOfRepliesForIssue(Integer issueId) {
        Integer count = null;

        String sqlQuery = "SELECT COUNT(*) FROM reply WHERE issue_id=?";

        try (PreparedStatement pstmt = connection
                .prepareStatement(sqlQuery)) {
            pstmt.setInt(1, issueId);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOG.error("Query: " + sqlQuery + " has aborted.", e);
        }

        return count;
    }

    public ReplyNotificationDto getReplyNotificationDto(Integer replyId) {
        ReplyNotificationDto notification = null;

        String sqlQuery = "SELECT r.owner_id, r.id AS reply_id, r.message, r.issue_id, i.title, i.file_id, cr.id AS review_id, cr.name AS review_name, r.time "
                + "FROM reply r "
                + "JOIN issue i ON i.id=r.issue_id "
                + "JOIN file f ON f.id=i.file_id "
                + "JOIN branch b ON b.id=f.branch_id "
                + "JOIN project p ON p.id=b.project_id "
                + "JOIN code_review cr ON cr.id=p.code_review_id "
                + "WHERE r.id=?";

        try (PreparedStatement pstmt = connection
                .prepareStatement(sqlQuery)) {
            pstmt.setInt(1, replyId);
            ResultSet rs = pstmt.executeQuery();
            notification = Transformer.transformToObject(rs,
                    ReplyNotificationDto.class);
        } catch (SQLException | InstantiationException | IllegalAccessException e) {
            LOG.error("Query: " + sqlQuery + " has aborted.", e);
        }

        return notification;
    }

}
