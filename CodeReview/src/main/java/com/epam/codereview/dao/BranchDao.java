package com.epam.codereview.dao;

import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.manager.db.Transformer;
import com.epam.codereview.model.Branch;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BranchDao extends AbstractDAO<Branch> {

	private final static Logger LOG = Logger.getLogger(BranchDao.class);

	public BranchDao(Connection connection) {
		super(Branch.class, connection);
	}

	public Branch getByProjectId(int projectId) {
		Branch branch = null;
		String sql = "SELECT * FROM branch WHERE project_id = " + projectId;
		try(Statement stmt = connection.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			branch  = Transformer.transformToObject(rs, Branch.class);
		} catch (SQLException | InstantiationException | IllegalAccessException e) {
			LOG.error("Query: " + sql + " didn't pass.", e);
		}
		return branch;
	}
}
