package com.epam.codereview.dao;

import com.epam.codereview.manager.db.AbstractDAO;
import com.epam.codereview.model.ReplyLike;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ReplyLikeDao extends AbstractDAO<ReplyLike> {

    public ReplyLikeDao(Connection connection) {
        super(ReplyLike.class, connection);
    }

    private static String CHECK_CURRENT_LIKE = "SELECT COUNT(*) FROM reply_like i WHERE i.reply_id=? AND i.user_id=?";
    private static String COUNT_ALL_LIKES = "SELECT COUNT(*) FROM reply_like i WHERE i.reply_id=? AND i.value = 1";
    private static String COUNT_ALL_DISLIKES = "SELECT COUNT(*) FROM reply_like i WHERE i.reply_id=? AND i.value = -1";
    private static String UPDATE_LIKE = "UPDATE reply_like i SET i.value=? WHERE i.reply_id=? AND i.user_id=?";
    private static String GET_LIKE_VALUE ="SELECT i.value FROM reply_like i WHERE i.reply_id=? AND i.user_id=?";
    private static String GET_REPLY_IDS ="SELECT  r.reply_id FROM reply_like r WHERE r.user_id=?";
    private static String GET_ALL_REPLY_IDS_TO_ISSUE = "SELECT DISTINCT a.reply_id  from (SELECT r.reply_id, r.value, re.issue_id FROM reply_like r join reply re where r.reply_id=re.id ) a WHERE a.issue_id=?";

    public Set<Integer> getAllReplyId(int issue) {
        Set<Integer> list = new HashSet<>();
        try {
            connection.prepareStatement("set names utf8").executeQuery();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_REPLY_IDS_TO_ISSUE);
            preparedStatement.setInt(1, issue);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                list.add(rs.getInt("reply_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Integer> getAllUserReplyId(int userId) {
        List<Integer> list = new ArrayList<>();
        try {
            connection.prepareStatement("set names utf8").executeQuery();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_REPLY_IDS);
            preparedStatement.setInt(1, userId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                list.add(rs.getInt("reply_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /*
     * @author: Vitalii Horban For counting users in group
     */
    public Integer checkCurrentLike(Integer userID, Integer replyID) {
        int issueNumber = 0;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CHECK_CURRENT_LIKE);
            preparedStatement.setInt(1, replyID);
            preparedStatement.setInt(2, userID);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                issueNumber = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return issueNumber;
    }

    public Integer getValueOfLike(Integer userID, Integer replyID) {
        int issueNumber = 0;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_LIKE_VALUE);
            preparedStatement.setInt(1, replyID);
            preparedStatement.setInt(2, userID);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                issueNumber = rs.getInt(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return issueNumber;
    }

    public Integer countTotalLike( Integer replyID) {
        int issueNumber = 0;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(COUNT_ALL_LIKES);
            preparedStatement.setInt(1, replyID);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                issueNumber = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return issueNumber;
    }

    public Integer countTotalDisLike(Integer replyID) {
        int likesNumber = 0;
        try {
            PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement(COUNT_ALL_DISLIKES);
            preparedStatement.setInt(1, replyID);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                likesNumber = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return likesNumber;
    }

    public Integer likeOnReply(Integer userID, Integer replyID) {
        if (checkCurrentLike(userID, replyID) == 0) {
            ReplyLike like = new ReplyLike();
            like.setReplyId(replyID);
            like.setUserId(userID);
            like.setValue(1);
            return insert(like);
        } else {
            try {
                connection.prepareStatement("set names utf8").executeQuery();
                PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement(UPDATE_LIKE);
                preparedStatement.setInt(1, 1);
                preparedStatement.setInt(2, replyID);
                preparedStatement.setInt(3, userID);
                preparedStatement.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return -1;
        }
    }

    public Integer disLikeOnReply(Integer userID, Integer replyID) {
        if (checkCurrentLike(userID, replyID) == 0) {
            ReplyLike like = new ReplyLike();
            like.setReplyId(replyID);
            like.setUserId(userID);
            like.setValue(-1);
            return insert(like);
        } else {
            try {
                connection.prepareStatement("set names utf8").executeQuery();
                PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement(UPDATE_LIKE);
                preparedStatement.setInt(1, -1);
                preparedStatement.setInt(2, replyID);
                preparedStatement.setInt(3, userID);
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return -1;
        }
    }

    public Integer decrementLikeOnReply(Integer userID, Integer replyID) {
        if (checkCurrentLike(userID, replyID) == 0) {
            ReplyLike like = new ReplyLike();
            like.setReplyId(replyID);
            like.setUserId(userID);
            like.setValue(0);
            return insert(like);
        } else {
            try {
                connection.prepareStatement("set names utf8").executeQuery();
                PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement(UPDATE_LIKE);
                preparedStatement.setInt(1, 0);
                preparedStatement.setInt(2, replyID);
                preparedStatement.setInt(3, userID);
                preparedStatement.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return -1;
        }
    }

}
