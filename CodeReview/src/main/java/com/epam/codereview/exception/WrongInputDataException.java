package com.epam.codereview.exception;

public class WrongInputDataException extends Exception {
	
	private static final long serialVersionUID = 7717547110328625814L;

	public WrongInputDataException() {
		super("Wrong input data!!!");
	}

	public WrongInputDataException(String arg0) {
		super(arg0);
	}
	
	
	
}
