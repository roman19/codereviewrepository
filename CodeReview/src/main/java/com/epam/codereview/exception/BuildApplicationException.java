package com.epam.codereview.exception;

public class BuildApplicationException extends RuntimeException {
	private static final long serialVersionUID = -4818848661530059857L;

	public BuildApplicationException() {
		super();
	}

	public BuildApplicationException(String arg0) {
		super(arg0);
	}
}
