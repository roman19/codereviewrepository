package com.epam.codereview.exception;

public class SessionIsNotAvailableException extends Exception {
	private static final long serialVersionUID = -2787853824166984747L;

	public SessionIsNotAvailableException() {
		super();
	}

	public SessionIsNotAvailableException(String arg0) {
		super(arg0);
	}
	
	

}
