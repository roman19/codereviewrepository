package com.epam.codereview.exception;

public class TransactionRollbackException extends Exception {

    public TransactionRollbackException(String message) {
        super(message);
    }

    public TransactionRollbackException(String message, Throwable cause) {
        super(message, cause);
    }
}
