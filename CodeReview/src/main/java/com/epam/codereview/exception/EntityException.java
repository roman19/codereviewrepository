package com.epam.codereview.exception;

public class EntityException extends RuntimeException {
	
	private static final long serialVersionUID = -4720893639229288455L;

	public EntityException() {
		super();
	}

	public EntityException(String arg0) {
		super(arg0);
	}

	public EntityException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
	
	

}
