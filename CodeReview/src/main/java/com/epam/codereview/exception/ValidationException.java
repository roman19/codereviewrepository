package com.epam.codereview.exception;

public class ValidationException extends Exception {

	private static final long serialVersionUID = 6088146346278056256L;

	public ValidationException() {
		super();
	}

	public ValidationException(String arg0) {
		super(arg0);
	}

	public ValidationException(Throwable arg0) {
		super(arg0);
	}
	
}
