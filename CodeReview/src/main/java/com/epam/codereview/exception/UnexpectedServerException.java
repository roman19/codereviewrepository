package com.epam.codereview.exception;

public class UnexpectedServerException extends RuntimeException {

    public UnexpectedServerException() {
    }

    public UnexpectedServerException(String message) {
        super(message);
    }

    public UnexpectedServerException(String message, Throwable cause) {
        super(message, cause);
    }
}
