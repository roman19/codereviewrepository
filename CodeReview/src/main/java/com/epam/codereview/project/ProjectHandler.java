package com.epam.codereview.project;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class ProjectHandler {
	private List<String> pathes;
	private String projectsPath;
	private long projectSize = 0;

	public ProjectHandler(String[] pathes, String pathToProject){
		this.pathes = (pathes != null ? Arrays.asList(pathes):null);
		this.projectsPath = pathToProject.replaceAll("/", "\\\\");
	}
	
	public long getSizeOfProject(String projectName){
		return getSizeOfFolder(new File(projectsPath + projectName));
	}
	
	private long getSizeOfFolder(File project){
		long length = 0;
	    for (File file : project.listFiles()) {
	        if (file.isFile()){
	        	length += file.length();
	        }else if(!file.getName().equals(".git")){
	            length += getSizeOfFolder(file);
	        }
	    }
	    return length;
	}
	
	public void removeUncheckedFiles(String projectName){
		startRemoving(new File(projectsPath + projectName));
	}
	
	private void startRemoving(File dir) {
	    if (dir.isDirectory()) {
	        File[] items = dir.listFiles();
	        if (items != null && items.length > 0) {
	            for (File item : items) {
	            	removeDirectory(item);
	            }
	        }
	    }
	}
	
	private void removeDirectory(File dir) {
	    if (dir.isDirectory()) {
	        File[] items = dir.listFiles();
	        if (items != null && items.length > 0) {
	            for (File item : items) {
	                removeDirectory(item);
	            }
	        }
	        dir.delete();
	    } else {
	        if(!isFileChecked(dir)) dir.delete();
	        else projectSize += dir.length();
	    }
	}
	
	private boolean isFileChecked(File file){
		if(pathes == null) return false;
		for(int i = 0; i < pathes.size(); i++){
			if(file.getAbsolutePath().equals(projectsPath + pathes.get(i))){
				return true;
			}
		}
		return false;
	}

	public long getProjectSize() {
		return projectSize;
	}
	
	public static long convertBytesInMegaBytes(long bytes){
		return (bytes / 1024) / 1024;
	}
	
	public static Double convertBytesInMegaBytesDouble(long bytes){
		return new Double((double) (bytes / 1024) / 1024);
	}
}
