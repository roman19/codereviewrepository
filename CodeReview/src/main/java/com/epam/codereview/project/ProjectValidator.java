package com.epam.codereview.project;

import java.io.File;

import org.apache.commons.io.FilenameUtils;

import com.epam.codereview.service.LanguageService;

public class ProjectValidator {

	public static void validate(File item) {
		File[] files = item.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				validate(file);
				if (file.listFiles().length == 0) {
					file.delete();
				}
			} else if (!new LanguageService().exists(FilenameUtils.getExtension(file.getName()))) {
				file.delete();
			}
		}
	}
}
