package com.epam.codereview.tree;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ProjectFolder extends ProjectItem {
	private List<ProjectItem> items;

	public ProjectFolder(File item) {
		super(item);
		items = new ArrayList<>();
	}

	public ProjectFolder(File item, String name) {
		super(item, name);
		items = new ArrayList<>();
	}

	public List<ProjectItem> getItems() {
		return items;
	}

	public void addItem(ProjectItem item) {
		items.add(item);
	}

}
