package com.epam.codereview.tree;

import java.io.File;
import java.util.List;

import javafx.util.Pair;

import org.apache.commons.io.FilenameUtils;

import com.epam.codereview.dao.manager.DaoManager;
import com.epam.codereview.model.FileUser;
import com.epam.codereview.service.LanguageService;
import com.epam.codereview.service.ProjectService;

public class TreeBuilder {
	private String projectsPath;
	private String projectName;
	private Project project;
	private StringBuilder tree;
    private boolean isSlash;
    private DaoManager daoManager = new DaoManager();
    private Integer currentUserId;
    private String fullProjectName;

	public TreeBuilder(String project, String projectName, String projectsPath, Integer ownerID) {
		ProjectFolder projectFolder = new ProjectFolder(new File(projectsPath + ownerID + "/" + project));
		this.isSlash = true;
		getProjectItems(projectFolder);
		this.project = new Project(project, projectFolder);
		this.projectName = projectName;
		this.projectsPath = projectsPath;
	}

    public TreeBuilder(String project, String projectName, String projectsPath, Integer ownerID, Integer currentUserId) {
        this(project, projectName, projectsPath, ownerID);
        this.currentUserId = ownerID.equals(currentUserId) ? null : currentUserId;
    }

	public TreeBuilder(String project, String projectName, String projectsPath, Integer ownerID, boolean isSlash) {
		ProjectFolder projectFolder = new ProjectFolder(new File(projectsPath + ownerID + "/" + project));
		this.isSlash = isSlash;
		getProjectItems(projectFolder);
		this.project = new Project(project, projectFolder);
		this.projectName = projectName;
		this.projectsPath = projectsPath;
	}

	private void getProjectItems(ProjectFolder projectFolder) {
		File[] files = projectFolder.getItem().listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				if (file.getName().equals(".git"))
					continue;
				ProjectFolder folder;
				if(isSlash) folder = getWhileEmpty(new ProjectFolder(file));
				else folder = new ProjectFolder(file);
				projectFolder.addItem(folder);
				getProjectItems(folder);
			} else {
				if (!file.getName().contains("."))
					continue;
				projectFolder.addItem(new ProjectFile(file));
			}
		}
	}
	
	private ProjectFolder getWhileEmpty(ProjectFolder folder){
		if(!isFolderEmpty(folder)) return folder;
		ProjectFolder temp = new ProjectFolder(folder.getItem().listFiles()[0], folder.toString());
		temp.complementName(getNameOfFolder(folder));
		return getWhileEmpty(temp);
	}
	
	private String getNameOfFolder(ProjectFolder folder){
		File file = folder.getItem();
		File[] files = file.listFiles();
		return files[0] == null ? "":files[0].getName();
	}
	
	private boolean isFolderEmpty(ProjectFolder folder){
		File file = folder.getItem();
		File[] files = file.listFiles();
		if(files.length == 1 && files[0].isDirectory()) return true;
		return false;
	}

	public String buildCheckBoxTree() {
		tree = new StringBuilder("<div class=\"container tree\">" + "<div class=\"panel panel-default\">"
				+ "<div class=\"panel-heading\">Project name: "
				+ (projectName != null ? projectName : project.getName())
				+ "</div><div class=\"panel-body\"><ul class=\"treeview\">"
				+ "<li><input id=\"tree_1\" type=\"checkbox\" name=\""
				+ project.getParentFolder().getItem().getAbsolutePath()
				+ "\" onchange=\"selectFolder('1')\"><a href=\"#\">" + project.getName() + "</a>");
		createCheckBoxTree((ProjectFolder) project.getParentFolder(), "1");
		tree.append("</li></ul></div></div></div>");
		return tree.toString();
	}

	private void createCheckBoxTree(ProjectFolder projectFolder, String id) {
		List<ProjectItem> files = projectFolder.getItems();
		int folderId = 1;
		tree.append("<ul>");
		for (ProjectItem file : files) {
			if (file instanceof ProjectFolder) {
				tree.append("<li><input id=\"tree_" + id + "_" + folderId + "\"" + " type=\"checkbox\" name=\""
						+ getRelativePath(file.getItem()) + "\"" + " onchange=\"selectFolder('" + id + "_" + folderId
						+ "')\"><a href=\"#\">" + file + "</a>");
				createCheckBoxTree((ProjectFolder) file, id + "_" + folderId);
				tree.append("</li>");
			} else {
				String status = "";
				if(!new LanguageService().exists(FilenameUtils.getExtension(file.getItem().getName()))) status = "disabled";
				tree.append("<li><label><input id=\"tree_" + id + "_" + folderId + "\"" + " type=\"checkbox\" name=\""
						+ getRelativePath(file.getItem()) + "\" value=\"" + file.getItem().length() + "\"" + " onchange=\"selectFile('" + id + "_" + folderId
						+ "')\" " + status + ">" + file + "</label></li>");
			}
			folderId++;
		}
		tree.append("</ul>");
	}

	public String buildAttachmentCheckBoxTree(Integer codeReviewId) {
		com.epam.codereview.model.Project codeReviewProject = new ProjectService().getByReviewId(codeReviewId);
		fullProjectName = codeReviewProject.getFullName();
		tree = new StringBuilder("<div class=\"container tree\">" + "<div class=\"panel panel-default\">"
				+ "<div class=\"panel-heading\">Project name: "
				+ (projectName != null ? projectName : project.getName())
				+ "</div><div class=\"panel-body\"><ul class=\"treeview\">"
				+ "<li><input id=\"tree_1\" type=\"checkbox\" name=\""
				+ project.getParentFolder().getItem().getAbsolutePath()
				+ "\" onchange=\"selectFolder('1')\"><a href=\"#\">" + project.getName() + "</a>");
		createAttachmentCheckBoxTree((ProjectFolder) project.getParentFolder(), "1");
		tree.append("</li></ul></div></div></div>");
		return tree.toString();
	}

	private void createAttachmentCheckBoxTree(ProjectFolder projectFolder, String id) {
		List<ProjectItem> files = projectFolder.getItems();
		int folderId = 1;
		tree.append("<ul>");
		for (ProjectItem file : files) {
			if (file instanceof ProjectFolder) {
				tree.append("<li><input id=\"tree_" + id + "_" + folderId + "\"" + " type=\"checkbox\" name=\""
						+ getRelativePath(file.getItem()) + "\"" + " onchange=\"selectFolder('" + id + "_" + folderId
						+ "')\"><a href=\"#\">" + file + "</a>");
				createAttachmentCheckBoxTree((ProjectFolder) file, id + "_" + folderId);
				tree.append("</li>");
			} else {
				String status = "";
				if(!new LanguageService().exists(FilenameUtils.getExtension(file.getItem().getName()))) status = "disabled";
				tree.append("<li><label><input id=\"tree_" + id + "_" + folderId + "\"" + " type=\"checkbox\" name=\""
						+ getRelativePath(file.getItem()) + "\" value=\"" + file.getFileSize(fullProjectName, getRelativePath(file.getItem())) + "\"" + " onchange=\"selectFile('" + id + "_" + folderId
						+ "')\" " + status + ">" + file + "</label></li>");
			}
			folderId++;
		}
		tree.append("</ul>");
	}

	private String getRelativePath(File item) {
		return item.getAbsolutePath().substring(projectsPath.length());
	}

	public String buildReviewTree() {
		tree = new StringBuilder(""
				+ "<ul class=\"treeview\">");
		createTreeForReview((ProjectFolder) project.getParentFolder(), "1", 1);
		tree.append("</ul>");
		return tree.toString();
	}

	private void createTreeForReview(ProjectFolder projectFolder, String id, int level) {
		List<ProjectItem> files = projectFolder.getItems();
		int folderId = 1;
		if(level != 1) tree.append("<ul>");
		for (ProjectItem file : files) {
			if (file instanceof ProjectFolder) {
				tree.append("<li id=\"tree_" + (id + "_" + folderId).substring(2) + "\"><a href=\"#\">" + file + "</a>");
				createTreeForReview((ProjectFolder) file, id + "_" + folderId, level + 1);
				tree.append("</li>");
			} else {
                String relativeFilePath = getRelativePath(file.getItem()).replaceAll("\\\\", "/");
				tree.append("<li id=\"tree_" + (id + "_" + folderId).substring(2) + "\"><div style='display:inline-block'><a onclick=\"selectFileForReview('" + file.getItem().getAbsolutePath().replaceAll("\\\\", "/")
						+ "'" + ",'" + relativeFilePath + "')\">"
						+ file + "</a>" + addCountIssuesIcon(relativeFilePath)  + addViewedIconToFile(relativeFilePath) +
						"</div></li>");
			}
			folderId++;
		}
		if(level != 1) tree.append("</ul>");
	}

    public String buildFileAttachmentTree(){
		tree = new StringBuilder(""
				+ "<ul class=\"treeview\">");
		createFileAttachmentTree((ProjectFolder) project.getParentFolder());
		tree.append("</ul>");
		return tree.toString();
	}

	private void createFileAttachmentTree(ProjectFolder projectFolder) {
		List<ProjectItem> files = projectFolder.getItems();
		tree.append("<ul id=\"folder_" + getRelativePath(projectFolder.getItem()) + "\">");
		for (ProjectItem file : files) {
			if (file instanceof ProjectFolder) {
				tree.append("<li><a id=\"" + getRelativePath(file.getItem()) + 
						"\" href=\"#\" class=\"droppable\">" + file + "</a>");
				createFileAttachmentTree((ProjectFolder) file);
				tree.append("</li>");
			}
		}
		tree.append("</ul>");
	}

    private String addCountIssuesIcon(String relativeFilePath) {
        Pair<Integer, Integer> issuesCount = daoManager.executeAndClose(() -> daoManager.getIssueDao().getCountIssuesByFileUrl(relativeFilePath));
        String span = "";
        if (issuesCount != null && issuesCount.getKey() > 0) {
            String spanId = "file_" + issuesCount.getKey();
            span = issuesCount.getValue() > 0
                    ? "<span id='" + spanId + "' class='ui label circular issues-count-label'>" + issuesCount.getValue() + "</span>"
                    : "<span id='" + spanId + "'></span>";
        }
        return span;
    }

    private String addViewedIconToFile(String relativeFilePath) {
        String viewedIcon = "";
        if (currentUserId != null) {
            FileUser fileUser = daoManager.executeAndClose(() -> daoManager.getFileUserDao().getByFileUrlAndUserId(relativeFilePath, currentUserId));
            if (fileUser != null) {
                viewedIcon = "<span id='viewed_file_icon_" + fileUser.getFileId() + "' class='ui label' style='background: transparent; color:#777777; padding: 5px; margin-left:5px;'></span>";
                if (fileUser.isReviewed()) {
                    viewedIcon = "<span id='viewed_file_icon_" + fileUser.getFileId() + "' class='ui label' style='background: transparent; color:#777777; padding: 5px; margin-left:5px;'><i class='fa fa-eye'></i></span>";
                }
            }
        }
        return viewedIcon;
    }

}
