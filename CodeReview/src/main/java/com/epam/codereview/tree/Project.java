package com.epam.codereview.tree;

public class Project {
	private String name;
	private ProjectItem parentFolder;
	
	public Project(String name, ProjectItem parentFolder){
		this.name = name;
		this.parentFolder = parentFolder;
	}

	public String getName() {
		return name;
	}

	public ProjectItem getParentFolder() {
		return parentFolder;
	}
}
