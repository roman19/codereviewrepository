package com.epam.codereview.tree;

import java.io.File;

import com.epam.codereview.service.FileService;

public abstract class ProjectItem {
	private File item;
	private String name;

	public ProjectItem(File item){
		this.item = item;
		this.name = item.getName();
	}
	
	public ProjectItem(File item, String name){
		this.item = item;
		this.name = name;
	}
	
	public String complementName(String folder){
		name += "/" + folder;
		return name;
	}

	public File getItem() {
		return item;
	}
	
	public long getFileSize(String oldProjectName, String relativePath){
		Long oldFileSize = null;
		try{
			oldFileSize = new FileService().
					getByUrl(replaceProjectNameInPath(oldProjectName, relativePath)).getSize();
		}catch(Exception e){
			oldFileSize = 0l;
		}finally{
			if(oldFileSize == null) oldFileSize = 0l;
		}
		return item.length() - oldFileSize;
	}
	
	private String replaceProjectNameInPath(String oldProjectName, String relativePath){
		relativePath = relativePath.replaceAll("\\\\", "/");
		String[] path = relativePath.split("/");
		return relativePath.replace(path[1], oldProjectName);
	}

    @Override
	public String toString() {
		return name;
	}
	
}
