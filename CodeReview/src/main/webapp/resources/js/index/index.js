$(document).ready(function() {
	$('#bitbucketModal').on('hidden.bs.modal', function () {
		$.ajax({
			url : '',
			type : 'get'
		});
	});
	
	$('#googleModal').on('hidden.bs.modal', function () {
		$.ajax({
			url : '',
			type : 'get'
		});
	});
	
	function loop() {
		$('#file').css({
			top : 0
		});
		$('#file').animate({
			top : '+=270',
		}, 3000, 'linear', function() {
			loop();
		});
	}
	loop();
});

function validateNewPasswordForm() {

	var form = document.forms['newPasswordForm'];

	var ok = true;

	ok = passwordIsValid(form["password1"]) && ok;
	ok = confirmPasswordIsValid(form["password1"], form["password2"]) && ok;

	return ok;
}

function validateForm(name, locale) {

	var form = document.forms[name];

	var ok = true;

	ok = nameIsValid(form["name"]) && ok;
	ok = surnameIsValid(form["surname"]) && ok;
	ok = loginIsValid(form["login"], locale) && ok;
	ok = emailIsValid(form["email"], locale) && ok;
	ok = passwordIsValid(form["password"]) && ok;
	ok = confirmPasswordIsValid(form["password"], form["cpassword"]) && ok;
	ok = avaIsValid(form["image"]) && ok;

	return ok;
}

function valid(field) {
	$(field).removeClass("field-invalid");
	$(field).addClass("field-valid");
	$(field).tooltip('destroy');
}

function invalid(field) {
	$(field).removeClass("field-valid");
	$(field).addClass("field-invalid");
	$(field).tooltip('show');
	$(field).tooltip('hide');
}

function validate(field) {
	var re = /^([0-9a-zA-Z_\.]*)$/;
	return re.test(field.value);
}

function validateLetters(field) {
	var re = /^([a-zA-Zа-яА-ЯіІьїЇєЄґҐ'`’]*)$/;
	return re.test(field.value);
}

function nameIsValid(name) {
	if (name.value.length >= 1 && name.value.length <= 45
			&& validateLetters(name)) {
		valid(name);
		return true;
	} else {
		invalid(name);
		return false;
	}
}

function surnameIsValid(surname) {
	if (surname.value.length >= 1 && surname.value.length <= 45
			&& validateLetters(surname)) {
		valid(surname);
		return true;
	} else {
		invalid(surname);
		return false;
	}
}

function loginIsValid(login, locale) {
	var isValid = true;

	if (login.value.length < 5 || login.value.length > 45 || !validate(login)) {
		$(login).removeClass("field-valid");
		$(login).addClass("field-invalid");
		if (locale == 'uk_UA') {
			$(login).attr("data-original-title",
					"Логін має містити мінімум 5 літер, а максимум - 45");
		} else {
			$(login).attr("data-original-title",
					"Login must be greater then 5 and less then 45");
		}

		$(login).tooltip('show');
		$(login).tooltip('hide');
		return false;
	}

	$.ajax({
		async : false,
		url : 'checklogin',
		type : 'get',
		dataType : 'text',
		data : {
			"login" : login.value
		}
	}).success(function(data) {
		isValid = !(data == "true");
	}).error(function(data) {
		isValid = false;
	});

	if (isValid == true) {
		$(login).removeClass("field-invalid");
		$(login).addClass("field-valid");
		$(login).tooltip('destroy');
	} else {
		$(login).removeClass("field-valid");
		$(login).addClass("field-invalid");
		if (locale == 'uk_UA') {
			$(login).attr("data-original-title", "Такий логін вже існує");
		} else {
			$(login).attr("data-original-title", "Such login already exists");
		}
		$(login).tooltip('show');
		$(login).tooltip('hide');
	}

	return isValid;
}

function emailIsValid(email, locale) {
	var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var isValid = re.test(email.value);

	if (!isValid || email.value.length > 45) {
		$(email).removeClass("field-valid");
		$(email).addClass("field-invalid");
		if (locale == 'uk_UA') {
			$(email).attr("data-original-title", "E-mail невалідний");
		} else {
			$(email).attr("data-original-title", "E-mail is not valid");
		}
		$(email).tooltip('show');
		$(email).tooltip('hide');

		return false;
	}

	$.ajax({
		async : false,
		url : 'checkemail',
		type : 'get',
		dataType : 'text',
		data : {
			"email" : email.value
		}
	}).success(function(data) {
		isValid = !(data == "true");
	}).error(function(data) {
		isValid = false;
	});

	if (isValid == true) {
		$(email).removeClass("field-invalid");
		$(email).addClass("field-valid");
		$(email).tooltip('destroy');
	} else {
		$(email).removeClass("field-valid");
		$(email).addClass("field-invalid");
		if (locale == 'uk_UA') {
			$(email).attr("data-original-title", "Такий e-mail вже існує");
		} else {
			$(email).attr("data-original-title", "Such e-mail already exists");
		}
		$(email).tooltip('show');
		$(email).tooltip('hide');
	}

	return isValid;
}

function passwordIsValid(password) {
	var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
	var isValid = re.test(password.value);

	if (isValid) {
		valid(password);
		return true;
	} else {
		invalid(password);
		return false;
	}
}

function confirmPasswordIsValid(pwd, cpwd) {
	if (passwordIsValid(pwd) && pwd.value == cpwd.value) {
		valid(cpwd);
		return true;
	} else if (!passwordIsValid(pwd) && pwd.value == cpwd.value) {
		$(cpwd).removeClass("field-valid");
		$(cpwd).addClass("field-invalid");
		$(cpwd).tooltip('destroy');
		return false;
	} else {

		invalid(cpwd)
		return false;
	}
}

function avaIsValid(ava) {

	if (ava.value != "") {
		var val = ava.value.substring(ava.value.lastIndexOf('.') + 1)
				.toLowerCase();
		if ((val != 'gif' && val != 'jpg' && val != 'png' && val != 'jpeg')
				|| ava.files[0].size / 1024 / 1024 > 10) {
			invalid($('#fumes'));
			$('#imageToolTip').tooltip('show');
			$('#imageToolTip').tooltip('hide');
			return false;

		} else {
			valid($('#fumes'));
			$('#imageToolTip').tooltip('destroy');
			return true;
		}
	} else {
		return true;
	}
}

function checkSigninForm() {

	var isValid = false;

	var login = document.forms["signinForm"]["login"].value;
	var password = document.forms["signinForm"]["password"].value;
	var json_data = {
		'login' : login,
		'password' : password
	}

	$.ajax({
		async : false,
		url : 'checkSigninForm',
		type : 'post',
		dataType : 'text',
		data : json_data
	}).success(function(data) {
		isValid = (data == "good");
		if (!isValid) {
			$('#error').html(data);
		}

	});

	return isValid;

}

function checkForgotForm() {

	var isValidEmail = false;

	var email = document.forms["forgotForm"]["email"].value;

	$.ajax({
		async : false,
		url : 'checkForForgot',
		type : 'get',
		dataType : 'text',
		data : {
			"email" : email
		}
	}).success(function(data) {
		isValidEmail = (data == "good");
		if (!isValidEmail) {
			$('#forgotError').html(data);
		}

	});

	return isValidEmail;
}

$(function() {
	$('#signinModal').on('hidden.bs.modal', function() {
		document.getElementById("signinForm").reset();
		document.getElementById("forgotForm").reset();
		$('#forgotError').html('');
		$('#error').html('');
	});
});

$(function() {
	$('#signupModal').on('hidden.bs.modal', function() {

		document.getElementById("signupForm").reset();

		var $inputs = $('#signupForm :input');

		$inputs.each(function() {
			$(this).tooltip('destroy');
			$(this).removeClass("field-valid");
			$(this).removeClass("field-invalid");
		});
	});
});

$(document).ready(function() {
	$('#olvidado').click(function(e) {
		e.preventDefault();
		$('div#form-olvidado1').toggle('500');
	});
	$('#olvidado').click(function(e) {
		e.preventDefault();
		$('div#form-olvidado').toggle('500');
	});
	$('#acceso1').click(function(e) {
		e.preventDefault();
		$('div#form-olvidado').toggle('500');
	});
	$('#acceso1').click(function(e) {
		e.preventDefault();
		$('div#form-olvidado1').toggle('500');
	});
});

function checkUserByToken(locale){
	var isGood = false;
	
	var token = window.location.hash.substr(1);
	token = token.split("&")[0].split("=")[1];
	
	$.ajax({
		async : false,
		url : 'checkUserByToken',
		type : 'post',
		dataType : 'text',
		data : {
			token : token
		}
	}).success(function(data) {
		isGood = (data=="good");
		if(!isGood){
			if(locale=="en_US"){
				$('#googleError').html("You already have account or applied!");
			}else{
				$('#googleError').html("Ви вже маєте створений акаунт aбо подали заявку!");
			}
		}	
	}).error(function(data) {
		isGood = false;
	});
	
	return isGood;
}

function checkUserByBitbucketToken(locale){
	var isGood = false;
	
	var token = window.location.hash.substr(1);
	token = token.split("&")[0].split("=")[1];
	
	$.ajax({
		async : false,
		url : 'checkUserByBitbucketToken',
		type : 'post',
		dataType : 'text',
		data : {
			token : token
		}
	}).success(function(data) {
		isGood = (data=="good");
		if(!isGood){
			if(locale=="en_US"){
				$('#bitbucketError').html("You already have account or applied!");
			}else{
				$('#bitbucketError').html("Ви вже маєте створений акаунт aбо подали заявку!");	
			}
		}	
	}).error(function(data) {
		isGood = false;
	});
	
	return isGood;
}

function checkUserByGithubCode(locale){
	var isBad = true;
	
	$.ajax({
		async : false,
		url : 'checkUserByGithubCode',
		type : 'post',
		dataType : 'text',
		data : {
			'code' : $('#gitHubCode').val()
		}
	}).success(function(data) {
		isBad = (data=="bad");
		$('#gitHubToken').val(data);
		if(isBad){
			if(locale=="en_US"){
				$('#githubError').html("You already have account or applied!");
			}else{
				$('#githubError').html("Ви вже маєте створений акаунт aбо подали заявку!");
			}
		}	
	}).error(function(data) {
		isBad = true;
	});
	
	return !isBad;
}