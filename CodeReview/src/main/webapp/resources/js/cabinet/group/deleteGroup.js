var mapping = $('#mapping').val();
var locale = $('#locale_id').val();

function removeUser(groupId, userId){
	$.post(mapping + 'cabinet/group_delete_user', {
		userId : userId,
		groupId : groupId
	}, function() {
		$('#groupUser' + userId).animate({
			opacity : '0.5',
			height : '-=150px',
			width : '-=150px'
		}, "slow", function() {
			$('#groupUser' + userId).remove();
		});
	});
}

function leaveGroup(groupId, what) {
	swal({
		title : getLabel('sure'),
		text : getLabel('leave.sure'),
		type : "warning",
		showCancelButton : true,
		cancelButtonText : getLabel('button.cancel'),
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "OK",
		closeOnConfirm : true
	}, function() {
		$.post(mapping + 'cabinet/group_leave', {
			groupId : groupId
		}, function() {
			if(what == true){
		 	    document.location.replace(mapping + "cabinet");
			}else{
				$('#card' + groupId).animate({
					bottom : '250px',
					opacity : '0.5',
					height : '-=150px',
					width : '-=150px'
				}, "slow", function() {
					$('#card' + groupId).remove();
				});
			}
		});
	});
}

function deleteGroup(groupId, what) {
	swal({
		title : getLabel('sure'),
		text : getLabel('delete.sure'),
		type : "warning",
		showCancelButton : true,
		cancelButtonText : "Cancel",
		confirmButtonColor : "#DD6B55",
		cancelButtonText : getLabel('button.cancel'),
		confirmButtonText : "OK",
		closeOnConfirm : true
	}, function() {
		$.post(mapping + 'cabinet/group_delete', {
			groupId : groupId
		}, function() {
			if(what == true){
		 	    document.location.replace(mapping + "cabinet");
			}else{
				$('#card' + groupId).animate({
					bottom : '250px',
					opacity : '0.5',
					height : '-=150px',
					width : '-=150px'
				}, "slow", function() {
					$('#card' + groupId).remove();
				});
			}
		});
	});
}

function getLabel(label){
	if(locale == 'uk_UA'){
		switch(label){
			case 'sure':
				return 'Ви впевнені?';
			case 'leave.sure':
				return 'Ви не зможете повернутися без повторного запрошення власника групи!';
			case 'delete.sure':
				return 'Ви не зможете відновити групу!';
			case 'button.cancel':
				return 'Скасувати';
		}
	}else{
		switch(label){
			case 'sure':
				return 'Are you sure?';
			case 'leave.sure':
				return 'You will not be able to come back without offer from group owner!';
			case 'delete.sure':
				return 'You will not be able to recover this group!';
			case 'button.cancel':
				return 'Cancel';
		}
	}
}