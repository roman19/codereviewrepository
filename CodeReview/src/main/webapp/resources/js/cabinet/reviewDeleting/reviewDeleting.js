
var mapping = $('#mapping').val();
var locale = $('#locale_id').val();

function hideDeletedReview(deletedReviewId) {
	$.post(mapping + 'cabinet/hidereview', {
		deletedReviewId : deletedReviewId
	}, function() {
		$('#deletedReview' + deletedReviewId).animate({
			bottom : '250px',
			opacity : '0.5',
			height : '-=150px',
			width : '-=150px'
		}, "slow", function() {
			$('#deletedReview' + deletedReviewId).remove();
		});
	});
}

function hideFromDeletedReviewer(reviewerStatus, codeReviewId) {
	$.post(mapping + 'cabinet/hide_review_from_reviewer', {
		reviewerStatus : reviewerStatus,
		codeReviewId : codeReviewId
	}, function() {
		$('#deniedReview' + codeReviewId).animate({
			right : '250px',
			opacity : '0.5',
			height : '-=150px',
			width : '-=150px'
		}, "slow", function() {
			$('#deniedReview' + codeReviewId).remove();
		});
	});
}

function setStatusLeft(codeReviewId, reviewName, status) {
	swal({
		title : getLabelDelete('left.title'),
		text : getLabelDelete('left.msg1') + reviewName + getLabelDelete('left.msg2'),
		type : "warning",
		showCancelButton : true,
		cancelButtonText: getLabelDelete('button.cancel'),
		confirmButtonColor : "#DD6B55",
		confirmButtonText : getLabelDelete('left.button'),
		closeOnConfirm : true
	}, function() {
		$.post(mapping + 'cabinet/set_status_left', {
			codeReviewId : codeReviewId
		}, function() {
			if(status == true){
				$('#review' + codeReviewId).animate({
					right : '250px',
					opacity : '0.5',
					height : '-=150px',
					width : '-=150px'
				}, "slow", function() {
					$('#review' + codeReviewId).remove();
				});
			}else{
		 	    document.location.replace("../cabinet");
			}
		});
	});
}


function finishReview(codeReviewId) {
	swal({
		title : getLabelDelete('left.title'),
		text : getLabelDelete('finish.msg'),
		type : "warning",
		showCancelButton : true,
		cancelButtonText: getLabelDelete('button.cancel'),
		confirmButtonColor : "#DD6B55",
		confirmButtonText : getLabelDelete('finish.button'),
		closeOnConfirm : true
	}, function() {
		$.get(mapping + 'review/finish', {
			id : codeReviewId
		}, function() {
			$('#review' + codeReviewId).animate({
				right : '250px',
				opacity : '0.5',
				height : '-=150px',
				width : '-=150px'
			}, "slow", function() {
				$('#review' + codeReviewId).remove();
			});
		});
	});
}

function confirmFinishReview(reviewId){
	$.post(mapping + 'cabinet/finish_confirm', {
		reviewId : reviewId
	}, function() {
		$('#review' + reviewId).animate({
			left : '250px',
			opacity : '0.5',
			height : '-=150px',
			width : '-=150px'
		}, "slow", function() {
			$('#review' + reviewId).remove();
		});
	});
}

function deleteReviewInHistory(reviewId){
	$.get(mapping + 'review/delete', {
		id : reviewId
	}, function(){
		$('#review' + reviewId).animate({
			left : '250px',
			opacity : '0.5',
			height : '-=150px',
			width : '-=150px'
		}, "slow", function() {
			$('#review' + reviewId).remove();
		});
	});
}

function getLabelDelete(label){
	if(locale == 'uk_UA'){
		switch(label){
		case 'button.cancel':
			return 'Скасувати';
		case 'finish.button':
			return 'Завершити рев`ю!';
		case 'finish.msg':
			return 'Якщо ви підтвердите завершення рев`ю, воно буде в історії з цього моменту і ви не зможете відновити його!';
		case 'left.button':
			return 'Покинути рев`ю!';
		case 'left.title':
			return 'Ви впевнені?';
		case 'left.msg1':
			return 'Ви не зможете повернутись назад до «';
		case 'left.msg2':
			return '» рев`ю!';
		}
	}else{
		switch(label){
		case 'button.cancel':
			return 'Cancel';
		case 'finish.button':
			return 'Finish review!';
		case 'finish.msg':
			return 'If you confirm review finish, it will be in history from this moment and you woun`t be able to restor it!';
		case 'left.button':
			return 'Leave review!';
		case 'left.title':
			return 'Are you sure?';
		case 'left.msg1':
			return 'You will not be able to come back to «';
		case 'left.msg2':
			return '» review!';
		}
	}
}