
function openBuyModal(){
	$('#buyMemoryModal').modal('show');
}

function buyMemory(size, price){
	$('#buyCardsContent').empty();
	$('#buyCardsContent').html(
			'<div class="container-fluid">' + 
				'<div class="row">' + 
					'<div class="col-xs-8 card" style="width: 450px; height: 220px; margin: 30px;">' + 
						'<div><p class="cardDetail">' + getLabel('card.details') + '</p></div>' + 
						'<div id="cardNumber" style="margin-top: 30px; margin-left: 35px;">' + 
							'<input id="cardnum1" class="cardnumber" maxlength="4" onkeyup="focusAnother(1)" onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" type="text"/>' + 
							'<input id="cardnum2" class="cardnumber" maxlength="4" onkeyup="focusAnother(2)" onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" type="text"/>' + 
							'<input id="cardnum3" class="cardnumber" maxlength="4" onkeyup="focusAnother(3)" onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" type="text"/>' + 
							'<input id="cardnum4" class="cardnumber" maxlength="4" onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" type="text"/>' + 
						'</div>' + 
						'<div id="cardNumberError" class="error" style="height: 30px;"></div>' + 
						'<div style="margin-left: 125px;">' +
							'<select class="selectTag"><option>01</option><option>02</option><option>03</option><option>04</option><option>05</option><option>06</option><option>07</option><option>08</option><option>09</option><option>10</option><option>11</option><option>12</option></select>' + 
							'<select class="selectTag" style="margin-left: 75px;"><option>2016</option><option>2017</option><option>2018</option><option>2019</option><option>2020</option><option>2021</option><option>2022</option><option>2023</option><option>2024</option><option>2025</option><option>2026</option><option>2027</option></select>' + 
						'</div>' +
						'<div class="ui clearing divider" style="margin-top: 30px;"></div>' +
						'<div style="margin-top: -50px; margin-left: 35px;">' +
							'<img src="resources/images/visa.png" />' + 
							'<img src="resources/images/mastercard.png" style="margin-left: 20px;" />' + 
						'</div>' + 
					'</div>' + 
					
					'<div class="col-xs-3" style="margin-top: 80px;">CVC2/CVV2: <input id="CVC" type="password" maxlength="3"/>' + 
						'<a id="makePurchase" class="btn btn-lg btn-block btn-primary" style="margin-top: 20px; width: 156px;" onclick="makePurchase(' + size + ',' + price + ')">' + getLabel('button.buy') + '</a>' + 
					'</div>' + 
				'</div>' + 
			'</div>');
}

function makePurchase(size, price){
	if(isPurchaseValid()){
		$.post('cabinet/settings/confirm',{
			price : price
		},function(result){
			if(result == 'succes'){
				$('#makePurchase').remove();
				$('#CVC').after('<div><a id="confirmPurchase" class="btn btn-lg btn-block btn-primary" style="margin-top: 20px; width: 156px;" onclick="confirmPurchase(' + size + ')">' + getLabel('button.submit') + '</a></div>');
				$('#confirmPurchase').after('<div id="codeTip">' + getLabel('succes.code') + '</div>');
				$('#confirmPurchase').after('<div style="margin-top: 20px;"><input id="confirmCode" maxlength="9" onkeypress="return (event.charCode >= 48 && event.charCode <= 57)" type="text"/></div>');
			}else{
				$('#makePurchase').after('<div class="error">' + getLabel('error.credentials') + '</div>');
			}
		})
	}
}

function confirmPurchase(size){
	var code = $('#confirmCode').val();
	if(code == '') return;
	$.post('cabinet/settings/confirm_memory_purchase', {
		code : code,
		size : size
	}, function(result){
		if(result == 'succes'){
			$('#codeTip').html(getLabel('succes.purchase'));
			$('#codeTip').css('color', 'green');
		}else{
			$('#codeTip').html('Wrong code!');
			$('#codeTip').css('color', 'red');
			$('#confirmPurchase').attr("disabled", true);
			setTimeout(function() {
						$('#buyMemoryModal').modal('hide');
						onExit();
					}, 3000);
		}
	})
}

function getLabel(label){
	if(locale == 'uk_UA'){
		switch(label){
			case 'card.details':
				return 'Введіть дані з вашої карточки';
			case 'button.buy':
				return 'Купити';
			case 'button.submit':
				return 'Підтвердити';
			case 'succes.code':
				return '*Введіть код, який ми вам надіслали на email.';
			case 'error.credentials':
				return 'Неправильні дані!';
			case 'succes.purchase':
				return 'Ви успішно купили нову пам`ять!';
			case 'error.cvc':
				return 'Кількість цифр - 3!';
			case 'error.code':
				return 'Кількість цифр - 16!';
		}
	}else{
		switch(label){
			case 'card.details':
				return 'Enter details of your card';
			case 'button.buy':
				return 'Buy';
			case 'button.submit':
				return 'Submit';
			case 'succes.code':
				return '*Enter code we have sent you on email.';
			case 'error.credentials':
				return 'Wrong card credentials!';
			case 'succes.purchase':
				return 'You succesfully bought new memory!';
			case 'error.cvc':
				return 'Number of digits - 3!';
			case 'error.code':
				return 'Number of digits - 16!';
		}
	}
}

function isPurchaseValid(){
	$('.error').empty();
	var isValid = true;
	if($('#CVC').val().length != 3){
		$('#CVC').after('<div class="error">' + getLabel('error.cvc') + '</div>');
		isValid = false;
	}
	var code = value($('#cardnum1').val()) + value($('#cardnum2').val())
		+ value($('#cardnum3').val()) + value($('#cardnum4').val());
	if(code.length != 16){
		$('#cardNumberError').html(getLabel('error.code'));
		isValid = false;
	}
	return isValid;
}

function value(value){
	return value == null ? '':value;
}

function focusAnother(id){
	if(isLengthFull('#cardnum' + id)){
		$('#cardnum' + (id + 1)).focus();
	}
}

function isLengthFull(id){
	return $(id).val().length == 4;
}

$("#exitBuyModal").unbind("click").delay(0).bind("click", (function (){
	onExit();
})).delay(0);

function onExit(){
	$.get("cabinet/settings", function(responseText) {   
	    $("#switchContent").html(responseText);
	    clearURL();
	    window.location.hash = "#settings";
	});
}


////////////////////////////////////////	image	///////////////////////////////////////////////////

$("#image").click(function() {
	$("input[id='newPhoto']").click();
});

function previewFile() {
	var preview = document.querySelector('#image');
	var file = document.querySelector('input[type=file]').files[0];
	if (file) {
		var reader = new FileReader();
		reader.onloadend = function() {
			preview.src = reader.result;
			$('#cabinetAvatar').attr('src', reader.result);
		}
		reader.readAsDataURL(file);
		var data = new FormData();
		$.each($('#newPhoto')[0].files, function(i, file) {
			data.append('file-' + i, file);
		});
		$.ajax({
			url : 'cabinet/settings/photo',
			data : data,
			cache : false,
			contentType : false,
			processData : false,
			type : 'POST',
			success : function(result) {
				if (result == 'error') {
					swal("Error!", "You should upload only images!", "error");
				}
			}
		});
	}
}

// //////////////////////////////////////////////// personal data
// //////////////////////////////////////

function savePersonalData(locale) {
	$.post('cabinet/settings/personal', {
		name : nameIsValid($('#fname').val()),
		surname : surnameIsValid($('#lname').val()),
		login : loginIsValid($('#login').val(), locale)
	}, function(result) {
		if (result.name != null && result.name != '') {
			$('#fname').val('');
			$('#fname').attr("placeholder", result.name);
			$('#settings_fname').text(result.name);
		}
		if (result.surname != null && result.surname != '') {
			$('#lname').val('');
			$('#lname').attr("placeholder", result.surname);
			$('#settings_lname').text(result.surname);
		}
		if (result.login != null && result.login != '') {
			$('#login').val('');
			$('#login').attr("placeholder", result.login);
			$('#settings_login').text(result.login);
		}
		setTimeout(function() {
			$('#fname').css('border', '1px solid rgba(34, 36, 38, 0.15)');
			$('#lname').css('border', '1px solid rgba(34, 36, 38, 0.15)');
			$('#login').css('border', '1px solid rgba(34, 36, 38, 0.15)');
		}, 2000);
	});
}

function valid(field) {
	field.css('border', '1px solid green');
	field.tooltip('destroy');
}
function invalid(field) {
	field.css('border', '1px solid red');
	field.tooltip('show');
	field.tooltip('hide');
}
function validate(field) {
	var re = /^([0-9a-zA-Zа-яА-ЯіІьїЇєЄґҐ'`’_\.]*)$/;
	return re.test(field);
}

function validateLetters(field) {
	var re = /^([a-zA-Zа-яА-ЯіІьїЇєЄґҐ'`’]*)$/;
	return re.test(field);
}

function nameIsValid(name) {
	debugger;
	if (name == '') {
		return null;
	}
	if (name.length <= 45 && validateLetters(name)) {
		valid($('#fname'));
		return name;
	} else {
		invalid($('#fname'));
		return null;
	}
}

function surnameIsValid(surname) {
	if (surname == '') {
		return null;
	}
	if (surname.length <= 45 && validateLetters(surname)) {
		valid($('#lname'));
		return surname;
	} else {
		invalid($('#lname'));
		return null;
	}
}

function loginIsValid(login, locale) {
	if (login == '') {
		return null;
	}
	var isValid = true;

	if (login.length < 5 || login.length > 45 || !validate(login)) {
		$('#login').css('border', '1px solid red');
		if (locale == 'uk_UA') {
			$('#login').attr("data-original-title",
					"Логін має містити мінімум 5 літер, а максимум - 45");
		} else {
			$('#login').attr("data-original-title",
					"Login must be greater then 5 and less then 45");
		}

		$('#login').tooltip('show');
		$('#login').tooltip('hide');
		return null;
	}

	$.ajax({
		async : false,
		url : 'checklogin',
		type : 'get',
		dataType : 'text',
		data : {
			"login" : login
		}
	}).success(function(data) {
		isValid = !(data == "true");
	}).error(function(data) {
		isValid = false;
	});

	if (isValid == true) {
		$('#login').css('border', '1px solid green');
		$('#login').tooltip('destroy');
	} else {
		$('#login').css('border', '1px solid red');
		if (locale == 'uk_UA') {
			$('#login').attr("data-original-title", "Такий логін вже існує");
		} else {
			$('#login')
					.attr("data-original-title", "Such login already exists");
		}
		$('#login').tooltip('show');
		$('#login').tooltip('hide');
	}

	return isValid == true ? login : null;
}

// ////////////////////////////////////// password
// ////////////////////////////////////////////////

function savePassword() {
	if($('#currentPassword').val() == '' || $('#newPassword').val() == '' || $('#repeatPassword').val() == '') return;
	$.post('cabinet/settings/password', {
		currentPassword : $('#currentPassword').val(),
		newPassword : $('#newPassword').val(),
		repeatPassword : $('#repeatPassword').val()
	}, function(result) {
		clearPasswordFields();
		if (result.currentPassword == null) {
			$('#currentPassword').css('border', '1px solid red');
			$('#currentPassword').tooltip('show');
			$('#currentPassword').tooltip('hide');
		} else if (result.currentPassword != '') {
			$('#currentPassword').css('border', '1px solid green');
		}
		if (result.newPassword == null) {
			$('#newPassword').css('border', '1px solid red');
			$('#newPassword').tooltip('show');
			$('#newPassword').tooltip('hide');
		} else if (result.newPassword != '') {
			$('#newPassword').css('border', '1px solid green');
		}
		if (result.repeatPassword == null) {
			$('#repeatPassword').css('border', '1px solid red');
			$('#repeatPassword').tooltip('show');
			$('#repeatPassword').tooltip('hide');
		} else if (result.repeatPassword != '') {
			$('#repeatPassword').css('border', '1px solid green');
		}
		if(result.currentPassword == 'succes' && result.newPassword == 'succes' && result.repeatPassword == 'succes'){
			$('#savePassword').after('<p id="password_succes" style="font-size: 14 px; color: green;">' + 
					'Password succesfully changed!</p>');
		}
		setTimeout(
				function() {
					$('#currentPassword').css('border',
							'1px solid rgba(34, 36, 38, 0.15)');
					$('#newPassword').css('border',
							'1px solid rgba(34, 36, 38, 0.15)');
					$('#repeatPassword').css('border',
							'1px solid rgba(34, 36, 38, 0.15)');
					$('#password_succes').remove();
				}, 2000);
	});
}

function clearPasswordFields() {
	$('#currentPassword').tooltip('destroy');
	$('#newPassword').tooltip('destroy');
	$('#repeatPassword').tooltip('destroy');
	$('#currentPassword').val('');
	$('#newPassword').val('');
	$('#repeatPassword').val('');

}

// ////////////////////////////////////// Notfications on email
// ////////////////////////////////

function notificationsAll(value) {
	$('#group_notification').prop('checked', value);
	$('#review_notification').prop('checked', value);
	$('#file_notification').prop('checked', value);
	$('#issue_notification').prop('checked', value);
	$('#reply_notification').prop('checked', value);
}

function notificationItem(item, all) {
	if(all != null){
		notificationsAll(all);
	}
	$.post('cabinet/settings/notifications/item', {
		item : getNumberOfItem(item),
		value : all == null ? $('#' + item).is(':checked'):all
	});
}

function getNumberOfItem(item) {
	switch (item) {
	case "all_notifications":
		return 0;
	case "group_notification":
		return 1;
	case "review_notification":
		return 2;
	case "file_notification":
		return 3;
	case "issue_notification":
		return 4;
	case "reply_notification":
		return 5;
	case "sound_notification":
		return 6;
	}
}
