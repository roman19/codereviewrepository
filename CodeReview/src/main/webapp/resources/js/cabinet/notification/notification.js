function decrementCountOfNotifications(){
	var count = $('#notificationsBtn').attr('data-badge');
	
	count = parseInt(count);
	if(count==1){
		$('#notificationsBtn').removeClass('badge1');
		$('#notificationsBtn').attr('data-badge', 0);
	}else{
		$('#notificationsBtn').attr('data-badge', --count);
	}
}

function incrementCountOfNotifications(){
	var count = $('#notificationsBtn').attr('data-badge');
	
	if(!count){
		$('#notificationsBtn').addClass('badge1');
		$('#notificationsBtn').attr('data-badge', 1);
	}else{
		count = parseInt(count);
		$('#notificationsBtn').attr('data-badge', ++count);
	}
}

function confirmGroup(groupId) {

	$.ajax({
		url : 'cabinet/confirmGroup',
		type : 'post',
		data : {
			"groupId" : groupId
		}
	}).success(function(data) {
		$('#newGroup' + groupId).remove();
		decrementCountOfNotifications();
	}).error(function(data) {
		alert('Not confirmed: error!');
	});
}

function rejectGroup(groupId) {

	$.ajax({
		url : 'cabinet/rejectGroup',
		type : 'post',
		data : {
			"groupId" : groupId
		}
	}).success(function(data) {
		$('#newGroup' + groupId).remove();
		decrementCountOfNotifications()
	}).error(function(data) {
		alert('Not rejected: error!');
	});
}

function confirmReview(reviewId) {
	$.ajax({
		url : 'cabinet/confirmReview',
		type : 'post',
		data : {
			"reviewId" : reviewId
		}
	}).success(function(data) {
		$('#newReview' + reviewId).remove();
		decrementCountOfNotifications();
	}).error(function(data) {
		alert('Not confirmed review: error!');
	});
}

function rejectReview(reviewId) {
	$.ajax({
		url : 'cabinet/rejectReview',
		type : 'post',
		data : {
			"reviewId" : reviewId
		}
	}).success(function(data) {
		$('#newReview' + reviewId).remove();
		decrementCountOfNotifications();
	}).error(function(data) {
		alert('Not rejected review: error!');
	});
}

function confirmIssue(issueId) {

	$.ajax({
		url : 'cabinet/confirmIssue',
		type : 'post',
		data : {
			"issueId" : issueId
		}
	}).success(function(data) {
		$('#newIssue' + issueId).remove();
		decrementCountOfNotifications();
	}).error(function(data) {
		alert('Not confirmed issue: error!');
	});
}

function confirmFile(reviewId) {
	debugger;
	
	$.ajax({
		url : 'cabinet/confirmFile',
		type : 'post',
		data : {
			"reviewId" : reviewId
		}
	}).success(function(data) {
		$('#newFiles' + reviewId).remove();
		decrementCountOfNotifications();
	}).error(function(data) {
		alert('Not confirmed file: error!');
	});
}

function confirmReply(replyId) {
	debugger;
	
	$.ajax({
		url : 'cabinet/confirmReply',
		type : 'post',
		data : {
			"replyId" : replyId
		}
	}).success(function(data) {
		$('#newReply' + replyId).remove();
		decrementCountOfNotifications();
	}).error(function(data) {
		alert('Not confirmed reply: error!');
	});
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function removeHash () { 
    history.pushState("", document.title, window.location.pathname
                                                       + window.location.search);
}