$.fn.extend({
        treeview: function () {
            return this.each(function () {
                // Initialize the top levels;
                var tree = $(this);

                tree.addClass('treeview-tree');
                tree.find('li').each(function () {
                    var stick = $(this);
                });
                tree.find('li').has("ul").each(function () {
                    var branch = $(this); //li with children ul

                    branch.prepend("<i class='tree-indicator fa fa-folder'></i>");
                    branch.addClass('tree-branch');
                    branch.on('click', function (e) {
                        if (this == e.target) {
                            var icon = $(this).children('i:first');

                            icon.toggleClass("fa-folder-open fa-folder");
                            $(this).children().children().toggle();
                        }
                    });
                    branch.children().children().toggle();
                    branch.children('.tree-indicator, button, a').click(function (e) {
                        branch.click();
                        e.preventDefault();
                    });
                });
            });
        }
    });

/**
 *	The following snippet of code automatically converst
 *	any '.treeview' DOM elements into a treeview component.
 */
//$(window).on('load', function () {
//	$('.treeview').each(function () {
//		var tree = $(this);
//		tree.treeview();
//	})
//})

function treeViewer() {
	$('.treeview').each(function () {
		var tree = $(this);
		tree.treeview();
	});
}