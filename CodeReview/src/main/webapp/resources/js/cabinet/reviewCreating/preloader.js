var cSpeed = 9;
var cWidth = 64;
var cHeight = 64;
var cTotalFrames = 12;
var cFrameWidth = 64;

function stringStartsWith (string, prefix) {
    return string.slice(0, prefix.length) == prefix;
}
var url = document.URL;
var indexLast = url.split('/');
var lastPath = indexLast[indexLast.length - 1];
if(stringStartsWith(lastPath, "cabinet")){
	var cImageSrc = 'resources/images/sprites.gif';
}
else{
	var cImageSrc = '../resources/images/sprites.gif';
}
var cImageTimeout = false;
var cIndex = 0;
var cXpos = 0;
var cPreloaderTimeout = false;
var SECONDS_BETWEEN_FRAMES = 0;

var savedWidth;
var savedHeight;

var id;

function startAnimation() {

	document.getElementById(id).style.backgroundImage = 'url('
			+ cImageSrc + ')';
	savedWidth = document.getElementById(id).style.width;
	savedHeight = document.getElementById(id).style.height;
	document.getElementById(id).style.width = cWidth + 'px';
	document.getElementById(id).style.height = cHeight + 'px';

	// FPS = Math.round(100/(maxSpeed+2-speed));
	FPS = Math.round(100 / cSpeed);
	SECONDS_BETWEEN_FRAMES = 1 / FPS;

	cPreloaderTimeout = setTimeout('continueAnimation()',
			SECONDS_BETWEEN_FRAMES / 1000);

}

function continueAnimation() {

	cXpos += cFrameWidth;
	// increase the index so we know which frame of our animation we are
	// currently on
	cIndex += 1;

	// if our cIndex is higher than our total number of frames, we're at the end
	// and should restart
	if (cIndex >= cTotalFrames) {
		cXpos = 0;
		cIndex = 0;
	}

	if (document.getElementById(id))
		document.getElementById(id).style.backgroundPosition = (-cXpos)
				+ 'px 0';

	cPreloaderTimeout = setTimeout('continueAnimation()',
			SECONDS_BETWEEN_FRAMES * 1000);
}

function stopAnimation() {// stops animation
	clearTimeout(cPreloaderTimeout);
	cPreloaderTimeout = false;
	if(document.getElementById(id) == null) return;
	document.getElementById(id).style.width = savedWidth;
	document.getElementById(id).style.height = savedHeight;
	document.getElementById(id).style.backgroundImage = null;
}

function imageLoader(s, fun, ID)// Pre-loads the sprites image
{
	id = ID;
	clearTimeout(cImageTimeout);
	cImageTimeout = 0;
	genImage = new Image();
	genImage.onload = function() {
		cImageTimeout = setTimeout(fun, 0)
	};
	genImage.onerror = new Function('alert(\'Could not load the image\')');
	genImage.src = s;
}

