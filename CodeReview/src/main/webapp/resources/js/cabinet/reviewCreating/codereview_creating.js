
var mapping = $('#mapping').val();
var groupID = null;
var locale = $('#locale_id').val();

var projectName = null;
var branch = null;
var git_url = null;
var tempGitUrl;

var usedMemory = null;
var newMemory = null;
var enabledMemory = null;

function openCodeReviewModal(groupId){
	groupID = groupId;
	$.post(mapping + "groupcheck", {
		groupId : (groupId == null || groupId == '') ? 0 : groupId
	}, function(result) {
		if(result == 'error'){
			swal(reviewTranslate('error.open.title'), reviewTranslate('error.open.msg'), "error");
		}else{
			$('#codeReviewCreatingModal').modal('show');
		}
	});
}

function git(url) {		//1. put url
	$('#reviewCreatingModalBodyContent').empty();
	$('#reviewCreatingModalBodyContent').html('<div id="urlDiv"><input id="url" maxlength="100" type="text" name="url" placeholder="' + reviewTranslate('https') + '" >');//url
	$('input[maxlength]').maxlength();
	$('#url').after('<input id="clone" onclick="getBranches()" class="btn btn-1" type="button" name="" value="OK"></div>');//OK
}

function archive(){		//1. upload archive
	$('#reviewCreatingModalBodyContent').empty();
	$('#reviewCreatingModalBodyContent').html('<span id="archiveSpan" class="btn btn-primary btn-file">' +
			reviewTranslate('archive.upload') + '<input id="file" class="btn-file" type="file" onchange="uploadArchive()" accept=".zip"></span>');
}

function uploadArchive() {
	$('#archiveSpan').after('<div id="upload"></div>');
	new imageLoader(cImageSrc, 'startAnimation()', 'upload');
	$('#upload').attr("disabled", true);
	var data = new FormData();
	$.each($('#file')[0].files, function(i, file) {
		data.append('file-' + i, file);
	});
	$.ajax({
		url : mapping + 'archive',
		data : data,
		cache : false,
		contentType : false,
		processData : false,
		type : 'POST',
		success : function(data) {
			$('.error').remove();
			if(data.status == 'succes'){
				$('#upload').remove();
				projectName = data.projectName;
				handleResultOfDownloading(data.project);
			}else if(data.status == 'non_zip'){
				$('#upload').after('<div class="error" style="color: red">' + reviewTranslate('error.archive.type') + '</div>');
			}else{
				$('#upload').after('<div class="error" style="color: red">' + reviewTranslate('error.archive.type.unknown') + '</div>');
			}
			$('#upload').attr("disabled", false);
			setTimeout('stopAnimation()', 0);
		}
	});
}

function isGitUrl(str) {
	var re = /(?:git|ssh|https?|git@[\w\.]+):(?:\/\/)?[\w\.@:\/~_-]+\.git(?:\/?|\#[\d\w\.\-_]+?)$/;
	return re.test(str);
}

function getBranches(){	//3. choose branch
	if($("#url").val() == null || $("#url").val() == '') return;
	$('.error').remove();
	if(isGitUrl($("#url").val())){
		new imageLoader(cImageSrc, 'startAnimation()', 'clone');
		 $('#clone').attr("disabled", true);
		$.post(mapping + "git_branches", {
			url : $("#url").val(),
			password : $("#passwordToRepo").val()
		}, function(result) {
			if(result.status == 'no_repository'){
				$('#urlDiv').after('<label class="error">' + reviewTranslate('error.url') + '</label>');
				setTimeout('stopAnimation()', 0);
				$('#clone').attr("disabled", false);
			}else if(result.status == 'auth_error'){
				if($('#passwordToRepo').val() == null) createPasswordInput();		//2. write password
				else $('#passDiv').after('<label class="error">' + reviewTranslate('error.password') + '</label>');
				setTimeout('stopAnimation()', 0);
				$('#clone').attr("disabled", false);
			}else{
				var url = $("#url").val();
				var password = $("#passwordToRepo").val();
				$('#reviewCreatingModalBodyContent').empty();
				$('#reviewCreatingModalBodyContent').html('<div class="step">' + reviewTranslate('step.branch') + '<select id="branches"></select>');	//branch select
				var branches = document.getElementById('branches');
				for(var i = 0; i < result.length; i++){
					var option = document.createElement('option');
					var temp = result[i].split('/');
					option.text = temp[temp.length - 1];
					option.id = result[i];
					branches.add(option, i);
				}
				$('#branches').after('<input id="clone" onclick="' 
						+ (password == null ? 'clone(\'' + url + '\')':'clonePrivate(\'' + url + '\', \'' + password +'\')') + '"' +
						' class="btn btn-1" type="button" name="" value="OK"></div>');//OK
				setTimeout('stopAnimation()', 0);
			}
		});
	}else{
		$('#urlDiv').after('<label class="error">' + reviewTranslate('error.url') + '</label>');
	}
}

function createPasswordInput(){
	$('.error').remove();
	$('#clone').remove();
	$('#urlDiv').after('<div id="passDiv"><input id="passwordToRepo" maxlength="45" type="password" name="password"' +
	' placeholder="' + reviewTranslate('input.password') + '" >');		//password input
	$('input[maxlength]').maxlength();
	$('#passwordToRepo').after('<input id="clone" onclick="getBranches()"' +
		'class="btn btn-1" type="button" name="" value="OK"></div>');//OK
}

function clone(url){
	new imageLoader(cImageSrc, 'startAnimation()', 'clone');
	 $('#clone').attr("disabled", true);
	$.get(mapping + "git_downloader", {
		url : url,
		branch : $('#branches').children(":selected").attr("id")
	}, function(result) {
		git_url = url;
		handleResultOfDownloading(result);
	});
}

function clonePrivate(url, password){
	new imageLoader(cImageSrc, 'startAnimation()', 'clone');
	 $('#clone').attr("disabled", true);
	$.post(mapping + "git_downloader", {
		url : url,
		password :password,
		branch : $('#branches').children(":selected").attr("id")
	}, function(result){
		if(result != 'invalid url' || result != 'private' || result != 'wrong_link_or_pswd') {
			var temp = result.split(',');
			result = temp[0];
			projectName = temp[1];
		}
		git_url = url;
		handleResultOfDownloading(result);
	})
}

function handleResultOfDownloading(result){
	if(projectName == null) {
		projectName = result;
	}
	branch = $('#branches').children(":selected").attr("id");
	$('#reviewCreatingModalBodyContent').empty();
	//tree building
	$('#reviewCreatingModalBodyContent').html('<div id="treeTitle">' + reviewTranslate('step.tree') + '</div>');
	setTimeout('stopAnimation()', 0);
	buildTree(result);
}

/**
 * Builds tree and create after tree next things: 1) input for code review name,
 *  2) selector for group, 3) selector for users, 4) 'Create code review' button
 * @param project
 */
function buildTree(project){
	$.get(mapping + "tree", {
		project : project,
		projectName : projectName
	}, function(result){
		$('#tree').html(result.tree);
		treeViewer();
		usedMemory = result.usedMemory;
		enabledMemory = result.enabledMemory;
		$('#treeTitle').after('<hr>' + 
				'<div style="margin-top: 20px; margin-bottom: -40px;">' + 
					'<h5 style="margin-left: 15%;text-align: left">' + mTranslate('memory.title') + ' <span id="usedMemory">' + convertBytesInMegaBytes(usedMemory) + '</span>' + result.memory + '</h5>' + 
					'<div class="progress" style="margin-left: 10%; width: 80%; height: 25px; background-color: #CBC8C8">' + 
						'<div id="memoryProgressBar" class="progress-bar active" role="progressbar" ' +
							'aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: ' + 
							 result.memoryPercentage + '%;">' + 
						'</div>' +
					'</div>' + 
				'</div>');
		refreshMemoryState(0);
		$('#tree').after('<input id="reviewName" type="text" maxlength="45" name="reviewName" placeholder="' + reviewTranslate('review.name') + '" >');
		$('input[maxlength]').maxlength();
		$.get(mapping + "groups_by_user", function(result){
			if(groupID == null){
				$('#reviewName').after('<div id="groupsDiv">' + reviewTranslate('review.group') + ' <select id="groups" data-placeholder="' + reviewTranslate('review.group') + '" onchange="getUsersBySelectedGroup()" '
						+ 'class="chosen-select-no-results">' + result + '</select></div>');
				$('#groups').after('<div id="users"><select id="userSelect" data-placeholder="' + reviewTranslate('review.users') + '" '
						+ 'multiple class="chosen-select-no-results"></select></div>');
			}else{
				$('#reviewName').after('<div style="margin-left: 15%; margin-top: 15px;"><div id="users"><select id="userSelect" data-placeholder="' + reviewTranslate('review.users') + '" '
						+ 'multiple class="chosen-select-no-results"></select></div></div>');
			}
			$.get(mapping + "users_by_selected_group", {
				groupID : groupID == null ? $('#groups').find('option:selected').attr('id') : groupID
			}, function(result){
				var users = document.getElementById('userSelect');
				for(var i = 0; i < result.length; i++){
					var temp = result[i];
					var option = document.createElement('option');
					option.text = temp.name;
					option.id = temp.id;
					users.add(option, i);
				}
				for (var selector in config) {
				    $(selector).chosen(config[selector]);
				}
				$('#users').after('<input id="createReview" onclick="createReview()"' +
					'class="btn btn-1" type="button" name="" value="' + reviewTranslate('review.create') + '">');
			});
		});
	})
}

/**
 * Sets in users dropdown list of users in selected group  
 */
function getUsersBySelectedGroup(){
	$.get(mapping + "users_by_selected_group", {
		groupID : $('#groups').find('option:selected').attr('id')
	}, function(result){
		$('#users').empty();
		$('#users').html('<select id="userSelect" data-placeholder="' + reviewTranslate('review.users') + '" '
				+ 'multiple class="chosen-select-no-results"></select>');
		var users = document.getElementById('userSelect');
		for(var i = 0; i < result.length; i++){
			var temp = result[i];
			var option = document.createElement('option');
			option.text = temp.name;
			option.id = temp.id;
			users.add(option, i);
		}
		for (var selector in config) {
		    $(selector).chosen(config[selector]);
		}
	});
}

function mTranslate(label){
	if(locale == 'uk_UA'){
		switch(label){
			case 'memory.title':
				return 'Використана пам`ять:';
			case 'memory.error':
				return 'Недостатньо пам`яті для створення рев`ю!';
		}
	}else{
		switch(label){
			case 'memory.title':
				return 'Used memory:';
			case 'memory.error':
				return 'You haven`t enough memory to create review!';
		}
	}
}

function createReview(){
	$('.reviewError').remove();
	if(usedMemory + newMemory > enabledMemory){
		$('#users').after('<h3 class="reviewError">' + mTranslate('memory.error') + '</h2>');
		return;
	}
	new imageLoader(cImageSrc, 'startAnimation()', 'createReview');
	 $('#createReview').attr("disabled", true);
	var pathes = [];
	getAllPathesFromCheckedItems('1', pathes);
	if(git_url != null) tempGitUrl = git_url;
	git_url = null;
	$.post(mapping + "create_code_review", {
		projectName : projectName,
		codeReviewName : $('#reviewName').val() == '' ? projectName:$('#reviewName').val(),
		reviewers : ($('#userSelect').val() == null ? null:$('#userSelect').val().join()),
		pathes : pathes.join(),
		branch : branch,
		git_url : tempGitUrl,
		group_id : groupID == null ? $('#groups').find('option:selected').attr('id') : groupID
	}, function(result){
		setTimeout('stopAnimation()', 0);
		$('h3').remove();
		if(result.notificationType=="REVIEW"){
			socket.send(JSON.stringify(result));
			tempGitUrl = null;
			$('#users').after('<h2 class="reviewSucces">' + reviewTranslate('succes.review.create') + '</h2>');
		}else {
			$('#users').after('<h3 class="reviewError">' + reviewTranslate('error.review.create') + '</h2>');
			 $('#createReview').attr("disabled", false);
		}
	});
}

/**
 * Return all relative paths of files that are checked
 * @param id
 * @param pathes
 */
function getAllPathesFromCheckedItems(id, pathes){
	if($('#tree_' + id + '_1').val() == null) return;
	for(var i = 1; $('#tree_' + id + '_' + i).val() != null; i++){
		var item = $('#tree_' + id + '_' + i);
		if(item.is(':checked')) {
			if(item.attr('name').indexOf('.') > -1) pathes.push(item.attr('name'));
		}
		getAllPathesFromCheckedItems(id + '_' + i, pathes);
	}
}

function selectFile(id){
	var valueOfSelectedCheckBox = $('#tree_' + id).is(':checked');
	if(valueOfSelectedCheckBox){
		selectAllFoldersBefore(id);
	}
	refreshMemoryState(getSumMemoryOfCheckedFiles('1'));
}

function selectFolder(id) {
	var valueOfSelectedCheckBox = $('#tree_' + id).is(':checked');
	if(valueOfSelectedCheckBox){
		selectAllFoldersBefore(id);
	}
	selectAllItemsAfter(id, valueOfSelectedCheckBox);
	refreshMemoryState(getSumMemoryOfCheckedFiles('1'));
}

function refreshMemoryState(newMemoryTemp){
	newMemory = newMemoryTemp;
	var used = usedMemory + newMemory;
	document.getElementById('usedMemory').innerHTML = convertBytesInMegaBytes(used);
	$('#memoryProgressBar').css('width', ((used / enabledMemory) * 100) + "%");
	if(used > enabledMemory){
		$('#memoryProgressBar').addClass('progress-bar-danger');
	}else{
		$('#memoryProgressBar').removeClass('progress-bar-danger');
	}
}

function convertBytesInMegaBytes(bytes){
	return (bytes / 1024) / 1024;
}

function getSumMemoryOfCheckedFiles(id){
	var sum = 0;
	if($('#tree_' + id + '_1').val() == null) return sum;
	for(var i = 1; $('#tree_' + id + '_' + i).val() != null; i++){
		if(!$('#tree_' + id + '_' + i).is(':disabled') && $('#tree_' + id + '_' + i).is(':checked')){
			if($('#tree_' + id + '_' + i).attr('value') != null){
				sum += parseInt($('#tree_' + id + '_' + i).attr('value'));
			}
		}
		sum += parseInt(getSumMemoryOfCheckedFiles(id + '_' + i));
	}
	return sum;
}

/**
 * Makes all folders and files after current checked
 * @param id
 * @param valueOfSelectedCheckBox
 */
function selectAllItemsAfter(id, valueOfSelectedCheckBox){
	if($('#tree_' + id + '_1').val() == null) return;
	for(var i = 1; $('#tree_' + id + '_' + i).val() != null; i++){
		if(!$('#tree_' + id + '_' + i).is(':disabled')){
			$('#tree_' + id + '_' + i).prop('checked', valueOfSelectedCheckBox);
		}
		selectAllItemsAfter(id + '_' + i, valueOfSelectedCheckBox);
	}
}

/**
 * Makes all folders before checked
 * @param id
 */
function selectAllFoldersBefore(id){
	var res = id.split("_");
	var temp = "";
	for (var i = 0; i < res.length - 1; i++) {
		if (temp == "") {
			temp += res[i];
		}
		else{
			temp += "_" + res[i];
		}
		$('#tree_' + temp).prop('checked', true);
	}
}

function exit(){
	projectName = null;
	usedMemory = null;
	enabledMemory = null;
	newMemory = null;
	if($('#createReview').is(':disabled')){
		$('#reviewCreatingModalBodyContent').empty();
		$('#anotherContent').empty();
		$('#reviewCreatingModalBodyContent').html('<input id="downloadChoiceButton" onclick="git()" class="btn btn-1 downloadButton"' + 
				'type="button" name="" value="' + reviewTranslate('clone.git') + '">' + 
			'<input id="archiveButton" onclick="archive()" class="btn btn-1 downloadButton"' +
				'type="button" name="" value="' + reviewTranslate('clone.archive') + '">');
		$('#reviewCreatingModalBodyContent').after('<div id="anotherContent"><div id="tree"></div></div>');
	}else{
		$.get(mapping + "cancel", {
			projectFolder : $('#tree_1').next().text()
		}, function(){
			$('#reviewCreatingModalBodyContent').empty();
			$('#anotherContent').empty();
			$('#reviewCreatingModalBodyContent').html('<input id="downloadChoiceButton" onclick="git()" class="btn btn-1 downloadButton"' + 
						'type="button" name="" value="' + reviewTranslate('clone.git') + '">' + 
					'<input id="archiveButton" onclick="archive()" class="btn btn-1 downloadButton"' +
						'type="button" name="" value="' + reviewTranslate('clone.archive') + '">');
			$('#reviewCreatingModalBodyContent').after('<div id="anotherContent"><div id="tree"></div></div>');
		});
	}
}

$('#codeReviewCreatingModal').on('hidden.bs.modal', function () {
	exit();
});

function reviewTranslate(label){
	if(locale == 'uk_UA'){
		switch(label){
			case 'clone.archive':
				return 'Завантажити архів';
			case 'clone.git':
				return 'Клонувати проект з Git репозиторія';
			case 'error.review.create':
				return 'Ви повинні обрати хоча б один файл і одного користувача!';
			case 'succes.review.create':
				return 'Ви успішно створили код рев`ю!';
			case 'review.create':
				return 'Створити Рев`ю';
			case 'review.users':
				return 'Оберіть користувачів..';
			case 'review.group':
				return 'Оберіть групу';
			case 'review.name':
				return 'Назва код рев`ю..';
			case 'step.tree':
				return 'Наступний крок: оберіть файли для Код Рев`ю';
			case 'input.password':
				return 'Пароль до вашого репозиторія';
			case 'step.branch':
				return 'Наступний крок: оберіть гілку ';
			case 'error.password':
				return 'Неправильний пароль або url!';
			case 'error.url':
				return 'Неправильний url!';
			case 'error.open.title':
				return 'Помилка!';
			case 'error.open.msg':
				return 'Ви не можете створити Code Review, бо у вас немає жодної групи, в якій були б учасники!';
			case 'https':
				return 'HTTPS URL..';
			case 'archive.upload':
				return 'Завантажити архів';
			case 'error.archive.type':
				return 'Ви можете завантажувати лише архіви (.zip)!';
			case 'error.archive.type.unknown':
				return  'Невідомий тип обраного архіву!';
		}
	}else{
		switch(label){
			case 'clone.archive':
				return 'Upload archive';
			case 'clone.git':
				return 'Clone project from Git repository';
			case 'error.review.create':
				return 'You should choose at least one file of tree and one user!';
			case 'succes.review.create':
				return 'You succesfully created new Code Review!';
			case 'review.create':
				return 'Create Review';
			case 'review.users':
				return 'Choose users..';
			case 'review.group':
				return 'Choose group';
			case 'review.name':
				return 'Code Review name..';
			case 'step.tree':
				return 'Next step: select files and folders you want to add to Code Review';
			case 'input.password':
				return 'Password to your repository';
			case 'step.branch':
				return 'Next step: choose branch of your repo ';
			case 'error.password':
				return 'Wrong password or url!';
			case 'error.url':
				return 'Wrong url!';
			case 'error.open.title':
				return 'Sorry, here`s error!';
			case 'error.open.msg':
				return 'You cannot create Code Review, because you have no groups with users!';
			case 'https':
				return 'HTTPS URL..';
			case 'archive.upload':
				return 'Upload archive';
			case 'error.archive.type':
				return 'You should upload only archives (.zip)!';
			case 'error.archive.type.unknown':
				return  'Unknown type of archive!';
		}
	}
}



