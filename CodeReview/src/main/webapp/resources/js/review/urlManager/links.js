var previusFileId = null;

var URLManager = {isRedirected:false};

URLManager.rememberFileId = function(){
	
	debugger;
	previusFileId = getParameterByName('file');
}

URLManager.clearURL = function (){
	debugger;
	
	if(!this.isRedirected){
		history.pushState('', document.title, window.location.pathname);
	}
}

URLManager.clearHash = function (){
	history.pushState("", document.title, window.location.pathname
			+ window.location.search);
}

function getReviewIdFromURL(){
	var url = document.URL;
    reg = /review\/(\d+)\\?/;
	return url.match(reg)[1];
}

URLManager.goToIssueFromUrl = function(){
	var issueId = getParameterByName('issue');
	
	if(issueId){
		$(function(){
			location.href = '#show_issue_'+issueId;
			URLManager.clearHash();
			
			$('#show_issue_'+issueId).transition('pulse');
			
			var replyId = getParameterByName('reply');
			
			if(replyId){
				$('#comments'+issueId).collapse('show');
				location.href = '#reply'+replyId;
				URLManager.clearHash();
				$('#reply'+replyId).css('background','#e8f5e9');
				
				var color = $('#comments'+issueId).css('background-color');
				
				setTimeout(function(){
			        $('#reply'+replyId).animate({
			          backgroundColor: color
			        }, 2000 );
				},2000);
			}
			
		});
	}
}

$(function() {
	var reviewId = getReviewIdFromURL();
	
	var fileId = getParameterByName('file');
	var issueId = getParameterByName('issue');
	var replyId = getParameterByName('reply');
	
	if(fileId || issueId || replyId){
		URLManager.isRedirected=true;
	}
	
	if(fileId){
		
		loadProject(reviewId);
		
		$('#dashboard').removeClass('active');
		$('#project').addClass('active');
		
	}else{
		loadDashboard(reviewId);
	}
	
});

URLManager.loadFileForReview = function (){
	debugger;
	
	var fileId = getParameterByName('file');
	
	if(fileId){
		$.post('filePath', {'fileId':fileId}, function(data){
			
			selectFileForReview(data.fullPath, data.path);
			
			//open tree
			var treeItemId = $('#file_'+fileId).parent().closest('li').attr('id');
			openTree(treeItemId);
		});
	}else if(previusFileId){
		loadPreviusState();
	}
}

loadPreviusState = function (){
		$.post('filePath', {'fileId':previusFileId}, function(data){
			debugger;
			
			//change url
			URLManager.goToFile(previusFileId);
			//open file
			selectFileForReview(data.fullPath, data.path);
			//open tree
			var treeItemId = $('#file_'+previusFileId).parent().closest('li').attr('id');
			openTree(treeItemId);
		});
}



URLManager.goToFile = function insertParam(fileId)
{
	
	if(!URLManager.isRedirected){
		URLManager.clearURL();
		
		var newUrl = document.URL+'?file='+fileId;
		
		window.history.pushState("review", "Title", newUrl)
	}
	
	URLManager.isRedirected=false;
}

$(function(){
	$('#openBtn').click(function(){

	console.log('open tree');
	openTree();
	
	});
});

function openTree(treeItemId){
	var root = 'tree_'+treeItemId.split('_')[1];
	openTreeBranch(root, treeItemId);
}

//branch - li element with class="tree-branch"
function openTreeBranch(branch, itemId){
	$('#'+branch+'>i').attr('class','tree-indicator fa fa-folder-open');
	
	var items = $('#'+branch+'>ul>li');
	
	for (var i = 0; i < items.length; ++i) {
	  var li = items[i];
	  $(li).css('display','block');
	  
	  if(itemId.startsWith(li.id) && $(li).hasClass('tree-branch')){
		  openTreeBranch(li.id, itemId);
	  }
	}
}
