function incrementCountOfMessages(){
	var count = $('#menu-toggle').attr('data-badge');
	
	if(!count){
		$('#menu-toggle').addClass('badge1');
		$('#menu-toggle').attr('data-badge', 1);
	}else{
		count = parseInt(count);
		$('#menu-toggle').attr('data-badge', ++count);
	}
}


var isClose = true;
$(document).ready(function () {
	
	debugger;
	
	var reviewId = document.getElementById("reviewIdForChat").value
	var userId = document.getElementById("userId").value
	var count = document.getElementById("countNewMessages").value
	
	if(count!=0){
		$('#menu-toggle').attr('data-badge', count);
	}

	var endPointURL = "ws://" + window.location.host
			+ "/CodeReview/websocket/reviewchat";
	var socket = new WebSocket(endPointURL
			+ "?reviewId="+reviewId);
	
	socket.onmessage = function(event) {

		showMessage(event.data);
	};
	
	socket.onopen = function() {
		console.log('connection is open');
	};
	
	$("#btn-chat").click(function() {
		sendMessage();
	});
	
	function sendMessage(){
		var message = document.getElementById("btn-input").value;

		var jsonObj = {
			reviewId : reviewId,
			userId : userId,
			message : message
		}

		socket.send(JSON.stringify(jsonObj));
		$("#btn-input").val("");
		$("#btn-input").focus();
	}
	
	
	function showMessage(messageId) {
		if(isClose){
			incrementCountOfMessages();
		}

		$.ajax({
			url : 'chat/sendMessage',
			type : 'get',
			dataType : 'text',
			data : {
				'messageId' : messageId
			}
		}).success(function(data) {

			$("#forMessage").append(data);
			location.href = '#message'+messageId;
			clearHash();

		}).error(function(data) {

			alert("ne ok");
		});
	}


    $("#menu-toggle").click(function (e) {
        $("#sidebar-wrapper").toggleClass("active");
        e.preventDefault();
        
        if(isClose){
        	
			$("#btn-input").focus(function() {
				$(".all-comments li.newMessage").css("background", "#fff");
				$('#newMessages').remove();
				$('#menu-toggle').removeAttr("data-badge");
			});
			
			$.ajax({
				url : 'chat/openReviewChat',
				type : 'get',
				dataType : 'text',
				data : { 'reviewId' : reviewId }
			}).success(function(data) {
				$("#forMessage").empty();
				
				$("#forMessage").append(data);
				
				location.href = '#btn-chat';
				location.href = '#newMessages';
				clearHash();
				
			}).error(function(data) {
				alert("ne ok");
			});
        }else{
        	
            $.ajax({
				url : 'chat/closeReviewChat',
				type : 'get'
			}).error(function(data) {
				alert("ne ok");
			});
        	
        }
    });

    $("#menu-toggle").bind('click', function (e) {

        if (isClose) {
            $("#sidebar-wrapper").toggleClass("active", true);
            e.preventDefault();
            isClose = false;

        } else {
            $('#sidebar-wrapper').toggleClass('active', false);
            e.preventDefault();

            isClose = true; 
        }

    });
    
    $('#btn-input').keydown(function(event) {
        if (event.keyCode == 13) {
        	if (!event.shiftKey){
        		sendMessage();
        		event.preventDefault();
        	} 
         }
    }).focus();
});