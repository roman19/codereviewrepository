SyntaxHighlighter.defaults['quick-code'] = false;
SyntaxHighlighter.defaults['toolbar'] = false;
SyntaxHighlighter.defaults['auto-links'] = false;

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

(function ($) {
    $.fn.hasScrollBar = function () {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);

//DragManager
DragManager.onDragCancel = function (dragObject) {
    dragObject.avatar.rollback();
};

DragManager.onDragEnd = function (dragObject, dropElem) {
    dragObject.elem.style.display = 'none';
    dropElem.className = 'droppable file';
    setTimeout(function () {
        dropElem.classList.remove('file');
    }, 600);
};

//Tree
$.fn.extend({
    treeview: function () {
        return this.each(function () {
            // Initialize the top levels;
            var tree = $(this);

            tree.addClass('treeview-tree');
            tree.find('li').each(function () {
                var stick = $(this);
            });
            tree.find('li').has("ul").each(function () {
                var branch = $(this); //li with children ul

                branch.prepend("<i class='tree-indicator fa fa-folder'></i>");
                branch.addClass('tree-branch');
                branch.on('click', function (e) {
                    if (this == e.target) {
                        var icon = $(this).children('i:first');

                        icon.toggleClass("fa-folder-open fa-folder");
                        $(this).children().children().toggle();
                    }
                });
                branch.children().children().toggle();
                branch.children('.tree-indicator, button, a').click(function (e) {
                    branch.click();
                    e.preventDefault();
                });
            });
        });
    }
});

function getRelativePathForShow(path) {
    var indices = [];
    for (var i = 0; i < path.length; i++) {
        if (path[i] === "/") indices.push(i);
    }
    return path.substring(indices[1] + 1, path.length).replace(new RegExp('/', 'g'), ' / ');
}

function disablePostIssueButtons(state) {
    $('#error').prop('disabled', state);
    $('#warning').prop('disabled', state);
    $('#info').prop('disabled', state);
    $('#issue_cancel_btn').prop('disabled', state);
}


function updateFileIssueLabel(fileId, countIssues) {
    var fileLabelSelector = $('#file_' + fileId);
    fileLabelSelector.text(countIssues > 0 ? countIssues : '');
    if (countIssues > 0) {
        fileLabelSelector.attr('class', 'ui label circular issues-count-label');
        fileLabelSelector.text(countIssues);
    } else {
        fileLabelSelector.attr('class', '');
        fileLabelSelector.text('');
    }
}

function addIssues(issues) {
    for (var i = 0; i < issues.length; i++) {
        showIssueTemplate(issues[i].id,
            issues[i].type,
            issues[i].userLogin,
            issues[i].title,
            issues[i].body,
            issues[i].time,
            issues[i].isEditable,
            issues[i].lines[issues[i].lines.length - 1],
            issues[i].lines,
            issues[i].countOfReplies);
    }
}

function getSelectedLines() {
    var lines = [];
    var selection = window.getSelection();
    if (selection && !isEmpty(selection.toString()) && !isBlank(selection.toString()) && selection.rangeCount) {
        var allSelected = [];
        var range = selection.getRangeAt(0);
        var topRangeNode = range.commonAncestorContainer;
        if (topRangeNode.nodeName == "DIV" && topRangeNode.className.indexOf("line") == -1) {
            var allWithinRangeParent = topRangeNode.getElementsByClassName("line");
            for (var i = 0, el; el = allWithinRangeParent[i]; i++) {
                if (selection.containsNode(el, true)) {
                    allSelected.push(el);
                }
            }
        } else {
            while (topRangeNode.nodeName != "DIV") {
                topRangeNode = topRangeNode.parentNode;
            }
            allSelected.push(topRangeNode);
        }
        selection.removeAllRanges();
        for (i = 0; i < allSelected.length; i++) {
            lines.push(allSelected[i].getAttribute("class").match(/(number)\d+/)[0].match(/\d+/)[0]);
        }
    }
    return lines;
}

//for comments
function placeCaretAtEnd(el) {
    el.focus();
    if (typeof window.getSelection != "undefined"
        && typeof document.createRange != "undefined") {
        var range = document.createRange();
        range.selectNodeContents(el);
        range.collapse(false);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (typeof document.body.createTextRange != "undefined") {
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(el);
        textRange.collapse(false);
        textRange.select();
    }
}

function createIssueHighlightLines(lines) {
    addHighlightLines(lines);
    $("#create_issue").on("remove", function () {
        removeHighlightLines(lines);
    });
}

function addIssueLinesHover(issueSelector, linesHighlight) {
    issueSelector.hover(
        function () {
            addHighlightLines(linesHighlight);
        }, function () {
            removeHighlightLines(linesHighlight);
        }
    );
}

function changeHeight(issueBlock, issueStub, issueBody) {
    var x = issueBlock.height();
    console.log("--------" + x);
    issueStub.attr('style', 'height:' + x + 'px !important');
    issueBlock.on('show.bs.collapse', function () {
        issueStub.attr('style', 'height:' + (issueBlock.height() + issueBody.height()) + 'px !important');
    });
    issueBlock.on('shown.bs.collapse', function () {
        issueStub.attr('style', 'height:' + issueBlock.height() + 'px !important');
    });
    issueBlock.on('hide.bs.collapse', function () {
        issueStub.attr('style', 'height:' + (issueBlock.height() - issueBody.height()) + 'px !important');
    });
    issueBlock.on('hidden.bs.collapse', function () {
        issueStub.attr('style', 'height:' + issueBlock.height() + 'px !important');
    });
}

function showIssueChangeStubHeight(issueSelector, issueBodySelector, issueStubSelector) {
    changeHeight(issueSelector, issueStubSelector, issueBodySelector);
    issueSelector.bind('show', function () {
        changeHeight(issueSelector, issueStubSelector, issueBodySelector);
    });
}

function cancelIssueTyping(issueId) {
    $("#create_issue_stub").remove();
    $("#create_issue").remove();
    onSelectLines();
    if (issueId) {
        $("#show_issue_" + issueId).show();
        $("#show_issue_stub_" + issueId).show();
    }
}

function addHighlightLines(lines) {
    for (var i = 0; i < lines.length; i++) {
        $(".code .line.number" + lines[i]).addClass('highlighted');
        $(".gutter .line.number" + lines[i]).addClass('highlighted');
    }
}

function removeHighlightLines(lines) {
    for (var i = 0; i < lines.length; i++) {
        $(".code .line.number" + lines[i]).removeClass('highlighted');
        $(".gutter .line.number" + lines[i]).removeClass('highlighted');
    }
}

function getPanelClassForIssueType(type) {
    if (type == 'underfund' || type == null) return '';
    if (type == 'ERROR') return 'panel-danger';
    if (type == 'WARNING') return 'panel-warning';
    if (type == 'INFO') return 'panel-info';
    return '';
}

function getBtnClassIssueForType(type) {
    if (type == 'underfund' || type == null) return '';
    if (type == 'ERROR') return 'btn-danger';
    if (type == 'WARNING') return 'btn-warning';
    if (type == 'INFO') return 'btn-info';
    return '';
}

function getGlyphIconForIssueType(type) {
    if (type == 'underfund' || type == null) return '';
    if (type == 'ERROR') return "<i class='fa fa-exclamation-circle issue-sign'></i>";
    if (type == 'WARNING') return "<i class='fa fa-exclamation-triangle issue-sign'></i>";
    if (type == 'INFO') return "<i class='fa fa-info-circle issue-sign'></i>";
    return '';
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function getBackgroudColorForComments(type){
    if(type=='ERROR'){
        return '#fefafa';
    }else if(type=='WARNING'){
        return '#fff8e1';
    }else if(type=='INFO'){
        return '#f1f9fe';
    }
}
