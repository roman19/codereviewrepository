//for deleting attached files
function deleteAttachedFile(nodeId, shortPath, size){
	
	nodeId = decodeURI(nodeId);
	shortPath = decodeURI(shortPath);
	
	currentNode = document.getElementById(nodeId);
	if (currentNode) {
		currentNode.parentNode.removeChild(currentNode);
	}
	
    refreshMemoryStateForPCAttachment(parseInt('-' + size));
	
	var ulFiles = document.getElementById('ulFiles');
	var nodeLi = document.createElement("li");
	nodeLi.innerHTML = "<a id='"+ shortPath +"' href='#' class='draggable' value='" + size + "'>" + shortPath + "</a>";
	ulFiles.appendChild(nodeLi);
}
// end for deleting attached files
	  
var DragManager = new function() {

	  /**
	   * составной объект для хранения информации о переносе:
	   * {
	   *   elem - элемент, на котором была зажата мышь
	   *   avatar - аватар
	   *   downX/downY - координаты, на которых был mousedown
	   *   shiftX/shiftY - относительный сдвиг курсора от угла элемента
	   * }
	   */
	  var dragObject = {};

	  var self = this;

	  function onMouseDown(e) {

	    if (e.which != 1) return;

	    var elem = e.target.closest('.draggable');
	    if (!elem) return;

	    dragObject.elem = elem;

	    // запомним, что элемент нажат на текущих координатах pageX/pageY
	    dragObject.downX = e.pageX;
	    dragObject.downY = e.pageY;

	    return false;
	  }

	  function onMouseMove(e) {
	    if (!dragObject.elem) return; // элемент не зажат

	    if (!dragObject.avatar) { // если перенос не начат...
	      var moveX = e.pageX - dragObject.downX;
	      var moveY = e.pageY - dragObject.downY;

	      // если мышь передвинулась в нажатом состоянии недостаточно далеко
	      if (Math.abs(moveX) < 3 && Math.abs(moveY) < 3) {
	        return;
	      }

	      // начинаем перенос
	      dragObject.avatar = createAvatar(e); // создать аватар
	      if (!dragObject.avatar) { // отмена переноса, нельзя "захватить" за эту часть элемента
	        dragObject = {};
	        return;
	      }

	      // аватар создан успешно
	      // создать вспомогательные свойства shiftX/shiftY
	      var coords = getCoords(dragObject.avatar);
	      dragObject.shiftX = dragObject.downX - coords.left;
	      dragObject.shiftY = dragObject.downY - coords.top;

	      startDrag(e); // отобразить начало переноса
	    }

	    // отобразить перенос объекта при каждом движении мыши
	    dragObject.avatar.style.left = e.pageX - dragObject.shiftX + 'px';
	    dragObject.avatar.style.top = e.pageY - dragObject.shiftY + 'px';

	    return false;
	  }

	  function onMouseUp(e) {
	    if (dragObject.avatar) { // если перенос идет
	      finishDrag(e);
	    }

	    // перенос либо не начинался, либо завершился
	    // в любом случае очистим "состояние переноса" dragObject
	    dragObject = {};
	  }

	  function finishDrag(e) {
	    var dropElem = findDroppable(e);

	    if (!dropElem) {
	      self.onDragCancel(dragObject);
	    } else {
	      self.onDragEnd(dragObject, dropElem);
	      
	      //create file item in tree
	      var node = document.createElement("li");
	      var textnode = document.createTextNode($(dragObject.elem).attr("id"));
	      var className = document.getElementById($(dropElem).attr("id")).previousSibling.className;
	      node.style.display = className.indexOf('open') > -1 ? "block":"none";
	      node.id = $(dropElem).attr("id") + '\\' + $(dragObject.elem).attr("id");
	      node.className = 'newFile';
	      
	      var span = document.createElement("SPAN");
	      debugger;
	      var attrId  = $(dragObject.elem).attr("id");
	      var size  = $(dragObject.elem).attr("value");
	      refreshMemoryStateForPCAttachment(size);
	      span.innerHTML = attrId+'<button onClick="deleteAttachedFile(\'' + encodeURI(node.id) +"\',\'"+ encodeURI(attrId) +'\',' + size + ')" id="button_'+attrId+'" type="button" class="close" style="float:none;padding-left:3px; bottom:-2px; position:relative">&times;</button>';
	      
	      
	      node.appendChild(span);
	      document.getElementById('folder_' + $(dropElem).attr("id")).appendChild(node);
	    }
	  }
	  
	  
	  
	  function getFolders(path){
		  var folders = path.split('\\');
		  var newPath = '';
		  for(var i = 2; i < folders.length; i++){
			  newPath += folders[i] + '/';
		  }
		  return newPath;
	  }

	  function createAvatar(e) {

	    // запомнить старые свойства, чтобы вернуться к ним при отмене переноса
	    var avatar = dragObject.elem;
	    var old = {
	      parent: avatar.parentNode,
	      nextSibling: avatar.nextSibling,
	      position: avatar.position || '',
	      left: avatar.left || '',
	      top: avatar.top || '',
	      zIndex: avatar.zIndex || ''
	    };

	    // функция для отмены переноса
	    avatar.rollback = function() {
	      old.parent.insertBefore(avatar, old.nextSibling);
	      avatar.style.position = old.position;
	      avatar.style.left = old.left;
	      avatar.style.top = old.top;
	      avatar.style.zIndex = old.zIndex
	    };

	    return avatar;
	  }

	  function startDrag(e) {
	    var avatar = dragObject.avatar;

	    // инициировать начало переноса
	    document.body.appendChild(avatar);
	    avatar.style.zIndex = 9999;
	    avatar.style.position = 'absolute';
	  }

	  function findDroppable(event) {
	    // спрячем переносимый элемент
	    dragObject.avatar.hidden = true;

	    // получить самый вложенный элемент под курсором мыши
	    var elem = document.elementFromPoint(event.clientX, event.clientY);

	    // показать переносимый элемент обратно
	    dragObject.avatar.hidden = false;

	    if (elem == null) {
	      // такое возможно, если курсор мыши "вылетел" за границу окна
	      return null;
	    }

	    return elem.closest('.droppable');
	  }

	  document.onmousemove = onMouseMove;
	  document.onmouseup = onMouseUp;
	  document.onmousedown = onMouseDown;

	  this.onDragEnd = function(dragObject, dropElem) {};
	  this.onDragCancel = function(dragObject) {};

	};


	function getCoords(elem) { // кроме IE8-
	  var box = elem.getBoundingClientRect();

	  return {
	    top: box.top + pageYOffset,
	    left: box.left + pageXOffset
	  };
	}