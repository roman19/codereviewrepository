
var projectFolder = null;
var locale = $('#locale_id').val();

var usedMemory = null;
var newMemory = null;
var enabledMemory = null;


function animation(id, isOn){
	if(isOn){
		new imageLoader(cImageSrc, 'startAnimation()', id);
	}else{
		setTimeout('stopAnimation()', 0);
	}
	$('#' + id).attr("disabled", isOn);
}

//////////////////////////////////////////////// pc* ////////////////////////////

function pc_attach(){
	$('#reviewCreatingModalBodyContentAll').attr('style', 'overflow: none; max-height: 60%; min-height:5%;');
	$.post("extensions", function(extensions){
		$('#reviewCreatingModalBodyContent').html('<span id="files_to_upload" class="btn btn-primary btn-file">' +
				getLabel('pc.upload') + '<input id="file" type="file" onchange="uploadFiles()"' +
				' accept="' + extensions + '" multiple /></span>');
	});
}

function uploadFiles(){
	$('#files_to_upload').after('<div id="uploadProcess"><div id="upload"></div>' + getLabel('pc.upload') + '</div>');
	animation('upload', true);
	var data = new FormData();
	var files = [];
	$.each($('#file')[0].files, function(i, file) {
		data.append('file-' + i, file);
		files.push(file.name);
	});
	if(files.length == 0) return;
	$.ajax({
		url : 'pc_attachment',
		data : data,
		cache : false,
		contentType : false,
		processData : false,
		type : 'POST',
		success : function(data) {
			$('#reviewCreatingModalBodyContentAll').attr('style', 'overflow: auto; max-height: 60%; min-height:5%;');
			$('#uploadProcess').remove();
			projectFolder = data.tempFolder;
			if(data.files.length == 0){
				deleteFolder(function(){
					swal(getLabel('error'), getLabel('error.pc.files.msg') + data.failedExtensions, "error");
				});
			}else{
				$('#reviewCreatingModalBodyContent').html('<h3 id="pcTitle" style="margin-left: 3%;">' + getLabel('step.drag&drop') + '</h3>' + 
						'<div id="filesDragNDrop" class="container-fluid"></div>');
				showTree();
				var filesTemp = [];
				for(var i = 0; i < data.files.length; i++){
					filesTemp.push(JSON.parse(data.files[i]));
				}
				showFiles(filesTemp);
				$('#filesDragNDrop').after('<input id="pc_save" onclick="pc_save()"' +
						'class="btn btn-1" type="button" name="" value="OK">');
				if(data.failedExtensions != ""){
					swal(getLabel('error'), getLabel('error.pc.files.msg') + data.failedExtensions, "error");
				}
			}
		}
	});
}

function pc_save(){
	$('.reviewError').empty();
	if(usedMemory + newMemory > enabledMemory){
		$('#filesDragNDrop').after('<h3 class="reviewError" style="margin-left: 10%;">' + mTranslate('memory.error') + '</h2>');
		return;
	}
	animation('pc_save', true);
	var files = getNewFiles();
	$.post('pc_attachment_save_changes',{
		tempFolder : projectFolder,
		newFiles : files.join(),
		code_review_id : $('#reviewIdForChat').val()
	}, function(result){
		if(result == 'no files'){
			$('#filesDragNDrop').after('<h3 class="reviewError" style="margin-left: 10%;">' +
					getLabel('error.drag&drop') + '</h2>');
			animation('pc_save', false);
		}else{
			socket.send(JSON.stringify(result));
			setTimeout('stopAnimation()', 0);
			$('#filesDragNDrop').after('<h3 class="reviewSucces" style="margin-left:10%">' + getLabel('succes.files.msg1')
					+ (result.countFiles == 1 ? getLabel('succes.files.msg3'): result.countFiles + getLabel('succes.files.msg2')) + '</h2>');
		}
	});
}

function getNewFiles(){
	var files = [];
	$(".newFile").each(function() {
		files.push($(this).attr('id'));
	});
	return files;
}

function showTree(tree){
	$('#filesDragNDrop').html('<div id="treeToDrag" class="col-xs-7" style="margin-top: 30px;">' +
									'<h4>' + getLabel('project.tree') + '</h4><div class="panel panel-default">' +
										'<div id="folderTree" class="panel-body" style="overflow-y:auto"></div>' +
									'</div>' +
								'</div>');
	$.post("folder_tree", {
		code_review_id : $('#reviewIdForChat').val()
	}, function(result){
		$('#folderTree').html(result.tree);

		usedMemory = result.usedMemory;
		enabledMemory = result.enabledMemory;
		$('#pcTitle').after('<hr>' + 
				'<div style="margin-top: 20px; margin-bottom: -40px;">' + 
					'<h5 style="margin-left: 15%;text-align: left">' + mTranslate('memory.title') + ' <span id="usedMemory">' + convertBytesInMegaBytes(usedMemory) + '</span>' + result.memory + '</h5>' + 
					'<div class="progress" style="margin-left: 10%; width: 80%; height: 25px; background-color: #CBC8C8">' + 
						'<div id="memoryProgressBar" class="progress-bar active" role="progressbar" ' +
							'aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: ' + 
							 result.memoryPercentage + '%;">' + 
						'</div>' +
					'</div>' + 
				'</div>');
		refreshMemoryStateForPCAttachment(0);
		
		$('#folderTree').each(function () {
			var tree = $(this);
			tree.treeview();
		});
	});
}

function showFiles(files){
	$('#treeToDrag').after('<div class="col-xs-4 fixed">' +
								'<h4>' + getLabel('files.added') + '</h4><div class="panel panel-default">' +
									'<div id="files" class="panel-body" style="overflow: auto; height: 100px;"></div>' +
								'</div>' +
						   '</div>');
	$('#files').html('<ul id="ulFiles"><li id="0"><a id="' + files[0].name + '" href="#" class="draggable" value="' + files[0].size + '">' + files[0].name + '</a></li>');
	for(var i = 1; i < files.length; i++){
		$('#' + (i - 1)).after('<li id="' + i + '"><a id="' + files[i].name + '"' + 
				' href="#" class="draggable" value="' + files[i].size + '">' + files[i].name + '</a></li>');
	}
	$('#' + (i - 1)).after('</ul>');
}

function refreshMemoryStateForPCAttachment(newMemoryTemp){
	if(newMemory == null){
		newMemory = 0;
	}
	newMemory += parseInt(newMemoryTemp);
	var used = parseInt(usedMemory) + parseInt(newMemory);
	document.getElementById('usedMemory').innerHTML = convertBytesInMegaBytes(used);
	$('#memoryProgressBar').css('width', ((used / enabledMemory) * 100) + "%");
	if(used > enabledMemory){
		$('#memoryProgressBar').addClass('progress-bar-danger');
	}else{
		$('#memoryProgressBar').removeClass('progress-bar-danger');
	}
}

//////////////////////////////////////////////// git* ////////////////////////////

var projectName = null;

function git_attach(){
	$('#reviewCreatingModalBodyContent').attr('style', '');
	if($('#clone').val() != null) {
		$('#preloaderOnStart').remove();
		animation('clone', true);
	}else{
		$('#reviewCreatingModalBodyContent').html('<div id="preloaderOnStart"><div id="preloaderOn"></div>' +
				'<label style="margin-left: 80px; margin-top: 25px; font-size: 20px">' + getLabel('git.downloading') + '</label></div>');
		animation('preloaderOnStart', true);
	}
	$.post("git_attachment", {
		password : $('#passwordToRepo').val(),
		code_review_id : $('#reviewIdForChat').val()
	}, function(data){
		if(data.status == 'no_repository'){
			$('#reviewCreatingModalBodyContent').html('<label class="error">' + getLabel('error.git.norepository') + '</label>');
		}else if(data.status == 'auth_error'){
			if($('#passwordToRepo').val() == null) createPasswordInput();
			else {
				$('.error').remove();
				$('#passDiv').after('<label class="error">' + getLabel('error.password') + '</label>');
			}
			animation('clone', false);
		}else{
			var password = $("#passwordToRepo").val();
			$('#reviewCreatingModalBodyContent').empty();
			$('#reviewCreatingModalBodyContent').html('<div class="step">' + getLabel('step.branch') + '<select id="branches"></select>');
			var branchesEl = document.getElementById('branches');
			for(var i = 0; i < data.branches.length; i++){
				var option = document.createElement('option');
				var temp = data.branches[i].split('/');
				option.text = temp[temp.length - 1];
				option.id = data.branches[i];
				branchesEl.add(option, i);
			}
			$('#branches').after('<input id="clone" onclick="' + (password == null ? 'clone(\'' + data.url + '\')':
				'clonePrivate(\'' + data.url + '\', \'' + password +'\')') + '"' +
					' class="btn btn-1" type="button" name="" value="OK"></div>');//OK
			animation('clone', false);
		}
	});
}

function createPasswordInput(){
	$('#reviewCreatingModalBodyContent').html('<div id="passDiv"><input id="passwordToRepo" type="password"' +
	' placeholder="' + getLabel('git.password') + '" >');		//password input
	$('#passwordToRepo').after('<input id="clone" onclick="git_attach()"' +
		'class="btn btn-1" type="button" name="" value="OK"></div>');//OK
}

function clone(url){
	animation('clone', true);
	$.get("../git_downloader", {
		url : url,
		branch : $('#branches').children(":selected").attr("id")
	}, function(result) {
		handleResultOfDownloading(result);
	});
}

function clonePrivate(url, password){
	animation('clone', true);
	$.post("../git_downloader", {
		url : url,
		password : password,
		branch : $('#branches').children(":selected").attr("id")
	}, function(result){
		if(result != 'invalid url' || result != 'private' || result != 'wrong_link_or_pswd') {
			var temp = result.split(',');
			result = temp[0];
			projectName = temp[1];
		}
		handleResultOfDownloading(result);
	})
}

function handleResultOfDownloading(result){
	if(projectName == null) {
		projectName = result;
	}
	branch = $('#branches').children(":selected").attr("id");
	animation('clone', false);
	$('#reviewCreatingModalBodyContent').empty();
	$('#reviewCreatingModalBodyContent').html('<div id="treeTitle">' + getLabel('step.tree') + '</div>');
	buildTree(result);
}
function mTranslate(label){
	if(locale == 'uk_UA'){
		switch(label){
			case 'memory.title':
				return 'Використана пам`ять:';
			case 'memory.error':
				return 'Недостатньо пам`яті для додавання обраних файлів!';
		}
	}else{
		switch(label){
			case 'memory.title':
				return 'Used memory:';
			case 'memory.error':
				return 'You haven`t enough memory to attach selected files!';
		}
	}
}

function save_changes(){
	$('.reviewError').remove();
	if(usedMemory + newMemory > enabledMemory){
		$('#attachTree').after('<h3 class="reviewError" style="margin-left: 10%;">' + mTranslate('memory.error') + '</h2>');
		return;
	}
	animation('saveChanges', true);
	var pathes = [];
	getAllPathesFromCheckedItems('1', pathes);
	$.post('git_attachment_save_changes', {
		pathes : pathes.join(),
		code_review_id : $('#reviewIdForChat').val()
	}, function(result){
		$('.reviewError').empty()
		if(result == 'no pathes'){
			$('#attachTree').after('<h3 class="reviewError" style="margin-left: 10%;">' + getLabel('error.review.create') + '</h2>');
			animation('saveChanges', false);
		}else{
			socket.send(JSON.stringify(result));
			setTimeout('stopAnimation()', 0);
			$('#reviewError').remove();
			$('#attachTree').after('<h3 class="reviewSucces" style="margin-left:10%">' + getLabel('succes.files.msg1')
					+ (result.countFiles == 1 ? getLabel('succes.files.msg3'): result.countFiles + getLabel('succes.files.msg2')) + '</h2>');
		}
	});
}

/////////////////////////////////////////////////	tree*	////////////////////////////////////

function buildTree(project){
	$.get("attachment_tree", {
		project : project,
		projectName : projectName,
		code_review_id : $('#reviewIdForChat').val()
	}, function(result){
		$('#anotherAttachContent').html('<div id="attachTree"></div>');
		$('#attachTree').html(result.tree);
		usedMemory = result.usedMemory;
		enabledMemory = result.enabledMemory;
		$('#treeTitle').after('<hr>' + 
				'<div style="margin-top: 20px; margin-bottom: -40px;">' + 
					'<h5 style="margin-left: 15%;text-align: left">' + mTranslate('memory.title') + ' <span id="usedMemory">' + convertBytesInMegaBytes(usedMemory) + '</span>' + result.memory + '</h5>' + 
					'<div class="progress" style="margin-left: 10%; width: 80%; height: 25px; background-color: #CBC8C8">' + 
						'<div id="memoryProgressBar" class="progress-bar active" role="progressbar" ' +
							'aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: ' + 
							 result.memoryPercentage + '%;">' + 
						'</div>' +
					'</div>' + 
				'</div>');
		refreshMemoryState(0);
		projectFolder = $('#tree_1').next().text();
		$('#attachTree').each(function () {
			var tree = $(this);
			tree.treeview();
		});
		$('#attachTree').after('<input id="saveChanges" onclick="save_changes()"' +
			'class="btn btn-1" type="button" name="" value="OK"></div>');//OK
	});
}

function getAllPathesFromCheckedItems(id, pathes){
	if($('#tree_' + id + '_1').val() == null) return;
	for(var i = 1; $('#tree_' + id + '_' + i).val() != null; i++){
		var item = $('#tree_' + id + '_' + i);
		if(item.is(':checked')) {
			if(item.attr('name').indexOf('.') > -1) pathes.push(item.attr('name'));
		}
		getAllPathesFromCheckedItems(id + '_' + i, pathes);
	}
}

function selectFile(id){
	var valueOfSelectedCheckBox = $('#tree_' + id).is(':checked');
	if(valueOfSelectedCheckBox){
		selectAllFoldersBefore(id);
	}
	refreshMemoryState(getSumMemoryOfCheckedFiles('1'));
}

function selectFolder(id) {
	var valueOfSelectedCheckBox = $('#tree_' + id).is(':checked');
	if(valueOfSelectedCheckBox){
		selectAllFoldersBefore(id);
	}
	selectAllItemsAfter(id, valueOfSelectedCheckBox);
	refreshMemoryState(getSumMemoryOfCheckedFiles('1'));
}

function refreshMemoryState(newMemoryTemp){
	newMemory = newMemoryTemp;
	var used = usedMemory + newMemory;
	document.getElementById('usedMemory').innerHTML = convertBytesInMegaBytes(used);
	$('#memoryProgressBar').css('width', ((used / enabledMemory) * 100) + "%");
	if(used > enabledMemory){
		$('#memoryProgressBar').addClass('progress-bar-danger');
	}else{
		$('#memoryProgressBar').removeClass('progress-bar-danger');
	}
}

function convertBytesInMegaBytes(bytes){
	return (bytes / 1024) / 1024;
}

function getSumMemoryOfCheckedFiles(id){
	var sum = 0;
	if($('#tree_' + id + '_1').val() == null) return sum;
	for(var i = 1; $('#tree_' + id + '_' + i).val() != null; i++){
		if(!$('#tree_' + id + '_' + i).is(':disabled') && $('#tree_' + id + '_' + i).is(':checked')){
			if($('#tree_' + id + '_' + i).attr('value') != null){
				sum += parseInt($('#tree_' + id + '_' + i).attr('value'));
			}
		}
		sum += parseInt(getSumMemoryOfCheckedFiles(id + '_' + i));
	}
	return sum;
}

/**
 * Makes all folders and files after current checked
 * @param id
 * @param valueOfSelectedCheckBox
 */
function selectAllItemsAfter(id, valueOfSelectedCheckBox){
	if($('#tree_' + id + '_1').val() == null) return;
	for(var i = 1; $('#tree_' + id + '_' + i).val() != null; i++){
		if(!$('#tree_' + id + '_' + i).is(':disabled')){
			$('#tree_' + id + '_' + i).prop('checked', valueOfSelectedCheckBox);
		}
		selectAllItemsAfter(id + '_' + i, valueOfSelectedCheckBox);
	}
}

/**
 * Makes all folders before checked
 * @param id
 */
function selectAllFoldersBefore(id){
	var res = id.split("_");
	var temp = "";
	for (var i = 0; i < res.length - 1; i++) {
		if (temp == "") {
			temp += res[i];
		}
		else{
			temp += "_" + res[i];
		}
		$('#tree_' + temp).prop('checked', true);
	}
}

//////////////////////////////////////////////////	exit*	////////////////////////////////////////////////////

function exit(){
	deleteFolder(function(){
		if($('#saveChanges').is(':disabled') || $('#pc_save').is(':disabled')){
			$('#reviewCreatingModalBodyContent').empty();
			$('#anotherAttachContent').empty();
			location.reload(true);
		}else{
			$('#reviewCreatingModalBodyContent').empty();
			$('#anotherAttachContent').empty();
		}
	});
}

function deleteFolder(resultFunction){
	$.get("cancel", {
		projectFolder : projectFolder,
		code_review_id : $('#reviewIdForChat').val()
	}, resultFunction);
}

$('#fileAttachment').on('hidden.bs.modal', function () {
	exit();
});

function getLabel(label){
	if(locale == 'uk_UA'){
		switch(label){
		case 'files.added':
			return 'Додані файли';
		case 'error.review.create':
			return 'Ви повинні обрати хоча б один файл!';
		case 'step.tree':
			return 'Наступний крок: оберіть файли для Код Рев`ю ';
		case 'git.password':
			return 'Пароль до вашого репозиторія';
		case 'step.branch':
			return 'Наступний крок: оберіть файли для Код Рев`ю';
		case 'error.password':
			return 'Неправильний пароль!';
		case 'error.git.norepository':
			return 'Для цього проекту не існує репозиторію!';
		case 'project.tree':
			return 'Дерево проекту';
		case 'succes.files.msg1':
			return 'Ви успішно додали ';
		case 'succes.files.msg2':
			return ' нових файлів!';
		case 'succes.files.msg3':
			return '1 новий файл!';
		case 'error.drag&drop':
			return 'Ви повинні перетягнути в дерево хоча б один файл!';
		case 'step.drag&drop':
			return 'Наступний крок: просто перетягніть файли справа в папки дерева.';
		case 'error.pc.files.msg':
			return 'Ви не можете завантажити файли з розширенням ';
		case 'error':
			return 'Помилка!';
		case 'git.downloading':
			return 'Завантаження..';
		case 'pc.upload':
			return  'Завантажити файли';
		}
	}else{
		switch(label){
		case 'files.added':
			return 'Added files';
		case 'error.review.create':
			return 'You should choose at least one file of tree!';
		case 'step.tree':
			return 'Next step: select files and folders you want to add to Code Review ';
		case 'git.password':
			return 'Password to your repository';
		case 'step.branch':
			return 'Next step: choose branch of your repo ';
		case 'error.password':
			return 'Wrong password!';
		case 'error.git.norepository':
			return 'There are no repository for this project!';
		case 'project.tree':
			return 'Project tree';
		case 'succes.files.msg1':
			return 'You succesfully added ';
		case 'succes.files.msg2':
			return ' new files!';
		case 'succes.files.msg3':
			return '1 new file!';
		case 'error.drag&drop':
			return 'You should drag&drop into the tree at least one file!';
		case 'step.drag&drop':
			return 'Next step: just drag&drop files from right into the folders.';
		case 'error.pc.files.msg':
			return 'You cannot upload files with extensions ';
		case 'error':
			return 'Error!';
		case 'git.downloading':
			return 'Downloading..';
		case 'pc.upload':
			return  'Upload files';
		}
	}
}