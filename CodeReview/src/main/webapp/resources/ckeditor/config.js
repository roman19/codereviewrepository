/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    //config.toolbarGroups = [
    //    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
    //    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
    //    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
    //    { name: 'forms', groups: [ 'forms' ] },
    //    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    //    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
    //    { name: 'links', groups: [ 'links' ] },
    //    { name: 'insert', groups: [ 'insert' ] },
    //    { name: 'styles', groups: [ 'styles' ] },
    //    { name: 'colors', groups: [ 'colors' ] },
    //    { name: 'tools', groups: [ 'tools' ] },
    //    { name: 'others', groups: [ 'others' ] },
    //    { name: 'about', groups: [ 'about' ] }
    //];
    //
    //config.removeButtons = 'Cut,Copy,Paste,Undo,Redo,Anchor,Subscript,Superscript,Unlink,Link,About,autoFormat,CommentSelectedRange,UncommentSelectedRange';
    config.toolbarGroups = [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'forms', groups: [ 'forms' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] },
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'others', groups: [ 'others' ] },
        { name: 'about', groups: [ 'about' ] }
    ];

    config.removeButtons = 'Cut,Copy,Paste,Undo,Redo,Anchor,Subscript,Superscript,Source,autoFormat,CommentSelectedRange,UncommentSelectedRange,AutoComplete,NumberedList,BulletedList,Link,Unlink,About';
    config.autoParagraph = false;

    config.codeSnippet_languages = {
        javascript: 'JavaScript',
        php: 'PHP',
        apache: 'Apache',
        bash: 'Bash',
        cpp: 'C++',
        cs: 'C#',
        css: 'CSS',
        html: 'HTML',
        java: 'Java',
        json: 'JSON',
        python: 'Python',
        ruby: 'Ruby',
        sql: 'SQL',
        xml: 'XML'
    };
};
