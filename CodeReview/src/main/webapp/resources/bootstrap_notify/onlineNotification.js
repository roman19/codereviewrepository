var offset = 0;
var countOfPortion = 10;
var isNotification = false;

function incrementCountOfNotifications(){
	var count = $('#notificationsBtn').attr('data-badge');
	
	if(!count || count==0){
		$('#notificationsBtn').addClass('badge1');
		$('#notificationsBtn').attr('data-badge', 1);
	}else{
		count = parseInt(count);
		$('#notificationsBtn').attr('data-badge', ++count);
	}
}


var audio = new Audio('/CodeReview/resources/audio/sound.mp3');

$.notifyDefaults({
	placement : {
		from : "bottom"
	},
	animate : {
		enter : "animated bounceInRight",
		exit : "animated fadeOutDown"
	}
});

function getImageUrl(jsonObj){
	if (!jsonObj.imageFromServer) {
		return jsonObj.ownerImageUrl;
	} else {
		return "/CodeReview/get_user_image/" + jsonObj.ownerImageUrl;
	}
}

var endPointURL = "ws://" + window.location.host
		+ "/CodeReview/websocket/notification";
var socket = null;

function connect(userId) {
	socket = new WebSocket(endPointURL + "?userId=" + userId);

	socket.onopen = function() {
		console.log('notify connection is open');
	};

	socket.onmessage = function(event) {

		var jsonObj = JSON.parse(event.data);
		showMessage(jsonObj);
	};

	function showMessage(jsonObj) {
		debugger;
		
		 if(jsonObj.type != 'ISSUE' || (jsonObj.level=="ERROR")){
			 incrementCountOfNotifications();
		 }
		
		if(jsonObj.isSound){
			audio.play();
		}
		
		jsonObj.url = getImageUrl(jsonObj);
		
		if (jsonObj.type == 'GROUP') {
			
			notificationForGroup(jsonObj);
			
		}else if(jsonObj.type == 'REVIEW'){
			
			notificationForReview(jsonObj);
			
		}else if(jsonObj.type == 'ISSUE'){
			
			notificationForIssue(jsonObj);
			
		}else if(jsonObj.type == 'FILE_ADDED'){
			
			notificationForNewFile(jsonObj);
			
		}else if(jsonObj.type == 'REPLY'){
			
			notificationForReply(jsonObj);
		}
	}

}

function disconnect() {
	socket.close();
	
	chatSocket.close();
}

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex
			.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g,
			" "));
}

function clearURL(){
	history.pushState('', document.title, window.location.pathname);
}

function removeHash() {
	history.pushState("", document.title, window.location.pathname
			+ window.location.search);
}

function goToCabinet(id) {
	$.ajax({
		url : 'cabinet/notifications',
		type : 'get',
		data: {'offset': offset, 'count': countOfPortion}
	}).success(function(data) {
		debugger;
		
		offset+=countOfPortion;
		
		$("#switchContent").html(data);
		$(id).transition('pulse');
		$(id).css('background-color', '#e8f5e9');
		
		location.href = id;
		clearURL();
		window.location.hash = "#notifications";
		
		setTimeout(function(){
	        $(id).animate({
	          backgroundColor: 'rgb(250, 250, 250)'
	        }, 2000 );
		},2000);
	}).error(function(data) {
		alert('Not switched goToCabinet!');
	});
}

$(function() {
	
	console.log('onlineNotification.js, on ready -> (go to Cabinet)');
	debugger;
	
	var groupId = getParameterByName('newGroup');
	var reviewId = getParameterByName('newReview');
	var reviewFilesId = getParameterByName('newFiles');

	if (groupId) {
		isNotification = true;
		goToCabinet('#newGroup' + groupId);
	} else if (reviewId) {
		isNotification = true;
		goToCabinet('#newReview' + reviewId);
	}else if (reviewFilesId) {
		isNotification = true;
		goToCabinet('#newFiles' + reviewFilesId);
	}
});


function notificationForGroup(jsonObj){
	$.notify(
			{
				icon : jsonObj.url,
				title : jsonObj.ownerName,
				message : jsonObj.itemName,
				url : 'http://localhost:8080/CodeReview/cabinet?newGroup='
						+ jsonObj.itemId
			},
			{
				type : 'minimalist',
				delay : 1000000,
				icon_type : 'image',
				template : '<div data-notify="container" class="col-xs-6 col-sm-3 z-depth-4 alert alert-{0}" role="alert">'
					+ "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"
					+ '<img data-notify="icon" class="img-circle pull-left">'
					+ '<span data-notify="title">{1}</span>'
					+ " invite you to group <a class='goToNewNotification' href='{3}'>{2}</a>"
					+ '</div>'
			});
	
}

function notificationForReview(jsonObj){
	$.notify(
			{
				icon : jsonObj.url,
				title : jsonObj.ownerName,
				message : jsonObj.itemName,
				url : 'http://localhost:8080/CodeReview/cabinet?newReview='
						+ jsonObj.itemId
			},
			{
				type : 'minimalist',
				delay : 1000000,
				icon_type : 'image',
				template : '<div data-notify="container" class="col-xs-6 col-sm-3 z-depth-4 alert alert-{0}" role="alert">'
						+ "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"
						+ '<img data-notify="icon" class="img-circle pull-left">'
						+ '<span data-notify="title">{1}</span>'
						+ " invite you to review <a class='goToNewNotification' href='{3}'>{2}</a>"
						+ '</div>'
			});
}

function notificationForIssue(jsonObj){
	var borderColor = null;
	var reviewUrl = "http://localhost:8080/CodeReview/review/"+jsonObj.reviewId;
	
	debugger;
	
	if(jsonObj.level=="ERROR"){
		borderColor = "#ff5252";
	}else if(jsonObj.level=="WARNING"){
		borderColor = "#ff9800";
	}else if(jsonObj.level=="INFO"){
		borderColor = "#03a9f4";
	}
		
	$.notify(
			{
				icon : jsonObj.url,
				title : jsonObj.ownerName,
				message : jsonObj.title,
				url: "http://localhost:8080/CodeReview/review/"
					+ jsonObj.reviewId + "?file=" + jsonObj.fileId
					+ "&issue=" + jsonObj.issueId
			},
			{
				type : 'minimalist',
				delay : 1000000,
				icon_type : 'image',
				template : '<div data-notify="container" class="col-xs-6 col-sm-3 z-depth-4 alert alert-{0}" style="background-color: #fff; border: 2px solid '+borderColor+'" role="alert">'
					+ "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"
					+ '<img data-notify="icon" class="img-circle pull-left">'
					+ '<span data-notify="title">{1}</span>'
					+ " left new issue <a class='goToNewNotification' href='{3}'>{2}</a>"
					+ "<div> in review <a class='goToNewNotification' href='"+reviewUrl+"'>"+jsonObj.reviewName+"</a></div>"
					+ '</div>'
			});
	
}

function notificationForNewFile(jsonObj){
	var reviewUrl = "http://localhost:8080/CodeReview/review/"+jsonObj.reviewId;
	var file = jsonObj.countFiles==1 ? "file" : "files";
	
	$.notify(
			{
				icon : jsonObj.url,
				title : jsonObj.ownerName,
				message : jsonObj.countFiles,
				url: "http://localhost:8080/CodeReview/cabinet?newFiles="+jsonObj.reviewId
			},
			{
				type : 'minimalist',
				delay : 1000000,
				icon_type : 'image',
				template : '<div data-notify="container" class="col-xs-6 col-sm-3 z-depth-4 alert alert-{0}" role="alert">'
					+ "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"
					+ '<img data-notify="icon" class="img-circle pull-left">'
					+ '<span data-notify="title">{1}</span>'
					+ " added <a class='ui teal tag label goToNewNotification' href='{3}'>{2} "+file+"</a>"
					+ "<div> to the review <a class='goToNewNotification' href='"+reviewUrl+"'>"+jsonObj.reviewName+"</a></div></div>"
			});
	
}

function notificationForReply(jsonObj){
	var reviewUrl = "http://localhost:8080/CodeReview/review/"+jsonObj.reviewId;
	
	$.notify(
			{
				icon : jsonObj.url,
				title : jsonObj.ownerName,
				message : jsonObj.message,
				url: "http://localhost:8080/CodeReview/review/"+jsonObj.reviewId+"?file="+jsonObj.fileId+"&issue="+jsonObj.issueId+"&reply="+jsonObj.replyId
			},
			{
				type : 'minimalist',
				delay : 1000000,
				icon_type : 'image',
				template : '<div data-notify="container" class="col-xs-6 col-sm-3 z-depth-4 alert alert-{0}" role="alert">'
					+ "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"
					+ '<img data-notify="icon" class="img-circle pull-left">'
					+ '<span data-notify="title">{1}</span>'
					+ " replied you in review <a class='ui green basic label' href='" + reviewUrl + "'>" + jsonObj.reviewName + "</a>"
					+ "<div class='overflow-text'><a href='{3}'>{2}</a></div>"
					+ '</div>'
			});
}