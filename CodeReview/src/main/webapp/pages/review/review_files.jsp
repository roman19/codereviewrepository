<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>
<div class="col-xs-12 simple-splitter">
    <div id='tree_panel' class='panel panel-default'>
        <div class='panel-body'>
            <input type="hidden" id="file_id" value=""/>
            <c:out value="${reviewTree}" escapeXml="false"/>
        </div>
    </div>
    <div id='code_panel' class='panel panel-default'>
        <div class='panel-body'>
            <div class="code-snippet">
                <h1 id="file_message" style="color:#E3E3E3"><u:message locale='${locale}' key='review.files.select'/></h1>
            </div>
        </div>
    </div>
</div>