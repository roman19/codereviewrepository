<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>
<html>
<head>
<link rel="icon" type="image/png" href="${pageContext.servletContext.contextPath}/resources/images/codereview_icon.png">
    <title>review | ${codeReviewName}</title>

 <style>
         .red { color:#DE5145; }
         .green{ color: #17A05E;}
          a.white{
		         background-color:#FFFFFF; 
          }
         
         a.likeup{
         	background-color: #17A05E;
         }
         a.likedown{
         	background-color: #DE5145;        
         }
         .likeup.fa{
         color:white;
         }
         .likedown.fa{
         color:white;
         }
 	 </style>

    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/syntaxhighlight/css/shCore.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/syntaxhighlight/css/shThemeDefault.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/reviewCreating/tree.css"/>" type="text/css"
          media="screen">
    <link href="<c:url value="/resources/ckeditor/plugins/codesnippet/lib/highlight/styles/default.css"/>"
          rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery-2.2.0.min.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/resources/js/jquery-ui.min.js"/>" type="text/javascript"></script>

    <script src="<c:url value="/resources/syntaxhighlight/js/shCore.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/resources/js/cabinet/reviewCreating/chosen.jquery.js"/>"
            type="text/javascript"></script>
    <%--diagrams--%>
    <link href="<c:url value="/resources/c3_charts/c3-b03125fa.css"/>" rel="stylesheet" type="text/css">
    <script src="<c:url value="/resources/c3_charts/d3-3.5.0.min-3b564392.js"/>" charset="utf-8"></script>
    <script src="<c:url value="/resources/c3_charts/c3.min-12912fb6.js"/>" charset="utf-8"></script>

    <%--Multi selector--%>
    <script src="<c:url value="/resources/lou-multi-selector/js/jquery.multi-select.js"/>" charset="utf-8"></script>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/lou-multi-selector/css/multi-select.css"/>"/>

	<!-- URL Manager -->
	<script src="<c:url value="/resources/js/review/urlManager/jquery.query-object.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/js/review/urlManager/links.js"/>" type="text/javascript"></script>
    <!-- for chat -->
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/semantic_ui/css/semantic.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/review/chat/chat.css"/>"/>
    <script src="<c:url value="/resources/js/review/chat/chat.js"/>" type="text/javascript"></script>
    <!--end for chat-->
    
    <!-- for online notifications -->
	<script src="<c:url value="/resources/bootstrap_notify/bootstrap-notify.js"/>"></script>
	<script src="<c:url value="/resources/bootstrap_notify/onlineNotification.js"/>"></script>
	<link rel="stylesheet" href="<c:url value="/resources/bootstrap_notify/animate.css"/>">
	<link rel="stylesheet" href="<c:url value="/resources/bootstrap_notify/onlineNotification.css"/>">
	<!-- end for online notifications -->

	<!-- for comment autocomplete -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/review/replies.css"/>"/>
	<script src="<c:url value="/resources/autocomplete/bootstrap-typeahead.js"/>"></script>
	<script src="//underscorejs.org/underscore-min.js"></script>	
	<script src="<c:url value="/resources/autocomplete/rangy-core.js"/>"></script>
	<script src="<c:url value="/resources/autocomplete/caret-position.js"/>"></script>
	<script src="<c:url value="/resources/autocomplete/bootstrap-tagautocomplete.js"/>"></script>
	<!-- end for comment autocomplete -->

    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/main.css">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/review/review.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/review/dashboard.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/review/issues.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/reviewCreating/reviewCreating.css"/>" type="text/css"
          media="screen">
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/reviewCreating/chosen.css">
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet"
          type="text/css" href="${pageContext.servletContext.contextPath}/resources/sweetalert/css/sweetalert.css">
    <link rel="stylesheet"
          href="${pageContext.servletContext.contextPath}/resources/css/review/attachment/attachment.css">

    <script src="${pageContext.servletContext.contextPath}/resources/sweetalert/js/sweetalert.min.js"></script>
	
	<script type="text/javascript" src="<c:url value="/resources/js/main.js"/>"></script>
</head>
<body onload="connect(${userID});" onunload="disconnect();">

<jsp:include page="../header.jsp"/>
<input id="mapping" type="hidden" value="${pageContext.servletContext.contextPath}/"/>
<input id="locale_id" type="hidden" value="${locale}"/>

<!-- for file attachment -->
<jsp:include page="attachment/fileAttachment.jsp"/>
<!-- end for file attachment -->

<!-- for chat -->
<input id="countNewMessages" type="hidden" value="${countNewMessages}">
<input id="reviewIdForChat" type="hidden" value="${reviewId}">
<input id="userId" type="hidden" value="${userID}">

<c:if test="${isActiveReview}">

<a id="menu-toggle" type="button" class="btn btn-primary btn-circle btn-xl toggle badge1"><img
        src="${pageContext.servletContext.contextPath}/resources/images/chat_icon.png" alt=""> </a>

<nav id="sidebar-wrapper">
    <div class="container-fluid">
        <ul class="sidebar-nav">
            <li>
                <ul id="forMessage" class="ui comments all-comments">


                </ul>
            </li>
            <li>
                <div class="ui form send-form">
                    <div class="field">
                        <textarea id="btn-input" rows="2"></textarea>
                    </div>

                    <div id="btn-chat" class="ui primary submit button pull-right">
                        <i class="glyphicon glyphicon-send"></i> Send
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>

</c:if>

<!-- end for chat -->

<%--finish review dialog--%>
<div class="modal fade" id="confirm-finish" tabindex="-1" role="dialog" aria-labelledby="review_confirm_finish_model" aria-hidden="true" style="width: 50%; margin-left: 25%;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="review_confirm_finish_model"><u:message locale="${locale}" key="review.dialog.finish.title"/></h4>
            </div>
            <div class="modal-body">
                <p><u:message locale="${locale}" key="review.dialog.finish.msg1"/></p>
                <p><u:message locale="${locale}" key="review.dialog.finish.msg2"/> <b>"<c:out value="${codeReviewName}"/>"</b> <u:message locale="${locale}" key="review.dialog.finish.msg3"/></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><u:message locale="${locale}" key="grop.content.cancel"/></button>
                <a href="${pageContext.servletContext.contextPath}/cabinet"
                    onclick="finishSomeReview()" class="btn btn-danger"><u:message locale="${locale}" key="notifications.confirm"/></a>
            </div>
        </div>
    </div>
</div>
<%--end finish review dialog--%>
<%--delete review dialog--%>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
    aria-labelledby="review_confirm_delete_review" aria-hidden="true"
    style="width: 50%; margin-left: 25%;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                    aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="review_confirm_delete_review"><u:message locale="${locale}" key="review.dialog.delete.title"/></h4>
            </div>
            <div class="modal-body">
                <p><u:message locale="${locale}" key="review.dialog.delete.msg1"/></p>
                <p><u:message locale="${locale}" key="review.dialog.delete.msg2"/> <b>"<c:out value="${codeReviewName}" />"
                    </b> <u:message locale="${locale}" key="review.dialog.finish.msg3"/>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><u:message locale="${locale}" key="grop.content.cancel"/></button>
                <a href="${pageContext.servletContext.contextPath}/cabinet"
                   onclick="deleteReview()" class="btn btn-danger"><u:message locale="${locale}" key="notifications.confirm"/></a>
            </div>
        </div>
    </div>
</div>
<%--end delete review dialog--%>
<%--add users for review dialog--%>
<div class="modal fade" id="add_users_modal" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="users_model"><u:message locale="${locale}" key="review.dashboard.modal.users.select"/> "<c:out
                        value="${codeReviewName}"/>"</h3>
            </div>
            <div class="modal-body">
                <div id="review_users_selector">

                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" data-dismiss="modal" id="add_users_btn"
                   onclick="addUsers('${reviewId}', $('#user-select').val());"><u:message locale="${locale}" key="review.dashboard.modal.users.add"/></a>
            </div>
        </div>
    </div>
</div>
<%--end add users for review dialog--%>

<div class="container-fluid" style="width: 100%;">

    <div class="col-xs-10" style="padding-right: 0; padding-left: 0;">
        <div id="review-header">
            <span class="h2" style="padding-right: 10px;">
                <c:choose>
                    <c:when test="${isActiveReview}">
                        <i style="color: #21BA45" class="fa fa-check-circle-o"></i>
                    </c:when>
                    <c:otherwise>
                        <i style="color: #F2711C;" class="fa fa-history"></i>
                    </c:otherwise>
                </c:choose>
                    <c:out value="${codeReviewName}"/>
            </span>
            <span class="h4"><c:out value="${projectName}"/>
                <c:if test="${not empty branchName}">
                        [ <i class="fa fa-code-fork"></i> <c:out value="${branchName}"/> ]
                </c:if>
            </span>
        </div>
    </div>
    <div class="col-xs-2" style="padding-right: 0; padding-left: 0;">
        <c:choose>
            <c:when test="${isActiveReview}">
                <c:choose>
                    <c:when test="${isOwnerReview}">
                        <div class="dropdown"
                            style="margin-top: 20px; margin-left: 80px;">
                            <button class="btn btn-primary dropdown-toggle" type="button"
                                data-toggle="dropdown">
                                <u:message locale="${locale}" key="review.settings.title"/><span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <c:if test="${isProjectFromGitRepository != null}">
                                    <li><a href="#" onclick="git_attach()"
                                        data-toggle="modal" data-target="#fileAttachment"><u:message locale="${locale}" key="review.settings.git"/></a></li>
                                </c:if>
                                <li><a href="#" onclick="pc_attach()" data-toggle="modal"
                                    data-target="#fileAttachment"><u:message locale="${locale}" key="review.settings.pc"/></a></li>
                                <li><a data-toggle="modal" data-target="#confirm-finish"><u:message locale="${locale}" key="review.settings.finish"/></a></li>
                                <li><a data-toggle="modal" data-target="#confirm-delete"><u:message locale="${locale}" key="review.settings.delete"/></a></li>
                            </ul>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <button class="btn btn-primary" style="margin-top: 20px; margin-left: 80px;"
                         type="button" onclick="setStatusLeft(${reviewId}, '${codeReviewName}', false)">
                         	<u:message locale="${locale}" key="review.settings.leave"/>
                        </button>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <div style="float: right; margin-top: 20px;" class="ui orange label"><u:message
                        locale="${locale}" key="review.history"/></div>
            </c:otherwise>
        </c:choose>
    </div>
     
    <%--Tabs--%>
    <div class="col-xs-12" id="tabs_menu">
            <div class="col-xs-3 tabs-menu">
                <ul class="nav nav-tabs" role="tablist">
                    <li id="dashboard" class="active"><a data-toggle="tab" role="tab" href="#dashboard"
                                                         onclick="loadDashboard('${reviewId}');"
                                                         aria-expanded="true">
                        <u:message locale="${locale}" key="review.tabs.dashboard"/></a>
                    </li>
                    <li id="project"><a data-toggle="tab" href="#project" onclick="loadProject('${reviewId}');" role="tab"
                                        aria-expanded="true">
                        <u:message locale="${locale}" key="review.tabs.files"/></a></li>
                    <li id="issues"><a data-toggle="tab" href="#issues" onclick="loadIssues('${reviewId}')" role="tab"
                                       aria-expanded="true">
                        <u:message locale="${locale}" key="review.tabs.issues"/></a></li>
                </ul>
            </div>
            <div class="col-xs-9 file-selected" id="show_selected_file"></div>
    </div>
    <%--End tabs--%>

    <div id="tab_content"> <%-- Div for content load from tabs --%>
        <%-- <jsp:include page="dashboard/dashboard.jsp"/> --%>
    </div>

</div>

<jsp:include page="../footer.jsp"/>

<script src="<c:url value="/resources/js/cabinet/reviewDeleting/reviewDeleting.js"/>"></script>
<script src="<c:url value="/resources/js/review/attachment/fileAttachment.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/js/review/attachment/dragManager.js"/>" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/cabinet/reviewCreating/tree.js"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/cabinet/reviewCreating/preloader.js"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/review/review.js"></script>

<script>
	function deleteReview(){
		$.get('${pageContext.servletContext.contextPath}/review/delete', {
			id : ${reviewId}
		}, function(){
	 	    document.location.replace("${pageContext.servletContext.contextPath}/cabinet");
		});
	}

	function finishSomeReview(){
		$.get('${pageContext.servletContext.contextPath}/review/finish', {
			id : ${reviewId}
		}, function(){
	 	    document.location.replace("${pageContext.servletContext.contextPath}/cabinet");
		});
	}
</script>

<script type="text/javascript">
    function loadProject(reviewId) {
        $('#show_selected_file').text('');
        var dashboardTab = $("#project");
        if (!dashboardTab.hasClass("active")) {
            $.post("${pageContext.servletContext.contextPath}/review/project", {
                reviewId: reviewId
            }).done(function (data) {
                if (data == null) {
                    $('#tab_content').html("<h1 style='color:#E3E3E3'><u:message locale='${locale}' key='review.files.load_failed'/></h1>");
                } else {
                    $('#tab_content').html(data);
                    $(".simple-splitter").splitter();
                    $(".simple-splitter").trigger("resize", [ 350 ]);
                    $('.treeview').each(function () {
                        var tree = $(this);
                        tree.treeview();
                    });
                  	//test
                	//if during loading Project in url is ?file=... then load this file
                    URLManager.loadFileForReview();
                }
            }).fail(function () {
                alert("<u:message locale='${locale}' key='review.files.load_failed'/>");
            });
        }
    }

    function loadDashboard(reviewId) {
    	URLManager.rememberFileId();
    	URLManager.clearURL();
    	
        $('#show_selected_file').text('');
        $.post("${pageContext.servletContext.contextPath}/review/dashboard", {
            reviewId: reviewId
        }).done(function (data) {
            if (data == null) {
                $('#tab_content').html("<h1 style='color:#E3E3E3'><u:message locale="${locale}" key="review.dashboard.alert.dashboard_failed"/></h1>");
            } else {
                $('#tab_content').html(data);
            }
        }).fail(function () {
            alert("<u:message locale="${locale}" key="review.dashboard.alert.dashboard_failed"/>");
        });
    }

    function loadIssues(reviewId) {
    	URLManager.rememberFileId();
    	URLManager.clearURL();
    	
        $('#show_selected_file').text('');
        $.post("${pageContext.servletContext.contextPath}/review/issues", {
            reviewId: reviewId
        }).done(function (data) {
            if (data == null) {
                $('#tab_content').html("<h1 style='color:#E3E3E3'><u:message locale="${locale}" key="review.issues.load_failed"/></h1>");
            } else {
                $('#tab_content').html(data);
            }
        }).fail(function () {
            alert("<u:message locale="${locale}" key="review.issues.load_failed"/>");
        });
    }

    function selectFileForReview(fileFullPath, fileRelativePath, fileOfTreeId) {
        $.post("${pageContext.servletContext.contextPath}/review/file", {
            fullPath: fileFullPath,
            relativePath: fileRelativePath,
            fileOfTreeId : fileOfTreeId
        }).done(function (data) {
        	debugger;
        	
        	URLManager.goToFile(data.id);
        	
            console.log(data);
            
            if (data == null || data.content == null) {
                $('#show_selected_file').text('');
                $('div.code-snippet').html("<h1 id='file_message' style='color:#E3E3E3'><u:message locale="${locale}" key="review.files.load_file_failed"/>: <br>" + fileRelativePath + "</h1>");
            } else {
                var codePanelSelector = $('div.code-snippet');
                codePanelSelector.hide();
                $('#file_id').val(data.id);
                $('#show_selected_file').html("<span style='padding-left: 10px;'><i style='font-size: 18px;' class='fa fa-file-code-o'></i>&nbsp;&nbsp;" + getRelativePathForShow(fileRelativePath) + "</span>");
                codePanelSelector.html('<pre class="brush:' + data.brush + '">' + data.content + '</pre>');
                SyntaxHighlighter.highlight();
                onSelectLines();
                var issues = JSON.parse(data.issues);
                if (issues != null && issues.length > 0) {
                    addIssues(issues);
                }
                codePanelSelector.scrollTop(0);
                codePanelSelector.show(0, function () {
                    $('.issue.show_issue').trigger('show');
                });
                onScrollReviewedFileEvent($('#code_panel'), data.id);
                
                //href on issue if exists in url
                URLManager.goToIssueFromUrl();
            }
        }).fail(function () {
            $('#show_selected_file').text('');
            alert("<u:message locale="${locale}" key="review.files.load_file_failed"/>.");
        });
    }

    function sendReviewedFileRequest(fileId) {
        $.post("${pageContext.servletContext.contextPath}/review/file/reviewed", {
            fileId: fileId
        }).done(function () {
            $('#viewed_file_icon_' + fileId).html("<i class='fa fa-eye'></i>");
        });
    }

    function onPostIssue(issueType, issueId) {
        disablePostIssueButtons(true);
        var fileIdSelector = $('#file_id');
        var linesSelector = $('#issue_lines');
        var titleSelector = $('#issue_title');
        var lines = JSON.parse(linesSelector.val()).lines;
        console.log(lines);
        $.post("${pageContext.servletContext.contextPath}/review/file/issue", {
            fileId: fileIdSelector.val(),
            type: issueType.toUpperCase(),
            title: titleSelector.val(),
            body: CKEDITOR.instances.issue_body_editor.getData(),
            lineFrom: lines[0],
            lineTo: lines[lines.length - 1],
            issueId: issueId
        }).done(function (data) {
            console.log(JSON.stringify(data));
            cancelIssueTyping();
            if (issueId) {
                $("#show_issue_" + issueId).remove();
                $("#show_issue_stub_" + issueId).remove();
            }
            var issue = JSON.parse(data.issue);
            socket.send(JSON.stringify(issue));
            showIssueTemplate(issue.id, issue.type, issue.userLogin, issue.title, issue.body, issue.time, true, issue.lines[issue.lines.length - 1], issue.lines);
            updateFileIssueLabel(issue.fileId, data.issuesCount);
        }).fail(function () {
            disablePostIssueButtons(false);
            alert("<u:message locale="${locale}" key="review.issues.post_issue_failed"/>");
        });
    }

    function onEditIssue(issueId) {
        $.post("${pageContext.servletContext.contextPath}/review/file/issue/edit", {
            issueId: issueId
        }).done(function (data) {
            console.log(JSON.stringify(data));
            cancelIssueTyping();
            $("#show_issue_" + issueId).hide();
            $("#show_issue_stub_" + issueId).hide();
            $(".syntaxhighlighter .line").removeClass('highlighted');
            createIssueTemplate(JSON.parse(data.lines), data.title, data.body, issueId);
        }).fail(function () {
            alert("<u:message locale="${locale}" key="review.issues.post_issue_failed"/>");
        });
    }

    function onRemoveIssue(issueId) {
        $.post("${pageContext.servletContext.contextPath}/review/file/issue/delete", {
            issueId: issueId
        }).done(function (data) {
            console.log(JSON.stringify(data));
            if (data.isRemoved == true) {
                updateFileIssueLabel(data.fileId, data.issuesCount);
                $("#show_issue_" + issueId).remove();
                $("#show_issue_stub_" + issueId).remove();
                $(".syntaxhighlighter .line").removeClass('highlighted');
            } else {
                alert("<u:message locale="${locale}" key="review.issues.remove_issue_failed"/>");
            }
        }).fail(function () {
            alert("<u:message locale="${locale}" key="review.issues.remove_issue_failed"/>");
        });
    }

    function onScrollReviewedFileEvent(scrollPanelSelector, fileId) {
        scrollPanelSelector.unbind('scroll');
        if (${isReviewer} && ${isActiveReview}) {
            if (!scrollPanelSelector.hasScrollBar() || (scrollPanelSelector.scrollTop() + scrollPanelSelector.innerHeight() >= scrollPanelSelector[0].scrollHeight * 0.75)) {
                sendReviewedFileRequest(fileId);
            } else {
                var scrolledFirst = false;
                scrollPanelSelector.bind('scroll', function () {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight * 0.75) {
                        if (!scrolledFirst) {
                            sendReviewedFileRequest(fileId);
                            scrolledFirst = true;
                        }
                    }
                });
            }
        }
    }

    function createIssueTemplate(lines, title, body, issueId) {
        var JSONLines = {lines: lines};
        var issuePanelHeader = issueId ? "<u:message locale='${locale}' key='review.files.issue.edit'/>" : "<u:message locale='${locale}' key='review.files.issue.create'/>";
        $("<div id='create_issue' class='unselectable issue'>" +
                "<div class='panel panel-default'>" +
                "<div class='panel-heading'><h3 class='panel-title'>" + issuePanelHeader + "</h3></div>" +
                "<div class='panel-body'>" +
                "<form id='create_issue_form'>" +
                "<div id='title_group' class='input-group input-group-md'>" +
                "<span class='input-group-addon' id='sizing-addon1'><u:message locale='${locale}' key='review.files.issue.title'/></span>" +
                "<input type='text' class='form-control' placeholder='<u:message locale='${locale}' key='review.files.issue.title.placeholder'/>' id='issue_title' aria-describedby='sizing-addon1'>" +
                "</div>" +
                "<input type=hidden id='issue_lines' value='" + JSON.stringify(JSONLines) + "'/>" +
                "<div id='issue_body' class='collapse'>" +
                "<textarea name='issue_body_editor' id='issue_body_editor'></textarea>" +
                "</div>" +
                "<a id='issue_body_collapse' class='underlined' data-toggle='collapse' data-target='#issue_body'><u:message locale='${locale}' key='review.files.issue.body.expand'/></a>" +
                "<div class='row'>" +
                "<div class='col-xs-6'>" +
                "<div class='btn-group btn-group-md' role='group' >" +
                "<input id='error' class='btn btn-danger' type=submit value='<u:message locale='${locale}' key='review.btn.error'/>'/>" +
                "<input id='warning' class='btn btn-warning' type=submit value='<u:message locale='${locale}' key='review.btn.warning'/>'/>" +
                "<input id='info' class='btn btn-info' type=submit value='<u:message locale='${locale}' key='review.btn.info'/>'/> " +
                "</div>" +
                "</div><div class='col-xs-4'></div>" +
                "<div class='col-xs-2'>" +
                "<input id='issue_cancel_btn' class='btn btn-default' type=button onclick='cancelIssueTyping(" + issueId + ")' value='<u:message locale='${locale}' key='review.btn.cancel'/>'/>" +
                "</div>" +
                "</div>" +
                "</form>" +
                "</div>" +
                "</div></div>").insertAfter(".code .line.number" + lines[lines.length - 1]);
        $("<div id='create_issue_stub' class='unselectable stub-issue'>&nbsp;</div>").insertAfter(".gutter .line.number" + lines[lines.length - 1]);
        CKEDITOR.replace('issue_body_editor');

        var createIssueSelector = $('#create_issue');
        var titleSelector = $('#issue_title');

        createIssueSelector.on('show.bs.collapse', function () {
            $('#issue_body_collapse').html('<u:message locale='${locale}' key='review.files.issue.body.hide'/>');
        });
        createIssueSelector.on('hide.bs.collapse', function () {
            $('#issue_body_collapse').html('<u:message locale='${locale}' key='review.files.issue.body.expand'/>');
        });

        changeHeight(createIssueSelector, $('#create_issue_stub'), $('#issue_body'));
        createIssueHighlightLines(lines);
        addIssueLinesHover(createIssueSelector, lines);

        $("form input[type=submit]").click(function () {
            $("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
            $(this).attr("clicked", "true");
        });


        titleSelector.focus();

        $("#create_issue_form").submit(function (e) {
            e.preventDefault();
            var titleValue = titleSelector.val();
            if (isEmpty(titleValue) || isBlank(titleValue) || titleValue.match(/^(\s*(&nbsp;)+\s*)+$/)) {
                $('#title_group').addClass('has-error');
                titleSelector.tooltip({title: "<u:message locale='${locale}' key='review.files.issue.title.tooltip'/>", placement: "bottom", trigger: "manual"});
                titleSelector.tooltip('show');
                titleSelector.focus();
                titleSelector.bind('keydown paste click', function () {
                    titleSelector.tooltip('hide');
                    $('#title_group').removeClass('has-error');
                });
            } else {
                var submitValue = $("input[type=submit][clicked=true]").val();
                onPostIssue(submitValue, issueId);
            }
        });
        $(".syntaxhighlighter .code .container").unbind("mouseup");

        if (title && !isEmpty(title) && !isBlank(title)) {
            titleSelector.val(title);
        }
        if (body && !isEmpty(body) && !isBlank(body)) {
            $('#issue_body_editor').val(body);
            $('#issue_body').collapse('show');
        }
    }

    function addCreateIssuePanel() {
        var lines = getSelectedLines();
        if (lines.length > 0) {
////            removeHighlightLines(lines);
//            $('#add_issue_btn').remove();
//            var lineSelector = $('.gutter .line.number' + lines[lines.length - 1]);
//            $("<button style='padding: 0 6px; position: absolute; left:-" + (lineSelector.width()+10) + "px;' id='add_issue_btn' class='btn'>+</button>").insertBefore(lineSelector);
//            addHighlightLines(lines);
            createIssueTemplate(lines);
        }
    }

    function showIssueTemplate(id, type, author, title, body, date, isEditable, lineNumber, linesHighlight, countOfReplies) {
        var readMoreHtml = '';
        var bottomPanelHtml = '';
        if (${isActiveReview}) {
            readMoreHtml = !isEmpty(body) && !isBlank(body) ?
            "<div class='row'>" +
            "<div class='col-xs-6'>" +
            "<p><a class='read_more underlined' data-toggle='collapse' id='body_issue_collapse_" + id + "' data-target='#body_issue_" + id + "'><u:message locale='${locale}' key='review.files.issue.read_more'/></a></p>" +
            "</div><div class='col-xs-6'></div></div>" : "";

            var editDeleteHtml = isEditable ?
            "<a class='underlined btn-icon btn " + getBtnClassIssueForType(type) + "' onclick='onEditIssue(" + id + ")'><i class='fa fa-pencil'></i></a> " +
            "<a class='underlined btn-icon btn " + getBtnClassIssueForType(type) + "' onclick='onRemoveIssue(" + id + ")'><i class='fa fa-trash-o'></i></a>" : "";

            bottomPanelHtml =
                    "<div>" + readMoreHtml +
                    "<div class='row'>" +
                    "<div class='col-xs-6' style='padding-bottom: 5px;'>" +
                    "<p>" + "<a data-toggle='collapse' href='#comments" + id + "' class='underlined btn-icon btn " + getBtnClassIssueForType(type) + "'><i class='fa fa-reply'></i></a> " + editDeleteHtml + "</p>" +
                    "</div>" +
                    "<div class='col-xs-6' style='padding-top: 5px;'>" +
                    "<span class='ui label' style='margin-left: 5px; margin-right: 0; padding: 0 0 0 5px; line-height: 18px;'>" + moment(new Date(date)).format('YYYY-MM-DD HH:mm') + "</span>" +
                    "<span class='ui label' style='margin-left: 5px; margin-right: 0; padding: 0 0 0 5px; line-height: 18px;'>" + author + "</span>" +
                    "</div>" +
                    "</div>" +
                    " </div>";

        } else {
            readMoreHtml = !isEmpty(body) && !isBlank(body) ?
            "<span><a class='read_more underlined' data-toggle='collapse' id='body_issue_collapse_" + id + "' data-target='#body_issue_" + id + "'><u:message locale='${locale}' key='review.files.issue.read_more'/></a></span>"
             : "";
            
            var comments = null;
            if(countOfReplies === undefined || countOfReplies === null){
            	alert('countOfReplies is undefined or null');
            }else{
            	comments = countOfReplies>0 ? "<span style='padding-right:10px'><a class='read_more underlined' data-toggle='collapse' data-target='#comments" + id + "'>Comments (" + countOfReplies + ") <i class='fa fa-comments-o'></i></a></span>" 
            			: "";
            }

             bottomPanelHtml =
                    "<div class='row'>" +
                    "<div class='col-xs-6'>" +  comments + readMoreHtml + "</div>" +
                    "<div class='col-xs-6'>" +
                    "<span class='ui label' style='margin-left: 5px; margin-right: 0; padding: 0 0 0 5px; line-height: 18px;'>" + moment(new Date(date)).format('DD.MM.YYYY HH:mm') + "</span>" +
                    "<span class='ui label' style='margin-left: 5px; margin-right: 0; padding: 0 0 0 5px; line-height: 18px;'>" + author + "</span>" +
                    "</div>" +
                    "</div>"
        }

        var issueHtml =
        "<div id='show_issue_" + id + "' class='unselectable issue show_issue'>" +
        "<div class='panel " + getPanelClassForIssueType(type) + "'>" +
        "<div class='panel-heading'>" +((${isActiveReview})?   "<a  id='likedown"+id+"'  class='underlined btn-icon btn likedown'    onclick='onLikeDownIssue(" + id + ")' style='float:right;  margin-right:15px; '><i id='like-down" + id + "' class='fa fa-thumbs-down fa my-like my-like-down'><span id='total-dislikes" + id + "'      style='font-size: 10px;'>0</span></i></a>"+
                "<a  id='likeup"+id+"' class='underlined btn-icon btn likeup'    onclick='onLikeUpIssue(" + id + ")'  style='float:right;  margin-right:15px; '><i  id='like-up" + id + "'    class='fa fa-thumbs-up fa my-like my-like-up'><span id='total-likes" + id + "'       style='font-size: 10px;'>0</span></i></a>"
      :  "<a  id='likedown"+id+"'  class='underlined btn-icon btn likedown'  disabled=’disabled’  style='float:right; cursor: default;pointer-events: none; margin-right:15px; '><i id='like-down" + id + "' class='fa fa-thumbs-down fa my-like my-like-down'><span id='total-dislikes" + id + "'      style='font-size: 10px;'>0</span></i></a>"+
      "<a  id='likeup"+id+"' class='underlined btn-icon btn likeup'  disabled=’disabled’   style='float:right; cursor: default;pointer-events: none; margin-right:15px; '><i  id='like-up" + id + "'    class='fa fa-thumbs-up fa my-like my-like-up'><span id='total-likes" + id + "'       style='font-size: 10px;'>0</span></i></a>")+
      
      
        "<p class='issue_title'>" + getGlyphIconForIssueType(type) + " " + title + "</p>" +
        "</div>" +
        "<div class='panel-body'>" +
        "<div id ='body_issue_" + id + "' class='collapse' style='padding: 5px 0;'>" + body + "</div>" +
        bottomPanelHtml +
        "</div>" +
        "</div>" +
        insertCommentTemplate(id, type, ${isActiveReview}) +
        "</div><div/>";
        
        var issueStubHtml = "<div id='show_issue_stub_" + id + "' class='unselectable stub-issue'>&nbsp;</div>";
        $(issueHtml).insertAfter(".code .line.number" + lineNumber);
        $(issueStubHtml).insertAfter(".gutter .line.number" + lineNumber);
        var showIssueSelector = $('#show_issue_' + id);
        var showIssueBodySelector = $('#body_issue_' + id);
        var showIssueStubSelector = $('#show_issue_stub_' + id);
        showIssueBodySelector.on('show.bs.collapse', function () {
            $('#body_issue_collapse_' + id).text('<u:message locale='${locale}' key='review.files.issue.hide'/>');
        });
        showIssueBodySelector.on('hide.bs.collapse', function () {
            $('#body_issue_collapse_' + id).text('<u:message locale='${locale}' key='review.files.issue.read_more'/>');
        });
        showIssueSelector.find('pre code').each(function (i, block) {
            hljs.highlightBlock(block);
        });
        showIssueChangeStubHeight(showIssueSelector, showIssueBodySelector, showIssueStubSelector);
        addIssueLinesHover(showIssueSelector, linesHighlight);
        
      //for comments
      	$('#comments'+id).on('shown.bs.collapse', function(){
      		$('#commentArea'+id).html('@'+author+', ');
      		placeCaretAtEnd(document.getElementById('commentArea'+id) );
    	});
      
      	setUsersForReplyByIssueId(id, '#commentArea'+id);
        
        $('#commentArea'+id).attr('contenteditable', true);
        
        $('[data-toggle="popover"]').popover({container: 'body'});
        

 		//for likes  on issue
        
        $.get('${pageContext.servletContext.contextPath}/loadLikes', {IssueId:id}, function(response) { 
 	      
        	
        	$('#total-dislikes'+id).text(response.totalDisLikes); 
        	
        	
        	
        	$('#total-likes'+id).text(response.totalLikes);
 	        var likeValue=response.likeValue;
 	        
 	        if(likeValue == 1){
 	        	$("#like-up"+id).toggleClass("green");	
 	      		 document.getElementById("likeup"+id).className = "underlined btn-icon btn white";
 	      		 
 	        }
 	        if(likeValue == -1){
 	        	$("#like-down"+id).toggleClass("red");	
 	        	 document.getElementById("likedown"+id).className = "underlined btn-icon btn white";
 	        	 
 	        }
 	        
 	        
	        					    	        
	    });			
        
        
        //for likes on reply
       
        $.get('${pageContext.servletContext.contextPath}/loadReplyIds', {IssueId:id}, function(response) { 
        	
        	
        	if(response != undefined){
        	   
        		$.each(response, function (index, value) {
        			  
        			
        		
        	     var rId=value;
        	    
        		 $.get('${pageContext.servletContext.contextPath}/loadLikesReply', {ReplyId:rId}, function(response) { 
        	 	
        	        	
        	 	      	$('#total-reply-dislikes'+rId).text(response.totalDisLikes); 
        	        	$('#total-reply-likes'+rId).text(response.totalLikes);
        	        	
        	 	        var likeValue=response.likeValue;
        	 	        
        	 	        if(likeValue == 1){
        	 	        
        	 	        	 $("#like-up-reply"+rId).attr("class", "fa fa-thumbs-up fa my-like my-like-up green");
        	 	      		
        	 	      		 
        	 	        }
        	 	        if(likeValue == -1){
        	 	        	
        	 	        	 $("#like-down-reply"+rId).attr("class", "fa fa-thumbs-down fa my-like my-like-down red");
        	 	        	 
        	 	        }
        	 	        
        	 	        
        		        					    	        
        		    });		
        		
        		
        		});
        }
        	
        });	
        
    }
    
    function onLikeUpIssue(issueId){
    	var id=issueId;
    	
    if($("#like-up"+id).css('color')!="rgb(23, 160, 94)"){						 
		 if($("#like-down"+id).css('color')=="rgb(222, 81, 69)") {
		    	 $('#like-down'+id).toggleClass("red");
		    	 document.getElementById("likedown"+id).className = "underlined btn-icon btn likedown";
		    	 $("#like-up"+id).toggleClass("green");	
		    	 document.getElementById("likeup"+id).className = "underlined btn-icon btn white";
		    	 
		    	 //decrement like-down and increment like-up
		    	 $.get('${pageContext.servletContext.contextPath}/decrementLikeDownIssue', {IssueId:id}, function(response) { 
 		    	        $('#total-dislikes'+id).text(response);  						    	      	  
		    	        	$.get('${pageContext.servletContext.contextPath}/incrementLikeUpIssue',{IssueId:id},function(response) { 
 			    	        		$('#total-likes'+id).text(response);  
			 	    	   		 });					    	        
		    	    });					    	 						    	 
		     } else{						    						    	 
		    	 $("#like-up"+id).toggleClass("green");	    	
		    	 document.getElementById("likeup"+id).className = "underlined btn-icon btn white";
		    
		    	 
		    	//increment like-up 
		    	 $.get('${pageContext.servletContext.contextPath}/incrementLikeUpIssue',{IssueId:id},function(response) { 
 		    	        $('#total-likes'+id).text(response);         
		    	    });						    	 
		     }	 			  				 
			}
			else{			  			
				 $("#like-up"+id).toggleClass("green");
				 document.getElementById("likeup"+id).className = "underlined btn-icon btn likeup";
			  
				//decrement like-up
				 $.get('${pageContext.servletContext.contextPath}/decrementLikeUpIssue',{IssueId:id},function(response) { 
 	    	        $('#total-likes'+id).text(response);         
	    	    });
			}			  		
    	
    }
    
	function onLikeDownIssue(issueId){
		var id=issueId;
		 if($("#like-down"+id).css('color')!="rgb(222, 81, 69)"){								 
			    if($("#like-up"+id).css('color')=="rgb(23, 160, 94)") {
			    	
			    	$('#like-up'+id).toggleClass("green");
			    	 
			    	 document.getElementById("likeup"+id).className = "underlined btn-icon btn likeup";
			    	 
			    	 $('#like-down'+id).toggleClass("red");	
			    	 
			    	 document.getElementById("likedown"+id).className = "underlined btn-icon btn white";
			    	 
			    //decrement like-up and increment like-down
			    	 $.get('${pageContext.servletContext.contextPath}/decrementLikeUpIssue',{IssueId:id},function(response) { 
 			    	       		 $('#total-likes'+id).text(response);     			    	        
			    	        $.get('${pageContext.servletContext.contextPath}/incrementLikeDownIssue',{IssueId:id},function(response) { 
 				    	         $('#total-dislikes'+id).text(response);         
				    	    });			    	        
			    	    });			    	 
			     } else{			    	 
			    	 $('#like-down'+id).toggleClass("red");
			    	 document.getElementById("likedown"+id).className = "underlined btn-icon btn white";
			    	//increment like-down 
			    	 $.get('${pageContext.servletContext.contextPath}/incrementLikeDownIssue',{IssueId:id},function(response) { 
   				    	        $('#total-dislikes'+id).text(response);         
			    	    });			     		     
			     }	  				 
			}
			else{
				 $('#like-down'+id).toggleClass("red"); 			
				 document.getElementById("likedown"+id).className = "underlined btn-icon btn likedown";
				//decrement like-down
				 $.get('${pageContext.servletContext.contextPath}/decrementLikeDownIssue',{IssueId:id},function(response) { 
 		    	        $('#total-dislikes'+id).text(response);         
		    	    });
			}
    }

    
    //actions with comments
    function insertCommentTemplate(issueId, type, reviewIsActive){
    	var comments = null;
    	
    	var json_data = {
    			'issueId' : issueId,
    			'reviewIsActive' : reviewIsActive
    		};
    	
    	$.ajax({
    		async:false,
        	url : '${pageContext.servletContext.contextPath}/review/uploadReplies',
        	type : 'get',
        	dataType : 'text',
        	data : json_data
        }).success(function(data) {
        	debugger;
        	
        	
        	var commentArea = reviewIsActive ? "<form class='ui reply form'>"+
                    "<div class='field'>"+
                    "<div class='form-control commentArea' id='commentArea" + issueId + "' contenteditable='true'></div>"+
                    "</div>"+
                    "<div onclick='sendReply(" + issueId + ");' class='btn pull-right'>"+
                    "<i class='fa fa-edit'></i> <u:message locale='${locale}' key='review.btn.reply'/></div></form>" : "";
        	
        	comments = "<div id='comments" + issueId + "' class='collapse comments-block clearfix' style='background-color: " + getBackgroudColorForComments(type) + "'>"+
            "<div id='forCommentsInsert" + issueId + "' class='ui threaded comments'>"+
            data +
            commentArea +
            "</div></div>";
        }).error(function(data) {
        		alert('Error during uploading replies');
        });
    	
    	return comments;
    }
    
    function sendReply(issueId){
    	
    	var message = $('#commentArea'+issueId).html();
    	
    	var json_data = {
    			'issueId' : issueId,
    			'message' : message
    		};
    	
    	$.ajax({
        	url : '${pageContext.servletContext.contextPath}/review/saveReply',
        	type : 'get',
        	dataType : 'text',
        	data : json_data
        }).success(function(replyDto) {
        	
        	var replyObj = JSON.parse(replyDto);
        	
        	socket.send(replyDto);

        	$.get( "writeReply", { 'replyId': replyObj.replyId } )
        	  .done(function( data ) {
        		  
        		$(data).insertBefore('#commentArea' + issueId);
                $("#show_issue_stub_" + issueId).attr('style', 'height:' + $("#show_issue_" + issueId).height() + 'px !important');
              	$('#commentArea'+issueId).html('');
              	placeCaretAtEnd(document.getElementById('commentArea'+issueId));
              	$('[data-toggle="popover"]').popover({container: 'body'});
        	  });
        }).error(function(data) {
        		alert('Reply didnt save');
        });
    }
    
    function editReply(replyId, issueId){
    	$.ajax({
        	url : '${pageContext.servletContext.contextPath}/review/getReply',
        	type : 'get',
        	dataType : 'text',
        	data : {'replyId': replyId}
        }).success(function(reply) {
        	$('#replyMessage'+replyId).html("<div class='form-control commentArea' id='commentAreaForEdit" + replyId + "' contenteditable='true'>" + reply + "</div>");
        	$('#editReplyBtn'+replyId).html("<a onclick='editReplyOK(" + replyId + "," + issueId + ");' id='editReplyOK" + replyId + "' class='comment-action pull-right'>Ok</a>");
        	setUsersForReplyByIssueId(issueId, '#commentAreaForEdit' + replyId);
        }).error(function(data) {
        		alert('Proplem with getting of reply');
        });
    	
    }
    
    function editReplyOK(replyId, issueId){
    	
    	var message = $('#commentAreaForEdit'+replyId).text();
    	
    	var json_data = {
    			'replyId' : replyId,
    			'message' : message
    		};
    	
    	$.ajax({
        	url : '${pageContext.servletContext.contextPath}/review/editReply',
        	type : 'post',
        	dataType : 'text',
        	data : json_data
        }).success(function(data) {
        	$('#reply'+replyId).replaceWith(data);
        	$("#show_issue_stub_" + issueId).attr('style', 'height:' + $("#show_issue_" + issueId).height() + 'px !important');
        	$('[data-toggle="popover"]').popover({container: 'body'});
        }).error(function(data) {
        		alert('Proplem with editing of reply');
        });
    	
    }
    
    function deleteReply(replyId){
    	$.ajax({
        	url : '${pageContext.servletContext.contextPath}/review/deleteReply',
        	type : 'post',
        	dataType : 'text',
        	data : {'replyId': replyId}
        }).success(function(data) {
        	$('#reply'+replyId).remove();
        }).error(function(data) {
        		alert('Reply didnt delete');
        });
    }
    
    function replyTo(login, issueId){
    	$('#commentArea'+issueId).html('@'+login+', ');
    	placeCaretAtEnd(document.getElementById('commentArea'+issueId) );
    }
    //end actions with comments

    function onSelectLines() {
        var codeBlockSelector = $(".syntaxhighlighter .code .container");
        if (${isActiveReview} && ${isReviewer}) { //only reviewers can add issues
            codeBlockSelector.bind("mouseup", function () {
                        addCreateIssuePanel();
                    }
            );
        }
        codeBlockSelector.on('mousedown mouseup mousemove', function() {
            var sel = window.getSelection();
            if (sel.isCollapsed) {
                $('.line').removeClass('highlight2');
            } else {
                $('.line').each(function () {
                    $(this).toggleClass('highlight2', sel.containsNode(this, true));
                });
            }
        });

        $(window).click(function() {
            $('.line').removeClass('highlight2');
        });
    }

    function setUsersForReplyByIssueId(issueId, target){
    	$.ajax({
        	url : '${pageContext.servletContext.contextPath}/review/uploadUsers',
        	type : 'get',
        	dataType : 'text',
        	data : {
			'issueId' : issueId
			}
        }).success(function(data) {
        	var arr = JSON.parse(data);
        	
        	$(target).tagautocomplete({
             	source : arr
        	});
        }).error(function(data) {
        		alert('Error during uploading users');
        });
    }
</script>
<script src="<c:url value="/resources/js/moment.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/ckeditor/ckeditor.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/ckeditor/plugins/codesnippet/lib/highlight/highlight.pack.js"/>"
        type="text/javascript"></script>

<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/js/splitter.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/semantic_ui/js/semantic.js"/>" type="text/javascript"></script>

<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushAS3.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushBash.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushCpp.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushCSharp.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushCss.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushJava.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushJavaFX.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushJScript.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushPerl.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushPhp.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushPlain.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushPython.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushRuby.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushScala.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushSql.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/syntaxhighlight/js/brush/shBrushXml.js"/>" type="text/javascript"></script>

</body>
</html>