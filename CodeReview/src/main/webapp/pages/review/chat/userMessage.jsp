<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="i" uri="../../../WEB-INF/showImageUrl.tld"%>

<li id="message${message.id}">
	<div class="comment">


		<div class="row">

			<div class="col-md-2">
				<a class="chat-ava">
				<img src="<i:img-url fromServer="${message.user.imageFromServer}" url="${message.user.imageUrl}" />"
				alt="${message.user.login}"/>
				</a>

			</div>

			<div class="col-md-10">

				<div class="content">
					<a class="author">${message.user.firstName}</a>
					<div class="metadata">
						<span class="date"><fmt:formatDate pattern="hh:mm a" value="${message.time}" /></span>
					</div>
					<div class="text"><c:out value="${message.text}"/></div>
				</div>
			</div>


		</div>
	</div>
</li>