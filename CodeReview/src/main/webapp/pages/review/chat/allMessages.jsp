<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="i" uri="../../../WEB-INF/showImageUrl.tld"%>

<c:set var="isDivider" value="${true}"/>

<c:forEach items="${messages}" var="message">

<c:choose>

<c:when test="${userID == message.user.id}">

<li id="message${message.id}" style="background: #fafafa">
	<div class="comment">
		<div class="row">
			<div class="col-md-9">
				<div class="content">

					<div class="row">
						<a class="author author-right pull-right">${message.user.firstName}</a>
						<div class="metadata pull-right">
							<span class="date"><fmt:formatDate pattern="hh:mm a" value="${message.time}" /></span>
						</div>
					</div>
					<div class="text"><c:out value="${message.text}"/></div>
				</div>
			</div>

			<div class="col-md-2">
				<a class="chat-ava">
				<img src="<i:img-url fromServer="${message.user.imageFromServer}" url="${message.user.imageUrl}" />"
									alt="${message.user.login}"/>
                 </a>
			</div>
		</div>
	</div>
</li>

</c:when>

<c:otherwise>

<c:choose>
<c:when test="${message.time>userTime}">

<c:if test="${isDivider}">

<div id='newMessages' class="ui horizontal divider">
    New messages
  </div>

<c:set var="isDivider" value="${false}"/>

</c:if>

<li id="message${message.id}" class="newMessage" style="background:#e8f5e9">
	<div class="comment">


		<div class="row">

			<div class="col-md-2">
				<a class="chat-ava">
				<img src="<i:img-url fromServer="${message.user.imageFromServer}" url="${message.user.imageUrl}" />"
				alt="${message.user.login}"/>
				</a>

			</div>

			<div class="col-md-10">

				<div class="content">
					<a class="author">${message.user.firstName}</a>
					<div class="metadata">
						<span class="date"><fmt:formatDate pattern="hh:mm a" value="${message.time}" /></span>
					</div>
					<div class="text"><c:out value="${message.text}"/></div>
				</div>
			</div>


		</div>
	</div>
</li>
</c:when>

<c:otherwise>

<li id="message${message.id}">
	<div class="comment">


		<div class="row">

			<div class="col-md-2">
				<a class="chat-ava">
				<img src="<i:img-url fromServer="${message.user.imageFromServer}" url="${message.user.imageUrl}" />"
				alt="${message.user.login}"/>
				</a>

			</div>

			<div class="col-md-10">

				<div class="content">
					<a class="author">${message.user.firstName}</a>
					<div class="metadata">
						<span class="date"><fmt:formatDate pattern="hh:mm a" value="${message.time}" /></span>
					</div>
					<div class="text"><c:out value="${message.text}"/></div>
				</div>
			</div>


		</div>
	</div>
</li>

</c:otherwise>

</c:choose>

</c:otherwise>

</c:choose>

</c:forEach>