<!-- <input type="submit" name="submit" value="Add file from your Git Repository" onclick="git_attach()" -->
<!-- 	data-toggle="modal" data-target="#fileAttachment"> -->
	
<!-- <input type="submit" name="submit" value="Add file from your PC" onclick="pc_attach()" -->
<!-- 	data-toggle="modal" data-target="#fileAttachment"> -->
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>

<div class="modal fade attachmentModal" id="fileAttachment" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h1 class="modal-title">
				<u:message locale="${locale}" key="attachment.title1" /> <u:message locale="${locale}" key="attachment.title2" /></h1>
			</div>
			<div>
				<div id ="reviewCreatingModalBodyContentAll">
					<div id="reviewCreatingModalBodyContent" class="modal-body"></div>
					<div id="anotherAttachContent">
						<div id="attachTree"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" onclick="exit()" class="btn btn-default"
						data-dismiss="modal"><u:message locale="${locale}" key="review.creating.exit" /></button>
				</div>
			</div>
		</div>
	</div>
</div>