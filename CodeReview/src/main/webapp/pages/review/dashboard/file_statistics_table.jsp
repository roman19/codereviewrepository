<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>
<h3><u:message locale="${locale}" key="review.dashboard.files.average"/></h3>
<table class="table">
    <tbody>
    <tr>
        <td><u:message locale="${locale}" key="review.dashboard.files.count"/></td>
        <td style="text-align: center"><c:out value="${totalFilesCount}"/></td>
    </tr>
    <tr>
        <td><u:message locale="${locale}" key="review.dashboard.issues.count"/></td>
        <td>
            <table class="table inside-table" style="text-align: center">
                <tbody>
                <tr><td><i class='fa fa-exclamation-circle issue-sign'></i></td><td><c:out value="${errorIssuesCount}"/></td></tr>
                <tr><td><i class='fa fa-exclamation-triangle issue-sign'></i></td><td><c:out value="${warningIssuesCount}"/></td></tr>
                <tr><td><i class='fa fa-info-circle issue-sign'></i></td><td><c:out value="${infoIssuesCount}"/></td></tr>
                </tbody>
                <tfoot style="font-weight: bold; border-top: 1px solid #ddd;">
                <tr><td><u:message locale="${locale}" key="review.dashboard.total"/></td><td><c:out value="${totalIssuesCount}"/></td></tr>
                </tfoot>
            </table>
        </td>
    </tr>
    </tbody>
</table>
