<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>
<table class="table table-hover table-condensed">
    <thead>
    <tr>
        <th><u:message locale="${locale}" key="review.dashboard.user"/>
            <c:if test="${isOwnerReview && isActiveReview}">
                <button style="margin-left: 5px;" class="btn btn-default" id="add_users" data-toggle="modal" data-target="#add_users_modal" onclick="loadUsersForAddToReview('${reviewId}');">
                    <i class="fa fa-plus"></i></button>
            </c:if>
        </th>
        <th><u:message locale="${locale}" key="review.dashboard.status"/></th>
        <th><u:message locale="${locale}" key="review.dashboard.coverage"/></th>
        <th><u:message locale="${locale}" key="review.tabs.issues"/></th>
        <c:if test="${isOwnerReview && isActiveReview}">
            <th></th>
        </c:if>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="user" items="${reviewUsers}">
        <c:if test="${ (isActiveReview &&
                        ((!isOwnerReview && (user.status == 'confirmed' || user.status == 'pending')) ||  isOwnerReview))
                         || (!isActiveReview && (user.status == 'finished_confirmed' || user.status == 'confirmed')) }">
            <c:choose>
                <c:when test="${user.status == 'confirmed' || user.status == 'finished_confirmed'}">
                    <c:set var="status" value="ui green label"/>
                </c:when>
                <c:when test="${user.status == 'pending'}">
                    <c:set var="status" value="ui blue label"/>
                </c:when>
                <c:otherwise>
                    <c:set var="status" value="ui red label"/>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${user.status == 'finished_confirmed'}">
                    <c:set var="userStatus" value='confirmed'/>
                </c:when>
                <c:otherwise>
                    <c:set var="userStatus" value="${user.status}"/>
                </c:otherwise>
            </c:choose>
            <fmt:formatNumber type="number" maxFractionDigits="0" var="userCoveragePercent"
                              value="${user.coveragePercent}"/>

            <tr id="user_${user.userId}" <c:if test="${sessionScope.userID eq user.userId}">class="success"</c:if>>
                <td><c:out value="${user.firstName} ${user.lastName} (${user.login})"/></td>
                <td><span class="${status}"><u:message locale='${locale}' key='review.dashboard.status.${userStatus}'/></span></td>
                <td><c:out value="${userCoveragePercent}%"/></td>
                <td><c:out value="${user.issuesCount}"/></td>
                <c:if test="${isOwnerReview && isActiveReview}">
                    <c:choose>
                        <c:when test="${user.status == 'confirmed' || user.status == 'pending'}">
                            <td>
                                <button class="btn btn-default" onclick="removeUser('${reviewId}', '${user.userId}');"><i
                                        class="fa fa-trash-o"></i></button>
                            </td>    
                        </c:when>
                        <c:when test="${user.status == 'rejected' || user.status == 'left' || user.status == 'deleted_by_admin'}">
                            <td>
                                <button class="btn btn-default" onclick="sendIsNotifiedByOwner('${reviewId}', '${user.userId}');">ok</button>
                            </td>
                        </c:when>
                    </c:choose>
                </c:if>
            </tr>
        </c:if>
    </c:forEach>
    </tbody>
</table>
