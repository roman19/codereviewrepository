<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>
<div>
    <c:choose>
        <c:when test="${not empty usersFromReviewGroup}">
            <select multiple="multiple" id="user-select">
                <c:forEach var="userFromReviewGroup" items="${usersFromReviewGroup}">
                    <option value='${userFromReviewGroup.id}'>${userFromReviewGroup.firstName} ${userFromReviewGroup.lastName}
                        (${userFromReviewGroup.login})
                    </option>
                </c:forEach>
            </select>
            <script>
                $('#user-select').multiSelect({
                    selectableHeader: "<div class='custom-header'>&nbsp;</div>",
                    selectionHeader: "<div class='custom-header'><u:message locale='${locale}' key='review.dashboard.modal.users.selected'/></div>"
                });
                $("#add_users_btn").show();
            </script>
        </c:when>
        <c:otherwise>
            <p><u:message locale='${locale}' key='review.dashboard.modal.users.no'/></p>
            <script>
                $("#add_users_btn").hide();
            </script>
        </c:otherwise>
    </c:choose>
</div>