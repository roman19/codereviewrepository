<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>
<fmt:formatNumber type="number" maxFractionDigits="0" var="percentTotal" value="${totalReviewPercent}"/>
<div class="col-xs-6" style="padding-left: 0;">
    <div class='panel panel-default' style="margin-top: 10px;">
        <div class='panel-body'>
            <h3><u:message locale="${locale}" key="review.dashboard.complete"/> - ${percentTotal}%</h3>
            <div class="progress">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="${percentTotal}"
                     aria-valuemin="0" aria-valuemax="100" style="width:${percentTotal}%">
                    ${percentTotal}%
                </div>
            </div>
            <c:if test="${not empty reviewOwner}">
                <div style="margin-bottom: 10px; margin-left: 5px;">
                    <span style="font-size: 14px; font-weight: bold;"><u:message locale="${locale}" key="review.dashboard.owner"/> <c:out value="${reviewOwner.firstName} ${reviewOwner.lastName} (${reviewOwner.login})"/></span>
                </div>
            </c:if>
            <div id="dashboard_user_table_container">
                <jsp:include page="user_table.jsp"/>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-6" style="padding-right: 0;">
        <div class="col-xs-6" style="padding-left: 0;">
            <div id="lang_chart"></div>
        </div>
        <div class="col-xs-6" style="padding-right: 0;">
            <div class='panel panel-default' style="margin-top: 10px;">
                <div class='panel-body'>
                    <jsp:include page="file_statistics_table.jsp"/>
                </div>
            </div>
        </div>
    </div>
<script>
    (function(){
        var obj = JSON.parse('${langData}');
        console.log('${langData}');
        var arr = [];
        for (var k in obj) {
            if (obj.hasOwnProperty(k)) {
                arr.push([k, obj[k]]);
            }
        }
        var chart = c3.generate({
            bindto: '#lang_chart',
            data: {
                columns: arr,
                type: 'donut'
            },
            donut: {
                title: "<u:message locale="${locale}" key="review.dashboard.languages"/>"
            }
        });
    })();

    function removeUser(reviewId, userId) {
        $.post("${pageContext.servletContext.contextPath}/review/dashboard/remove_user", {
            reviewId: reviewId,
            userId: userId
        }).done(function (data) {
            $('#tab_content').html(data);
        }).fail(function () {
            alert("<u:message locale="${locale}" key="review.dashboard.alert.dashboard_failed"/>");
        });
    }

    function loadUsersForAddToReview(reviewId) {
        $('#review_users_selector').html('<div class="ui segment">' +
                '<div class="ui active inverted dimmer">' +
                '<div class="ui large text loader"><u:message locale="${locale}" key="review.dashboard.modal.users.load"/></div>' +
                '</div><p></p><p></p><p></p></div>');
        $.post("${pageContext.servletContext.contextPath}/review/dashboard/load_users", {
            reviewId: reviewId
        }).done(function (data) {
            $('#review_users_selector').html(data);
        }).fail(function () {
            alert("<u:message locale="${locale}" key="review.dashboard.alert.users_failed"/>");
        });
    }

    function addUsers(reviewId, usersIdForAdd) {
        if (usersIdForAdd == null) return;
        $.post("${pageContext.servletContext.contextPath}/review/dashboard/add_users", {
            reviewId: reviewId,
            users: JSON.stringify(usersIdForAdd)
        }).done(function (data) {
            $('#dashboard_user_table_container').html(data);
        }).fail(function () {
            alert("<u:message locale="${locale}" key="review.dashboard.alert.users_failed"/>");
        });
    }

    function sendIsNotifiedByOwner(reviewId, userId) {
        if (userId == null) return;
        $.post("${pageContext.servletContext.contextPath}/review/dashboard/send_is_notified", {
            reviewId: reviewId,
            userId: userId
        }).done(function (data) {
            $('#dashboard_user_table_container').html(data);
        }).fail(function () {
            alert("<u:message locale="${locale}" key="review.dashboard.alert.users_failed"/>");
        });
    }
</script>