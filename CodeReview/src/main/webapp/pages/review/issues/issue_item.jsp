<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>






<style>



.replylikeup{
	color: #CDCDCD;
}

.replylikedown{
	color: #CDCDCD;
}

.replylikedown:hover {
	color: red;
}

.replylikeup:hover {
	color: green;
}
.green{
	color:rgb(23, 160, 94);
}

.red{
	color: rgb(222, 81, 69);
}

</style>
<div id='show_issue_${issue.id}' class='issue show_issue'>
    <div class='panel <c:choose>
                    <c:when test="${issue.type == 'ERROR'}">
                        panel-danger
                    </c:when>
                    <c:when test="${issue.type == 'WARNING'}">
                        panel-warning
                    </c:when>
                    <c:otherwise>
                        panel-info
                    </c:otherwise>
                </c:choose>'>
        <div class='panel-heading'>
        <c:choose>
                <c:when test="${isActiveReview}">
        
         <a  id="likedown${issue.id}"  class="underlined btn-icon btn likedown"    onclick="onLikeDownIssue(${issue.id})" style="float:right;  margin-right:15px; "><i id="like-down${issue.id}" class='fa fa-thumbs-down fa my-like my-like-down'><span id='total-dislikes${issue.id}'      style='font-size: 10px;'>0</span></i></a>
                <a  id="likeup${issue.id}" class='underlined btn-icon btn likeup'    onclick='onLikeUpIssue(${issue.id})'  style='float:right;  margin-right:15px; '><i  id='like-up${issue.id}'    class='fa fa-thumbs-up fa my-like my-like-up'><span id='total-likes${issue.id}'       style='font-size: 10px;'>0</span></i></a>
     
     </c:when>
      <c:otherwise>
     
        <a  id="likedown${issue.id}"  class="underlined btn-icon btn likedown"  disabled=’disabled’  style='float:right; cursor: default;pointer-events: none; margin-right:15px; '><i id="like-down${issue.id}" class='fa fa-thumbs-down fa my-like my-like-down'><span id='total-dislikes${issue.id}'       style='font-size: 10px;'>0</span></i></a>
      <a  id="likeup${issue.id}" class='underlined btn-icon btn likeup'   disabled=’disabled’   style='float:right; cursor: default;pointer-events: none; margin-right:15px; '><i  id='like-up${issue.id}'    class='fa fa-thumbs-up fa my-like my-like-up'><span id='total-likes${issue.id}'       style='font-size: 10px;'>0</span></i></a>
     
     
      </c:otherwise>
       </c:choose> 
        
            <p id='title_issue_${issue.id}' class='issue_title'><c:choose>
                <c:when test="${issue.type == 'ERROR'}">
                    <i class='fa fa-exclamation-circle issue-sign'></i>
                </c:when>
                <c:when test="${issue.type == 'WARNING'}">
                    <i class='fa fa-exclamation-triangle issue-sign'></i>
                </c:when>
                <c:otherwise>
                    <i class='fa fa-info-circle issue-sign'></i>
                </c:otherwise>
            </c:choose> <c:out value="${issue.title}" escapeXml="false"/></p>
        </div>
        <div class='panel-body'>
            <div id='body_issue_${issue.id}' class='collapse' style='padding: 5px 0;'>
                <c:out value="${issue.body}" escapeXml="false"/>
            </div>
            <c:choose>
                <c:when test="${isActiveReview}">
                    <div>
                        <c:if test="${not empty issue.body}">
                            <div class='row'>
                                <div class='col-xs-6'>
                                    <p><a class='read_more underlined' data-toggle='collapse'
                                          id='body_issue_collapse_${issue.id}'
                                          data-target='#body_issue_${issue.id}'><u:message locale='${locale}' key='review.files.issue.read_more'/></a></p>
                                </div>
                                <div class='col-xs-6'></div>
                            </div>
                        </c:if>
                        <div class='row'>
                            <div class='col-xs-6' style='padding-bottom: 5px;'>
                                <p>
                                    <a id="reply_issue_${issue.id}" data-toggle='collapse'
                                       href='#comments${issue.id}' class='btn-icon btn <c:choose>
                                    <c:when test="${issue.type == 'ERROR'}">
                                        btn-danger
                                                </c:when>
                                                <c:when test="${issue.type == 'WARNING'}">
                                                    btn-warning
                                                </c:when>
                                                <c:otherwise>
                                                    btn-info
                                                </c:otherwise>
                                                </c:choose>'>
                                        <i class='fa fa-reply'></i>
                                    </a>
                                    <c:if test="${issue.isEditable}">
                                        <a id="edit_issue_${issue.id}" class='btn-icon btn <c:choose>
                                    <c:when test="${issue.type == 'ERROR'}">
                                        btn-danger
                                                </c:when>
                                                <c:when test="${issue.type == 'WARNING'}">
                                                    btn-warning
                                                </c:when>
                                                <c:otherwise>
                                                    btn-info
                                                </c:otherwise>
                                                </c:choose>' onclick='onEditIssueInIssuesTab("${issue.id}")'><i
                                                class='fa fa-pencil'></i></a>
                                        <a id="remove_issue_${issue.id}" class='btn-icon btn <c:choose>
                                    <c:when test="${issue.type == 'ERROR'}">
                                        btn-danger
                                                </c:when>
                                                <c:when test="${issue.type == 'WARNING'}">
                                                    btn-warning
                                                </c:when>
                                                <c:otherwise>
                                                    btn-info
                                                </c:otherwise>
                                                </c:choose>' onclick='onRemoveIssueInIssuesTab("${issue.id}")'><i
                                                class='fa fa-trash-o'></i></a>
                                    </c:if>
                                </p>
                            </div>
                            <div class='col-xs-6' style='padding-top: 5px;'>
                                                <span id="issue_post_date_${issue.id}" class='ui label'
                                                      style='margin-left: 5px; margin-right: 0; padding: 0 0 0 5px; line-height: 18px;'><fmt:formatDate
                                                        pattern="yyyy-MM-dd HH:mm" value="${issue.time}"/></span>
                                                <span class='ui label'
                                                      style='margin-left: 5px; margin-right: 0; padding: 0 0 0 5px; line-height: 18px;'><c:out
                                                        value="${issue.userFullName} (${issue.userLogin})"/></span>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class='row'>
                        <div class='col-xs-6'>
                        	<!-- for replies -->
                        	<c:if test="${issue.replies.size()!=0}">
                        		<span style='padding-right:10px'><a class='read_more underlined' data-toggle='collapse' data-target='#comments${issue.id}'>Comments (${issue.replies.size()}) <i class='fa fa-comments-o'></i></a></span>
                        	</c:if>
                            <c:if test="${not empty issue.body}">
                                <span>
                                    <a class='read_more underlined' data-toggle='collapse'
                                       id='body_issue_collapse_${issue.id}'
                                       data-target='#body_issue_${issue.id}'><u:message locale='${locale}' key='review.files.issue.read_more'/></a>
                                </span>
                            </c:if>
                        </div>
                        <div class='col-xs-6'>
                                            <span id="issue_post_date_${issue.id}" class='ui label'
                                                  style='margin-left: 5px; margin-right: 0; padding: 0 0 0 5px; line-height: 18px;'><fmt:formatDate
                                                    pattern="yyyy-MM-dd HH:mm" value="${issue.time}"/></span>
                                            <span class='ui label'
                                                  style='margin-left: 5px; margin-right: 0; padding: 0 0 0 5px; line-height: 18px;'><c:out
                                                    value="${issue.userFullName} (${issue.userLogin})"/></span>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <jsp:include page="replies.jsp"/>
</div>

<script>

$(function() {
	
	
	var id=${issue.id};

//to upload all like number
			$.get('${pageContext.servletContext.contextPath}/loadLikes', {IssueId:id}, function(response) { 
			   
				
				$('#total-dislikes'+id).text(response.totalDisLikes); 
				
				
				
				$('#total-likes'+id).text(response.totalLikes);
			     var likeValue=response.likeValue;
			     
			     if(likeValue == 1){
			     	$("#like-up"+id).toggleClass("green");	
			   		 document.getElementById("likeup"+id).className = "underlined btn-icon btn white";
			   		 
			     }
			     if(likeValue == -1){
			     	$("#like-down"+id).toggleClass("red");	
			     	 document.getElementById("likedown"+id).className = "underlined btn-icon btn white";
			     	 
			     }
			     
			     
			    					    	        
			});		


});

function onLikeUpIssue(issueId){
	var id=issueId;
	
if($("#like-up"+id).css('color')!="rgb(23, 160, 94)"){						 
	 if($("#like-down"+id).css('color')=="rgb(222, 81, 69)") {
	    	 $('#like-down'+id).toggleClass("red");
	    	 document.getElementById("likedown"+id).className = "underlined btn-icon btn likedown";
	    	 $("#like-up"+id).toggleClass("green");	
	    	 document.getElementById("likeup"+id).className = "underlined btn-icon btn white";
	    	 
	    	 //decrement like-down and increment like-up
	    	 $.get('${pageContext.servletContext.contextPath}/decrementLikeDownIssue', {IssueId:id}, function(response) { 
		    	        $('#total-dislikes'+id).text(response);  						    	      	  
	    	        	$.get('${pageContext.servletContext.contextPath}/incrementLikeUpIssue',{IssueId:id},function(response) { 
			    	        		$('#total-likes'+id).text(response);  
		 	    	   		 });					    	        
	    	    });					    	 						    	 
	     } else{						    						    	 
	    	 $("#like-up"+id).toggleClass("green");	    	
	    	 document.getElementById("likeup"+id).className = "underlined btn-icon btn white";
	    
	    	 
	    	//increment like-up 
	    	 $.get('${pageContext.servletContext.contextPath}/incrementLikeUpIssue',{IssueId:id},function(response) { 
		    	        $('#total-likes'+id).text(response);         
	    	    });						    	 
	     }	 			  				 
		}
		else{			  			
			 $("#like-up"+id).toggleClass("green");
			 document.getElementById("likeup"+id).className = "underlined btn-icon btn likeup";
		  
			//decrement like-up
			 $.get('${pageContext.servletContext.contextPath}/decrementLikeUpIssue',{IssueId:id},function(response) { 
	    	        $('#total-likes'+id).text(response);         
    	    });
		}			  		
	
}

function onLikeDownIssue(issueId){
	var id=issueId;
	 if($("#like-down"+id).css('color')!="rgb(222, 81, 69)"){								 
		    if($("#like-up"+id).css('color')=="rgb(23, 160, 94)") {
		    	
		    	$('#like-up'+id).toggleClass("green");
		    	 
		    	 document.getElementById("likeup"+id).className = "underlined btn-icon btn likeup";
		    	 
		    	 $('#like-down'+id).toggleClass("red");	
		    	 
		    	 document.getElementById("likedown"+id).className = "underlined btn-icon btn white";
		    	 
		    //decrement like-up and increment like-down
		    	 $.get('${pageContext.servletContext.contextPath}/decrementLikeUpIssue',{IssueId:id},function(response) { 
			    	       		 $('#total-likes'+id).text(response);     			    	        
		    	        $.get('${pageContext.servletContext.contextPath}/incrementLikeDownIssue',{IssueId:id},function(response) { 
				    	         $('#total-dislikes'+id).text(response);         
			    	    });			    	        
		    	    });			    	 
		     } else{			    	 
		    	 $('#like-down'+id).toggleClass("red");
		    	 document.getElementById("likedown"+id).className = "underlined btn-icon btn white";
		    	//increment like-down 
		    	 $.get('${pageContext.servletContext.contextPath}/incrementLikeDownIssue',{IssueId:id},function(response) { 
				    	        $('#total-dislikes'+id).text(response);         
		    	    });			     		     
		     }	  				 
		}
		else{
			 $('#like-down'+id).toggleClass("red"); 			
			 document.getElementById("likedown"+id).className = "underlined btn-icon btn likedown";
			//decrement like-down
			 $.get('${pageContext.servletContext.contextPath}/decrementLikeDownIssue',{IssueId:id},function(response) { 
		    	        $('#total-dislikes'+id).text(response);         
	    	    });
		}
}




</script>


<script>




function onLikeUpReply(replyId){
	
	debugger;
	var id=replyId;

	 if($("#like-up-reply"+id).css('color')!="rgb(23, 160, 94)"){							 
		    if($("#like-down-reply"+id).css('color')=="rgb(222, 81, 69)") {
		    	
		    		
		   	 	$("#like-down-reply"+id).attr("class", "fa fa-thumbs-down fa my-like my-like-down replylikedown");
		    
		   	 		
		    	$("#like-up-reply"+id).attr("class", "fa fa-thumbs-up fa my-like my-like-up green");
		    	
		    
		    	 
		    //decrement like-down and increment like-up
		    	 $.get('${pageContext.servletContext.contextPath}/decrementLikeDownReply',{ReplyId:id},function(response) { 
			    	       		 $('#total-reply-dislikes'+id).text(response);     			    	        
		    	        $.get('${pageContext.servletContext.contextPath}/incrementLikeUpReply',{ReplyId:id},function(response) { 
				    	        $('#total-reply-likes'+id).text(response);         
			    	    });			    	        
		    	    });			    	 
		     } else{			    	 
		    	
		    	 
		    	 $("#like-up-reply"+id).attr("class", "fa fa-thumbs-up fa my-like my-like-up green");
		    	 
		    	 
		    	//increment like-up 
		    	 $.get('${pageContext.servletContext.contextPath}/incrementLikeUpReply',{ReplyId:id},function(response) { 
				    	        $('#total-reply-likes'+id).text(response);         
		    	    });			     		     
		     }	  				 
		}
		else{
					
			 $("#like-up-reply"+id).attr("class", "fa fa-thumbs-up fa my-like my-like-up replylikeup");
			//decrement like-up
			 $.get('${pageContext.servletContext.contextPath}/decrementLikeUpReply',{ReplyId:id},function(response) { 
				 console.log("response = "+response);    
				 $('#total-reply-likes'+id).text(response);         
	    	    });
		}
          
	
}


function onLikeDownReply(replyId){
		
		debugger;
		var id=replyId;
		
		 if($("#like-down-reply"+id).css('color')!="rgb(222, 81, 69)"){								 
			    if($("#like-up-reply"+id).css('color')=="rgb(23, 160, 94)") {
			    	
			    
			    	
			    	$("#like-down-reply"+id).attr("class", "fa fa-thumbs-down fa my-like my-like-down red");
			    	 
			    	
			    	 $("#like-up-reply"+id).attr("class", "fa fa-thumbs-up fa my-like my-like-up replylikeup");
			    
			    	 
			    //decrement like-up and increment like-down
			    	 $.get('${pageContext.servletContext.contextPath}/decrementLikeUpReply',{ReplyId:id},function(response) { 
 			    	       		 $('#total-reply-likes'+id).text(response);     			    	        
			    	        $.get('${pageContext.servletContext.contextPath}/incrementLikeDownReply',{ReplyId:id},function(response) { 
 				    	        $('#total-reply-dislikes'+id).text(response);         
				    	    });			    	        
			    	    });			    	 
			     } else{			    	 
			    	    	 
			    	 $("#like-down-reply"+id).attr("class", "fa fa-thumbs-down fa my-like my-like-down red");
			    	 
			    	//increment like-down 
			    	 $.get('${pageContext.servletContext.contextPath}/incrementLikeDownReply',{ReplyId:id},function(response) { 
   				    	        $('#total-reply-dislikes'+id).text(response);         
			    	    });			     		     
			     }	  				 
			}
			else{
				
				 $("#like-down-reply"+id).attr("class", "fa fa-thumbs-down fa my-like my-like-down replylikedown");
				//decrement like-down
				 $.get('${pageContext.servletContext.contextPath}/decrementLikeDownReply',{ReplyId:id},function(response) { 
					 console.log("response = "+response);    
					 
 		    	        $('#total-reply-dislikes'+id).text(response);         
		    	    });
			}
              
    	
    }
    

</script>
