<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>
<c:choose>
    <c:when test="${not empty issues}">
        <c:forEach var="issue" items="${issues}" varStatus="loop">
            <c:if test="${(loop.index == 0) || (issues[loop.index].filePath != issues[loop.index - 1].filePath)}">
                <c:set var="fileClass" value="file${loop.index}"/>
                <span class="issue-file-path ${fileClass}"><i style="font-size: 18px;" class="fa fa-file-code-o"></i> ${issue.filePath}</span>
            </c:if>
            <div id="issue_holder_${issue.id}" class="holder-issue ${fileClass}">
                <c:set var="issue" value="${issue}" scope="request"/>
                <jsp:include page="issue_item.jsp"/>
                <div id='code_issue_${issue.id}'>
                    <pre class="brush: ${issue.fileBrush}; first-line: ${issue.lineFrom}">${issue.fileLinesContent}</pre>
                </div>
            </div>
        </c:forEach>
    </c:when>
    <c:otherwise>
        <h1 style="color:#E3E3E3; text-align: center"><u:message locale='${locale}' key='review.issues.no'/></h1>
    </c:otherwise>
</c:choose>

<script>
    (function () {
        $('.panel-body pre code').each(function (i, block) {
            hljs.highlightBlock(block);
        });
        SyntaxHighlighter.highlight();
    })();
</script>
