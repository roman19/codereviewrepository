<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>

<div id="issues_tab_content" class="col-xs-12">
    <jsp:include page="issues_filter.jsp"/>
    <div id="issues_list">
        <jsp:include page="issues_list.jsp"/>
    </div>
</div>

<script>
function onEditIssueInIssuesTab(issueId) {
        $.post("${pageContext.servletContext.contextPath}/review/file/issue/edit", {
            reviewId: $("#reviewIdForChat").val(),
            issueId: issueId
        }).done(function (data) {
            $("#create_issue").remove();
            $("#show_issue_" + issueId).hide();
            editIssueTemplate(data.title, data.body, issueId);
        }).fail(function () {
            alert("Edit failed!");
        });
    }

    function onRemoveIssueInIssuesTab(issueId) {
        $.post("${pageContext.servletContext.contextPath}/review/file/issue/delete", {
            reviewId: $("#reviewIdForChat").val(),
            issueId: issueId
        }).done(function (data) {
            if (data.isRemoved == true) {
                var issueHolder = $("#issue_holder_" + issueId);
                var fileClassLabel = issueHolder.attr("class").match(/file\d+/g);
                issueHolder.remove();
                if ($(".holder-issue." + fileClassLabel).length == 0) {
                    $(".issue-file-path." + fileClassLabel).remove();
                }
            } else {
                alert("<u:message locale="${locale}" key="review.issues.remove_issue_failed"/>");
            }
        }).fail(function () {
            alert("<u:message locale="${locale}" key="review.issues.remove_issue_failed"/>");
        });
    }

    function editIssueTemplate(title, body, issueId) {
        $("<div id='create_issue' class='unselectable issue'>" +
                "<div class='panel panel-default'>" +
                "<div class='panel-heading'><h3 class='panel-title'><u:message locale='${locale}' key='review.files.issue.edit'/></h3></div>" +
                "<div class='panel-body'>" +
                "<form id='create_issue_form'>" +
                "<div id='title_group' class='input-group input-group-md'>" +
                "<span class='input-group-addon' id='sizing-addon1'><u:message locale='${locale}' key='review.files.issue.title'/></span>" +
                "<input type='text' class='form-control' placeholder='<u:message locale='${locale}' key='review.files.issue.title.placeholder'/>' id='issue_title' aria-describedby='sizing-addon1'>" +
                "</div>" +
                "<div id='issue_body' class='collapse'>" +
                "<textarea name='issue_body_editor' id='issue_body_editor'></textarea>" +
                "</div>" +
                "<a id='issue_body_collapse' class='underlined' data-toggle='collapse' data-target='#issue_body'><u:message locale='${locale}' key='review.files.issue.body.expand'/></a>" +
                "<div class='row'>" +
                "<div class='col-xs-6'>" +
                "<div class='btn-group btn-group-md' role='group' >" +
                "<input id='error' class='btn btn-danger' type=submit value='<u:message locale='${locale}' key='review.btn.error'/>'/>" +
                "<input id='warning' class='btn btn-warning' type=submit value='<u:message locale='${locale}' key='review.btn.warning'/>'/>" +
                "<input id='info' class='btn btn-info' type=submit value='<u:message locale='${locale}' key='review.btn.info'/>'/> " +
                "</div>" +
                "</div><div class='col-xs-4'></div>" +
                "<div class='col-xs-2'>" +
                "<input id='issue_cancel_btn' class='btn btn-default' type=button onclick='cancelIssueEditing(" + issueId + ")' value='Cancel''/>" +
                "</div>" +
                "</div>" +
                "</form>" +
                "</div>" +
                "</div></div>").insertAfter("#show_issue_" + issueId);
        CKEDITOR.replace('issue_body_editor');

        var createIssueSelector = $('#create_issue');
        var titleSelector = $('#issue_title');

        createIssueSelector.on('show.bs.collapse', function () {
            $('#issue_body_collapse').html('<u:message locale='${locale}' key='review.files.issue.body.hide'/>');
        });
        createIssueSelector.on('hide.bs.collapse', function () {
            $('#issue_body_collapse').html('<u:message locale='${locale}' key='review.files.issue.body.expand'/>');
        });

        $("form input[type=submit]").click(function () {
            $("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
            $(this).attr("clicked", "true");
        });

        titleSelector.focus();

        $("#create_issue_form").submit(function (e) {
            e.preventDefault();
            var titleValue = titleSelector.val();
            if (isEmpty(titleValue) || isBlank(titleValue) || titleValue.match(/^(\s*(&nbsp;)+\s*)+$/)) {
                $('#title_group').addClass('has-error');
                titleSelector.tooltip({title: "<u:message locale='${locale}' key='review.files.issue.title.tooltip'/>", placement: "bottom", trigger: "manual"});
                titleSelector.tooltip('show');
                titleSelector.focus();
                titleSelector.bind('keydown paste click', function () {
                    titleSelector.tooltip('hide');
                    $('#title_group').removeClass('has-error');
                });
            } else {
                var submitValue = $("input[type=submit][clicked=true]").val();
                supplyChanges(submitValue, issueId);
            }
        });
        titleSelector.val(title);
        if (body && !isEmpty(body) && !isBlank(body)) {
            $('#issue_body_editor').val(body);
            $('#issue_body').collapse('show');
        }
    }

    function supplyChanges(issueType, issueId) {
        var titleSelector = $('#issue_title');
        $.post("${pageContext.servletContext.contextPath}/review/issues/edit_issue", {
            reviewId: $("#reviewIdForChat").val(),
            issueId: issueId,
            type: issueType.toUpperCase(),
            title: titleSelector.val(),
            body: CKEDITOR.instances.issue_body_editor.getData()
        }).done(function (data) {
            var showIssueSelector = $("#show_issue_" + issueId);
            showIssueSelector.remove();
            $("#create_issue").remove();
            $(data).insertBefore('#code_issue_' + issueId);
            showIssueSelector.find('pre code').each(function (i, block) {
                hljs.highlightBlock(block);
            });
        }).fail(function () {
            alert("<u:message locale='${locale}' key='review.issues.post_issue_failed'/>");
        });
    }

    function cancelIssueEditing(issueId) {
        $("#create_issue").remove();
        $("#show_issue_" + issueId).show();
    }
</script>
