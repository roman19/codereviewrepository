<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>
<%@ taglib prefix="i" uri="../../../WEB-INF/showImageUrl.tld"%>
<%@ taglib prefix="r" uri="../../../WEB-INF/showReply.tld"%>
<%@ taglib prefix="s" uri="../../../WEB-INF/showTime.tld"%>

<div id='comments${issue.id}' class='collapse comments-block clearfix' style='background-color:
								<c:choose>
								<c:when test="${issue.type == 'ERROR'}">
                                    #fefafa
                                </c:when>
                                <c:when test="${issue.type == 'WARNING'}">
                                    #fff8e1
                                </c:when>
                                <c:otherwise>
                                    #f1f9fe
                                </c:otherwise>
                                </c:choose>'>
<div id='forCommentsInsert${issue.id}' class='ui threaded comments'>

<c:set var="totalRepliesHistory" value="${issue.replies.size()}" />
<c:forEach items="${issue.replies}" var="reply" varStatus="replyCounter">
<div id="reply${reply.id}" class='comment'>
	<a class='reply-ava'> <img
		src="<i:img-url fromServer="${reply.user.imageFromServer}" url="${reply.user.imageUrl}" />"
		alt="${reply.user.login}" />
	</a>
	<div class='content'>
		<a class='author'>${reply.user.firstName} ${reply.user.lastName}</a>
		<div class='metadata'>
			<span class='date'><s:showTime locale="${locale}" time="${reply.time}"/></span>
		</div>
		
		
		
		<div style="float: right;">
					
			<a   onclick='onLikeDownReply(${reply.id})' class="down" style='float: right; margin-right: 15px;'>
					<i id='like-down-reply${reply.id}' class='fa fa-thumbs-down fa my-like my-like-down replylikedown'>
						<span id='total-reply-dislikes${reply.id}' style='font-size: 10px;'>0</span></i></a> 
							
					<a   onclick='onLikeUpReply(${reply.id})' class="up" style='float: right; margin-right: 15px;'>
					<i id='like-up-reply${reply.id}' class='fa fa-thumbs-up fa my-like my-like-up replylikeup'>
						<span id='total-reply-likes${reply.id}' style='font-size: 10px;'>0</span></i></a>
				
					
			</div>
		
		
		<div id="replyMessage${reply.id}" class='text comment-text'>
			<r:showReply message="${reply.message}" />
		</div>
		<div class='actions'>
		<c:if test="${isActiveReview}">
		<c:choose>
			<c:when test="${userID==reply.user.id}">
				<a onclick="deleteReply('${reply.id}');" class='comment-action pull-right'><i class='fa fa-trash-o'></i></a>
				<a onclick="editReply('${reply.id}','${reply.issueId}');" id="editReplyBtn${reply.id}" class='comment-action pull-right'><i class='fa fa-edit'></i></a>
			</c:when>
			<c:otherwise>
				<a onclick="replyTo('${reply.user.login}', '${reply.issueId}');" class='comment-action pull-right'><u:message locale='${locale}' key='review.reply'/></a>
			</c:otherwise>
		</c:choose>
		</c:if>
		</div>
	</div>
	<c:if test="${isActiveReview || replyCounter.count!=totalRepliesHistory}"><div class='ui clearing divider'></div></c:if>
</div>
</c:forEach>

<c:if test="${isActiveReview}">
					<form class='ui reply form'>
                	<div class='field'>
                    <div class='form-control commentArea' id='commentArea${issue.id}' contenteditable='true'></div>
                    </div>
                    <div onclick="sendReply('${issue.id}');" class='btn pull-right'>
                    <i class='fa fa-edit'></i> <u:message locale='${locale}' key='review.btn.reply'/></div>
                    </form>
</c:if>

</div>
</div>

<script>
debugger;

$('#comments${issue.id}').on('shown.bs.collapse', function(){
		$('#commentArea${issue.id}').html('@${issue.userLogin}, ');
		placeCaretAtEnd(document.getElementById('commentArea${issue.id}') );
});

setUsersForReplyByIssueId(${issue.id}, '#commentArea${issue.id}');

$('#commentArea${issue.id}').attr('contenteditable', true);

$('[data-toggle="popover"]').popover({container: 'body'});



//TO DO insert likes from allReplies
    
</script>