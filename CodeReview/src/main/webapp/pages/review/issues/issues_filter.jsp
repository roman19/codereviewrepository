<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>
<div class="row" style="padding-top: 10px;">
    <div class="col-xs-6">
        <div class="ui selection dropdown filter-issue" id="select-code-lang">
            <input type="hidden" name="lang" value="0">
            <i class="dropdown icon"></i>
            <div class="default text"><u:message locale='${locale}' key='review.issues.filter.languages'/></div>
            <div class="menu">
                <div class="item" data-value="0" data-text="<u:message locale='${locale}' key='review.issues.filter.languages'/> &nbsp;&nbsp;<b> <u:message locale='${locale}' key='review.issues.filter.all'/></b>">
                    <u:message locale='${locale}' key='review.issues.filter.all'/>
                </div>
                <c:if test="${not empty languages}">
                    <c:forEach var="language" items="${languages}">
                        <div class="item" data-value="${language.id}" data-text="<u:message locale='${locale}' key='review.issues.filter.language'/> &nbsp;&nbsp;<b> <c:out value="${language.name}"/></b>">
                            <c:out value="${language.name}"/>
                        </div>
                    </c:forEach>
                </c:if>
            </div>
        </div>
        <div class="ui selection dropdown filter-issue" id="select-issue-type">
            <input type="hidden" name="issue_type" value="all">
            <i class="dropdown icon"></i>
            <div class="default text"><u:message locale='${locale}' key='review.issues.filter.issue_type'/></div>
            <div class="menu">
                <div class="item" data-value="all" data-text="Issue type &nbsp;&nbsp;&nbsp;<b> <u:message locale='${locale}' key='review.issues.filter.all'/></b>">
                    <u:message locale='${locale}' key='review.issues.filter.all'/>
                </div>
                <div class="item" data-value="error" data-text="<u:message locale='${locale}' key='review.issues.filter.issue_type'/> &nbsp;&nbsp;&nbsp;<i class='fa fa-exclamation-circle issue-sign'></i><b> <u:message locale='${locale}' key='review.btn.error'/></b>">
                    <i class='fa fa-exclamation-circle issue-sign'></i> <u:message locale='${locale}' key='review.btn.error'/>
                </div>
                <div class="item" data-value="warning" data-text="<u:message locale='${locale}' key='review.issues.filter.issue_type'/> &nbsp;&nbsp;&nbsp;<i class='fa fa-exclamation-triangle issue-sign'></i><b> <u:message locale='${locale}' key='review.btn.warning'/></b>">
                    <i class='fa fa-exclamation-triangle issue-sign'></i> <u:message locale='${locale}' key='review.btn.warning'/>
                </div>
                <div class="item" data-value="info" data-text="<u:message locale='${locale}' key='review.issues.filter.issue_type'/> &nbsp;&nbsp;&nbsp;<i class='fa fa-info-circle issue-sign'></i><b> <u:message locale='${locale}' key='review.btn.info'/></b>">
                    <i class='fa fa-info-circle issue-sign'></i> <u:message locale='${locale}' key='review.btn.info'/>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    (function () {
        $('#select-code-lang').dropdown({
            onChange: function() {
                filterIssues();
            }
        });
        $('#select-issue-type').dropdown({
            onChange: function() {
                filterIssues();
            }
        });
    })();

    function filterIssues() {
        var langId = $('#select-code-lang').dropdown('get value');
        var issueType = $('#select-issue-type').dropdown('get value');
        $.post("${pageContext.servletContext.contextPath}/review/issues/filter", {
            reviewId: $("#reviewIdForChat").val(),
            langId: langId,
            issueType: issueType
        }).done(function (data) {
            if (data == null) {
                $('#tab_content').html("<h1 style='color:#E3E3E3'><u:message locale='${locale}' key='review.issues.load_failed'/></h1>");
            } else {
                $('#issues_list').html(data);
            }
        }).fail(function () {
            alert("<u:message locale='${locale}' key='review.issues.load_failed'/>");
        });
    }
</script>
