<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>
<%@ taglib prefix="i" uri="../../../WEB-INF/showImageUrl.tld"%>
<%@ taglib prefix="r" uri="../../../WEB-INF/showReply.tld"%>
<%@ taglib prefix="s" uri="../../../WEB-INF/showTime.tld"%>

<style>



.replylikeup{
	color: #CDCDCD;
}

.replylikedown{
	color: #CDCDCD;
}

.replylikedown:hover {
	color: red;
}

.replylikeup:hover {
	color: green;
}
.green{
	color:rgb(23, 160, 94);
}

.red{
	color: rgb(222, 81, 69);
}

</style>

<div id="reply${reply.id}" class='comment'>
	<a class='reply-ava'> <img
		src="<i:img-url fromServer="${reply.user.imageFromServer}" url="${reply.user.imageUrl}" />"
		alt="${ntfn.notification.user.login}" />
	</a>
	<div class='content'>
		<a class='author'>${reply.user.firstName} ${reply.user.lastName}</a>
		<div class='metadata'>
			<span class='date'><s:showTime locale="${locale}" time="${reply.time}"/></span>
		</div>
		
		
		
		
		
			<div style="float: right;">
			
			
			<a   onclick='onLikeDownReply(${reply.id})' class="down" style='float: right; margin-right: 15px;'>
								<i id='like-down-reply${reply.id}' class='fa fa-thumbs-down fa my-like my-like-down replylikedown'>
									<span id='total-reply-dislikes${reply.id}' style='font-size: 10px;'>0</span></i></a> 
										
								<a   onclick='onLikeUpReply(${reply.id})' class="up" style='float: right; margin-right: 15px;'>
								<i id='like-up-reply${reply.id}' class='fa fa-thumbs-up fa my-like my-like-up replylikeup'>
									<span id='total-reply-likes${reply.id}' style='font-size: 10px;'>0</span></i></a>
			
			
			
<%-- 						<c:choose> --%>
<%-- 						  <c:when test="${isActiveReview}"> --%>
<%-- 						  		<a   onclick='onLikeDownReply(${reply.id})' class="down" style='float: right; margin-right: 15px;'> --%>
<%-- 								<i id='like-down-reply${reply.id}' class='fa fa-thumbs-down fa my-like my-like-down replylikedown'> --%>
<%-- 									<span id='total-reply-dislikes${reply.id}' style='font-size: 10px;'>0</span></i></a>  --%>
										
<%-- 								<a   onclick='onLikeUpReply(${reply.id})' class="up" style='float: right; margin-right: 15px;'> --%>
<%-- 								<i id='like-up-reply${reply.id}' class='fa fa-thumbs-up fa my-like my-like-up replylikeup'> --%>
<%-- 									<span id='total-reply-likes${reply.id}' style='font-size: 10px;'>0</span></i></a> --%>
<%-- 						  </c:when> --%>
						  
<%-- 						  <c:otherwise> --%>
<%-- 						     	<a   onclick='onLikeDownReply(${reply.id})' disabled=’disabled’ class="down" style='float: right; cursor: default;pointer-events: none; margin-right: 15px;'> --%>
<%-- 								<i id='like-down-reply${reply.id}' class='fa fa-thumbs-down fa my-like my-like-down replylikedown'> --%>
<%-- 									<span id='total-reply-dislikes${reply.id}' style='font-size: 10px;'>0</span></i></a>  --%>
										
<%-- 								<a   onclick='onLikeUpReply(${reply.id})' disabled=’disabled’ class="up" style='float: right;cursor: default;pointer-events: none; margin-right: 15px;'> --%>
<%-- 								<i id='like-up-reply${reply.id}' class='fa fa-thumbs-up fa my-like my-like-up replylikeup'> --%>
<%-- 									<span id='total-reply-likes${reply.id}' style='font-size: 10px;'>0</span></i></a> --%>
<%-- 						  </c:otherwise> --%>
<%-- 						</c:choose> --%>
			</div>
		
		<div id="replyMessage${reply.id}" class='text comment-text'>
			<r:showReply message="${reply.message}" />
		</div>
		<div class='actions'>
			<a onclick="deleteReply('${reply.id}');" class='comment-action pull-right'><i class='fa fa-trash-o'></i></a>
			<a onclick="editReply('${reply.id}','${reply.issueId}');" id="editReplyBtn${reply.id}" class='comment-action pull-right'><i class='fa fa-edit'></i></a>
		</div>
		
		
		
		
	</div>
<div class='ui clearing divider'></div>
</div>


<script>




function onLikeUpReply(replyId){
	
	debugger;
	var id=replyId;

	 if($("#like-up-reply"+id).css('color')!="rgb(23, 160, 94)"){							 
		    if($("#like-down-reply"+id).css('color')=="rgb(222, 81, 69)") {
		    	
		    		
		   	 	$("#like-down-reply"+id).attr("class", "fa fa-thumbs-down fa my-like my-like-down replylikedown");
		    
		   	 		
		    	$("#like-up-reply"+id).attr("class", "fa fa-thumbs-up fa my-like my-like-up green");
		    	
		    
		    	 
		    //decrement like-down and increment like-up
		    	 $.get('${pageContext.servletContext.contextPath}/decrementLikeDownReply',{ReplyId:id},function(response) { 
			    	       		 $('#total-reply-dislikes'+id).text(response);     			    	        
		    	        $.get('${pageContext.servletContext.contextPath}/incrementLikeUpReply',{ReplyId:id},function(response) { 
				    	        $('#total-reply-likes'+id).text(response);         
			    	    });			    	        
		    	    });			    	 
		     } else{			    	 
		    	
		    	 
		    	 $("#like-up-reply"+id).attr("class", "fa fa-thumbs-up fa my-like my-like-up green");
		    	 
		    	 
		    	//increment like-up 
		    	 $.get('${pageContext.servletContext.contextPath}/incrementLikeUpReply',{ReplyId:id},function(response) { 
				    	        $('#total-reply-likes'+id).text(response);         
		    	    });			     		     
		     }	  				 
		}
		else{
					
			 $("#like-up-reply"+id).attr("class", "fa fa-thumbs-up fa my-like my-like-up replylikeup");
			//decrement like-up
			 $.get('${pageContext.servletContext.contextPath}/decrementLikeUpReply',{ReplyId:id},function(response) { 
				 console.log("response = "+response);    
				 $('#total-reply-likes'+id).text(response);         
	    	    });
		}
          
	
}


function onLikeDownReply(replyId){
		
		debugger;
		var id=replyId;
		
		 if($("#like-down-reply"+id).css('color')!="rgb(222, 81, 69)"){								 
			    if($("#like-up-reply"+id).css('color')=="rgb(23, 160, 94)") {
			    	
			    
			    	
			    	$("#like-down-reply"+id).attr("class", "fa fa-thumbs-down fa my-like my-like-down red");
			    	 
			    	
			    	 $("#like-up-reply"+id).attr("class", "fa fa-thumbs-up fa my-like my-like-up replylikeup");
			    
			    	 
			    //decrement like-up and increment like-down
			    	 $.get('${pageContext.servletContext.contextPath}/decrementLikeUpReply',{ReplyId:id},function(response) { 
 			    	       		 $('#total-reply-likes'+id).text(response);     			    	        
			    	        $.get('${pageContext.servletContext.contextPath}/incrementLikeDownReply',{ReplyId:id},function(response) { 
 				    	        $('#total-reply-dislikes'+id).text(response);         
				    	    });			    	        
			    	    });			    	 
			     } else{			    	 
			    	    	 
			    	 $("#like-down-reply"+id).attr("class", "fa fa-thumbs-down fa my-like my-like-down red");
			    	 
			    	//increment like-down 
			    	 $.get('${pageContext.servletContext.contextPath}/incrementLikeDownReply',{ReplyId:id},function(response) { 
   				    	        $('#total-reply-dislikes'+id).text(response);         
			    	    });			     		     
			     }	  				 
			}
			else{
				
				 $("#like-down-reply"+id).attr("class", "fa fa-thumbs-down fa my-like my-like-down replylikedown");
				//decrement like-down
				 $.get('${pageContext.servletContext.contextPath}/decrementLikeDownReply',{ReplyId:id},function(response) { 
					 console.log("response = "+response);    
					 
 		    	        $('#total-reply-dislikes'+id).text(response);         
		    	    });
			}
              
    	
    }
    

</script>