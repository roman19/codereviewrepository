<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>

<!DOCTYPE html>
<html>
<link rel="icon" type="image/png" href="${pageContext.servletContext.contextPath}/resources/images/codereview_icon.png">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" href="resources/css/index/socialButtons.css">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- ******************* FILE UPLOAD ****************** -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

<!-- ****************** NOTIFICATIONS ****************** -->

<script src="resources/bootstrap_notify/bootstrap-notify.js"></script>
<link rel="stylesheet" href="resources/bootstrap_notify/animate.css">

<!-- ************************************************** -->

<script type="text/javascript" src="resources/js/index/index.js"></script>
<script type="text/javascript" src="resources/js/main.js"></script>

<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/resources/css/main.css">

<link rel="stylesheet" href="resources/css/index/style.css">

<link href='https://fonts.googleapis.com/css?family=Lato:400,300'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/semantic_ui/css/semantic.css"/>"/>
<title><u:message locale="${locale}" key="index.password.restore"/></title>
</head>
<body>
	<div class="mainBlock">

		<jsp:include page="header.jsp"></jsp:include>

		<h1 style="margin-top: 50px;">we make your code review easier.</h1>
		
		<div class="container-fluid">
				<div class="reset-form">

						<p id="newp"><u:message locale="${locale}" key="index.password.restore"/></p>
						<form id="newPasswordForm" method="post" action="newPassword"
							onsubmit="return validateNewPasswordForm()">
							<input name="email" value="${email}" type="hidden">
							<input
								name="password1" type="password" class="form-control"
								placeholder="<u:message locale="${locale}" key="index.password.enter"/>" required data-toggle="tooltip" data-placement="right" data-original-title="<u:message locale="${locale}" key="registration.tooltip.password"/>"> <input
								name="password2" type="password" class="form-control"
								placeholder="<u:message locale="${locale}" key="registration.p.cpassword"/>" required data-toggle="tooltip" data-placement="left" data-original-title="<u:message locale="${locale}" key="registration.tooltip.cpassword"/>">
								<div id="error"></div>
							<button type="submit" class="form-control btn btn-default">Ok</button>
						</form>

				</div>


			</div>
	</div>

	<jsp:include page="../footer.jsp"></jsp:include>

</body>


</html>

