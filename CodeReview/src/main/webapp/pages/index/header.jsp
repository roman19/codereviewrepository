<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>


<header>
    <div class="logo">
        <a href="${pageContext.servletContext.contextPath}/">
            <img
                    src="${pageContext.servletContext.contextPath}/resources/images/logo.png"
                    alt="logo">
            <p class="logo-text">Code Review</p>
        </a>


    </div>

    <c:choose>
        <c:when test="${userID == null}">
            <div class="rightHeader">
                <a style="color:white;" href="#step1"><u:message locale="${locale}" key="index.howItWorks" /></a> <a href="#" class="sign_"
                                                                                                                     data-toggle="modal" data-target="#signinModal"><u:message
                    locale="${locale}" key="signin" /></a> <a href="#" class="sign_"
                                                              data-toggle="modal" data-target="#signupModal"><u:message
                    locale="${locale}" key="signup" /></a>
            </div>
        </c:when>
        <c:otherwise>
            <div class="rightHeader">

                <a id="settings_login" href="${pageContext.servletContext.contextPath}/cabinet" style="color:white">${login} </a>

                <div class="cabinet-icon">
                    <img
                            src="${pageContext.servletContext.contextPath}/resources/images/cabinet/profile.png"
                            alt="logo">
                </div>

                <a href="${pageContext.servletContext.contextPath}/logout"
                   class="sign_"><u:message locale="${locale}" key="logout" /></a>
            </div>
        </c:otherwise>
    </c:choose>
</header>
