<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld" %>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" type="image/png"
          href="${pageContext.servletContext.contextPath}/resources/images/codereview_icon.png">
    <link rel="stylesheet"
          href="${pageContext.servletContext.contextPath}/resources/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/index/socialButtons.css">

    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/fullPage.js-master/jquery.fullPage.css"/>"/>

    <script src="<c:url value="/resources/js/jquery-2.2.0.min.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/resources/fullPage.js-master/jquery.fullPage.min.js"/>" type="text/javascript"></script>

    <script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>

    <!-- ******************* FILE UPLOAD ****************** -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

    <!-- ****************** NOTIFICATIONS ****************** -->

    <script src="resources/bootstrap_notify/bootstrap-notify.js"></script>
    <link rel="stylesheet" href="resources/bootstrap_notify/animate.css">

    <!-- ************************************************** -->

    <script type="text/javascript" src="resources/js/index/index.js"></script>
    <script type="text/javascript" src="resources/js/main.js"></script>

    <link rel="stylesheet"
          href="${pageContext.servletContext.contextPath}/resources/css/main.css">

    <link rel="stylesheet" href="resources/css/index/style.css">

    <link href='https://fonts.googleapis.com/css?family=Lato:400,300'
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/semantic_ui/css/semantic.css"/>"/>

    <title>Welcome to CodeReview</title>
    <style>
        html, body {
            height: 100%;
        }

        .mainBlock .fp-tableCell {
            display: block !important;
        }

        .slide .fp-tableCell {
            vertical-align: middle;
            padding-bottom: 100px;
        }

        .section img.img-border {
            padding: 5px;
            box-shadow: 0 1px 3px rgba(34, 25, 25, 0.4);
            -moz-box-shadow: 0 1px 2px rgba(34, 25, 25, 0.4);
            -webkit-box-shadow: 0 1px 3px rgba(34, 25, 25, 0.4);
        }

        #fp-nav {
            top: 30%;
        }

        header .rightHeader a:hover, header .rightHeader a:visited, header .rightHeader a:active {
            color: white;
        }
    </style>
</head>
<body>
<div id="fullpage">
    <div class="section mainBlock">
        <jsp:include page="header.jsp"></jsp:include>
        <h1 style="margin-top: 40px;">we make your code review easier.</h1>

        <div id="back">
            <img style="width: 100%" src="<c:url value="/resources/images/bg_img1.jpg"/>">
        </div>
        <div id="social-login"><u:message locale="${locale}" key="index.loginWith"/></div>

        <div class="container">
            <div class="onl_login">
                <div class="row onl_row-sm-offset-3 onl_socialButtons">
                    <div class="col-xs-2 col-sm-2">
                        <a href="githubLogin" class="btn btn-lg btn-block onl_btn-github" data-toggle="tooltip"
                           data-placement="top" title="GitHub">
                            <i class="fa fa-github fa-2x"></i>
                            <span class="hidden-xs"></span>
                        </a>
                    </div>
                    <div class="col-xs-2 col-sm-2">
                        <a href="googleLogin"
                           class="btn btn-lg btn-block onl_btn-google-plus"
                           data-toggle="tooltip" data-placement="top" title="Google Plus">
                            <i class="fa fa-google-plus fa-2x"></i> <span class="hidden-xs"></span>
                        </a>
                    </div>
                    <div class="col-xs-2 col-sm-2">
                        <a href="bitbucketLogin" class="btn btn-lg btn-block onl_btn-gitbucket" data-toggle="tooltip"
                           data-placement="top" title="BitBucket">
                            <i class="fa fa-bitbucket fa-2x"></i>
                            <span class="hidden-xs"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <h1 style="text-align: center; color: white; margin-top: 0;">
            <u:message locale="${locale}" key="index.gide.register.title"/> <br><br></h1>

        <h3 style="text-align: center; color: white;">
            <u:message locale="${locale}" key="index.gide.register.content"/></h3>

        <div style="text-align: center; padding-top: 30px;">
            <img class="img-border" src="${pageContext.servletContext.contextPath}/resources/images/gide/signup.png"
                 alt="">
            <span style="color: white; font-size: 24px; padding: 15px;"><i class="fa fa-arrows-h"></i></span>
            <img class="img-border" src="${pageContext.servletContext.contextPath}/resources/images/gide/aoth2.png"
                 alt="">
        </div>
    </div>
    <div class="section">
        <h1 style="text-align: center; color: white; margin-top: 0;">
            <u:message locale="${locale}" key="index.gide.group.title"/><br><br></h1>

        <h3 style="text-align: center; color: white;">
            <u:message locale="${locale}" key="index.gide.group.content1"/><br>
            <u:message locale="${locale}" key="index.gide.group.content2"/>
        </h3>

        <div style="text-align: center; padding-top: 30px;">
            <img class="img-border" src="${pageContext.servletContext.contextPath}/resources/images/gide/group.png"
                 alt="">
            <span style="color: white; font-size: 24px; padding: 10px;"><i class="fa fa-long-arrow-right"></i></span>
            <img class="img-border"
                 src="${pageContext.servletContext.contextPath}/resources/images/gide/create_group.png" alt="">
            <span style="color: white; font-size: 24px; padding: 10px;"><i class="fa fa-long-arrow-right"></i></span>
            <img class="img-border"
                 src="${pageContext.servletContext.contextPath}/resources/images/gide/group_dialog.png" alt="">
        </div>
    </div>
    <div class="section">
        <h1 style="text-align: center; color: white; padding-top: 30px; margin-top: 0;">
            <u:message locale="${locale}" key="index.gide.review.title"/><br></h1>

        <div class="slide">
            <h3 style="text-align: center; color: white;">
                <u:message locale="${locale}" key="index.gide.review.content1"/></h3>

            <div style="text-align: center; padding-top: 30px;">
                <img class="img-border"
                     src="${pageContext.servletContext.contextPath}/resources/images/gide/review_create.png" alt=""
                     style="width: 700px">
            </div>
        </div>
        <div class="slide">
            <h3 style="text-align: center; color: white;">
                <u:message locale="${locale}" key="index.gide.review.content2"/><br>
                <u:message locale="${locale}" key="index.gide.review.content3"/></h3>

            <div style="text-align: center; padding-top: 30px;">
                <img class="img-border"
                     src="${pageContext.servletContext.contextPath}/resources/images/gide/review_cr2.png" alt=""
                     style="width: 550px">
            </div>
        </div>
    </div>
    <div class="section">
        <h1 style="text-align: center; color: white; padding-top: 30px; margin-top: 0;"><u:message locale="${locale}"
                                                                                                   key="index.gide.progress.title"/><br>
        </h1>

        <div class="slide">
            <h3 style="text-align: center; color: white;">
                <u:message locale="${locale}" key="index.gide.progress.content1"/></h3>

            <div style="text-align: center; padding-top: 30px;">
                <img class="img-border"
                     src="${pageContext.servletContext.contextPath}/resources/images/gide/review_users.png" alt=""
                     style="width: 650px">
                <span style="color: white; font-size: 36px; padding: 15px;">&nbsp;</span>
                <img class="img-border"
                     src="${pageContext.servletContext.contextPath}/resources/images/gide/review_statistics.png" alt=""
                     style="width: 550px">
            </div>
        </div>
        <div class="slide">
            <h3 style="text-align: center; color: white;">
                <u:message locale="${locale}" key="index.gide.progress.content2"/></h3>

            <div style="text-align: center; padding-top: 30px;">
                <img class="img-border"
                     src="${pageContext.servletContext.contextPath}/resources/images/gide/issues_tab.png" alt=""
                     style="width: 950px">
            </div>
        </div>
        <div class="slide">
            <h3 style="text-align: center; color: white;">
                <u:message locale="${locale}" key="index.gide.progress.content3"/></h3>

            <div style="text-align: center; padding-top: 30px;">
                <img class="img-border"
                     src="${pageContext.servletContext.contextPath}/resources/images/gide/review_issues_notification.png"
                     alt=""
                     style="width: 950px">
            </div>
        </div>
        <div class="slide">
            <h3 style="text-align: center; color: white;">
                <u:message locale="${locale}" key="index.gide.progress.content4"/></h3>

            <div style="text-align: center; padding-top: 30px;">
                <img class="img-border"
                     src="${pageContext.servletContext.contextPath}/resources/images/gide/review_file_issues.png" alt=""
                     style="width: 950px">
            </div>
        </div>
        <div class="slide">
            <h3 style="text-align: center; color: white;">
                <u:message locale="${locale}" key="index.gide.progress.content5"/>
            </h3>

            <div style="text-align: center; padding-top: 30px;">
                <img class="img-border" src="${pageContext.servletContext.contextPath}/resources/images/gide/chat.png"
                     alt=""
                     style="width: 950px">
            </div>
        </div>
    </div>
    <div class="section fp-auto-height">
        <jsp:include page="../footer.jsp"></jsp:include>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#fullpage').fullpage({
            anchors: ['index', 'step1', 'step2', 'step3', 'step4', 'footer'],
//        sectionsColor: ['yellow', 'orange', '#C0C0C0', '#ADD8E6']
            sectionsColor: ['#F2F2F2', '#74BEAD', '#7FB1E6', '#A6DC6D', '#D9BABF', ''],
            navigation: true,
            scrollingSpeed: 700
        });
    });
</script>

<!-- Sign in modal -->
<div class="modal fade bd-example-modal-sm" id="signinModal"
     tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 style="margin-top:0">
                    <i class="glyphicon glyphicon-user"> </i>
                    <u:message locale="${locale}" key="accountAccess"/>
                </h4>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-11">
                        <div style="padding: 20px;" id="form-olvidado">
                            <form id="signinForm" action="signin" method="post"
                                  onsubmit="return checkSigninForm()">
                                <div class="form-group input-group">
											<span class="input-group-addon"> <i
                                                    class="glyphicon glyphicon-user"> </i>
											</span> <input class="form-control"
                                                           placeholder="<u:message locale="${locale}" key="login.login" />"
                                                           name="login" type="text" required autofocus>
                                </div>
                                <div class="form-group input-group">
											<span class="input-group-addon"> <i
                                                    class="glyphicon glyphicon-lock"> </i>
											</span> <input class="form-control"
                                                           placeholder="<u:message locale="${locale}" key="login.password" />"
                                                           name="password" type="password" required>
                                </div>
                                <div id="error"></div>
                                <div class="form-group">

                                    <button type="submit" class="btn btn-primary btn-block">
                                        <u:message locale="${locale}" key="signin"/>
                                    </button>
                                    <p class="help-block">
                                        <a class="pull-right text-muted" href="#" id="olvidado">
                                            <small><u:message
                                                    locale="${locale}" key="forgotPassword"/></small>
                                        </a>

                                    </p>
                                </div>
                            </form>
                        </div>

                        <div style="display: none;" id="form-olvidado1">
                            <h4 class="">
                                <u:message locale="${locale}" key="forgotPassword"/>
                            </h4>

                            <form action="forgotPassword" id="forgotForm" method="post"
                                  onsubmit="return checkForgotForm()">
										<span class="help-block"> <u:message locale="${locale}"
                                                                             key="weWillSend"/>
										</span>

                                <div class="form-group input-group">
                                    <span class="input-group-addon"> @ </span> <input
                                        class="form-control" placeholder="Email" name="email"
                                        type="email" required>
                                </div>
                                <div id="forgotError"></div>
                                <button type="submit" class="btn btn-primary btn-block"
                                        id="btn-olvidado">
                                    <u:message locale="${locale}" key="continue"/>
                                </button>
                                <p class="help-block" style="padding-bottom:10px">
                                    <a class="text-muted" href="#" id="acceso1">
                                        <small><u:message
                                                locale="${locale}" key="accountAccess"/></small>
                                    </a>

                                </p>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Sign up Modal -->
<div id="signupModal" class="modal fade" role="dialog">
    <div class="modal-dialog sm-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>
                    <span class="glyphicon glyphicon-globe mrgn-right"></span> <u:message locale="${locale}"
                                                                                          key="index.registration"/>
                </h4>
            </div>
            <div class="modal-body">

                <div class="container-fluid">
                    <div class="row">
                        <form id="signupForm" action="signup" method="post" class="form"
                              role="form"
                              onsubmit="return validateForm('signupForm', '${locale}')"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-xs-6 col-md-6">
                                    <input class="form-control" name="name"
                                           placeholder="<u:message locale="${locale}" key="registration.p.name" />"
                                           type="text" autofocus
                                           data-toggle="tooltip" data-placement="left"
                                           data-original-title="<u:message locale="${locale}" key="registration.tooltip.firstName" />"/>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <input class="form-control" name="surname"
                                           placeholder="<u:message locale="${locale}" key="registration.p.surname" />"
                                           type="text" data-toggle="tooltip"
                                           data-placement="right"
                                           data-original-title="<u:message locale="${locale}" key="registration.tooltip.lastName" />"/>
                                </div>
                            </div>
                            <input class="form-control" name="login"
                                   placeholder="<u:message locale="${locale}" key="registration.p.login" />" type="text"
                                   data-toggle="tooltip"
                                   data-placement="right"/> <input class="form-control"
                                                                   name="email"
                                                                   placeholder="<u:message locale="${locale}" key="registration.p.email" />"
                                                                   type="email"
                                                                   data-toggle="tooltip" data-placement="left"
                                                                   data-original-title="<u:message locale="${locale}" key="registration.tooltip.email" />"/>
                            <input id="pwd"
                                   class="form-control" name="password"
                                   placeholder="<u:message locale="${locale}" key="registration.p.password" />"
                                   type="password"
                                   data-toggle="tooltip" data-placement="right"
                                   data-original-title="<u:message locale="${locale}" key="registration.tooltip.password" />"/>
                            <input name="cpassword" class="form-control" type="password"
                                   placeholder="<u:message locale="${locale}" key="registration.p.cpassword" />"
                                   data-toggle="tooltip"
                                   data-placement="left"
                                   data-original-title="<u:message locale="${locale}" key="registration.tooltip.cpassword" />">

                            <div id="imageToolTip"
                                 class="fileinput fileinput-new input-group"
                                 data-provides="fileinput" data-toggle="tooltip"
                                 data-placement="right"
                                 data-original-title="<u:message locale="${locale}" key="registration.tooltip.image" />">
                                <div id="fumes" class="form-control" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i> <span
                                        class="fileinput-filename"></span>
                                </div>
									<span class="input-group-addon btn btn-default btn-file"><span
                                            class="fileinput-new"><u:message locale="${locale}"
                                                                             key="registration.image.select"/></span><span
                                            class="fileinput-exists"><u:message locale="${locale}"
                                                                                key="registration.image.change"/></span> <input
                                            type="file"
                                            name="image" accept="image/*"></span> <a href="#"
                                                                                     class="input-group-addon btn btn-default fileinput-exists"
                                                                                     data-dismiss="fileinput"><u:message
                                    locale="${locale}" key="registration.image.remove"/></a>
                            </div>

                            <input type="submit" class="btn btn-lg btn-primary btn-block"
                                   value="<u:message locale="${locale}" key="signup" />">
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Google Modal -->
<div class="modal fade" id="googleModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body">

                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-3">
                            <img id="google-icon"
                                 src="${pageContext.request.contextPath}/resources/images/google-plus.png"
                                 alt="google-plus">
                        </div>
                        <div class="col-md-9" style="padding-top:20px">
                            <h1>google+ <u:message locale="${locale}" key="index.authorization"/></h1>

                            <div id="googleError"></div>
                        </div>

                    </div>
                    <form id="googleForm" action="${pageContext.request.contextPath}/googleAuthentication"
                          onsubmit="return checkUserByToken('${locale}')" method="post">
                        <input id="token" hidden="true" name="token" type="text"
                               value="">
                        <input id="tokenSubmit" hidden="true" type="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bitbucket Modal -->
<div class="modal fade" id="bitbucketModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body">

                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-3">
                            <img id="bitbucket-icon"
                                 src="${pageContext.request.contextPath}/resources/images/bitbucket_icon.png"
                                 alt="bitbucket">
                        </div>
                        <div class="col-md-9" style="padding-top:20px">
                            <h1>Bitbucket <u:message locale="${locale}" key="index.authorization"/></h1>

                            <div id="bitbucketError"></div>
                        </div>
                    </div>
                    <form id="bitbucketForm" action="${pageContext.request.contextPath}/bitbucketAuthentication"
                          onsubmit="return checkUserByBitbucketToken('${locale}')" method="post">
                        <input id="btoken" hidden="true" name="token" type="text"
                               value="">
                        <input id="btokenSubmit" hidden="true" type="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Github Modal -->
<div class="modal fade" id="githubModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body">

                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-3">
                            <img id="github-icon"
                                 src="${pageContext.request.contextPath}/resources/images/github_icon.png"
                                 alt="git hub">
                        </div>
                        <div class="col-md-9" style="padding-top:20px">
                            <h1>Github <u:message locale="${locale}" key="index.authorization"/></h1>

                            <div id="githubError"></div>
                        </div>

                        <form id="githubForm" action="${pageContext.request.contextPath}/githubAuthentication"
                              onsubmit="return checkUserByGithubCode('${locale}')" method="post">
                            <input id="gitHubCode" hidden="true" name="code" type="text"
                                   value="${code}">
                            <input id="gitHubToken" hidden="true" name="token" type="text"
                                   value="">
                            <input id="githubSubmit" hidden="true" type="submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

<c:if test="${goToCabinet}">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#signInModal').modal('show');
        });
    </script>
</c:if>

<c:if test="${isReg}">
    <script type="text/javascript">
        $.notifyDefaults({
            animate: {
                enter: "animated bounceInDown",
                exit: "animated fadeOutDown"
            }
        });

        $
                .notify(
                {
                    title: '<strong><u:message locale="${locale}" key="registration.completed" /></strong>',
                    message: '',
                    delay: 5000
                }, {
                    type: 'success'
                });
    </script>
</c:if>

<c:if test="${authGoogle}">
    <script type="text/javascript">
        $(document).ready(function () {

            $(window).load(function () {

                $('#googleModal').modal('show');

                var type = window.location.hash.substr(1);
                type = type.split("&")[0].split("=")[1];
                document.getElementById("token").value = type;
                document.getElementById("tokenSubmit").click();
            });

        });
    </script>
</c:if>

<c:if test="${authBitbucket}">
    <script type="text/javascript">
        $(document).ready(function () {

            $(window).load(function () {
                debugger;

                $('#bitbucketModal').fadeOut(2000, function () {

                });

                $('#bitbucketModal').modal('show');

                var type = window.location.hash.substr(1);
                type = type.split("&")[0].split("=")[1];
                document.getElementById("btoken").value = type;
                document.getElementById("btokenSubmit").click();
            });

        });
    </script>
</c:if>

<c:if test="${authGithub}">
    <script type="text/javascript">
        $('#githubModal').on('shown.bs.modal', function () {
            debugger;
            document.getElementById("githubSubmit").click();
        });
        $('#githubModal').modal('show');
    </script>

</c:if>


</html>