<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>
	
	<div class="modal fade" id="codeReviewCreatingModal" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h1 class="modal-title"><u:message locale="${locale}" key="review.creating.title" /></h1>
				</div>
				<div style="overflow: auto; max-height: 400px;">
					<div id="reviewCreatingModalBodyContent" class="modal-body">
						<div id="downloadChoice">
							<input id="downloadChoiceButton" onclick="git()"
								class="btn btn-1 downloadButton" type="button" name=""
								value="<u:message locale="${locale}" key="review.creating.git" />"> <input
								id="archiveButton" onclick="archive()"
								class="btn btn-1 downloadButton" type="button" name=""
								value="<u:message locale="${locale}" key="review.creating.archive" />">
						</div>
					</div>
					<div id="anotherContent">
						<div id="tree"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><u:message locale="${locale}" key="review.creating.exit" /></button>
				</div>
			</div>
		</div>
	</div>