<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<style>
	
	.typeahead{
		min-width: 314px;
		max-width: 314px;
		overflow: hidden;	
		min-height: 10px;
		max-height:300px;
	}
	
	ul.typeahead.dropdown-menu li{
		margin-left:0px;
		margin-bottom:0px; 
	}
	
	
	.user img{
		float:left;
		max-width: 50px;
		max-height: 50px;
		min-width:50px;
		min-height:50px;
		margin-left: 5px
	}
	 .user .login{
	 float:right;
	 margin-left: 5px;
	}
	.user .lname{
	float:right;
	margin-left: 5px;
	 }
	 .user .fname{
	float:right;
	margin-left: 10px;
	}

	.vertical-center {
	  min-height: 1%;  
 	  min-height: 1vh; 
      display: flex;
      align-items: center;
      }
	</style>
               
         <div class="row">  
                <div class="col-sm-4">
                	
                    <input type="text" style="margin-top:20px;" id="nameOfGroup" class="form-control"  required placeholder="Enter name of group" />
                    <div class="ui basic red pointing prompt label transition visible">Name of group can't be blank</div></div>
                <span>
                  <button id="cancel" type="button" style="margin-bottom: 15px;   background-color:#DE5246;width: 100px" class="btn">CANCEL</button>
                  <button id="send" type="button" style="margin-bottom: 15px;width: 100px;   background-color:#17A05E" class="btn">CONFIRM</button>
          		</span>
                    <input id="search"
                        type="text"
                        class="form-control typeahead"
                        placeholder="Who is You looking for?"
                        autocomplete="off"
                        data-provide="typeahead"
                    />
                    <br/>
                    
                    
                  
                    
                    <input type="hidden" class="form-control" name="userId" id="userId" value="" />
            </div>


	 <div class="col-sm-7">




 <table  id="groups_table"class="table table-striped" style="margin-top: 20px; margin-left: 50px">
    <thead>
      <tr >
 		<th style=" text-align: center;"><b>Photo</b></th>
        <th style=" text-align: center;"><b>Firstname</b></th>
        <th style=" text-align: center;"><b>Lastname</b></th>
        <th style=" text-align: center;"><b>Login</b></th>
        <th style=" text-align: center;"><b>Action</b></th>
      </tr>
    </thead>
    <tbody>
          
    </tbody>
  </table>

</div>
</div>     

		
		
		
		<script type="text/javascript">

        $(function(){
        	
            var userObjs = {};
            var userNames = [];
			var selectedUsers=[];
 
            var throttledRequest = _.debounce(function(query, process){

                $.ajax({
                    url: 'cabinet/addgroup/SearchController'
                    ,cache: false
                    ,success: function(data){

                        userObjs = {};
                        userNames = [];

                        _.each( data, function(item, ix, list){
                            
                            
                            console.log(selectedUsers);
                            
                            if($.inArray(item.id,selectedUsers) === -1) {
									
                            	userNames.push(item.totalSearchedName);
                            	
								}

                            userObjs[item.totalSearchedName] = item;

                        });

                        process( userNames );

                    }
                });
            }, 300);


            $(".typeahead").typeahead({      
            	 input: "#search",
            	hint: true,
            	source: function ( query, process ) {
                    throttledRequest( query, process );

                }
        ,highlighter: function( item ){
          var user = userObjs[ item ];
          
          return '<div class="row  user  vertical-center">'
          	
                +'<div class=""> <img class="img-circle" src="' + user.photo + '" /></div>'
                +'<div class="fname">' + user.fname + '</div>'
                +'<div class="lname">' + user.lname + '</div>'
                +'<div class="login">' + user.login + '</div>'         

             +'</div>'
        }
                
        ,updater: function ( selectedName ) {

                      $( "#userId" ).val( userObjs[ selectedName ].id );
						
            			var userId=$( "#userId" ).val();		
     
            			 $.get("cabinet/addgroup/AddToTable",{id:userId}, function(user) { 
            				 debugger;
                    		selectedUsers.push(user.id);                  		
            				 $("#groups_table tr:last").after("<tr id='row"+user.id+"'><td> <img style='max-width: 35px; max-height: 25px;' class='img-circle'src="+user.photo+" /></td><td>"+user.fname+"</td> <td>"+user.lname+"</td><td>"+user.login+"</td><td> <button class='deleteUsers btn' type='button'   id='"+user.id+"' style='color:white;background-color: #1E8FC9;'  ><b>X</b></button>   </td></tr>");	           				
            			 });

                      return "";
                  }
                
           
            });

            $('#send').on("click",function(){
        		
            	var jsonObj={
            			name : $("#nameOfGroup").val(),
            			users : selectedUsers
            				
            			
            	}          
            	
            	
       		 				 $.get("cabinet/addgroup/SaveToDB",{group :JSON.stringify(jsonObj)}, function(response) {
       		 									socket.send(JSON.stringify(response));
       		 									//$("#switchContent").html(user);

       		 			});

       			});		
            
            $(document).on('click', "button.deleteUsers", function() {
            	
            	var toDelete = $(this).attr("id");
            	
            	$('#row'+toDelete).remove();
            	
            	var index=selectedUsers.indexOf(parseInt(toDelete));
            	selectedUsers.splice(index,1);
     
            });
            
        });
        
        
    </script>		
		
		
		
		
		
		
		
		
		