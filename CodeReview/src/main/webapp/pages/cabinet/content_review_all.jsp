<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<style>
.truncate {
  width: 200px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}


</style>

<fmt:setLocale value="${locale}"/>


<c:choose>
    <c:when test="${(fn:length(listReviews) gt 0) || (fn:length(deletedReviews) gt 0)}">
       




	<div class="ui cards">
		<c:forEach var="j" items="${deletedReviews}">
			<div id="deletedReview${j.id}" class="card"
				style="margin-top: 30px; margin-left: 35px; border: 1px solid #F3321D">
				<div class="content">

					<c:choose>
						<c:when test="${j.isGitProject == true}">
							<img class="right floated mini ui image"
								src="${pageContext.servletContext.contextPath}/resources/images/cabinet/git.png">
						</c:when>
						<c:otherwise>
							<img class="right floated mini ui image"
								src="${pageContext.servletContext.contextPath}/resources/images/cabinet/zip.png">
						</c:otherwise>
					</c:choose>
					<p class="right floated" style="font-size: 14px; color: #F3321D">
						<b><u:message locale="${locale}" key="review.content.deleled" /></b>
					</p>

					<div class="header truncate">
						<p>${j.name}</p>
					</div>

					<div class="meta truncate">
						<u:message locale="${locale}" key="review.content.owner" />: <b>${j.userName}</b>
					</div>

					<div class="meta">
						<u:message locale="${locale}" key="review.content.created" />:
						<fmt:formatDate type="date" value="${j.creationDate}" />
					</div>
					<div class="description">

						<p class="black-text">
							<b><u:message locale="${locale}" key="review.content.reviewcomplete" /> <fmt:formatNumber type="number"
									maxFractionDigits="0" value="${j.coverageAvgPercent}" />%
								(<u:message locale="${locale}" key="review.content.total" />)
							</b>
						</p>
						<div class="progress" style="height: 20px;">
							<div id="progressBar" class="progress-bar progress-bar-success "
								style="background-color: #54B1B9;  width: ${j.coverageAvgPercent}%; ">
								<fmt:formatNumber type="number" maxFractionDigits="0"
									value="${j.coverageAvgPercent}" />
								%
							</div>
						</div>
					</div>
					<div class="extra pull-right">
						<a class="btn" style="background-color: #F3321D; color: white"
							href="javascript:void(0);" onclick="hideDeletedReview(${j.id})"><u:message locale="${locale}" key="review.content.hide" /></a>
					</div>
				</div>

			</div>

		</c:forEach>

		<c:forEach var="i" items="${listReviews}">




			<c:choose>
				<c:when test="${userID == i.userId}">
					<div id="review${i.codeReviewId}" class="card"
						style="margin-top: 30px; margin-left: 35px; border: 1px solid #F2711C">
						<div class="content">

							<c:choose>
								<c:when test="${i.gitURL != null}">
									<img class="right floated mini ui image"
										src="${pageContext.servletContext.contextPath}/resources/images/cabinet/git.png">
								</c:when>
								<c:otherwise>

									<img class="right floated mini ui image"
										src="${pageContext.servletContext.contextPath}/resources/images/cabinet/zip.png">

								</c:otherwise>
							</c:choose>

							<div class="header truncate">
								<a href='review/${i.codeReviewId}'>${i.name}</a>
							</div>

							<div class="meta">
								<u:message locale="${locale}" key="review.content.owner" />: <b><u:message locale="${locale}" key="review.content.you" /></b>
							</div>
							<div class="meta"><u:message locale="${locale}" key="review.content.totalissue" />: ${i.totalIssues}</div>
							<div class="meta">
								<u:message locale="${locale}" key="review.content.created" />:
								<fmt:formatDate type="date" value="${i.creationDate}" />
							</div>
							<div class="description">

								<p class="black-text">
									<b><u:message locale="${locale}" key="review.content.reviewcomplete" /><fmt:formatNumber type="number"
											maxFractionDigits="0" value="${i.coverageAvgPercent}" />%
										(<u:message locale="${locale}" key="review.content.total" />)
									</b>
								</p>
								<div class="progress" style="height: 20px;">
									<div id="progressBar"
										class="progress-bar progress-bar-success "
										style="background-color: #54B1B9;  width: ${i.coverageAvgPercent}%; ">
										<fmt:formatNumber type="number" maxFractionDigits="0"
											value="${i.coverageAvgPercent}" />
										%
									</div>
								</div>



							</div>
							<div class="extra pull-right">
								<a class="btn" style="background-color: #F2711C; color: white"
									href="javascript:void(0);" onclick="finishReview(${i.codeReviewId})"><u:message locale="${locale}" key="review.content.finish" /></a>
							</div>
						</div>

					</div>

				</c:when>

				<c:otherwise>



					<c:choose>
						<c:when test="${i.reviewerStatus == 'CONFIRMED' }">
							<c:choose>
								<c:when test="${i.active == false}">
									<div id="review${i.codeReviewId}" class="card"
										style="margin-top: 30px; margin-left: 35px; border: 1px solid #6DF596; background-color: #CBF5D4">
										<div class="content">

											<c:choose>
												<c:when test="${i.gitURL != null}">
													<img class="right floated mini ui image"
														src="${pageContext.servletContext.contextPath}/resources/images/cabinet/git.png">
												</c:when>
												<c:otherwise>

													<img class="right floated mini ui image"
														src="${pageContext.servletContext.contextPath}/resources/images/cabinet/zip.png">

												</c:otherwise>
											</c:choose>
											<p class="right floated" style="font-size: 14px; color: #26D120">
												<b><u:message locale="${locale}" key="review.content.finished" /></b>
											</p>

											<div class="header truncate">
												<a href='review/${i.codeReviewId}'>${i.name}</a>
											</div>

											<div class="meta truncate">
												<div class="meta"><u:message locale="${locale}" key="review.content.owner" />: <b>${i.userName}</b></div>
											</div>
											<div class="meta"><u:message locale="${locale}" key="review.content.totalissue" />: ${i.totalIssues}</div>
											<div class="meta">
												<u:message locale="${locale}" key="review.content.created" />:
												<fmt:formatDate type="date" value="${i.creationDate}" />
											</div>
											<div class="description">

												<p class="black-text">
													<b><u:message locale="${locale}" key="review.content.reviewcomplete" /> <fmt:formatNumber type="number"
															maxFractionDigits="0" value="${i.coverageAvgPercent}" />%
														(<u:message locale="${locale}" key="review.content.total" />)
													</b>
												</p>
												<div class="progress" style="height: 20px;">
													<div id="progressBar"
														class="progress-bar progress-bar-success "
														style="background-color: #54B1B9;  width: ${i.coverageAvgPercent}%; ">
														<fmt:formatNumber type="number" maxFractionDigits="0"
															value="${i.coverageAvgPercent}" />
														%
													</div>
												</div>



											</div>
											<div class="extra pull-right">
												<a class="btn"
													style="background-color: #34F06D; color: white" href="javascript:void(0);"
													onclick="confirmFinishReview(${i.codeReviewId})">ok</a>
											</div>
										</div>

									</div>

								</c:when>
								<c:otherwise>
									<div id="review${i.codeReviewId}" class="card"
										style="margin-top: 30px; margin-left: 35px; border: 1px solid #2185D0">
										<div class="content">
											<c:choose>
												<c:when test="${i.gitURL != null}">
													<img class="right floated mini ui image"
														src="${pageContext.servletContext.contextPath}/resources/images/cabinet/git.png">
												</c:when>
												<c:otherwise>

													<img class="right floated mini ui image"
														src="${pageContext.servletContext.contextPath}/resources/images/cabinet/zip.png">

												</c:otherwise>
											</c:choose> 
											<div class="header truncate">
												<a href='review/${i.codeReviewId}'>${i.name}</a>
											</div>

											<div class="meta truncate">
												<u:message locale="${locale}" key="review.content.owner" />: <b>${i.userName}</b>
											</div>
											<div class="meta"><u:message locale="${locale}" key="review.content.totalissue" />: ${i.totalIssues}</div>
											<div class="meta">
												<u:message locale="${locale}" key="review.content.created" />:
												<fmt:formatDate type="date" value="${i.creationDate}" />
											</div>
											<div class="description">

												<p class="black-text">
													<b><u:message locale="${locale}" key="review.content.yourprogress" /></b>
												</p>
												<div class="progress" style="height: 20px;">
													<div id="progressBar"
														class="progress-bar progress-bar-success "
														style="background-color: #54B1B9;  width: ${i.coverageAvgPercent}%; ">
														<fmt:formatNumber type="number" maxFractionDigits="0"
															value="${i.coverageAvgPercent}" />
														%
													</div>
												</div>
											</div>
											<div class="extra pull-right">
												<a class="btn" style="background-color: #2185D0"  href="javascript:void(0);"
													onclick="setStatusLeft(${i.codeReviewId}, '${i.name}', true)"><u:message locale="${locale}" key="review.content.leave" /></a>
											</div>
										</div>

									</div>
								</c:otherwise>
							</c:choose>


						</c:when>
						<c:otherwise>
							<!--			when review is unactive because of reviewer was deleted from review or owner was deleted or left group  -->
							<div id="deniedReview${i.codeReviewId}" class="card"
								style="margin-top: 30px; margin-left: 35px; border: 1px solid #F3321D">
								<div class="content">
									<c:choose>
										<c:when test="${i.gitURL != null}">
											<img class="right floated mini ui image"
												src="${pageContext.servletContext.contextPath}/resources/images/cabinet/git.png">
										</c:when>
										<c:otherwise>

											<img class="right floated mini ui image"
												src="${pageContext.servletContext.contextPath}/resources/images/cabinet/zip.png">

										</c:otherwise>
									</c:choose>
									<div class="header truncate">
										<p>${i.name}</p>
									</div>

									<div class="meta truncate">
										<u:message locale="${locale}" key="review.content.owner" />: <b>${i.userName}</b>
									</div>
									<div class="meta"><u:message locale="${locale}" key="review.content.totalissue" />: ${i.totalIssues}</div>
									<div class="meta">
										<u:message locale="${locale}" key="review.content.created" />:
										<fmt:formatDate type="date" value="${i.creationDate}" />
									</div>
									<div class="meta">
										<c:choose>
											<c:when test="${i.reviewerStatus == 'DELETED'}">
												<p style="color: #F3321D">
													<b><u:message locale="${locale}" key="review.content.accessdenied" />: </b><u:message locale="${locale}" key="review.content.accessdeniedtext" />.
												</p>
											</c:when>
											<c:otherwise>
												<p style="color: #F3321D">
													<b><u:message locale="${locale}" key="review.content.accessdenied" />: </b><u:message locale="${locale}" key="review.content.accessdeniedtext2" />.
												</p>
											</c:otherwise>
										</c:choose>
									</div>
									<div class="extra pull-right" style="margin-top: 35px;">
										<a class="btn" style="background-color: #F3321D" href="javascript:void(0);"
											onclick="hideFromDeletedReviewer('${i.reviewerStatus}', ${i.codeReviewId})"><u:message locale="${locale}" key="review.content.hide" /></a>
									</div>
								</div>

							</div>
							<!--			end of unactive review card  -->
						</c:otherwise>
					</c:choose>






				</c:otherwise>
			</c:choose>




		</c:forEach>

	</div>










<div class="text-center" style="margin-top: 60px; margin-right: 60px">

	<ul class="pagination ">


		<c:forEach begin="1" end="${noOfPages}" var="i">
			<li><a href="javascript:void(0);" class="topageRev_ReviewAll" style="color:#212121; border-color: #212121;" id="${i}">${i}</a></li>
		</c:forEach>



	</ul>

</div>
    </c:when>    
    <c:otherwise>
        <h1 style="text-align: center; color: #E3E3E6"><u:message locale="${locale}" key="review.content.emptypage" /> </h1>
    </c:otherwise>
</c:choose>




<script>

var pagen=${currentPage};
if(document.getElementById(pagen)!= null){
	document.getElementById(pagen).style.backgroundColor = "#212121";
	document.getElementById(pagen).style.color = "#FFFFFF";
	}

	$(".topageRev_ReviewAll").unbind("click").bind("click", (function (e) {
		e.preventDefault();
		var toPage = $(this).attr("id");
		$.get("${pageContext.servletContext.contextPath}/cabinet/review/all", {
			page : toPage
		}, function(responseText) {
			$("#switchReviewContent").html(responseText);
		});
	}));
	

</script>






