<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="i" uri="../../../WEB-INF/showImageUrl.tld"%>
<%@ taglib prefix="r" uri="../../../WEB-INF/showReply.tld"%>
<%@ taglib prefix="s" uri="../../../WEB-INF/showTime.tld"%>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>

<c:if test="${notifications.size()==0}">
<h1 style="text-align: center; color: #E3E3E6"><u:message locale="${locale}" key="notifications.no" /> </h1>
</c:if>

<c:forEach items="${notifications}" var="ntfn">
					
					<c:choose>
					<c:when test="${ntfn.type eq 'GROUP'}">
					
					
					<!-- Group Notification -->
                    <div id="newGroup${ntfn.notification.groupId}" class="ui large feed not-card">
                        <div class="event ui">
                            <div class="col-md-1">
                                <div class="chat-ava">
                                    <img src="<i:img-url fromServer="${ntfn.notification.user.imageFromServer}" url="${ntfn.notification.user.imageUrl}" />"
				alt="${ntfn.notification.user.login}"/>
                                </div>
                            </div>
                            <div class="col-md-8 main-not">

                                <div class="row">
                                    <div class="not-summary">
                                        <a class="not-user">${ntfn.notification.user.firstName} ${ntfn.notification.user.lastName}
        </a> <u:message locale="${locale}" key="notifications.group.invite" /> <a class="ui blue basic label">${ntfn.notification.groupName}</a>
                                        <div class="not-date">
                                        	<s:showTime locale="${locale}" time="${ntfn.notification.date}"/>
                                		</div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="not-meta">
                                    ${ntfn.notification.countMembers} 
                                    <c:choose>
                                    <c:when test="${ntfn.notification.countMembers == 1}"><u:message locale="${locale}" key="notifications.group.member" /></c:when>
                                    <c:when test="${ntfn.notification.countMembers >= 2 && ntfn.notification.countMembers<=4}"><u:message locale="${locale}" key="notifications.group.thmembers" /></c:when>
                                    <c:otherwise>
                                    <u:message locale="${locale}" key="notifications.group.members" />
                                    </c:otherwise>
                                    </c:choose>
                                    </div>
                                </div>


                            </div>

                            <div class="col-md-3">
                                <div class="ui two buttons" style="width:110%;">
                                    <div onclick="confirmGroup('${ntfn.notification.groupId}')" class="ui basic green button not-confirm-btn"><u:message locale="${locale}" key="notifications.confirm" /></div>
                                    <div onclick="rejectGroup('${ntfn.notification.groupId}')" class="ui basic red button not-reject-btn"><u:message locale="${locale}" key="notifications.reject" /></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    </c:when>

					<c:when test="${ntfn.type eq 'REVIEW'}">

                    <!-- Code review Notification -->
                    <div id="newReview${ntfn.notification.reviewId}" class="ui large feed not-card card-review">
                        <div class="event ui">
                            <div class="col-md-1">
                                <div class="chat-ava">
                                    <img src="<i:img-url fromServer="${ntfn.notification.user.imageFromServer}" url="${ntfn.notification.user.imageUrl}" />"
				alt="${ntfn.notification.user.login}"/>
                                </div>
                            </div>
                            <div class="col-md-8 main-not">

                                <div class="row">
                                    <div class="not-summary">
                                        <a class="not-user user-on-review">${ntfn.notification.user.firstName} ${ntfn.notification.user.lastName}
        </a> <u:message locale="${locale}" key="notifications.review.invite" /> <a class="ui green basic label">${ntfn.notification.reviewName}</a>
                                    <div class="not-date">
                                            <s:showTime locale="${locale}" time="${ntfn.notification.date}"/>
                                	</div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-md-3">
                                <div class="ui two buttons" style="width:110%;">
                                    <div onclick="confirmReview('${ntfn.notification.reviewId}')" class="ui basic green button"><u:message locale="${locale}" key="notifications.confirm" /></div>
                                    <div onclick="rejectReview('${ntfn.notification.reviewId}')" class="ui basic red button"><u:message locale="${locale}" key="notifications.reject" /></div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    </c:when>
                    
                    <c:when test="${ntfn.type eq 'ISSUE'}">
                    
                    <!-- Issue Notification -->
                    <div id="newIssue${ntfn.notification.issueId}" class="ui large feed not-card card-issue">
                                
                        <div class="event ui">
                            <div class="col-md-1">
                                <div class="chat-ava">
                                	<img src="<i:img-url fromServer="${ntfn.notification.user.imageFromServer}" url="${ntfn.notification.user.imageUrl}" />"
									alt="${ntfn.notification.user.login}"/>
                                </div>
                            </div>
                            <div class="col-md-8 main-not">

                                <div class="row">
                                    <div class="not-summary">
                                        <a class="not-user user-on-issue">${ntfn.notification.user.firstName} ${ntfn.notification.user.lastName}
        								</a> <u:message locale="${locale}" key="notifications.issue.left" /> <a href="${pageContext.servletContext.contextPath}/review/${ntfn.notification.reviewId}?file=${ntfn.notification.fileId}&issue=${ntfn.notification.issueId}" class="ui left pointing red basic label">${ntfn.notification.issueTitle}</a>
                                        <div class="not-meta">
                                            <u:message locale="${locale}" key="notifications.issue.toFile" /> <a style="text-decoration: underline;" href="${pageContext.servletContext.contextPath}/review/${ntfn.notification.reviewId}?file=${ntfn.notification.fileId}">${ntfn.notification.fileName}</a>

                                        </div>
                                        <div class="not-meta">
                                            <u:message locale="${locale}" key="notifications.in" /> review: <a style="text-decoration: underline;" href="${pageContext.servletContext.contextPath}/review/${ntfn.notification.reviewId}">${ntfn.notification.reviewName}</a>

                                        </div>
                                        
                                        <div class="not-date">
                                            <s:showTime locale="${locale}" time="${ntfn.notification.date}"/>
                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-md-3">
                                <div class="ui one buttons pull-right">
                                    <div onclick="confirmIssue('${ntfn.notification.issueId}')" class="ui basic green button not-confirm-btn">OK</div>
                                </div>
                            </div>


                        </div>
                    </div>
                    
                    </c:when>
                    
                    <c:when test="${ntfn.type eq 'FILE_ADDED'}">
                    
                    <!-- File Notification -->
                    <div id="newFiles${ntfn.notification.reviewId}" class="ui large feed not-card card-file">
                                
                        <div class="event ui">
                            <div class="col-md-1">
                                <div class="chat-ava">
                                	<img src="<i:img-url fromServer="${ntfn.notification.user.imageFromServer}" url="${ntfn.notification.user.imageUrl}" />"
									alt="${ntfn.notification.user.login}"/>
                                </div>
                            </div>
                            <div class="col-md-8 main-not">

                                <div class="row">
                                    <div class="not-summary">
                                    <a class="not-user user-on-file">${ntfn.notification.user.firstName} ${ntfn.notification.user.lastName}
        </a> <u:message locale="${locale}" key="notifications.file.added" /> <a class="ui tag label" data-toggle="collapse" href="#collapse${ntfn.notification.reviewId}">${ntfn.notification.files.size()} <u:message locale="${locale}" key="notifications.file.files" /><span class="caret"></span></a>
                                        <div class="not-meta">
                                            <u:message locale="${locale}" key="notifications.in" /> review: <a href="${pageContext.servletContext.contextPath}/review/${ntfn.notification.reviewId}">${ntfn.notification.reviewName}</a>

                                        </div>
                                        
                                        <div class="not-date">
                                            <u:message locale="${locale}" key="notifications.file.lastChanges" /> <s:showTime locale="${locale}" time="${ntfn.notification.date}"/>
                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-md-3">
                                <div class="ui one buttons pull-right">
                                    <div onclick="confirmFile('${ntfn.notification.reviewId}')" class="ui basic green button not-confirm-btn">OK</div>
                                </div>
                            </div>


                        </div>
                        <div id="collapse${ntfn.notification.reviewId}" class="collapse files">
                        <c:forEach items="${ntfn.notification.files}" var="file">
                        	<div><a href="${pageContext.servletContext.contextPath}/review/${ntfn.notification.reviewId}?file=${file.id}">${file.name}</a>
                        	<span class="not-date"><s:showTime locale="${locale}" time="${file.date}"/></span>
                        	</div>
                        </c:forEach>
                        </div>
                    </div>
                    
                    </c:when>
                    
                    <c:when test="${ntfn.type eq 'REPLY'}">
                    
                    <!-- NEW REPLY -->
                    <div id="newReply${ntfn.notification.replyId}" class="ui large feed not-card card-reply">

                        <div class="event ui">
                            <div class="col-md-1">
                                <div class="chat-ava">
                                    <img src="<i:img-url fromServer="${ntfn.notification.owner.imageFromServer}" url="${ntfn.notification.owner.imageUrl}" />"
									alt="${ntfn.notification.owner.login}"/>
                                </div>
                            </div>
                            <div class="col-md-8">

                                <div class="row">
                                    <div class="not-summary">
                                        <a class="not-user user-on-reply">${ntfn.notification.owner.firstName} ${ntfn.notification.owner.lastName}
        								</a> <u:message locale="${locale}" key="notifications.reply.replied" /> <span class="not-date"><s:showTime locale="${locale}" time="${ntfn.notification.date}"/></span>
                                       
                                        <blockquote>
                                        	<div class="not-meta overflow-text">                          
                                            <r:showReply message="${ntfn.notification.message}" />
                                           	</div>
                                            <div class="not-date"><a href="${pageContext.servletContext.contextPath}/review/${ntfn.notification.reviewId}?file=${ntfn.notification.fileId}&issue=${ntfn.notification.issueId}&reply=${ntfn.notification.replyId}"><u:message locale="${locale}" key="notifications.reply.goto" /></a></div>
                                        </blockquote>
                                        
                                        <div class="not-meta">
                                        	<u:message locale="${locale}" key="notifications.issue" />: <a class="not-ancor" href="${pageContext.servletContext.contextPath}/review/${ntfn.notification.reviewId}?file=${ntfn.notification.fileId}&issue=${ntfn.notification.issueId}">${ntfn.notification.issueTitle}</a>
                                        </div>
                                        
                                        <div class="not-meta">
                                        	<u:message locale="${locale}" key="notifications.review" />: <a class="ui yellow basic label" href="${pageContext.servletContext.contextPath}/review/${ntfn.notification.reviewId}">${ntfn.notification.reviewName}</a>
                                        </div>
                                         
                                     </div>
                                  </div>
                                </div>

                            <div class="col-md-3">
                                <div class="ui buttons pull-right">
                                    <div onclick="confirmReply('${ntfn.notification.replyId}')" class="ui basic green button">OK</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                    </c:when>
                    
                    </c:choose>
                    
                    </c:forEach>
<script>
$('[data-toggle="popover"]').popover({container: 'body'});
</script>
