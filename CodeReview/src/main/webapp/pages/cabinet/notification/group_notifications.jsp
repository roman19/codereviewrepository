<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="i" uri="../../../WEB-INF/showImageUrl.tld"%>
<%@ taglib prefix="s" uri="../../../WEB-INF/showTime.tld"%>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>

<c:if test="${notifications.size()==0}">
<h1 style="text-align: center; color: #E3E3E6"><u:message locale="${locale}" key="notifications.no" /> </h1>
</c:if>

<c:forEach items="${notifications}" var="ntfn">
					
	<!-- Group Notification -->
                    <div id="newGroup${ntfn.groupId}" class="ui large feed not-card">
                        <div class="event ui">
                            <div class="col-md-1">
                                <div class="chat-ava">
                                    <img src="<i:img-url fromServer="${ntfn.user.imageFromServer}" url="${ntfn.user.imageUrl}" />"
				alt="${ntfn.user.login}"/>
                                </div>
                            </div>
                            <div class="col-md-8 main-not">

                                <div class="row">
                                    <div class="not-summary">
                                        <a class="not-user">${ntfn.user.firstName} ${ntfn.user.lastName}
        </a> <u:message locale="${locale}" key="notifications.group.invite" /> <a class="ui blue basic label">${ntfn.groupName}</a>
                                        <div class="not-date">
                                        	<s:showTime locale="${locale}" time="${ntfn.date}"/>
                                		</div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="not-meta">
                                    ${ntfn.countMembers} 
                                    <c:choose>
                                    <c:when test="${ntfn.countMembers == 1}"><u:message locale="${locale}" key="notifications.group.member" /></c:when>
                                    <c:when test="${ntfn.countMembers >= 2 && ntfn.countMembers<=4}"><u:message locale="${locale}" key="notifications.group.thmembers" /></c:when>
                                    <c:otherwise>
                                    <u:message locale="${locale}" key="notifications.group.members" />
                                    </c:otherwise>
                                    </c:choose>
                                    </div>
                                </div>


                            </div>

                            <div class="col-md-3">
                                <div class="ui two buttons" style="width:110%;">
                                    <div onclick="confirmGroup('${ntfn.groupId}')" class="ui basic green button not-confirm-btn"><u:message locale="${locale}" key="notifications.confirm" /></div>
                                    <div onclick="rejectGroup('${ntfn.groupId}')" class="ui basic red button not-reject-btn"><u:message locale="${locale}" key="notifications.reject" /></div>
                                </div>
                            </div>
                        </div>
                    </div>			
					
                    
</c:forEach>