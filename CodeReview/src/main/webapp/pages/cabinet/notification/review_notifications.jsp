<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="i" uri="../../../WEB-INF/showImageUrl.tld"%>
<%@ taglib prefix="s" uri="../../../WEB-INF/showTime.tld"%>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>

<c:if test="${notifications.size()==0}">
<h1 style="text-align: center; color: #E3E3E6"><u:message locale="${locale}" key="notifications.no" /> </h1>
</c:if>

<c:forEach items="${notifications}" var="ntfn">

					<!-- Code review Notification -->
                    <div id="newReview${ntfn.reviewId}" class="ui large feed not-card card-review">
                        <div class="event ui">
                            <div class="col-md-1">
                                <div class="chat-ava">
                                    <img src="<i:img-url fromServer="${ntfn.user.imageFromServer}" url="${ntfn.user.imageUrl}" />"
				alt="${ntfn.user.login}"/>
                                </div>
                            </div>
                            <div class="col-md-8 main-not">

                                <div class="row">
                                    <div class="not-summary">
                                        <a class="not-user user-on-review">${ntfn.user.firstName} ${ntfn.user.lastName}
        </a> <u:message locale="${locale}" key="notifications.review.invite" /> <a class="ui green basic label">${ntfn.reviewName}</a>
                                    <div class="not-date">
                                            <s:showTime locale="${locale}" time="${ntfn.date}"/>
                                	</div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-md-3">
                                <div class="ui two buttons" style="width:110%;">
                                    <div onclick="confirmReview('${ntfn.reviewId}')" class="ui basic green button"><u:message locale="${locale}" key="notifications.confirm" /></div>
                                    <div onclick="rejectReview('${ntfn.reviewId}')" class="ui basic red button"><u:message locale="${locale}" key="notifications.reject" /></div>
                                </div>
                            </div>
                            
                        </div>
                    </div>

</c:forEach>