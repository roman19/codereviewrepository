<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="i" uri="../../../WEB-INF/showImageUrl.tld"%>
<%@ taglib prefix="s" uri="../../../WEB-INF/showTime.tld"%>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>

<c:if test="${notifications.size()==0}">
<h1 style="text-align: center; color: #E3E3E6"><u:message locale="${locale}" key="notifications.no" /> </h1>
</c:if>

<c:forEach items="${notifications}" var="ntfn">

                    <!-- Issue Notification -->
                    <div id="newIssue${ntfn.issueId}" class="ui large feed not-card card-issue">
                                
                        <div class="event ui">
                            <div class="col-md-1">
                                <div class="chat-ava">
                                	<img src="<i:img-url fromServer="${ntfn.user.imageFromServer}" url="${ntfn.user.imageUrl}" />"
									alt="${ntfn.user.login}"/>
                                </div>
                            </div>
                            <div class="col-md-8 main-not">

                                <div class="row">
                                    <div class="not-summary">
                                        <a class="not-user user-on-issue">${ntfn.user.firstName} ${ntfn.user.lastName}
        								</a> <u:message locale="${locale}" key="notifications.issue.left" /> <a href="${pageContext.servletContext.contextPath}/review/${ntfn.reviewId}?file=${ntfn.fileId}&issue=${ntfn.issueId}" class="ui left pointing red basic label">${ntfn.issueTitle}</a>
                                        <div class="not-meta">
                                            <u:message locale="${locale}" key="notifications.issue.toFile" /> <a style="text-decoration: underline;" href="${pageContext.servletContext.contextPath}/review/${ntfn.reviewId}?file=${ntfn.fileId}">${ntfn.fileName}</a>

                                        </div>
                                        <div class="not-meta">
                                            <u:message locale="${locale}" key="notifications.in" /> review: <a style="text-decoration: underline;" href="${pageContext.servletContext.contextPath}/review/${ntfn.reviewId}">${ntfn.reviewName}</a>

                                        </div>
                                        
                                        <div class="not-date">
                                            <s:showTime locale="${locale}" time="${ntfn.date}"/>
                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-md-3">
                                <div class="ui one buttons pull-right">
                                    <div onclick="confirmIssue('${ntfn.issueId}')" class="ui basic green button not-confirm-btn">OK</div>
                                </div>
                            </div>


                        </div>
                    </div>
</c:forEach>