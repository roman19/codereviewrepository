<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="i" uri="../../../WEB-INF/showImageUrl.tld"%>
<%@ taglib prefix="s" uri="../../../WEB-INF/showTime.tld"%>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>

<c:if test="${notifications.size()==0}">
<h1 style="text-align: center; color: #E3E3E6"><u:message locale="${locale}" key="notifications.no" /> </h1>
</c:if>

<c:forEach items="${notifications}" var="ntfn">


					<!-- File Notification -->
                    <div id="newFiles${ntfn.reviewId}" class="ui large feed not-card card-file">
                                
                        <div class="event ui">
                            <div class="col-md-1">
                                <div class="chat-ava">
                                	<img src="<i:img-url fromServer="${ntfn.user.imageFromServer}" url="${ntfn.user.imageUrl}" />"
									alt="${ntfn.user.login}"/>
                                </div>
                            </div>
                            <div class="col-md-8 main-not">

                                <div class="row">
                                    <div class="not-summary">
                                    <a class="not-user user-on-file">${ntfn.user.firstName} ${ntfn.user.lastName}
        </a> <u:message locale="${locale}" key="notifications.file.added" /> <a class="ui tag label" data-toggle="collapse" href="#collapse${ntfn.reviewId}">${ntfn.files.size()} <u:message locale="${locale}" key="notifications.file.files" /><span class="caret"></span></a>
                                        <div class="not-meta">
                                            <u:message locale="${locale}" key="notifications.in" /> review: <a href="${pageContext.servletContext.contextPath}/review/${ntfn.reviewId}">${ntfn.reviewName}</a>

                                        </div>
                                        
                                        <div class="not-date">
                                            <u:message locale="${locale}" key="notifications.file.lastChanges" /> <s:showTime locale="${locale}" time="${ntfn.date}"/>
                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="col-md-3">
                                <div class="ui one buttons pull-right">
                                    <div onclick="confirmFile('${ntfn.reviewId}')" class="ui basic green button not-confirm-btn">OK</div>
                                </div>
                            </div>


                        </div>
                        <div id="collapse${ntfn.reviewId}" class="collapse files">
                        <c:forEach items="${ntfn.files}" var="file">
                        	<div><a href="${pageContext.servletContext.contextPath}/review/${ntfn.reviewId}?file=${file.id}">${file.name}</a>
                        	<span class="not-date"><s:showTime locale="${locale}" time="${file.date}"/></span>
                        	</div>
                        </c:forEach>
                        </div>
                    </div>


</c:forEach>