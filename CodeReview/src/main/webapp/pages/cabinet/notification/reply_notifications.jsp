<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="i" uri="../../../WEB-INF/showImageUrl.tld"%>
<%@ taglib prefix="r" uri="../../../WEB-INF/showReply.tld"%>
<%@ taglib prefix="s" uri="../../../WEB-INF/showTime.tld"%>
<%@ taglib prefix="u" uri="../../../WEB-INF/showMessage.tld"%>

<c:if test="${notifications.size()==0}">
<h1 style="text-align: center; color: #E3E3E6"><u:message locale="${locale}" key="notifications.no" /> </h1>
</c:if>

<c:forEach items="${notifications}" var="ntfn">

<div id="newReply${ntfn.replyId}" class="ui large feed not-card card-reply">

	<div class="event ui">
		<div class="col-md-1">
			<div class="chat-ava">
				<img
					src="<i:img-url fromServer="${ntfn.owner.imageFromServer}" url="${ntfn.owner.imageUrl}" />"
					alt="${ntfn.owner.login}" />
			</div>
		</div>
		<div class="col-md-8">

			<div class="row">
				<div class="not-summary">
					<a class="not-user user-on-reply">${ntfn.owner.firstName}
						${ntfn.owner.lastName} </a> <u:message locale="${locale}" key="notifications.reply.replied" /> <span
						class="not-date"><s:showTime locale="${locale}" time="${ntfn.date}"/></span>

					<blockquote>
						<div class="not-meta overflow-text">
							<r:showReply message="${ntfn.message}" />
						</div>
						<div class="not-date">
							<a
								href="${pageContext.servletContext.contextPath}/review/${ntfn.reviewId}?issue=${ntfn.issueId}&reply=${ntfn.replyId}">
								<u:message locale="${locale}" key="notifications.reply.goto" />
								</a>
						</div>
					</blockquote>

					<div class="not-meta">
						<u:message locale="${locale}" key="notifications.issue" />: <a class="not-ancor"
							href="${pageContext.servletContext.contextPath}/review/${ntfn.reviewId}?file=${ntfn.fileId}&issue=${ntfn.issueId}">${ntfn.issueTitle}</a>
					</div>

					<div class="not-meta">
						<u:message locale="${locale}" key="notifications.review" />: <a class="ui yellow basic label"
							href="${pageContext.servletContext.contextPath}/review/${ntfn.reviewId}">${ntfn.reviewName}</a>
					</div>

				</div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="ui buttons pull-right">
				<div onclick="confirmReply('${ntfn.replyId}')"
					class="ui basic green button">Ok</div>
			</div>
		</div>
	</div>
</div>

</c:forEach>