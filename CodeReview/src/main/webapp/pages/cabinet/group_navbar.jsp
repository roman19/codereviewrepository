<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>
	
	
	<div class="ui list">
				  	<div class="item">
						    	<i class="users icon"></i>
						    <div class="content" style="color:#767676">
						       <u:message locale="${locale}" key="cgroup.members" />: ${totalGroupMembers}
						    </div>
			 	   </div>
			 	  
			 	   <div class="item">
						    	<i class="check circle icon"></i>
						    <div class="content" style="color:#767676">
						        <u:message locale="${locale}" key="cgroup.activereviews" />: ${totalActiveReviews}
						    </div>
			 	   </div>
			 	   
			 	   <div class="item">
						    	<i class="check circle outline icon"></i>
						    <div class="content" style="color:#767676">
						     <u:message locale="${locale}" key="cgroup.myreviews" />: ${totalMyActiveReviews}
						    </div>
			 	   </div>
			 	   
			 	    <div class="item">
						    	<i class="minus circle icon"></i>
						    <div class="content" style="color:#767676">
						      <u:message locale="${locale}" key="cgroup.unactivereviews" />: ${totalHistoryReviews} 
						    </div>
			 	   </div>
			 	   
			   </div>