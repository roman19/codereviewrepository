<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<style>
.truncate {
  width: 200px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}


</style>

<style>

			.truncate {
				  width: 200px;
				  white-space: nowrap;
				  overflow: hidden;
				  text-overflow: ellipsis;
				}
</style>

<c:if test="${fn:length(listReviews) gt 0}">

	<div class="ui cards">

		<c:forEach var="i" items="${listReviews}">


	
<c:choose>
<c:when test="${userID == i.userId}">
<div id="review${i.codeReviewId}" class="card" style="margin-top: 50px; margin-left: 35px; border: 1px solid #F2711C">
				<div class="content">
						<c:choose>
					<c:when test="${i.gitURL != null}">
							<img class="right floated mini ui image"
								src="${pageContext.servletContext.contextPath}/resources/images/cabinet/git.png">
					</c:when>	
						<c:otherwise>
							
							<img class="right floated mini ui image"
						src="${pageContext.servletContext.contextPath}/resources/images/cabinet/zip.png">
						
						</c:otherwise>	
					</c:choose>	
					<div class="header truncate"><a href='review/${i.codeReviewId}'>${i.name}</a></div>
					
					<div class="meta truncate"><u:message locale="${locale}" key="review.content.owner" />:<b><u:message locale="${locale}" key="review.content.you" /></b></div>
					<div class="meta"><u:message locale="${locale}" key="review.content.totalissue" />: ${i.totalIssues}</div>
					<div class="meta">
						created:
						<fmt:formatDate type="date" value="${i.creationDate}" />
					</div>
					<div class="description">

						<p class="black-text">
							<b><u:message locale="${locale}" key="review.content.reviewcomplete" /> <fmt:formatNumber type="number" 
            maxFractionDigits="0" value="${i.coverageAvgPercent}" />% (<u:message locale="${locale}" key="review.content.total" />)</b>
						</p>
						<div class="progress" style="height: 20px;">
							<div id="progressBar" class="progress-bar progress-bar-success "
								style="background-color: #54B1B9;  width: ${i.coverageAvgPercent}%; "><fmt:formatNumber type="number" 
            maxFractionDigits="0" value="${i.coverageAvgPercent}" />%
							</div>
						</div>



					</div>
					<div class="extra pull-right" style="margin-top: 0px;">
						<a class="btn" style="background-color: #F3321D" href="javascript:void(0);"
							onclick="deleteReviewInHistory(${i.codeReviewId})"><u:message locale="${locale}" key="review.content.delete" /></a>
					</div>
				</div>
				
			</div>

</c:when>

<c:otherwise>

<div id="review${i.codeReviewId}" class="card" style="margin-top: 50px; margin-left: 35px; border: 1px solid #2185D0">
				<div class="content">
						<c:choose>
					<c:when test="${i.gitURL != null}">
							<img class="right floated mini ui image"
								src="${pageContext.servletContext.contextPath}/resources/images/cabinet/git.png">
					</c:when>	
						<c:otherwise>
							
							<img class="right floated mini ui image"
						src="${pageContext.servletContext.contextPath}/resources/images/cabinet/zip.png">
						
						</c:otherwise>	
					</c:choose>	
					<div class="header truncate"><a href='review/${i.codeReviewId}'>${i.name}</a></div>
					
					<div class="meta truncate"><u:message locale="${locale}" key="review.content.owner" />: <b>${i.userName}</b></div>
					<div class="meta"><u:message locale="${locale}" key="review.content.totalissue" />: ${i.totalIssues}</div>
					<div class="meta">
						<u:message locale="${locale}" key="review.content.created" />:
						<fmt:formatDate type="date" value="${i.creationDate}" />
					</div>
					<div class="description">

						<p class="black-text">
							<b><u:message locale="${locale}" key="review.content.yourprogress" /></b>
						</p>
						<div class="progress" style="height: 20px;">
							<div id="progressBar" class="progress-bar progress-bar-success "
								style="background-color: #54B1B9;  width: ${i.coverageAvgPercent}%; "><fmt:formatNumber type="number" 
            maxFractionDigits="0" value="${i.coverageAvgPercent}" />%
							</div>
						</div>



					</div>
					<div class="extra pull-right" style="margin-top: 0px;">
						<a class="btn" style="background-color: #F3321D" href="javascript:void(0);"
							onclick="setStatusLeft(${i.codeReviewId}, '${i.name}', true)"><u:message locale="${locale}" key="review.content.leave" /></a>
					</div>
				</div>
				
			</div>

</c:otherwise>
</c:choose>


		</c:forEach>

	</div>







<div class="text-center" style="margin-top: 60px; margin-right: 60px">

<ul class="pagination "  >


	<c:forEach begin="1" end="${noOfPages}" var="i">
		<li><a href="javascript:void(0);" class="topageRev_ReviewHistory" style="color:#212121; border-color: #212121;" id="${i}">${i}</a></li>
	</c:forEach>



</ul>

</div>

</c:if>

<c:if test="${fn:length(listReviews) == 0}">

     <h1 style="text-align: center; color: #E3E3E6"><u:message locale="${locale}" key="review.content.emptypage" /></h1>


</c:if>










<!-- ********************************************* PAGENATION ******************************************** -->

<script>

alert


var pagen=${currentPage};
if(document.getElementById(pagen)!= null){
	document.getElementById(pagen).style.backgroundColor = "#212121";
	document.getElementById(pagen).style.color = "#FFFFFF";
	}




	$(".topageRev_ReviewHistory").unbind("click").bind("click", (function (e) {
		e.preventDefault();
		var toPage = $(this).attr("id");

		$.get("${pageContext.servletContext.contextPath}/cabinet/review/history_all", {
			page : toPage
		}, function(responseText) {
			$("#switchReviewContentHistory").html(responseText);
		});
	}));
	

</script>
<!-- *********************************************// PAGENATION ******************************************** -->







