
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<style>
	
	.typeahead{
		min-width: 314px;
		max-width: 314px;
		overflow: hidden;	
		min-height: 10px;
  		max-height: 300px;  

	}
	
	
	.user img{
		float:left;
		max-width: 50px;
		max-height: 50px;
		min-width:50px;
		min-height:50px;
		margin-left: 5px
	}
	 .user .login{
	 float:right;
	 margin-left: 5px;
	}
	.user .lname{
	float:right;
	margin-left: 5px;
	 }
	 .user .fname{
	float:right;
	margin-left: 10px;
	}

	.vertical-center {
	  min-height: 1%;  
 	  min-height: 1vh; 
      display: flex;
      align-items: center;
      }
	</style>
		

               
         <div class="row">  
                <div class="col-sm-4">
                	
                    <input type="text" style="margin-top:20px;" id="nameOfGroup" class="form-control" value="${groupname}" required placeholder="Enter name of group" />
                <span>
                  <button id="cancel" type="button" style="margin-bottom: 15px;   background-color:#DE5246;width: 100px" class="btn">CANCEL</button>
                  <button id="send" type="button" style="margin-bottom: 15px;width: 100px;   background-color:#17A05E" class="btn">CONFIRM</button>
          		</span>
                    <input id="search"
                        type="text"
                        class="form-control typeahead"
                        placeholder="Who is You looking for?"
                        autocomplete="off"
                        data-provide="typeahead"
                    />
                    <br/>
                    
                    
                  
                    
                    <input type="hidden" class="form-control" name="userId" id="userId" value="" />
            </div>


	 <div class="col-sm-7">




 <table  id="groups_table"class="table table-striped" style="margin-top: 20px; margin-left: 50px">
    <thead>
      <tr >
 		<th style=" text-align: center;"><b>Photo</b></th>
        <th style=" text-align: center;"><b>Firstname</b></th>
        <th style=" text-align: center;"><b>Lastname</b></th>
        <th style=" text-align: center;"><b>Login</b></th>
        <th style=" text-align: center;"><b>Action</b></th>
      </tr>
    </thead>
    <tbody>
    
    <c:forEach var="i" items="${groupMembers}">
   		 <tr id='row${i.id}'>
   		 	<td> <img style='max-width: 35px; max-height: 25px;' class='img-circle'src="${i.photo}" /></td>
   		 	<td>${i.fname}</td> 
   		 	<td>${i.lname}</td>
   		 	<td>${i.login}</td>
   		 	<td> <button class='deleteUsers btn' type='button'   id='${i.id}' 
   		 	style='color:white;background-color: #1E8FC9;'  ><b>X</b></button> </td>
   		 </tr>
    </c:forEach>   
    </tbody>
  </table>

</div>
</div>     

		
		
		
		<script type="text/javascript">

        $(function(){
			debugger;
        	
            var userObjs = {};
            var userNames = [];
			var selectedUsers=[<c:forEach var="item" items="${arrayIds}"><c:out value="${item}"/>,</c:forEach>];
			var groupID=${groupID};
		
			console.log(selectedUsers);
			console.log(groupID);
			
 
            var throttledRequest = _.debounce(function(query, process){

                $.ajax({
                    url: 'cabinet/addgroup/SearchController'
                    ,cache: false
                    ,success: function(data){

                        userObjs = {};
                        userNames = [];

                        _.each( data, function(item, ix, list){
                            
                            debugger;
                            
                            if($.inArray(item.id,selectedUsers) === -1) {
									
                            	userNames.push(item.totalSearchedName);
                            	
								}

                            userObjs[item.totalSearchedName] = item;

                        });

                        process( userNames );

                    }
                });
            }, 300);


            $(".typeahead").typeahead({      
            	 input: "#search",
            	 limit: 4,
            	hint: true,
            	source: function ( query, process ) {
                    throttledRequest( query, process );

                }
        ,highlighter: function( item ){
          var user = userObjs[ item ];
          
          return '<div class="row  user  vertical-center">'
          	
                +'<div class=""> <img class="img-circle" src="' + user.photo + '" /></div>'
                +'<div class="fname">' + user.fname + '</div>'
                +'<div class="lname">' + user.lname + '</div>'
                +'<div class="login">' + user.login + '</div>'         

             +'</div>'
        }
                
        ,updater: function ( selectedName ) {

                      $( "#userId" ).val( userObjs[ selectedName ].id );
						
            			var userId=$( "#userId" ).val();		
     
            			 $.get("cabinet/addgroup/AddToTable",{id:userId}, function(user) { 
            				 debugger;
                    		selectedUsers.push(user.id);   
                    		console.log(selectedUsers);
            				 $("#groups_table tr:last").after("<tr id='row"+user.id+"'><td> <img style='max-width: 35px; max-height: 25px;' class='img-circle'src="+user.photo+" /></td><td>"+user.fname+"</td> <td>"+user.lname+"</td><td>"+user.login+"</td><td> <button class='deleteUsers btn' type='button'   id='"+user.id+"' style='color:white;background-color: #1E8FC9;'  ><b>X</b></button>   </td></tr>");	           				
            			 });

                      return "";
                  }
                
           
            });

            $('#send').on("click",function(){
        		
            	var jsonObj={
            			groupId: groupID,
            			name : $("#nameOfGroup").val(),
            			users : selectedUsers
            				
            			
            	}            	
       		 				$.get("cabinet/addgroup/SaveToDBedit",{group :JSON.stringify(jsonObj)}, function(user) { 
       		 				
       		 									$("#switchContent").html(user); 

       		 				});

       			});		
            
            $(document).on('click', "button.deleteUsers", function() {
				debugger;
            	
            	var toDelete = $(this).attr("id");
            	
            	$('#row'+toDelete).remove();
            	
            	var index=selectedUsers.indexOf(toDelete);
            	selectedUsers.splice(index,1);
            	console.log(selectedUsers);
     
            });
            
        });
        
        
    </script>		
		
		
		
		
		
		
		
		
		