<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>

<div class="col-md-10">

  <div class="container-fluid">

            <div class="row not-filter pull-right">

                <div id="typesSelector" class="ui floating dropdown button">
                    <span class="text"><u:message locale="${locale}" key="notifications.filter" /> <span class="caret"></span></span>
                    <div class="menu">
                        <div id="toGroup" class="item">
                            <div class="ui blue empty circular label"></div>
                            <span class="text"><u:message locale="${locale}" key="notifications.filter.group" /></span>

                        </div>
                        <div id="toReview" class="item">
                            <div class="ui green empty circular label"></div>
                            <span class="text"><u:message locale="${locale}" key="notifications.filter.review" /></span>
                        </div>
                        <div id="toIssue" class="item">
                            <div class="ui red empty circular label"></div>
                            <span class="text"><u:message locale="${locale}" key="notifications.filter.issues" /></span>
                        </div>
                        <div id="toReplies" class="item">
                            <div class="ui yellow empty circular label"></div>
                            <span class="text"><u:message locale="${locale}" key="notifications.filter.replies" /></span>
                        </div>
                        <div id="toAddedFiles" class="item">
                            <div class="ui orange empty circular label"></div>
                            <span class="text"><u:message locale="${locale}" key="notifications.filter.files" /></span>
                        </div>
                        <div id="toAll" class="item">
                            <div class="ui grey empty circular label"></div>
                            <span class="text"><u:message locale="${locale}" key="notifications.filter.all" /></span>
                        </div>
                    </div>
                </div>

            </div>
            
            <div class="row">

            <div class="col-md-12">

                <div class="container-fluid">
                	<div id="notificationsContent">
                		<jsp:include page="notification/all_notifications.jsp"></jsp:include>
                	</div>
					<div id="forNotificationsInsert"></div>
                </div>

            </div>
            
           </div>

    </div>
    </div>
<script src="${pageContext.servletContext.contextPath}/resources/js/cabinet/cabinet_main.js"></script>
    
    <script>
    
    $('#typesSelector').dropdown();
    
    
    $(window).scroll(function() {
    	
    	var hash = window.location.hash.substring(1);
        if(($(window).scrollTop() > $(document).height() - $(window).height() - $('#footer').height()) && hash=='notifications') {
        	if(!loading){
        		loading = true;
        		
	        	var jsonObj = { 'offset': offset, 'count': countOfPortion, 'type': TYPE }
	        	$.ajax({
	        		url : 'cabinet/loadNotifications',
	        		type : 'get',
	        		dataType : 'text',
	        		data : jsonObj
	        	}).success(function(data) { 
	        		
	        		offset+=countOfPortion;
	        		
	        		$('#forNotificationsInsert').append(data);
	        		
	        		loading = false;
	        		
	        	}).error(function(data) {
        			alert('non loaded else');
        			loading = false;
        	});
        	}
        }
    });
    
    function loadFirstNotifications(type){
    	TYPE = type;
    	offset = 0;
    	
    	var jsonObj = { 'offset': offset, 'count': countOfPortion, 'type': TYPE }
    	$.ajax({
    		url : 'cabinet/loadNotifications',
    		type : 'get',
    		dataType : 'text',
    		data : jsonObj
    	}).success(function(data) {
    		debugger;
    		offset+=countOfPortion;
    		$('#notificationsContent').html(data);
    		$('#forNotificationsInsert').empty();
    	}).error(function(data) {
			alert('non loaded ' + type + ' notifications');
		});
    }

    $('#toGroup').click(function () {
    	loadFirstNotifications('GROUP');
    });
    
    $('#toReview').click(function () {
    	loadFirstNotifications('REVIEW');
    });
    
    $('#toIssue').click(function () {
    	loadFirstNotifications('ISSUE');
    });
    
    $('#toReplies').click(function () {
    	loadFirstNotifications('REPLY');
    });
    
    $('#toAddedFiles').click(function () {
    	loadFirstNotifications('FILE');
    });
    
    $('#toAll').click(function () {
    	loadFirstNotifications('ALL');
    });
    
    
    
</script>