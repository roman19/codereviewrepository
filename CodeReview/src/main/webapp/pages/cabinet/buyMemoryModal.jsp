<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>

<style>
.lead {
	font-size: 33px;
	margin-bottom: 0px;
}

.buyCard {
	width: 30%;
}

.nohover:hover {
	color: white;
}

.card {
	box-shadow: 0 3px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0
		rgba(0, 0, 0, 0.12);
	transition: box-shadow .25s;
	border-radius: 5px;
	padding: 10px;
	background-color: white;
	background-color: #F3EAEA;
}

input {
	padding: 1%;
}

.cardDetail {
	margin-left: 30px;
	font-size: 20px;
}

.cardnumber {
	margin-left: 15px;
	width: 60px;
}

.error{
	color: red;
	font-size: 14px;
}

.containerTag {
   position: relative;
}
.selectTag {
	width: 60px;
	position: absolute;
	margin-top: -10px;
}

</style>

<div class="modal fade" id="buyMemoryModal" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h1 class="modal-title"><u:message locale="${locale}" key="settings.personal.memory.purchase.title" /></h1>
			</div>
			<div style="overflow: none; max-height: 400px;">
				<div id="buyCardsContent" class="row"
					style="margin-left: 0px; margin-right: 0px;">
					<div class="col-md-4 buyCard" style="margin-left: 50px;">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h4 class="text-center"><u:message locale="${locale}" key="settings.personal.memory.silver" /></h4>
							</div>
							<div class="panel-body text-center">
								<p class="lead">
									<strong>20 <u:message locale="${locale}" key="settings.personal.memory.uah" /></strong>
								</p>
							</div>
							<ul class="list-group list-group-flush text-center">
								<li class="list-group-item"><i class="icon-ok text-danger"></i>50
									MB</li>
							</ul>
							<div class="panel-footer">
								<a class="btn btn-lg btn-block btn-primary"
									onclick="buyMemory(50, 20)"><u:message locale="${locale}" key="settings.personal.memory.buynow" /></a>
							</div>
						</div>
					</div>
					<div class="col-md-4 buyCard">
						<div class="panel panel-warning">
							<div class="panel-heading">
								<h4 class="text-center"><u:message locale="${locale}" key="settings.personal.memory.gold" /></h4>
							</div>
							<div class="panel-body text-center">
								<p class="lead">
									<strong>50 <u:message locale="${locale}" key="settings.personal.memory.uah" /></strong>
								</p>
							</div>
							<ul class="list-group list-group-flush text-center">
								<li class="list-group-item"><i class="icon-ok text-danger"></i>200
									MB</li>
							</ul>
							<div class="panel-footer">
								<a class="btn btn-lg btn-block btn-warning"
									onclick="buyMemory(200, 50)"><u:message locale="${locale}" key="settings.personal.memory.buynow" /></a>
							</div>
						</div>
					</div>
					<div class="col-md-4 buyCard">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="text-center"><u:message locale="${locale}" key="settings.personal.memory.platinum" /></h4>
							</div>
							<div class="panel-body text-center">
								<p class="lead">
									<strong>100 <u:message locale="${locale}" key="settings.personal.memory.uah" /></strong>
								</p>
							</div>
							<ul class="list-group list-group-flush text-center">
								<li class="list-group-item"><i class="icon-ok text-danger"></i>500
									MB</li>
							</ul>
							<div class="panel-footer">
								<a class="btn btn-lg btn-block btn-default nohover"
									onclick="buyMemory(500, 100)"><u:message locale="${locale}" key="settings.personal.memory.buynow" /></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="exitBuyModal" type="button" class="btn btn-default"
					data-dismiss="modal"><u:message locale="${locale}" key="review.creating.exit" /></button>
			</div>
		</div>
	</div>
</div>