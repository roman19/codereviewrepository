
<%@ taglib prefix="i" uri="../../WEB-INF/showImageUrl.tld"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>
<style>
.truncate{
  width: 200px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}


</style>
		<!-- Sidebar -->
		<div id="sidebar-wrapper" style=" margin-top:1px; margin-bottom: 2px;">


			<ul class="sidebar-nav  .ui.fixed.sticky"  id="sidebar">
				<li class="sidebar-header"><img id="cabinetAvatar" alt=""
					src="<i:img-url fromServer="${user.imageFromServer}" url="${user.imageUrl}" />"
					class="img-circle img-responsive center-block"
					style="max-width: 100x;  max-height: 100px;  margin-bottom: 10px; margin-top: 30px">

				</li>
				<li class="sidebar-header" style="line-height: 25px;">

					<p
						style="color: white; max-width: 220px; text-align: center; line-height: 10px;">
						<span id="settings_fname">${user.firstName}</span>  <span id="settings_lname" class="truncate">${user.lastName}</span></p>
					<p
						style="color: white; max-width: 220px; text-align: center; line-height: 10px;">${user.email}</p>
				</li>

				<li style="margin-top: 60px;"><a id="reviewButton" href="#"><u:message locale="${locale}" key="cabinet.sidebar.reviews" /> <span
						class="glyphicon glyphicon-check" aria-hidden="true"
						style="float: right; margin-right: 25px; margin-top: 10px; font-size: 1.3em;"></span></a>

				</li>
				<li><a id="addReviewButton"  style="cursor:pointer"  onclick="openCodeReviewModal()"><u:message locale="${locale}" key="cabinet.sidebar.addreview" /> <span
						class="glyphicon glyphicon-plus" aria-hidden="true"
						style="float: right; margin-top: 10px; margin-right: 25px; font-size: 1.3em;"></span></a>

				</li>
				<li><a id="groupsButton" href="#"> <u:message locale="${locale}" key="cabinet.sidebar.groups" /><span
						class="glyphicon glyphicon-th" aria-hidden="true"
						style="float: right; margin-top: 10px; margin-right: 25px; font-size: 1.3em;"></span></a>
				</li>
				<li><a id="notificationsBtn" href="#"> <u:message locale="${locale}" key="cabinet.sidebar.notifications" /><span
						class="glyphicon glyphicon-bell" aria-hidden="true"
						style="float: right; margin-top: 10px; margin-right: 25px; font-size: 1.3em;"></span></a>
				</li>
				<li><a id="settingsBtn" href="#"><u:message locale="${locale}" key="cabinet.sidebar.settings" /> <span
						class="glyphicon glyphicon-cog glyphicon " aria-hidden="true"
						style="float: right; margin-top: 10px; margin-right: 25px; font-size: 1.3em;"></span></a>
				</li>

			</ul>
		</div>
		<!-- /#sidebar-wrapper -->
		
		
		        <script>
				

					 $("#reviewButton").unbind("click").bind("click", function(e) { 
						 e.preventDefault();
			                $.get("${pageContext.servletContext.contextPath}/cabinet/review", function(responseText) {   
			                    $("#switchContent").html(responseText);
			                    debugger;
			                    
			                    clearURL();
			                    window.location.hash = "#reviews";
			                });
			         });
				
					
					 $("#groupsButton").unbind("click").bind("click", function(e) { 
						 e.preventDefault();
				            $.get("${pageContext.servletContext.contextPath}/cabinet/groups", function(responseText) { 
				                $("#switchContent").html(responseText);
				                clearURL();
				                window.location.hash = "#groups";
				            });
			         });
					
					
					
					
					// notifications
					var countOfPortion = 10;
					var offset = 0;
					
				    var loading = false;
				    var TYPE = 'ALL';

					$( "#notificationsBtn" ).click(function(e) {
						e.preventDefault();
						
						TYPE='ALL';
						offset = 0;
						
						$.ajax({
							url : 'cabinet/notifications',
							type : 'get',
							data: {'offset': offset, 'count': countOfPortion}
						}).success(function(data) {
							offset+=countOfPortion;
							$("#switchContent").html(data);
							clearURL();
							window.location.hash = "#notifications";
						}).error(function(data) {
							alert('Not switched to notifications!')
						});
						
						});
		

       
		 
		 $("#settingsBtn").unbind("click").bind("click", function(e) { 
			 e.preventDefault();
             $.get("${pageContext.servletContext.contextPath}/cabinet/settings", function(responseText) {   
                 $("#switchContent").html(responseText);
                 clearURL();
                 window.location.hash = "#settings";
             });
         });
		 
		 

        </script>
        
        <script>
        
        $(function() {
        	debugger;
        	
        	//if we don't redirect to notifications
        	if(!isNotification){
        		
	        	if(window.location.hash) {
	        		
	                var hash = window.location.hash.substring(1);
	                if(hash=='reviews'){
	                	$("#reviewButton").click();
	                }else if(hash=='history'){
	                	$.get("${pageContext.servletContext.contextPath}/cabinet/review/history",
								function(responseText) {
									$("#switchContent").html(
											responseText);
								});
	                }else if(hash=='groups'){
	                	$("#groupsButton").click();
	                }else if(hash=='notifications'){
	                	$( "#notificationsBtn" ).click();
	                }else if(hash=='settings'){
	                	$("#settingsBtn").click();
	                }else{
	                	$("#reviewButton").click();
	                }
	            } else {
	            	$("#reviewButton").click();
	            }
        	}
        	isNotification = false;
        	
        	if(${countOfNotifications} !=0){
        		$('#notificationsBtn').addClass('badge1');
        		$('#notificationsBtn').attr('data-badge', ${countOfNotifications});
        	}
        });
        
        
        </script>
		