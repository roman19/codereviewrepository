<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>

<style>
.truncate {
  width: 200px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}


</style>


<style>
	
	.typeahead{
		min-width: 314px;
		max-width: 400px;
		overflow: hidden;	
		min-height: 10px;
		
	}
	
	ul.typeahead.dropdown-menu li{
		margin-left:0px;
		margin-bottom:0px; 
	}
	
	.user img{
		float:left;
		max-width: 50px;
		max-height: 50px;
		min-width:50px;
		min-height:50px;
		margin-left: 5px
	}
	 .user .login{
	 float:right;
	 margin-left: 5px;
	}
	.user .lname{
	float:right;
	margin-left: 5px;
	 }
	 .user .fname{
	float:right;
	margin-left: 10px;
	}

	.vertical-center {
	  min-height: 1%;  
 	  min-height: 1vh; 
      display: flex;
      align-items: center;
      }
	

	
      
      #selectedUsersList::-webkit-scrollbar {
      width: 5px;
	}
	
	#selectedUsersList::-webkit-scrollbar-button {
		width: 8px;
		height: 5px;
	}
	
	#selectedUsersList::-webkit-scrollbar-track {
		background: #eee;
		border: thin solid lightgray;
		box-shadow: 0px 0px 3px #dfdfdf inset;
		border-radius: 10px;
	}
	
	#selectedUsersList::-webkit-scrollbar-thumb {
		background: #999;
		border: thin solid gray;
		border-radius: 10px;
	}
	
	#selectedUsersList::-webkit-scrollbar-thumb:hover {
		background: #7d7d7d;
	}
	
	.dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover {
    color: #fff;
    text-decoration: none;
    background-color: rgba(84, 177, 185, 0.76) !important;
    outline: 0;
	
	</style>

<script src="//underscorejs.org/underscore-min.js"></script>
<script>
	var currentPageFilter= 'all' ;
</script>

<input id="mapping" type="hidden" value="${pageContext.servletContext.contextPath}/"/>
<input id="locale_id" type="hidden" value="${locale}"/>

<div class="row">
	<div class="input-group-btn search-panel">


		<div class="row not-filter pull-left">

			<div id="typesSelector_GroupsAll" class="ui floating dropdown button"
				style="margin-top: 30px">
				<span class="text"><div class="ui grey empty circular label"></div><u:message locale="${locale}" key="filters.all" /><span class="caret"></span></span>
				<div class="menu">

					<div id="allGroups"  class="item">
						<div class="ui grey empty circular label"></div>
						<a  class="text"><u:message locale="${locale}" key="filters.all" /></a>
					</div>
					<div id="myGroups" class="item">
						<div class="ui orange  empty circular label"></div>
						<a  class="text"><u:message locale="${locale}" key="filters.my" /></a>
					</div>
					<div id="otherGroups" class="item">
						<div class="ui blue empty circular label"></div>
						<a   class="text"><u:message locale="${locale}" key="filters.other" /></a>
					</div>

				</div>
			</div>

		</div>
		
		
		
		
		
		
		
		
		
		<div class="row not-filter pull-left" style="margin-left: 30px">

			<div id="perpage_GroupsAll" class="ui floating dropdown button"
				style="margin-top: 30px">
				<span class="text">  ${recordsOnPage} <u:message locale="${locale}" key="filters.perpage" /> <span class="caret"></span></span>
				<div class="menu">

					<div id="perpage3Group" class="item">
					
						<a  class="text">3 <u:message locale="${locale}" key="filters.perpage" /></a>
					</div>
					<div id ="perpage6Group" class="item">
						
						<a   class="text">6  <u:message locale="${locale}" key="filters.perpage" /></a>
					</div>
					<div id ="perpage9Group" class="item">
						
						<a   class="text">9  <u:message locale="${locale}" key="filters.perpage" /></a>
					</div>

				</div>
			</div>

		</div>







		<span style="float: right;">

			<button data-toggle="modal" data-target="#createGroupModal" id="addGroup" class="ui green basic button"
				style=" margin-top: 50px; margin-right: 70px">
				<i class="fa fa-plus"></i>  <u:message locale="${locale}" key="grop.content.creategroup" />
			</button>
			
			


		</span>
	</div>
	
				<!-- CREATING GROUP MODAL -->
			  <div class="modal fade" id="createGroupModal" role="dialog">
			    <div style="width:900px; margin: 50px auto" class="modal-dialog modal-lg">
			      <div class="modal-content">
			        <div class="modal-header" style="text-align:center">
			          <button type="button" class="close" data-dismiss="modal">&times;</button>
			          <h2 style="font-size:20px" class="modal-title"><u:message locale="${locale}" key="grop.content.creatinggroup" /> <i class="fa fa-users"></i></h2>
			        </div>
			        <div class="modal-body">
			          <div class="container-fluid">
			          <div class="row">
						
			          	<form style="width:40%; margin:0px auto 10px auto" class="ui fluid form">
			          	<div class="field">
						    <input id="nameOfGroup" data-toggle="tooltip" data-placement="top" data-original-title="Name is empty or invalid" type="text" name="name" placeholder="<u:message locale="${locale}" key="grop.content.entername" />">
						  </div>
						  </form>
						  
						  <div style="max-width:348px; margin:0px auto" id="imageToolTip"
									class="fileinput fileinput-new input-group"
									data-provides="fileinput" data-toggle="tooltip"
									data-placement="right"
									data-original-title="Please upload one image to 10 MB (only .jpg .png .gif .jpeg)">
									<div id="fumes" style="width:250px" class="form-control" data-trigger="fileinput">
										<i class="glyphicon glyphicon-file fileinput-exists"></i> <span
											class="fileinput-filename"></span>
									</div>
									<span class="input-group-addon btn btn-default btn-file"><span
										class="fileinput-new"><u:message locale="${locale}" key="grop.content.selectimage" /></span><span
										class="fileinput-exists"><u:message locale="${locale}" key="grop.content.change" /></span> <form id="groupImageForm"><input id="groupImage" type="file"
										name="image" accept="image/*"></form></span> <a href="#"
										class="input-group-addon btn btn-default fileinput-exists"
										data-dismiss="fileinput"><u:message locale="${locale}" key="grop.content.remove" /></a>
						</div>
						  
			          </div>
			          <div style="margin-top:10px; margin-bottom:10px" class="row">
			          
			         	 <h4 class="ui horizontal divider header">
  							<u:message locale="${locale}" key="grop.content.membersmodal" />
  							
					  	</h4>
			          </div>
			          <div class="row">
			          	<div class="col-md-6">
			          	
			          	<div class="row">
			          		<div class="col-md-10 col-md-offset-1">
			          		<div class="ui input" >
  							<input id="search2" class="typeahead" type="text" data-provide="typeahead" autocomplete="off" placeholder="<u:message locale="${locale}" key="grop.content.search" />">
							</div>
							</div>	
			          	</div>
			          	
			          	<div class="row">
			          	
			          	</div>
			          	
			          	</div>
			          	
			          	<div class="col-md-6">
			          			<!-- selected users -->
								<div id="selectedUsersList"
									class="ui vertical list"
									style="max-height: 320px; overflow-y: auto;">
								</div>
			          	</div>
			          </div>
			          </div>
			        </div>
			        <div class="modal-footer">
			          <div style="margin-right:5px" class="row">
			          <button data-dismiss="modal" class="ui orange basic button"><u:message locale="${locale}" key="grop.content.cancel" /></button>
			          <button id="send" class="ui green basic button"><u:message locale="${locale}" key="grop.content.create" /></button>
			          </div>
			        </div>
			      </div>
			    </div>
			  </div>
			  <input type="hidden" class="form-control" name="userId" id="userId" value="" />

</div>







<script src="${pageContext.servletContext.contextPath}/resources/js/cabinet/group/deleteGroup.js"></script>

<script src="${pageContext.servletContext.contextPath}/resources/js/cabinet/cabinet_main.js"></script>

<script >
$('#typesSelector_GroupsAll').dropdown();
$('#perpage_GroupsAll').dropdown();
	//confirmation on modal
	
	



</script>







<script>
//Per page
	$("#perpage3Group").unbind("click").bind("click", (function (e) {		
		e.preventDefault();

		
		if(currentPageFilter === 'all'){
		
				$.get("${pageContext.servletContext.contextPath}/cabinet/groups/all", {
					recordsOnPage : 3
				}, function(responseText) {
					$("#switchGroupContent").html(responseText);
				});
		
		}
		
		if(currentPageFilter === 'my'){
			
			$.get("${pageContext.servletContext.contextPath}/cabinet/groups/my", {
				recordsOnPage : 3
			}, function(responseText) {
				$("#switchGroupContent").html(responseText);
			});
			
		}
		
		if(currentPageFilter === 'other'){
			
			$.get("${pageContext.servletContext.contextPath}/cabinet/groups/other", {
				recordsOnPage : 3
			}, function(responseText) {
				$("#switchGroupContent").html(responseText);
			});
			
		}
		

	}));
	
	
	
	$("#perpage6Group").unbind("click").bind("click", (function (e) {		
		e.preventDefault();


		if(currentPageFilter === 'all'){
			
			$.get("${pageContext.servletContext.contextPath}/cabinet/groups/all", {
				recordsOnPage : 6
			}, function(responseText) {
				$("#switchGroupContent").html(responseText);
			});
	
		}
		
		if(currentPageFilter === 'my'){
			
			$.get("${pageContext.servletContext.contextPath}/cabinet/groups/my", {
				recordsOnPage : 6
			}, function(responseText) {
				$("#switchGroupContent").html(responseText);
			});
			
		}
		
		if(currentPageFilter === 'other'){
			
			$.get("${pageContext.servletContext.contextPath}/cabinet/groups/other", {
				recordsOnPage : 6
			}, function(responseText) {
				$("#switchGroupContent").html(responseText);
			});
			
		}
		
		

	}));
	
	
	
	$("#perpage9Group").unbind("click").bind("click", (function (e) {		
		e.preventDefault();

		
		
		if(currentPageFilter === 'all'){
			
			$.get("${pageContext.servletContext.contextPath}/cabinet/groups/all", {
				recordsOnPage : 9
			}, function(responseText) {
				$("#switchGroupContent").html(responseText);
			});
			
		}
		
		if(currentPageFilter === 'my'){
			
			$.get("${pageContext.servletContext.contextPath}/cabinet/groups/my", {
				recordsOnPage : 9
			}, function(responseText) {
				$("#switchGroupContent").html(responseText);
			});
			
		}
		
		if(currentPageFilter === 'other'){
			
			$.get("${pageContext.servletContext.contextPath}/cabinet/groups/other", {
				recordsOnPage : 9
			}, function(responseText) {
				$("#switchGroupContent").html(responseText);
			});
			
		}
	
	}));
	
	
	
	
	
</script>








<script>
	
	

	$('.special.cards .image').dimmer({
		on : 'hover'
	});


	$(".editCard").unbind("click").bind("click", (function (e) {		
		 e.preventDefault();
			var toEdit = $(this).attr("id");
			$.get("${pageContext.servletContext.contextPath}/cabinet/groups/editgroup",{
							groupId : toEdit}, function(responseText) {
								$("#switchGroupContent").html(responseText);
							});

	}));
	

 

 	$(".deleteCard").unbind("click").bind("click", (function (e) {		
 		e.preventDefault();
			var toDelete = $(this).attr("id");
		$('#card' + toDelete).remove();
// 			$.get("${pageContext.servletContext.contextPath}/cabinet/groups/deletegroupall",{
// 							groupId : toDelete
// 						}, function(responseText) {
// 						   $("#switchGroupContent").html(responseText);         
// 						});

	}));
 	
 	
 	
 	$("button.exitCard").unbind("click").bind("click", (function () {		
 		var toDelete = $(this).attr("id");
		$('#cardexit' + toDelete).remove();
// 		$.get("${pageContext.servletContext.contextPath}/cabinet/groups/exitgroup",{
// 							groupId : toDelete}, function(responseText) {
// 							});

	}));
 	
 	
 
 	
 	
 	
 	
 </script> 








 <script> 

 	$("#allGroups").unbind("click").bind("click", (function (e) {		
		 e.preventDefault();
		 currentPageFilter='all';
	
		 
		 $.get("${pageContext.servletContext.contextPath}/cabinet/groups/all",
					function(responseText) {
						$("#switchGroupContent").html(responseText);
					});

	}));

 	
 	$("#myGroups").unbind("click").bind("click", (function (e) {		
					 e.preventDefault();
					 currentPageFilter='my';
					
					 $.get("${pageContext.servletContext.contextPath}/cabinet/groups/my",
								function(responseText) {
									$("#switchGroupContent").html(responseText);
								});

	}));
 	

	
	$("#otherGroups").unbind("click").bind("click", (function (e) {		
					 e.preventDefault();
					 currentPageFilter='other';
				
			$.get("${pageContext.servletContext.contextPath}/cabinet/groups/other",
							function(responseText) {
								$("#switchGroupContent").html(responseText);
							});
		
	}));
	
 </script> 




<script>

function getImageUrl(jsonObj){
	if (!jsonObj.imageFromServer) {
		return jsonObj.photo;
	} else {
		return "/CodeReview/get_user_image/" + jsonObj.photo;
	}
}

        $(function(){
        	
            var userObjs = {};
            var userNames = [];
			var selectedUsers=[];
 
            var throttledRequest = _.debounce(function(query, process){

                $.ajax({
                    url: 'cabinet/addgroup/SearchController'
                    ,cache: false
                    ,success: function(data){

                        userObjs = {};
                        userNames = [];

                        _.each( data, function(item, ix, list){
                            
                            
                            console.log(selectedUsers);
                            
                            if($.inArray(item.id,selectedUsers) === -1) {
									
                            	userNames.push(item.totalSearchedName);
                            	
								}

                            userObjs[item.totalSearchedName] = item;

                        });

                        process( userNames );

                    }
                });
            }, 300);


            $("#search2").typeahead({      
            	 input: "#search2",
            	 hint: true,
            	 source: function ( query, process ) {
                    throttledRequest( query, process );
                  },items: 5
            ,highlighter: function( item ){
            	var user = userObjs[ item ];
            	
            	var imageUrl = getImageUrl(user)
          
            	 return "<div class='ui horizontal list '><div class='item user'>"+
               	
            	 "<img class='img-circle' src="+ getImageUrl(user) +">"+
					"<div class='content' style='margin-left: 80px; margin-top: 8px' ><div style='padding-bottom:5px'><b>"+user.fname+" "+user.lname+"</b>"+"</div>"+user.login+"</div></div>";
		
            	 
               
        }
                
        ,updater: function ( selectedName ) {

                      $( "#userId" ).val( userObjs[ selectedName ].id );
						
            			var userId=$( "#userId" ).val();		
     
            			 $.get("cabinet/addgroup/AddToTable",{id:userId}, function(user) { 
                    		selectedUsers.push(user.id);   
                    		
                    		//insert in table: selected users
                    			var userTemplate = "<div id='row"+user.id+"' class='item'>"+
                    								"<div class='right floated content'>"+
						      							"<button type='button' id='"+user.id+"' class='close deleteUsers'>&times;</button>"+
						      						"</div>"+
						      							"<img class='ui mini circular image' src="+ getImageUrl(user) +">"+
						      							"<div class='content' style='margin-left: 20px;' ><div><b>"+user.fname+" "+user.lname+"</b>"+"</div><span style='color:#757575'>"+user.login+"</span></div></div>";
						      				
						      				
                    		
                    		$('#selectedUsersList').append(userTemplate);
                    		
                    			           				
            			 });

                      return "";
                  }
                
           
            });

            $('#send').on("click",function(){
            	var name = $("#nameOfGroup").val();
            	debugger;
            	var image = document.forms["groupImageForm"]["image"];
            	
            	var imageFile = document.getElementById("groupImage").files[0];
            	
            	var isValid = nameIsValid(name);
            	isValid = avaIsValid(image) && isValid;
            	
            	if(isValid){
                    var jsonObj={ name : name, image:image.value != "", users : selectedUsers}
                    
                    $.get("cabinet/addgroup/SaveToDB",{group :JSON.stringify(jsonObj)}, function(response) {
            			socket.send(JSON.stringify(response));
            			
            			$('#createGroupModal').modal('hide');
            			
            			swal({
            			    title: "<u:message locale="${locale}" key="group.swal.create.title" />",
            			    text: "<u:message locale="${locale}" key="group.swal.create.text" />",
            			    type:"success",
            			    allowOutsideClick : false ,
            			    animation: true ,
            			    confirmButtonColor: "#66BB6A" ,
            			    confirmButtonText: "OK", 
            			  }, function(){
            			    window.location.reload();
            			  });
            			
            			
					if (image.value != "") {
						var formdata = new FormData();
						formdata.append("groupId", String(response.itemId));
						formdata.append("image", imageFile);

						$.ajax({
							url : 'cabinet/addgroup/saveImage',
							data : formdata,
							cache : false,
							contentType : false,
							processData : false,
							type : 'POST',
							success : function(response) {
								console.log("save group image");
							}
						});
					}
				});
			}
		});

		function nameIsValid(name) {
			var re = /^([0-9a-zA-Zа-яА-ЯиіІьїЇєЄґҐ`’_\.\-@#]*)$/;
			if (!re.test(name) || name == "") {
				$("#nameOfGroup").css('border', '1px solid red');
				$("#nameOfGroup").tooltip('show');
				$("#nameOfGroup").tooltip('hide');
				return false;
			} else {
				$("#nameOfGroup").css('border', '1px solid green');
				$("#nameOfGroup").tooltip('destroy');
				return true;
			}
		}

		function avaIsValid(ava) {

			if (ava.value != "") {
				var val = ava.value.substring(ava.value.lastIndexOf('.') + 1)
						.toLowerCase();
				if ((val != 'gif' && val != 'jpg' && val != 'png' && val != 'jpeg')
						|| ava.files[0].size / 1024 / 1024 > 10) {
					$("#fumes").css('border', '1px solid red');
					$('#imageToolTip').tooltip('show');
					$('#imageToolTip').tooltip('hide');
					return false;

				} else {
					$("#fumes").css('border', '1px solid green');
					$('#imageToolTip').tooltip('destroy');
					return true;
				}
			} else {
				return true;
			}
		}

		$(document).on('click', "button.deleteUsers", function() {

			var toDelete = $(this).attr("id");

			$('#row' + toDelete).remove();

			var index = selectedUsers.indexOf(parseInt(toDelete));
			selectedUsers.splice(index, 1);

		});
	});
        
        /* $('#createGroupModal').on('hidden.bs.modal', function () {
			 debugger;
			selectedUsers = [];
			$('#selectedUsersList').empty();
			
			$('#nameOfGroup').val('');
			$('#nameOfGroup').tooltip('destroy');
			$('#nameOfGroup').removeClass("field-valid");
			$('#nameOfGroup').removeClass("field-invalid");
			
			$('#imageToolTip').val('');
			$('#imageToolTip').tooltip('destroy');
			$('#imageToolTip').removeClass("field-valid");
			$('#imageToolTip').removeClass("field-invalid");
       }); */
        
</script>

<div id="switchGroupContent">
		<jsp:include page="content_groups_all.jsp"/>
</div>
