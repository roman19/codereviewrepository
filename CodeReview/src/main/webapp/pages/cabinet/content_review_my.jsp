<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<style>
.truncate {
  width: 200px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}


</style>

<fmt:setLocale value="${locale}"/>

<c:if test="${fn:length(listReviews) gt 0}">

	<div class="ui cards">
		<c:forEach var="i" items="${listReviews}">



			<div id="review${i.codeReviewId}" class="card" style="margin-top: 30px; margin-left: 35px; border: 1px solid #F2711C">
				<div class="content">
					<c:choose>
					<c:when test="${i.gitURL != null}">
							<img class="right floated mini ui image"
								src="${pageContext.servletContext.contextPath}/resources/images/cabinet/git.png">
					</c:when>	
						<c:otherwise>
							
							<img class="right floated mini ui image"
						src="${pageContext.servletContext.contextPath}/resources/images/cabinet/zip.png">
						
						</c:otherwise>	
					</c:choose>	
					<div class="header truncate"><a href='review/${i.codeReviewId}'>${i.name}</a></div>
					
					<div class="meta truncate"><u:message locale="${locale}" key="review.content.owner" />:<b> <u:message locale="${locale}" key="review.content.you" /></b></div>
					<div class="meta"><u:message locale="${locale}" key="review.content.totalissue" />: ${i.totalIssues}</div>
					<div class="meta">
						<u:message locale="${locale}" key="review.content.created" />:
						<fmt:formatDate type="date" value="${i.creationDate}" />
					</div>
					<div class="description">

						<p class="black-text">
							<b><u:message locale="${locale}" key="review.content.reviewcomplete" /> <fmt:formatNumber type="number" 
            maxFractionDigits="0" value="${i.coverageAvgPercent}" />% (<u:message locale="${locale}" key="review.content.total" />)</b>
						</p>
						<div class="progress" style="height: 20px;">
							<div id="progressBar" class="progress-bar progress-bar-success "
								style="background-color: #54B1B9;  width: ${i.coverageAvgPercent}%; "><fmt:formatNumber type="number" 
            maxFractionDigits="0" value="${i.coverageAvgPercent}" />%
							</div>
						</div>



					</div>
					<div class="extra pull-right">
					<a class="btn" style="background-color: #F2711C"
					 href="javascript:void(0);" onclick="finishReview(${i.codeReviewId})"><u:message locale="${locale}" key="review.content.finish" /></a>
				</div>
				</div>
				
			</div>


		</c:forEach>

	</div>








<div class="text-center" style="margin-top: 60px; margin-right: 60px">

<ul class="pagination "  >


	<c:forEach begin="1" end="${noOfPages}" var="i">
		<li><a href="javascript:void(0);" class="topageRevMy_ReviewMy" style="color:#212121; border-color: #212121;" id="${i}">${i}</a></li>
	</c:forEach>

	

</ul>

</div>


</c:if>

<c:if test="${fn:length(listReviews) == 0}">

     <h1 style="text-align: center; color: #E3E3E6"><u:message locale="${locale}" key="review.content.emptypage" /> </h1>


</c:if>


<script>
var pagen=${currentPage};
if(document.getElementById(pagen)!= null){
	document.getElementById(pagen).style.backgroundColor = "#212121";
	document.getElementById(pagen).style.color = "#FFFFFF";
	}

	//Pagenation


	$(".topageRevMy_ReviewMy").unbind("click").bind("click", (function (e) {
		e.preventDefault();
		var toPage = $(this).attr("id");
		$.get("${pageContext.servletContext.contextPath}/cabinet/review/my", {
			page : toPage
		}, function(responseText) {
			$("#switchReviewContent").html(responseText);
		});
	}));
</script>










