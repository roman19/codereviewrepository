<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="i" uri="../../WEB-INF/showImageUrl.tld"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>
<html>
<link rel="icon" type="image/png" href="${pageContext.servletContext.contextPath}/resources/images/codereview_icon.png">
<head>

<!--     for review creating -->
<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/resources/css/reviewCreating/tree.css"
	type="text/css" media="screen">
<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/resources/css/reviewCreating/chosen.css">
<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/resources/css/reviewCreating/style.css">
<!--      end of styles for review creating -->

<!--     Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">





<!--     jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


<!--     Latest compiled JavaScript -->
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!--     for review creating -->
<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/resources/css/reviewCreating/reviewCreating.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<!--      end of styles for review creating -->

<!-- for notifications -->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/semantic_ui/css/semantic.css"/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/cabinet/notification/notification.css"/>" />
<script src="<c:url value="/resources/semantic_ui/js/transition.js"/>"
	type="text/javascript"></script>
<script src="<c:url value="/resources/semantic_ui/js/dropdown.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/js/cabinet/notification/notification.js"/>"
	type="text/javascript"></script>

<!-- for online notifications -->
<script
	src="<c:url value="/resources/bootstrap_notify/bootstrap-notify.js"/>"></script>
<script
	src="<c:url value="/resources/bootstrap_notify/onlineNotification.js"/>"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap_notify/animate.css"/>">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap_notify/onlineNotification.css"/>">


<script
	src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
<script src="//underscorejs.org/underscore-min.js"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap-maxlength.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/js/cabinet/group/jquery.scrollTableBody.js"/>"
	type="text/javascript"></script>

<script src="<c:url value="/resources/semantic_ui/js/transition.js"/>"
	type="text/javascript"></script>
<script src="<c:url value="/resources/semantic_ui/js/dropdown.js"/>"
	type="text/javascript"></script>
<link rel="stylesheet" 
	type="text/css" href="${pageContext.servletContext.contextPath}/resources/sweetalert/css/sweetalert.css">

<script src="${pageContext.servletContext.contextPath}/resources/sweetalert/js/sweetalert.min.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/main.js"></script>

<style>


.typeahead {
	min-width: 314px;
	max-width: 400px;
	overflow: hidden;
	min-height: 10px;
	
} 
#searchCodeReview.typeahead{
		min-width: 400px;
		max-width: 400px;
		overflow: hidden;	
		min-height: 10px;
		max-height: 319px;
	}

ul.typeahead.dropdown-menu li {
	margin-left: 0px;
	margin-bottom: 0px;
}

.user img {
	float: left;
	max-width: 50px;
	max-height: 50px;
	min-width: 50px;
	min-height: 50px;
	margin-left: 5px
}

.user .login {
	float: right;
	margin-left: 5px;
}

.user .lname {
	float: right;
	margin-left: 5px;
}

.user .fname {
	float: right;
	margin-left: 10px;
}

.vertical-center {
	min-height: 1%;
	min-height: 1vh;
	display: flex;
	align-items: center;
}

#selectedUsersList::-webkit-scrollbar {
	width: 5px;
}

#selectedUsersList::-webkit-scrollbar-button {
	width: 8px;
	height: 5px;
}

#selectedUsersList::-webkit-scrollbar-track {
	background: #eee;
	border: thin solid lightgray;
	box-shadow: 0px 0px 3px #dfdfdf inset;
	border-radius: 10px;
}

#selectedUsersList::-webkit-scrollbar-thumb {
	background: #999;
	border: thin solid gray;
	border-radius: 10px;
}

#selectedUsersList::-webkit-scrollbar-thumb:hover {
	background: #7d7d7d;
}





.jqstb-scroll::-webkit-scrollbar {
	width: 5px;
}

.jqstb-scroll::-webkit-scrollbar-button {
	width: 8px;
	height: 5px;
}

.jqstb-scroll::-webkit-scrollbar-track {
	background: #eee;
	border: thin solid lightgray;
	box-shadow: 0px 0px 3px #dfdfdf inset;
	border-radius: 10px;
}

.jqstb-scroll::-webkit-scrollbar-thumb {
	background: #999;
	border: thin solid gray;
	border-radius: 10px;
}

.jqstb-scroll::-webkit-scrollbar-thumb:hover {
	background: #7d7d7d;
}


</style>




<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/resources/css/main.css">

<script>
	var currentPageFilterCG= 'all' ;
	var currentState="active";
	
</script>

<title><u:message locale="${locale}" key="cgroup.title" /> | ${groupName}</title>
</head>

<body>
<body onload="connect(${userID});" onunload="disconnect();">
	<input id="mapping" type="hidden"
		value="${pageContext.servletContext.contextPath}/" />
	<input id="locale_id" type="hidden" value="${locale}"/>
	<jsp:include page="../header.jsp"></jsp:include>


	<div class="container">
		<div class="row">
			<div class="col-xs-2"></div>


			<div class="col-xs-2">

					
					 <input id="image" type="image" src="${pageContext.servletContext.contextPath}/get_group_image/${group.imageUrl}"
						class="img-circle" style="min-height: 120px; margin-top: 50px; min-width: 120px; max-height: 120px; max-width: 120px" /> 
								
					 <input type="file" id="newPhoto" style="display: none;" onchange="previewFile()" accept="image/*" />
					 
					
					 
			</div>
			
			
			
			
			
			
			
			
			
			
			<div class="col-xs-4">
				 <h2 class="ui horizontal divider header" style="margin-top:30px">
					<c:out value="${groupName}" />
  				</h2>
  				
  			<div id="navBar_congroup" class="container" style="margin-top:20px">
  				<div class="ui list">
				  	<div class="item">
						    	<i class="users icon"></i>
						    <div class="content" style="color:#767676">
						       <u:message locale="${locale}" key="cgroup.members" />: ${totalGroupMembers}
						    </div>
			 	   </div>
			 	  
			 	   <div class="item">
						    	<i class="check circle icon"></i>
						    <div class="content" style="color:#767676">
						        <u:message locale="${locale}" key="cgroup.activereviews" />: ${totalActiveReviews}
						    </div>
			 	   </div>
			 	   
			 	   <div class="item">
						    	<i class="check circle outline icon"></i>
						    <div class="content" style="color:#767676">
						     <u:message locale="${locale}" key="cgroup.myreviews" />: ${totalMyActiveReviews}
						    </div>
			 	   </div>
			 	   
			 	    <div class="item">
						    	<i class="minus circle icon"></i>
						    <div class="content" style="color:#767676">
						      <u:message locale="${locale}" key="cgroup.unactivereviews" />: ${totalHistoryReviews} 
						    </div>
			 	   </div>
			 	   
			   </div>
  		</div>
  		
  		
  		
  		
  				
			</div>
			
			<input type="hidden"  id="GROUPNAME_ConcreteGroup"
					value="${groupName}" />
			
			
<div class="col-xs-3">
				
				

		<div class="ui vertical list" style="margin-top: 90px">								
					<div  class='item' style="padding: 0px; margin: 0px">
									     <img src='<i:img-url fromServer="${owner.imageFromServer}" url="${owner.imageUrl}" />'
											class="ui mini circular image">
									<div class='content' style='margin-left: 20px;' >
										<div class='ui sub header'><b> ${owner.firstName} ${owner.lastName}</b></div><span style='color:#757575'>${owner.login}</span>
									</div>
					</div><span style="font-size: 8px; margin-left: 5px;color:#767676"><b><u:message locale="${locale}" key="cgroup.owner" /></b></span>
		</div>	


</div><div class="col-xs-1">
<!--  **********************************************	FOR ANDREW			**************************************	 -->
		
<%-- 		<c:if test="${userID != ownerId}"> --%>

<!-- 			<button id="leaveGroup_ConcreteGroup" type="button" class="btn" -->
<!-- 						style="min-width: 100px; margin-top: 20px; margin-bottom: 50px; float:right;"  -->
<%-- 						onclick="leaveGroup(${groupId}, true)"><u:message locale="${locale}" key="cgroup.leavegroup" /></button> --%>
						
		
<%-- 		</c:if>	 --%>
	
	
<!-- 		********************************************//DropDown *********************************************** -->
		<div class="row not-filter pull-right" style="margin-left: 0px">

			<div id="setting_concreateGroup" class="ui floating dropdown button"
				style="margin-top: 0px; min-width: 122px; min-height: 30px ">
				<span class="text"> <u:message locale="${locale}" key="cgroup.setting" /> <span class="caret"style="margin-left: 20px"></span></span>
				<div class="menu">
				<c:if test="${userID != ownerId || userID == ownerId}">
					<div  id="createReview_ConcreteGroup" class="item">
					
						<a  onclick="openCodeReviewModal(${groupId})" class="text"><u:message locale="${locale}" key="cgroup.createreview" /> </a>
					</div>
				</c:if>			
				<c:if test="${userID != ownerId}">
					<div id="leaveGroup_ConcreteGroup" class="item">
						
						<a    onclick="leaveGroup(${groupId}, true)" class="text"><u:message locale="${locale}" key="cgroup.leavegroup" /> </a>
					</div>
				</c:if>		
				<c:if test="${userID == ownerId}">
					<div id ="editGroup_ConcreteGroup"  class="item">
						
						<a   data-toggle="modal" data-target="#editGroupModal"  class="text"><u:message locale="${locale}" key="cgroup.editgroup" /> </a>
					</div>
					<div id ="deleteGroup_ConcreteGroup" class="item">
						
						<a   class="text" onclick="deleteGroup(${groupId}, true)"><u:message locale="${locale}" key="cgroup.deletegroup" /> </a>
					</div>
				</c:if>	
				</div>
			</div>

		</div>
		
	<!--  **********************************************	//FOR ANDREW			**************************************	 -->	
					
					
			</div>
		</div>
	</div>

	
		
			      				
		
		


	<div class="container">
		<div class="row">
			<div class="col-xs-2"></div>
			<div class="col-xs-8"><hr>
			</div>
		</div>
	</div>






<!-- **************************  Table  ********************************* -->




	<div class="row" style="margin-top: 50px">


		<div class="col-xs-4" style="margin-top: 20px">
			<div id="table-wrapper" >
			<table id="concreteGroupTable"  class="ui  table" >
				<thead style="position: relative;">
					<tr>
						<th><span style="margin-left: 17px;"><i
								class="fa fa-user-plus" style="margin-right: 42px"></i><u:message locale="${locale}" key="grop.content.members" /></span></th>
						<th>
									<c:if test="${userID == ownerId}">
											
											<span style="margin-left: 45px;"><i
													class="fa fa-recycle"
													style="margin-right: 10px; text-align: center;"></i></span>
													
													
									</c:if>
						</th>
						
						<th>
							
							<u:message locale="${locale}" key="grop.content.status" />
							
						</th>
						
					</tr>
				</thead>
			
				<tbody >
			
					<c:forEach var="i" items="${listWithUsers}">
						<tr id="groupUser${i.id}" style="margin-left: 20px;">
							<td>
								<div class="row vertical-center">
									<div class="col-xs-4">
										<img
											src='<i:img-url fromServer="${i.imageFromServer}" url="${i.imageUrl}" />'
											class="img-circle img-responsive "
											style="max-height: 46px; max-width: 46px; min-height: 46px; min-width:46px">
									</div>
									<div class="col-xs-8">
										<span> ${i.firstName} ${i.lastName}, ${i.login}</span>
									</div>
								</div>
							</td>

							<td>
						<c:if test="${userID == ownerId}">
							
							<a  class="btn btn-default groupuserremove"
								style="margin-left: 35px; margin-top: 10px" onclick="removeUser(${groupId}, ${i.id})"> <i
									class="fa fa-trash-o"></i>
									
								
							</a>
							
						</c:if>		
							</td>
							
							
							<td>
									<c:if test="${i.statusInGroup == 'CONFIRMED'}">							
											<div class="  ui green horizontal label" style="margin-top:12px">
												${i.statusInGroup}
											</div>							
									</c:if>
									
									<c:if test="${i.statusInGroup == 'PENDING'}" >							
											<div class="  ui blue horizontal label"  style="margin-top:12px">
												${i.statusInGroup}
											</div>							
									</c:if>
									
									<c:if test="${i.statusInGroup == 'LEFT'}">							
											<div class="  ui red horizontal label"  style="margin-top:12px">
												${i.statusInGroup}
											</div>							
									</c:if>
							</td>
							
						</tr>

					</c:forEach>
				
				</tbody>
		
			</table>
		</div>
</div>








		<!--   **************************    Filters    ****************************      	 -->

		<div class="col-xs-8">

			<div class="row not-filter pull-left" style="margin-left: 60px">

				<div id="Reviews_ConcreateGroup" class="ui floating dropdown button">
					<span class="text"><div class="ui grey empty circular label"></div><u:message locale="${locale}" key="filters.all" />
						<span class="caret"></span></span>
					<div class="menu">

						<div id="allReviewsConGroup" class="item">
							<div  class="ui grey empty circular label"></div>
							<a  class="text"  style="color:black;"><u:message locale="${locale}" key="filters.all" /></a>
						</div>
						<div id="myReviewsConGroup" class="item">
							<div class="ui orange  empty circular label"></div>
							<a  class="text"style="color:black;"><u:message locale="${locale}" key="filters.my" /></a>
						</div>
						<div id="otherReviewsConGroup" class="item">
							<div class="ui blue empty circular label"></div>
							<a  class="text"style="color:black;"><u:message locale="${locale}" key="filters.other" /></a>
						</div>

					</div>
				</div>

			</div>




			<div class="row not-filter pull-left" style="margin-left: 30px">

				<div id="Reviews_Perpage_ConcreateGroup" class="ui floating  dropdown button">
					<span class="text grey"> ${recordsOnPagecg} <u:message locale="${locale}" key="filters.perpage" /> <span
						class="caret"></span></span>
					<div class="menu">

						<div id="perpage4G" class="item">
							<a  class="text" style="color:black;">4 <u:message locale="${locale}" key="filters.perpage" /></a>
						</div>
						<div id="perpage8G" class="item">
							<a  class="text" style="color:black;">8 <u:message locale="${locale}" key="filters.perpage" /></a>
						</div>


					</div>
				</div>

			</div>


			<span style="float: right;">
				<button id="activeButtonCGroup" type="button" class="btn myGreenGroupActive"
					style="margin-top: 20px; color: #FFFFFF; background-color:#66BB6A ;  margin-right: 5px; text-transform: capitalize">
					<u:message locale="${locale}" key="filters.active" /></button>     

				<button id="historyButtonCGroup" type="button" class="btn myGreyGroupDisActive"
					style="color: #FFFFFF; background-color: #bdbdbd; background-color:#BDBDBD; margin-top: 20px; margin-right: 70px">
					<u:message locale="${locale}" key="filters.history" /></button>
			</span>

			<div id="switchContent_ConcreteGroup" class="col-sm-12">

				<jsp:include page="concrete_group_content_review_all.jsp" />

			</div>
		</div>

		<!--   **********************************************************************      	 -->



	</div>











		<!-- CREATING GROUP MODAL -->
			  <div class="modal fade" id="editGroupModal" role="dialog">
			    <div style="width:900px; margin: 50px auto" class="modal-dialog modal-lg">
			      <div class="modal-content">
			        <div class="modal-header" style="text-align:center">
			          <button type="button" class="close" data-dismiss="modal">&times;</button>
			          <h2 style="font-size:20px" class="modal-title"><u:message locale="${locale}" key="cgroup.editinggroup" /> <i class="fa fa-users"></i></h2>
			        </div>
			        <div class="modal-body">
			          <div class="container-fluid">
			          <div class="row">
			          	<form style="width:40%; margin:0px auto 10px auto" class="ui fluid form">
			          	<div class="field">
						    <input id="nameOfGroup" placeholder="${groupName}" data-toggle="tooltip" data-placement="top" data-original-title="Name is empty or invalid" type="text" name="name" placeholder="<u:message locale="${locale}" key="grop.content.entername" />">
						  </div>
						  </form>
			          </div>
			          <div style="margin-bottom:10px" class="row">
			          
			         	 <h4 class="ui horizontal divider header">
  							<u:message locale="${locale}" key="cgroup.members" />
  							
					  	</h4>
			          </div>
			          <div class="row">
			          	<div class="col-md-6">
			          	
			          	<div class="row">
			          		<div class="col-md-10 col-md-offset-1">
			          		<div class="ui input" >
  							<input id="search" class="typeahead" type="text" data-provide="typeahead" autocomplete="off" placeholder="<u:message locale="${locale}" key="grop.content.search" />">
							</div>
							</div>	
			          	</div>
			          	
			          	<div class="row">
			          	
			          	</div>
			          	
			          	</div>
			          	
			          	<div class="col-md-6">
			          			<!-- selected users -->
								<div id="selectedUsersList"
									class="ui vertical list"
									style="max-height: 320px; overflow-y: auto;">
								</div>
			          	</div>
			          </div>
			          </div>
			        </div>
			        <div class="modal-footer">
			          <div style="margin-right:5px" class="row">
			          <button data-dismiss="modal" class="ui orange basic  button"><u:message locale="${locale}" key="grop.content.cancel" /></button>
			          <button id="send" class="ui basic green button"><u:message locale="${locale}" key="cgroup.save" /></button>
			          </div>
			        </div>
			      </div>
			    </div>
			  </div>
	<input type="hidden" class="form-control" name="userId" id="userId"
		value="" />





		





	<jsp:include page="../reviewCreating/reviewCreating.jsp"></jsp:include>


	<jsp:include page="../footer.jsp"></jsp:include>
	
	
<script>
$("input[id='image']").click(function() {
	$("input[id='newPhoto']").click();
});
var GroupID=${groupId};

function previewFile() {
	var preview = document.querySelector('#image');
	var file = document.querySelector('input[type=file]').files[0];
	debugger;
	
	if (file) {
		var reader = new FileReader();
		reader.onloadend = function() {
			preview.src = reader.result;
			
		}
		reader.readAsDataURL(file);
		var data = new FormData();
		$.each($('#newPhoto')[0].files, function(i, file) {
			data.append('file-' + i, file);
		});
		
		data.append("groudId",String(GroupID ));
		$.ajax({
			url : '${pageContext.servletContext.contextPath}/cabinet/changeImageGroup',
			data : data,
			cache : false,
			contentType : false,
			processData : false,
			type : 'POST',
			success : function(result) {
				if (result == 'error') {
					swal("<u:message locale="${locale}" key="cgroup.swal.error.title" />", "<u:message locale="${locale}" key="cgroup.swal.error.text" />", "error");
				}
			}
		});
	
	}
}








</script>
<script src="${pageContext.servletContext.contextPath}/resources/js/cabinet/group/deleteGroup.js"></script>



	<script type="text/javascript">
   
   $('#setting_concreateGroup').dropdown();
   $('#Reviews_ConcreateGroup').dropdown();
   $('#Reviews_Perpage_ConcreateGroup').dropdown();
   $('#concreteGroupTable').scrollTableBody({rowsToDisplay:8});
   
   
   

   $(".groupuserremove").unbind("click").bind("click", (function () {
	   window.location.reload();
	   var GroupID=${groupId};
	   $.get("${pageContext.servletContext.contextPath}/deletecurrentuserfromgroup",{groupId:GroupID},
				function(responseText) {
					$("#navBar_congroup").html(responseText);					
				});
	   
	  
	}));

   
  

   
   
   
   </script>






<script src="<c:url value="/resources/js/cabinet/reviewDeleting/reviewDeleting.js"/>"></script>


<!--  ***********************************FILTERS**************************************************** -->

<script>

	
	
	
	  $("#allReviewsConGroup").unbind("click").bind("click", (function (e) {  
		  
		   e.preventDefault();
			var GroupID=${groupId};
			
			 currentPageFilterCG='all';
			
			
			
			
			$.get("${pageContext.servletContext.contextPath}/concretegroup/review",{groupId:GroupID},function(responseText) {

												$("#switchContent_ConcreteGroup").html(responseText);
											});
		  
		}));
	
</script>


<script>

	
	
	
	  $("#myReviewsConGroup").unbind("click").bind("click", (function (e) {  
		  
		  e.preventDefault();
			var GroupID=${groupId};
			 currentPageFilterCG='my';
			$
					.get(
							"${pageContext.servletContext.contextPath}/concretegroup/reviewmy",{groupId:GroupID},
							function(responseText) {
								$("#switchContent_ConcreteGroup").html(
										responseText);
							});
		  
		  
		  
	  }));
	
</script>

<script>

	
	
	
	
	 $("#otherReviewsConGroup").unbind("click").bind("click", (function (e) {  
		 
		 e.preventDefault();
			var GroupID=${groupId};
			 currentPageFilterCG='other';
			$
					.get(
							"${pageContext.servletContext.contextPath}/concretegroup/reviewother",{groupId:GroupID},
							function(responseText) {
								$("#switchContent_ConcreteGroup").html(
										responseText);
							});
		 
		 
	 }));
	
	
	
</script>

<!--  ***********************************  //FILTERS**************************************************** -->




<!-- ************************************ FILTER BUTTONS ************************************************* -->


<script>

				
	
	 $("#activeButtonCGroup").unbind("click").bind("click", (function (e) {  
		 var GroupID=${groupId};
			$('#Reviews_ConcreateGroup').show();	
			 $('#historyButtonCGroup').css('background-color', '#BDBDBD');
			$('#activeButtonCGroup').css('background-color', '#66BB6A'); 
			
			 currentState="active";

		
		if(currentPageFilterCG === 'all'){
			$.get("${pageContext.servletContext.contextPath}/concretegroup/review",{groupId:GroupID},function(responseText) {

				$("#switchContent_ConcreteGroup").html(responseText);
			});
			
		}
		if(currentPageFilterCG === 'my'){
			
			$.get("${pageContext.servletContext.contextPath}/concretegroup/reviewmy",{groupId:GroupID},function(responseText) {

				$("#switchContent_ConcreteGroup").html(responseText);
			});
			
		}
		if(currentPageFilterCG === 'other'){
			$.get("${pageContext.servletContext.contextPath}/concretegroup/reviewother",{groupId:GroupID},function(responseText) {

				$("#switchContent_ConcreteGroup").html(responseText);
			});
			
			
		}
		
		
		
		 
	 }));
	
</script>






<script>




	
	 $("#historyButtonCGroup").unbind("click").bind("click", (function (e) {
			var GroupID=${groupId};
			
			 currentState="history";
			
			$('#Reviews_ConcreateGroup').hide();	
			
			 $('#historyButtonCGroup').css('background-color', '#757575');
			$('#activeButtonCGroup').css('background-color', '#A5D6A7'); 
			
			
			$.get("${pageContext.servletContext.contextPath}/concretegroup/reviewhistory",{groupId:GroupID},
							function(responseText) {
								$("#switchContent_ConcreteGroup").html(
										responseText);
								
							});
		 
		 
	 }));
	
</script>



<!-- ***************************************************************************************************** -->







<!--		***************************** PAGENATION ********************************		 -->
	<script>

	
	 $("#perpage4G").unbind("click").bind("click", (function (e) { 
		 e.preventDefault();
			
			var GroupID=${groupId};
			
			
			if(currentPageFilterCG == 'all' && currentState=="active"){
				
				$.get("${pageContext.servletContext.contextPath}/concretegroup/review", {
					recordsOnPagecg : 4,
					groupId:GroupID
					
				}, function(responseText) {
					$("#switchContent_ConcreteGroup").html(responseText);
				});
				
			}
			if(currentPageFilterCG == 'my' && currentState=="active"){
				
				$.get("${pageContext.servletContext.contextPath}/concretegroup/reviewmy", {
					recordsOnPagecg : 4,
					groupId:GroupID
					
				}, function(responseText) {
					$("#switchContent_ConcreteGroup").html(responseText);
				});
				
			}
			if(currentPageFilterCG == 'other' && currentState=="active"){
				$.get("${pageContext.servletContext.contextPath}/concretegroup/reviewother", {
					recordsOnPagecg : 4,
					groupId:GroupID
					
				}, function(responseText) {
					$("#switchContent_ConcreteGroup").html(responseText);
				});
				
				
			}
			//history
			
			if(currentPageFilterCG == 'all' && currentState=="history"){
				
				$.get("${pageContext.servletContext.contextPath}/concretegroup/reviewhistory", {
					recordsOnPagecg : 4,
					groupId:GroupID
					
				}, function(responseText) {
					$("#switchContent_ConcreteGroup").html(responseText);
				});
				
			}
			if(currentPageFilterCG == 'my' && currentState=="history"){
				
				$.get("${pageContext.servletContext.contextPath}/concretegroup/reviewhistory", {
					recordsOnPagecg : 4,
					groupId:GroupID
					
				}, function(responseText) {
					$("#switchContent_ConcreteGroup").html(responseText);
				});
				
			}
			if(currentPageFilterCG =='other' && currentState=="history"){
				$.get("${pageContext.servletContext.contextPath}/concretegroup/reviewhistory", {
					recordsOnPagecg : 4,
					groupId:GroupID
					
				}, function(responseText) {
					$("#switchContent_ConcreteGroup").html(responseText);
				});
				
				
			}
			
		 
		 
	 }));
	
	
	
	

	
	
	$("#perpage8G").unbind("click").bind("click", (function (e) {
		e.preventDefault();
		var GroupID=${groupId};
		
		
		if(currentPageFilterCG == 'all' && currentState=="active"){
			
			$.get("${pageContext.servletContext.contextPath}/concretegroup/review", {
				recordsOnPagecg : 8,
				groupId:GroupID
				
			}, function(responseText) {
				$("#switchContent_ConcreteGroup").html(responseText);
			});
			
		}
		if(currentPageFilterCG == 'my' && currentState=="active"){
			
			$.get("${pageContext.servletContext.contextPath}/concretegroup/reviewmy", {
				recordsOnPagecg : 8,
				groupId:GroupID
				
			}, function(responseText) {
				$("#switchContent_ConcreteGroup").html(responseText);
			});
			
		}
		if(currentPageFilterCG =='other' && currentState=="active"){
			$.get("${pageContext.servletContext.contextPath}/concretegroup/reviewother", {
				recordsOnPagecg : 8,
				groupId:GroupID
				
			}, function(responseText) {
				$("#switchContent_ConcreteGroup").html(responseText);
			});
			
			
		}
		//history
		
		if(currentPageFilterCG == 'all' && currentState=="history"){
			
			$.get("${pageContext.servletContext.contextPath}/concretegroup/reviewhistory", {
				recordsOnPagecg : 8,
				groupId:GroupID
				
			}, function(responseText) {
				$("#switchContent_ConcreteGroup").html(responseText);
			});
			
		}
		if(currentPageFilterCG == 'my' && currentState=="history"){
			
			$.get("${pageContext.servletContext.contextPath}/concretegroup/reviewhistory", {
				recordsOnPagecg : 8,
				groupId:GroupID
				
			}, function(responseText) {
				$("#switchContent_ConcreteGroup").html(responseText);
			});
			
		}
		if(currentPageFilterCG == 'other' && currentState=="history"){
			$.get("${pageContext.servletContext.contextPath}/concretegroup/reviewhistory", {
				recordsOnPagecg : 8,
				groupId:GroupID
				
			}, function(responseText) {
				$("#switchContent_ConcreteGroup").html(responseText);
			});
			
			
		}
		
		
		
		
	 }));

</script>


<!--		***************************** //PAGENATION ********************************		 -->










<script>
function getImageUrl(jsonObj){
	if (!jsonObj.imageFromServer) {
		return jsonObj.photo;
	} else {
		return "/CodeReview/get_user_image/" + jsonObj.photo;
	}
}

        $(function(){
        	
            var userObjs = {};
            var userNames = [];
			//var selectedUsers=[];
			var selectedUsers=[<c:forEach var="item" items="${arrayIds}"><c:out value="${item}"/>,</c:forEach>];
			var selectedNewUsers=[];
 			
			var group_id=${groupId};
			var group_Name=document.getElementById('GROUPNAME_ConcreteGroup').value;
			
            var throttledRequest = _.debounce(function(query, process){

                $.ajax({
                    url: '${pageContext.servletContext.contextPath}/cabinet/addgroup/SearchController'
                    ,cache: false
                    ,success: function(data){

                        userObjs = {};
                        userNames = [];

                        _.each( data, function(item, ix, list){
                            
                            
                            console.log(selectedUsers);
                            
                            if($.inArray(item.id,selectedUsers) === -1) {
									
                            	userNames.push(item.totalSearchedName);
                            	
                            	
								}

                            userObjs[item.totalSearchedName] = item;

                        });

                        process( userNames );

                    }
                });
            }, 300);


            $("#search").typeahead({      
            	 input: "#search",
            	 hint: true,
            	 source: function ( query, process ) {
                    throttledRequest( query, process );
                  } ,
            items: 5
                 
            ,highlighter: function( item ){
            	var user = userObjs[ item ];
            	
            	var imageUrl = getImageUrl(user)
          
            	 return "<div class='ui horizontal list '><div class='item user'>"+
               	
            	 "<img class='img-circle' src="+ getImageUrl(user) +">"+
					"<div class='content' style='margin-left: 80px; margin-top: 8px' ><div class='ui sub header' ><b>"+user.fname+" "+user.lname+"</b>"+"</div>"+user.login+"</div></div>";
		
            	 
               
        }
                
        ,updater: function ( selectedName ) {

                      $( "#userId" ).val( userObjs[ selectedName ].id );
						
            			var userId=$( "#userId" ).val();		
     
            			 $.get("${pageContext.servletContext.contextPath}/cabinet/addgroup/AddToTable",{id:userId}, function(user) { 
                    		selectedUsers.push(user.id);   
                    		selectedNewUsers.push(user.id); 
                    		
                    		
                    		//insert in table: selected users
                    			var userTemplate = "<div id='row"+user.id+"' class='item'>"+
                    								"<div class='right floated content'>"+
						      							"<button type='button' id='"+user.id+"' class='close deleteUsers'>&times;</button>"+
						      						"</div>"+
						      							"<img class='ui mini circular image' src="+ getImageUrl(user) +">"+
						      							"<div class='content' style='margin-left: 20px;' ><div class='ui sub header'><b>"+user.fname+" "+user.lname+"</b>"+"</div><span style='color:#757575'>"+user.login+"</span></div></div>";
						      				
						      				
                    		
                    		$('#selectedUsersList').append(userTemplate);
                    		
                    			           				
            			 });

                      return "";
                  }
                
           
            });

            $('#send').on("click",function(){
            	var name = $("#nameOfGroup").val();
            	var re = /^([0-9a-zA-ZÐ°-ÑÐ-Ð¯ÑÐ_\.]*)$/;
            	if(!re.test(name)){
            		$("#nameOfGroup").css('border','1px solid red');
            		$("#nameOfGroup").tooltip('show');
            		$("#nameOfGroup").tooltip('hide');
            	}else{
            		$("#nameOfGroup").css('border','1px solid green');
            		debugger;
            		
            		
            		if($("#nameOfGroup").val() != ""){ 
            			group_Name=$("#nameOfGroup").val();;
            		}
            		
            	
            		var jsonObj={ 
            				groupId:group_id,
                			name : group_Name,	
                			users : selectedNewUsers
                			}          
            	
            			
            					
            			
            					
            					if($("#nameOfGroup").val() == "" && selectedNewUsers.length ==0){
            						
            					}else{
            						$.get("${pageContext.servletContext.contextPath}/cabinet/addgroup/SaveToDBedit",{group :JSON.stringify(jsonObj) }, function(response) {
            	            			socket.send(JSON.stringify(response));
            						
            					});
            						$('#editGroupModal').modal('hide');
            						swal({
                					    title: "<u:message locale="${locale}" key="cgroup.swal.edit.title" />",
                					    text:  "<u:message locale="${locale}" key="cgroup.swal.edit.text" />",
                					    type:"success",
                					    allowOutsideClick : false ,
                					    animation: true ,
                					    confirmButtonColor: "#66BB6A" ,
                					    confirmButtonText: "OK", 
                					  }, function(){
                					    window.location.reload();
                					  });
            					}
            					
            					
            					
            			
            			
            			
            			
            			
            			
            			
            			

            	}
            	
        		
            	
       			});		
            
            $(document).on('click', "button.deleteUsers", function() {
            	
            	var toDelete = $(this).attr("id");
            	
            	$('#row'+toDelete).remove();
            	
            	var index=selectedUsers.indexOf(parseInt(toDelete));
            	selectedUsers.splice(index,1);
            	selectedNewUsers.splice(index,1);
     
            });
            
        });  
</script>



	<!-- 		scripts for review creating  -->
	<script
		src="${pageContext.servletContext.contextPath}/resources/js/cabinet/reviewCreating/codereview_creating.js"></script>
	<script
		src="${pageContext.servletContext.contextPath}/resources/js/cabinet/reviewCreating/tree.js"></script>
	<script
		src="${pageContext.servletContext.contextPath}/resources/js/cabinet/reviewCreating/preloader.js"></script>
	<script
		src="${pageContext.servletContext.contextPath}/resources/js/cabinet/reviewCreating/chosen.jquery.js"></script>
	<!-- 		end of scripts for review creating  -->
	
	
	
	<script>

$( document ).ready(function() {
    console.log( "ready!" );

$(function(){
	
    var reviewObjs = {};
    var reviewNames = [];
	var selectedReviews=[];
	 

    var throttledRequest = _.debounce(function(query, process){

         $.ajax({
            url: '${pageContext.servletContext.contextPath}/cabinet/searchReviews'
            ,cache: false
            ,success: function(data){

                reviewObjs = {};
                reviewNames = [];

                _.each( data, function(item, ix, list){
                    
                    
                    console.log(selectedReviews);
                    
                    if($.inArray(item.id,selectedReviews) === -1) {
							
                    	reviewNames.push(item.name);
                    	
						}

                    reviewObjs[item.name] = item;

                });

                process( reviewNames );

            }
        });
    }, 300);


    $("#searchCodeReview").typeahead({      
    	 input: "#searchCodeReview",
    	 hint: true,
    	 source: function ( query, process ) {
            throttledRequest( query, process );
          }
    ,highlighter: function( item ){
    	var review = reviewObjs[ item ];
    	
    	var imageUrl;
    	if(review.gitURL != null){
    		imageUrl='${pageContext.servletContext.contextPath}/resources/images/cabinet/git.png';
    	}else{
    		imageUrl='${pageContext.servletContext.contextPath}/resources/images/cabinet/zip.png';
    	}
    	debugger;
    	var reviewCId=review.codeReviewId;
    	
  
    	 return "<div   class='ui horizontal list ' onclick='loadCurrentReview("+reviewCId+")';><div   class='item'>"+
       	
    	 "<img  src="+ imageUrl +" >"+
			"<div class='content' style='margin-left: 80px; margin-top: 8px; ' ><div style='min-width:272px; max-width:272px; '><b>"+review.name+" "+review.userName+''+"</b>"+"</div>"+review.active+"</div></div>";

		}
        
	,updater: function ( selectedName ) {

              return "";
          }
        
   
    })});
    
    
    
    
    
    
function loadCurrentReview(reviewId) {

    $.get("${pageContext.servletContext.contextPath}/review/"+reviewId, function(responseText) {
    	location.replace("${pageContext.servletContext.contextPath}/review/"+reviewId);
	});
}

});
</script>
</body>

</html>