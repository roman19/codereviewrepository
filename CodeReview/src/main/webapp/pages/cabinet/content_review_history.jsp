<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>
<style>
.truncate {
  width: 200px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}


</style>
<script>
	var currentPageFilter= 'all' ;
</script>


<div class="row">

	<div class="input-group-btn search-panel">


		<div id="perpageReview_ReviewHistory" class="ui floating dropdown button"
				style="margin-top: 50px;">
				<span class="text">  ${recordsPerPage} <u:message locale="${locale}" key="filters.perpage" /> <span class="caret"></span></span>
				<div class="menu">

					<div  class="item">
					
						<a  class="text perpage3ReviewHistory">3 <u:message locale="${locale}" key="filters.perpage" /></a>
					</div>
					<div  class="item">
						
						<a   class="text perpage6ReviewHistory">6 <u:message locale="${locale}" key="filters.perpage" /></a>
					</div>
					<div  class="item">
						
						<a   class="text perpage9ReviewHistory">9 <u:message locale="${locale}" key="filters.perpage" /></a>
					</div>

				</div>
			</div>
		
		
		
		 <span style="float: right;">
			<button id="activeButtonReviewHistory" type="button" class="btn"
				style="margin-top: 50px; color: #FFFFFF; background-color:#a5d6a7; margin-right: 5px; text-transform: capitalize">
				<u:message locale="${locale}" key="filters.active" /></button>
<!-- background-color: #a5d6a7; -->


			<button id="historyButtonReviewHistory" type="button" class="btn "
				style="color: #FFFFFF; background-color: #757575; margin-top: 50px; margin-right: 70px">
				<u:message locale="${locale}" key="filters.history" /></button>
				
<!-- background-color: #757575; -->
				
		</span>

	</div>

</div>

		










<script>



$('#perpageReview_ReviewHistory').dropdown();

	
	$(".perpage3ReviewHistory").unbind("click").bind("click", (function (e) {
		e.preventDefault();
	
							$.get("${pageContext.servletContext.contextPath}/cabinet/review/history_all", {
								recordsOnPage : 3
							}, function(responseText) {
								$("#switchReviewContentHistory").html(responseText);
							});

	})).delay( 0 ).fadeIn(0);
	
	

	
	$(".perpage6ReviewHistory").unbind("click").bind("click", (function (e) {
		e.preventDefault();
	
						$.get("${pageContext.servletContext.contextPath}/cabinet/review/history_all", {
							recordsOnPage : 6
						}, function(responseText) {
							$("#switchReviewContentHistory").html(responseText);
						});

	})).delay( 0 ).fadeIn(0);


	
	
	
	$(".perpage9ReviewHistory").unbind("click").bind("click", (function (e) {
		e.preventDefault();
					
						$.get("${pageContext.servletContext.contextPath}/cabinet/review/history_all", {
							recordsOnPage : 9
						}, function(responseText) {
							$("#switchReviewContentHistory").html(responseText);
						});

	})).delay( 0 ).fadeIn(0);
	

</script>








<script>

	
	$("#activeButtonReviewHistory").unbind("click").bind("click", (function () {
		$.get("${pageContext.servletContext.contextPath}/cabinet/review",function(responseText) {
			$("#switchContent").html(responseText);
								window.location.hash = "#reviews";
							});
	}));
	


	$("#historyButtonReviewHistory").unbind("click").bind("click", (function () {
		$.get("${pageContext.servletContext.contextPath}/cabinet/review/history",function(responseText) {
			  $("#switchContent").html(responseText);
									clearURL();
									window.location.hash = "#history";
								});
	}));





</script>

		

<div id="switchReviewContentHistory">
	<jsp:include page="content_review_history_all.jsp"/>
</div>

