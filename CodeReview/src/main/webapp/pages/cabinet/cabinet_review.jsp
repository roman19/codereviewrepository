<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>

<html>

<head>

<link rel="icon" type="image/png" href="${pageContext.servletContext.contextPath}/resources/images/codereview_icon.png">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>${user.firstName} ${user.lastName}</title>
	

<!-- <!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script type="text/javascript" src="resources/js/main.js"></script>


<script
	src="${pageContext.servletContext.contextPath}/pages/cabinet/jquery-1.9.1.min.js"></script>

<script src="<c:url value="/resources/semantic_ui/js/semantic.js"/>"></script>


<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/resources/css/reviewCreating/tree.css"
	type="text/css" media="screen">
<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/resources/css/reviewCreating/chosen.css">
<%-- <link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/resources/css/reviewCreating/style.css"> --%>
<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/resources/css/reviewCreating/reviewCreating.css">
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/fontawesome/css/font-awesome.min.css">

<script
	src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
	
<script src="//underscorejs.org/underscore-min.js"></script>

 
<!-- for notifications -->
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/semantic_ui/css/semantic.css"/>"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/cabinet/notification/notification.css"/>"/>
<script src="<c:url value="/resources/semantic_ui/js/transition.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/semantic_ui/js/dropdown.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/js/cabinet/notification/notification.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap-maxlength.js"/>" type="text/javascript"></script>
<!-- <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

<!-- for online notifications -->
<script src="<c:url value="/resources/bootstrap_notify/bootstrap-notify.js"/>"></script>
<script src="<c:url value="/resources/bootstrap_notify/onlineNotification.js"/>"></script>
<link rel="stylesheet" href="<c:url value="/resources/bootstrap_notify/animate.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/bootstrap_notify/onlineNotification.css"/>">

<!-- for file input -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
<!-- end for file input -->

<link rel="stylesheet" 
	type="text/css" href="${pageContext.servletContext.contextPath}/resources/sweetalert/css/sweetalert.css">

<script src="${pageContext.servletContext.contextPath}/resources/sweetalert/js/sweetalert.min.js"></script>






<!-- ***********************  SideBar CSS  *************************** -->
<link rel="stylesheet" href="<c:url value="/resources/css/cabinet/simple-sidebar.css"/>">
<!-- ************************************************** -->


<%-- <script src="<c:url value="/resources/semantic_ui/js/semantic.js"/>"></script> --%>

<!-- ***************  From Main jsp  ***************** -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/main.css">
<!-- ************************************************** -->

<!-- for creating group -->
<%-- <script src="<c:url value="/resources/js/cabinet/groupCreating.js"/>" charset="utf-8"></script> --%>

<style>
.card-shadow{
	-webkit-box-shadow: 0 1px 2px #777;
	-moz-box-shadow: 0 2px 1px #777;
}


</style>
</head>
<body onload="connect(${userID});" onunload="disconnect();">

	<input id="mapping" type="hidden" value="${pageContext.servletContext.contextPath}/"/>
	<input id="locale_id" type="hidden" value="${locale}"/>
<!-- ****************** Header************************* -->		
	<jsp:include page="../header.jsp"></jsp:include>
<!-- ************************************************* -->


	<div  id="wrapper" class="row row1" > 
	
<div  class="col1 col-sm-1"  >	
<!-- ****************** Sidebar ************************* -->		
			<jsp:include page="cabinet_sidebar2.jsp"/>
<!-- ************************************************* -->	
</div>


<div class="col1 col-sm-11" id="switchContent" >			
			
<!-- *********************Page Content****************** -->
	
	
			<%-- <jsp:include page="content_review.jsp"/> --%>
	
	
<!-- *************** /#page-content-wrapper ************ -->
</div>
</div>
	
	<!-- /#wrapper -->
	
	



<!-- ****************** Footer ************************* -->
	<div class="row">
				<jsp:include page="../footer.jsp"/>
	</div>
<!-- ************************************************* -->	
				
	<!-- 	modal window for review creating -->
	<div class="modal fade" id="codeReviewCreatingModal" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h1 class="modal-title"><u:message locale="${locale}" key="review.creating.title" /></h1>
				</div>
				<div style="overflow: auto; max-height: 400px;">
					<div id="reviewCreatingModalBodyContent" class="modal-body">
						<div id="downloadChoice">
							<input id="downloadChoiceButton" onclick="git()"
								class="btn btn-1 downloadButton" type="button" name=""
								value="<u:message locale="${locale}" key="review.creating.git" />"> <input
								id="archiveButton" onclick="archive()"
								class="btn btn-1 downloadButton" type="button" name=""
								value="<u:message locale="${locale}" key="review.creating.archive" />">
						</div>
					</div>
					<div id="anotherContent">
						<div id="tree"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><u:message locale="${locale}" key="review.creating.exit" /></button>
				</div>
			</div>
		</div>
	</div>
	<!-- 	end of modal window for review creating -->



	<script src="<c:url value="/resources/js/cabinet/reviewDeleting/reviewDeleting.js"/>"></script>
	<script
		src="${pageContext.servletContext.contextPath}/resources/js/cabinet/reviewCreating/codereview_creating.js"></script>
	<script
		src="${pageContext.servletContext.contextPath}/resources/js/cabinet/reviewCreating/tree.js"></script>

	<script
		src="${pageContext.servletContext.contextPath}/resources/js/cabinet/reviewCreating/preloader.js"></script>
	<script
		src="${pageContext.servletContext.contextPath}/resources/js/cabinet/reviewCreating/chosen.jquery.js"></script>
	<!-- 		end of scripts for review creating  -->

<!-- ****************Custom for sidebar*************** -->
<script src="${pageContext.servletContext.contextPath}/resources/js/cabinet/cabinet_main.js"></script>
<!-- ******************************************************** -->





</body>

</html>
