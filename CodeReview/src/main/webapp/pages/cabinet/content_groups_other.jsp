<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<style>
.truncate {
  width: 200px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}


</style>
	
<c:if test="${fn:length(listGroups) gt 0}">
	<div class="ui special cards">
		<c:forEach var="i" items="${listGroups}">

			<div class="card"
				style="min-height: 260px; margin-top: 30px; margin-left: 35px; width:250px; border: 2px solid #2185D0"
				id="card${i.groupId}">
				<div class="blurring dimmable image" style="width:250px">
					<div class="ui dimmer">
						<div class="content">
							<div class="center">
								 <a
									class="ui blue inverted button deleteCard" 
									onclick="leaveGroup(${i.groupId})"><u:message locale="${locale}" key="grop.content.leavegroup" /></a>
							</div>
						</div>
					</div>
					<div style=" width:246px">
					<img    
						src="<c:url value="/get_group_image/${i.image_url}"/>" style="width: 100%">
						
					</div>	
				</div>
				<div class="content">
					<a class="header truncate"
						href="${pageContext.servletContext.contextPath}/group/${i.groupId}">
						${i.groupName}</a>
						
						
					<div class="meta truncate">
						<span class="date"><u:message locale="${locale}" key="review.content.owner" />: <b>${i.userFirstName}</b>
							${i.userLastName}</span>
					</div>
				</div>
				<div class="extra content">
					<a> <i class="users icon"></i> 
							<c:choose>
									<c:when test="${i.totalUserNumerous==1}">1 <u:message locale="${locale}" key="grop.content.member" /></c:when>
									<c:otherwise>${i.totalUserNumerous} <u:message locale="${locale}" key="grop.content.members" /></c:otherwise>
							</c:choose>
					</a>
				</div>
			</div>

		</c:forEach>

	</div>



<div class="text-center" style="margin-top: 60px; margin-right: 60px">

<ul class="pagination "  >

	<c:forEach begin="1" end="${noOfPages}" var="i">
		<li><a href="#" class="topageRevGroupOther" style="color:#212121; border-color: #212121;" id="${i}">${i}</a></li>
	</c:forEach>

	

</ul>

</div>


</c:if>

<c:if test="${fn:length(listGroups) == 0}">

     <h1 style="text-align: center; color: #E3E3E6"><u:message locale="${locale}" key="group.content.emptypage" /> </h1>


</c:if>



<script>


//Pagenation		
var pagen=${currentPage};
if(document.getElementById(pagen)!= null){
	document.getElementById(pagen).style.backgroundColor = "#212121";
	document.getElementById(pagen).style.color = "#FFFFFF";
	}


$('.special.cards .image').dimmer({
	on : 'hover'
});


$(".topageRevGroupOther").unbind("click").bind("click", (function (e) {		
	e.preventDefault();
	var toPage = $(this).attr("id");
	$.get("${pageContext.servletContext.contextPath}/cabinet/groups/other", {
		page : toPage
	}, function(responseText) {
		$("#switchGroupContent").html(responseText);
	});

}));

</script>

