<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="i" uri="../../WEB-INF/showImageUrl.tld"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>

<link
	href="${pageContext.servletContext.contextPath}/resources/css/cabinet/settings/settings.css"
	rel="stylesheet">
<script
	src="${pageContext.servletContext.contextPath}/resources/js/cabinet/settings/settings.js"></script>

<div class="col-md-10">

	<div class="container-fluid">

		<div class="row settings-title">
			<h2><u:message locale="${locale}" key="settings.personal.memory.title" /></h2>
		</div>
		<div class="ui clearing divider"></div>
		<div class="row">
			<div class="col-xs-9">
				<h5 style="text-align: center;">${memory}</h5>
				<div class="progress" style="height: 35px; background-color: #CBC8C8">
					<div class="progress-bar"
						role="progressbar" aria-valuenow="40" aria-valuemin="0"
						aria-valuemax="100" style="width: ${memoryPercentage}%; font-size: 14px;">
						<b><fmt:formatNumber type="number" 
           					 maxFractionDigits="0" value="${memoryPercentage}" />%</b></div>
				</div>
			</div>
			<div class="col-xs-3">
				<button class="ui primary button" onclick="openBuyModal()" 
				 style="margin-top: 30px; font-size: 14px; color: white">
				 <i class="fa fa-shopping-cart" style="display: inline-block; margin-right: 5px;">
				 </i><u:message locale="${locale}" key="settings.personal.memory.buy" /></button>
			</div>
		</div>

		<div class="row settings-title">
			<h2><u:message locale="${locale}" key="settings.personal.title" /></h2>
		</div>
		<div class="ui clearing divider"></div>
		<div class="row ui fluid form">
			<div class="col-xs-7">
				<div class="row fields">
					<div class="col-xs-3">
						<div class="ui right pointing label"><u:message locale="${locale}" key="settings.personal.fname" /></div>
					</div>
					<div class="col-xs-9">
						<input id="fname" type="text"
							placeholder="${user.firstName}" autofocus
							data-toggle="tooltip" data-placement="right"
							data-original-title="<u:message locale="${locale}" key="settings.personal.tooltip.fname" />">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-3 fields">
						<div class="ui right pointing label"><u:message locale="${locale}" key="settings.personal.lname" /></div>
					</div>
					<div class="col-xs-9 fields">
						<input id="lname" type="text"
							placeholder="${user.lastName}" data-toggle="tooltip"
							data-placement="right"
							data-original-title="<u:message locale="${locale}" key="settings.personal.tooltip.fname" />">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-3 fields">
						<div class="ui right pointing label"><u:message locale="${locale}" key="settings.personal.login" /></div>
					</div>
					<div class="col-xs-9 fields">
						<input id="login" type="text" placeholder="${user.login}"
							data-toggle="tooltip" data-placement="right">
					</div>
				</div>
				<div class="row" style="margin-top: 20px;">
					<div class="col-xs-9"></div>
					<div class="col-xs-3 ">
						<button id="savePersonal" class="ui primary button"
							onclick="savePersonalData('${locale}')"><u:message locale="${locale}" key="settings.personal.save" /></button>
					</div>
				</div>
				<c:if test="${user.social == null}">
					<div class="row settings-title password"
						style="margin-bottom: 10px;">
						<h2><u:message locale="${locale}" key="settings.personal.password.title" /></h2>
					</div>
					<div class="ui clearing divider"></div>
					<div class="row inline field fields">
						<div class="col-xs-4">
							<div class="ui right pointing label"><u:message locale="${locale}" key="settings.personal.password.current" /></div>
						</div>
						<div class="col-xs-7">
							<input id="currentPassword" type="password" data-toggle="tooltip"
								data-placement="right" data-original-title="<u:message locale="${locale}" key="settings.personal.password.tooltip.old" />">
						</div>
					</div>
					<div class="row inline field fields">
						<div class="col-xs-4">
							<div class="ui right pointing label"><u:message locale="${locale}" key="settings.personal.password.new" /></div>
						</div>
						<div class="col-xs-7">
							<input id="newPassword" type="password" data-toggle="tooltip"
								data-placement="right"
								data-original-title="<u:message locale="${locale}" key="settings.personal.password.tooltip.new" />">
						</div>
					</div>
					<div class="row inline field fields">
						<div class="col-xs-4">
							<div class="ui right pointing label"><u:message locale="${locale}" key="settings.personal.password.repeat" /></div>
						</div>
						<div class="col-xs-7">
							<input id="repeatPassword" type="password" data-toggle="tooltip"
								data-placement="right"
								data-original-title="<u:message locale="${locale}" key="settings.personal.password.tooltip.repeat" />">
						</div>
					</div>
					<div class="row" style="margin-top: 20px;">
						<div class="col-xs-9"></div>
						<div class="col-xs-3 ">
							<button id="savePassword" class="ui primary button"
								onclick="savePassword()"><u:message locale="${locale}" key="settings.personal.save" /></button>
						</div>
					</div>
				</c:if>
			</div>
			<div class="row">
				<div class="col-xs-1"></div>
				<div class="col-xs-4 fields">
					<div class="row">
						<div class="col-xs-12">
							<label style="text-align: center;"> <input id="image"
								type="image"
								src="<i:img-url fromServer="${user.imageFromServer}" url="${user.imageUrl}" />"
								class="img-circle" style="max-width: 320px; max-height: 320px;"
								style="margin-bottom: 10px;" /> <input type="file"
								id="newPhoto" style="display: none;" onchange="previewFile()"
								accept="image/*" />
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row settings-title">
			<h2><u:message locale="${locale}" key="settings.notifications.title" /></h2>
		</div>
		<div class="ui clearing divider"></div>
		<div class="row ui fluid form">
			<div class="col-xs-7">
				<div class="row fields">
					<div class="col-xs-1"></div>
					<div class="col-xs-10">
						<div class="ui toggle checkbox">
							<%-- 							<input id="all_notifications" type="checkbox" name="public" --%>
							<%-- 								onclick="notificationItem('all_notifications')"><label>All email --%>
							<!-- 								notifications</label> -->

							<a class="all_notifications"
								onclick="notificationItem('all_notifications', true)"><u:message locale="${locale}" key="settings.notifications.on.all" /></a>
							<a class="all_notifications"
								onclick="notificationItem('all_notifications', false)"><u:message locale="${locale}" key="settings.notifications.off.all" /></a>
						</div>
					</div>
				</div>
				<div class="row fields">
					<div class="col-xs-10">
						<div class="ui toggle checkbox">
							<input id="group_notification" type="checkbox" name="public"
								onclick="notificationItem('group_notification')"><label>
									<u:message locale="${locale}" key="settings.notifications.group" />
								</label>
						</div>
					</div>
				</div>
				<div class="row fields">
					<div class="col-xs-10">
						<div class="ui toggle checkbox">
							<input id="review_notification" type="checkbox" name="public"
								onclick="notificationItem('review_notification')"><label>
									<u:message locale="${locale}" key="settings.notifications.review" />
								</label>
						</div>
					</div>
				</div>
				<div class="row fields">
					<div class="col-xs-10">
						<div class="ui toggle checkbox">
							<input id="file_notification" type="checkbox" name="public"
								onclick="notificationItem('file_notification')"><label>
									<u:message locale="${locale}" key="settings.notifications.file" />
								</label>
						</div>
					</div>
				</div>
				<div class="row fields">
					<div class="col-xs-10">
						<div class="ui toggle checkbox">
							<input id="issue_notification" type="checkbox" name="public"
								onclick="notificationItem('issue_notification')"><label>
									<u:message locale="${locale}" key="settings.notifications.issue" />
								</label>
						</div>
					</div>
				</div>
				<div class="row fields">
					<div class="col-xs-10">
						<div class="ui toggle checkbox">
							<input id="reply_notification" type="checkbox" name="public"
								onclick="notificationItem('reply_notification')"><label>
									<u:message locale="${locale}" key="settings.notifications.reply" />
								</label>
						</div>
					</div>
				</div>
				<div class="row fields"></div>
				<div class="ui clearing divider"></div>
				<div class="row fields" style="margin-bottom: 20px;">
					<div class="col-xs-10">
						<div class="ui toggle checkbox">
							<input id="sound_notification" type="checkbox" name="public"
								onclick="notificationItem('sound_notification')"><label>
									<u:message locale="${locale}" key="settings.notifications.sound" />
								</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<jsp:include page="buyMemoryModal.jsp"/>


<script>
	var review = ${review};
	var group = ${group};
	var fileAdded = ${fileAdded};
	var issue = ${issue};
	var reply = ${reply};
	var isSound = ${isSound};
	$('#group_notification').prop('checked', group);
	$('#review_notification').prop('checked', review);
	$('#file_notification').prop('checked', fileAdded);
	$('#issue_notification').prop('checked', issue);
	$('#reply_notification').prop('checked', reply);
	$('#sound_notification').prop('checked', isSound);
</script>

