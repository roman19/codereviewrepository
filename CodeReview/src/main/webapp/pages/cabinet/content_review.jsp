<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>

<script>
	var currentPageFilter= 'all' ;
</script>


<div class="row">

	<div class="input-group-btn search-panel">

		<div class="row not-filter pull-left">

			<div id="typesSelector_ReviewAll" class="ui floating dropdown button"
				style="margin-top: 30px">
				<span class="text">
					<div class="ui grey empty circular label"></div><u:message locale="${locale}" key="filters.all" /> <span
					class="caret"></span>
				</span>
				<div class="menu">

					<div id="allReview_ReviewAll" class="item">
						<div class="ui grey empty circular label"></div>
						<span class="text"><u:message locale="${locale}" key="filters.all" /> </span>
					</div>
					<div id="myReview_ReviewAll" class="item">
						<div class="ui orange  empty circular label"></div>
						<span class="text"><u:message locale="${locale}" key="filters.my" /> </span>
					</div>
					<div id="otherReview_ReviewAll" class="item">
						<div class="ui blue empty circular label"></div>
						<span class="text"><u:message locale="${locale}" key="filters.other" /> </span>
					</div>

				</div>
			</div>

		</div>




		
		
		<div id="perpageReview_ReviewAll" class="ui floating dropdown button"
				style="margin-top: 50px;margin-left: 35px;">
				<span class="text">  ${recordsPerPage} <u:message locale="${locale}" key="filters.perpage" />  <span class="caret"></span></span>
				<div class="menu">

					<div  class="item perpage3ReviewAll">
					
						<a  class="text ">3 <u:message locale="${locale}" key="filters.perpage" /> </a>
					</div>
					<div  class="item perpage6ReviewAll">
						
						<a   class="text ">6  <u:message locale="${locale}" key="filters.perpage" /></a>
					</div>
					<div  class="item perpage9ReviewAll">
						
						<a   class="text ">9  <u:message locale="${locale}" key="filters.perpage" /></a>
					</div>

				</div>
			</div>
		
		
		
		 <span style="float: right;">
			<button id="activeButtonReviewAll" type="button" class="btn"
				style="margin-top: 50px; color: #FFFFFF; background-color: #66bb6a; margin-right: 5px; text-transform: capitalize">
				 <u:message locale="${locale}" key="filters.active" /></button>
<!-- background-color: #a5d6a7; -->


			<button id="historyButtonReviewAll" type="button" class="btn "
				style="color: #FFFFFF; background-color: #bdbdbd; margin-top: 50px; margin-right: 70px">
				 <u:message locale="${locale}" key="filters.history" /></button>
				
<!-- background-color: #757575; -->
				
		</span>

	</div>

</div>

		
		











<script>

$('#typesSelector_ReviewAll').dropdown();

$('#perpageReview_ReviewAll').dropdown();


	
	$(".perpage3ReviewAll").unbind("click").bind("click", (function (e) {
		e.preventDefault();
		
					if(currentPageFilter === 'all'){
							
							$.get("${pageContext.servletContext.contextPath}/cabinet/review/all", {
								recordsOnPage : 3
							}, function(responseText) {
								$("#switchReviewContent").html(responseText);
							});
					
					}
					
					if(currentPageFilter === 'my'){
						
							$.get("${pageContext.servletContext.contextPath}/cabinet/review/my", {
								recordsOnPage : 3
							}, function(responseText) {
								$("#switchReviewContent").html(responseText);
							});
						
					}
					
					if(currentPageFilter === 'other'){
						
							$.get("${pageContext.servletContext.contextPath}/cabinet/review/other", {
								recordsOnPage : 3
							}, function(responseText) {
								$("#switchReviewContent").html(responseText);
							});
						
					}

		
	})).delay( 0 ).fadeIn(0);
	
	

	
	$(".perpage6ReviewAll").unbind("click").bind("click", (function (e) {
		e.preventDefault();
		
				if(currentPageFilter === 'all'){
						
						$.get("${pageContext.servletContext.contextPath}/cabinet/review/all", {
							recordsOnPage : 6
						}, function(responseText) {
							$("#switchReviewContent").html(responseText);
						});
						
				
				}
				
				if(currentPageFilter === 'my'){
						
						$.get("${pageContext.servletContext.contextPath}/cabinet/review/my", {
							recordsOnPage : 6
						}, function(responseText) {
							$("#switchReviewContent").html(responseText);
						});
					
					
				}
				
				if(currentPageFilter === 'other'){
						
						$.get("${pageContext.servletContext.contextPath}/cabinet/review/other", {
							recordsOnPage : 6
						}, function(responseText) {
							$("#switchReviewContent").html(responseText);
						});
					
					
				}
	})).delay( 0 ).fadeIn(0);


	
	
	
	$(".perpage9ReviewAll").unbind("click").bind("click", (function (e) {
		e.preventDefault();
		
				if(currentPageFilter === 'all'){
						
						$.get("${pageContext.servletContext.contextPath}/cabinet/review/all", {
							recordsOnPage : 9
						}, function(responseText) {
							$("#switchReviewContent").html(responseText);
						});
						
				
				}
				
				if(currentPageFilter === 'my'){
						
						$.get("${pageContext.servletContext.contextPath}/cabinet/review/my", {
							recordsOnPage : 9
						}, function(responseText) {
							$("#switchReviewContent").html(responseText);
						});
					
					
				}
				
				if(currentPageFilter === 'other'){
						
						$.get("${pageContext.servletContext.contextPath}/cabinet/review/other", {
							recordsOnPage : 9
						}, function(responseText) {
							$("#switchReviewContent").html(responseText);
						});
					
					
				}
		
	})).delay( 0 ).fadeIn(0);
	
	
	
	

</script>








<script>

	
	$("#activeButtonReviewAll").unbind("click").bind("click", (function () {
		$.get("${pageContext.servletContext.contextPath}/cabinet/review",function(responseText) {
			$("#switchContent").html(responseText);
								window.location.hash = "#reviews";
							});
	}));
	


	$("#historyButtonReviewAll").unbind("click").bind("click", (function () {
		$.get("${pageContext.servletContext.contextPath}/cabinet/review/history",function(responseText) {
			  $("#switchContent").html(responseText);
									clearURL();
									window.location.hash = "#history";
								});
	}));




$("#allReview_ReviewAll").unbind("click").bind("click", (function () {
	
	
	currentPageFilter="all";
	
	$.get("${pageContext.servletContext.contextPath}/cabinet/review/all",
			function(responseText) {
				$("#switchReviewContent").html(
						responseText);
				window.location.hash = "#reviews";
			});
	
}));




$("#myReview_ReviewAll").unbind("click").bind("click", (function () {
	
	currentPageFilter="my";
	
	$.get("${pageContext.servletContext.contextPath}/cabinet/review/my",
			function(responseText) {
				$("#switchReviewContent").html(
						responseText);
			});
	
}));



$("#otherReview_ReviewAll").unbind("click").bind("click", (function () {
	
	currentPageFilter="other";
	
	$.get("${pageContext.servletContext.contextPath}/cabinet/review/other",
			function(responseText) {
				$("#switchReviewContent").html(
						responseText);
			});
	
}));



</script>


<div id="switchReviewContent">
	<jsp:include page="content_review_all.jsp"/>
</div>
