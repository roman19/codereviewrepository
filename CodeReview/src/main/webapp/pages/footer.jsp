<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="u" uri="../WEB-INF/showMessage.tld" %>

<footer id="footer1">
        <div class="leftFooter">
            <div class="firstBlock">
                <a href="#" data-toggle="modal" data-target="#signupModal"><h4><u:message locale="${locale}" key="footer.createAccount" /></h4></a>
                <ul>
                    <li><a href=""><u:message locale="${locale}" key="footer.rules" /></a></li>
                    <li><a href=""><u:message locale="${locale}" key="footer.support" /></a></li>
                    <li><a href=""><u:message locale="${locale}" key="footer.about" /></a></li>
                </ul>
            </div>
            <div class="secondBlock">
            <a href=""><h4><u:message locale="${locale}" key="footer.ourTeam" /></h4></a>
				<ul>
					<li><u:message locale="${locale}" key="footer.roman" /></li>
					<li><u:message locale="${locale}" key="footer.vika" /></li>
					<li><u:message locale="${locale}" key="footer.vitalii" /></li>
					<li><u:message locale="${locale}" key="footer.andrii" /></li>
				</ul>
			</div>
            <p class = "rights">©2016 Code-Review.com</p>
        </div>
        <div class="rightFooter">
            <div class = "logoDiv">
            <span>C</span>
            <img src="${pageContext.servletContext.contextPath}/resources/images/dark_logo.png" alt="logo">
            <span>de Review</span>
            </div>
             <p><u:message locale="${locale}" key="footer.weHelp" /></p>
             
             <div id="language">

				<c:choose>

					<c:when test="${locale eq 'uk_UA'}">

						<span class="toUA"><b>Українська</b></span>
						<span class="toUS">Англійська</span>
					</c:when>
					<c:otherwise>
						<span class="toUA">Ukrainian</span>
						<span class="toUS"><b>English</b></span>
					</c:otherwise>
				</c:choose>
			</div>
        </div>
    </footer>