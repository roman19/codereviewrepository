<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="u" uri="../WEB-INF/showMessage.tld"%>



<style>

  #searchCodeReview.typeahead{
		min-width: 400px;
		max-width: 400px;
		overflow: hidden;	
		min-height: 10px;
		max-height: 319px;
	}
	
	ul.typeahead.dropdown-menu li{
		margin-left:0px;
		margin-bottom:0px; 
	}
	
	.user img{
		float:left;
		max-width: 50px;
		max-height: 50px;
		min-width:50px;
		min-height:50px;
		margin-left: 5px
	}
	 .user .login{
	 float:right;
	 margin-left: 5px;
	}
	.user .lname{
	float:right;
	margin-left: 5px;
	 }
	 .user .fname{
	float:right;
	margin-left: 10px;
	}

	.vertical-center {
	  min-height: 1%;  
 	  min-height: 1vh; 
      display: flex;
      align-items: center;
      }
	

	
      
      #selectedUsersList::-webkit-scrollbar {
      width: 5px;
	}
	
	#selectedUsersList::-webkit-scrollbar-button {
		width: 8px;
		height: 5px;
	}
	
	#selectedUsersList::-webkit-scrollbar-track {
		background: #eee;
		border: thin solid lightgray;
		box-shadow: 0px 0px 3px #dfdfdf inset;
		border-radius: 10px;
	}
	
	#selectedUsersList::-webkit-scrollbar-thumb {
		background: #999;
		border: thin solid gray;
		border-radius: 10px;
	}
	
	#selectedUsersList::-webkit-scrollbar-thumb:hover {
		background: #7d7d7d;
	}
	
	.dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover {
    color: #fff;
    text-decoration: none;
    background-color: rgba(84, 177, 185, 0.76) !important;
    outline: 0;


</style>


<header>
	<div class="logo">
        <a href="${pageContext.servletContext.contextPath}/">
		<img
			src="${pageContext.servletContext.contextPath}/resources/images/logo.png"
			alt="logo">
		<p class='logo-text'>Code Review</p>
        </a>
        
        
	</div>
	
	<c:choose>
		<c:when test="${userID == null}">
			<div class="rightHeader">
                <a href="#step1"><u:message locale="${locale}" key="index.howItWorks" /></a> <a href="#" class="sign_"
                    data-toggle="modal" data-target="#signinModal"><u:message
                        locale="${locale}" key="signin" /></a> <a href="#" class="sign_"
                    data-toggle="modal" data-target="#signupModal"><u:message
                        locale="${locale}" key="signup" /></a>
            </div>
		</c:when>
		<c:otherwise>
			<div class="rightHeader">

									<div id="reviewResearcher" class="ui search" style="float:left; margin-right: 130px; margin-top: 4px; ">
										  <div class="ui icon input">																																				
										    <input id="searchCodeReview" class="prompt  typeahead" type="text"   data-provide="typeahead" autocomplete="off" style="min-width: 400px" placeholder="<u:message locale="${locale}" key="review.content.search" />">
										    
										    <i class="search icon"></i>
										  </div>										 
									</div>
				
				<a id="settings_login" href="${pageContext.servletContext.contextPath}/cabinet" style="color:white">${login} </a>

				<div class="cabinet-icon">
					<img
						src="${pageContext.servletContext.contextPath}/resources/images/cabinet/profile.png"
						alt="logo">
				</div>

				<a href="${pageContext.servletContext.contextPath}/logout"
					class="sign_"><u:message locale="${locale}" key="logout" /></a>
			</div>
		</c:otherwise>
	</c:choose>
</header>

<script>

$(function(){

	if (window.location.pathname == "${pageContext.servletContext.contextPath}/") {
        $('#reviewResearcher').hide();
  } else {
        $('#reviewResearcher').show();
  }
	
	
	});





$(function(){
	
    var reviewObjs = {};
    var reviewNames = [];
	var selectedReviews=[];
	 

    var throttledRequest = _.debounce(function(query, process){

         $.ajax({
            url: '${pageContext.servletContext.contextPath}/cabinet/searchReviews'
            ,cache: false
            ,success: function(data){

                reviewObjs = {};
                reviewNames = [];

                _.each( data, function(item, ix, list){
                    
                    
                    console.log(selectedReviews);
                    
                    if($.inArray(item.id,selectedReviews) === -1) {
							
                    	reviewNames.push(item.totalSearchName);
                    	
						}

                    reviewObjs[item.totalSearchName] = item;

                });

                process( reviewNames );

            }
        });
    }, 300);


    $("#searchCodeReview").typeahead({      
    	 input: "#searchCodeReview",
    	 hint: true,
    	 source: function ( query, process ) {
            throttledRequest( query, process );
          },items: 7
    ,highlighter: function( item ){
    	var review = reviewObjs[ item ];
    	
    	var imageUrl;
    	if(review.gitURL != null){
    		imageUrl='${pageContext.servletContext.contextPath}/resources/images/cabinet/git.png';
    	}else{
    		imageUrl='${pageContext.servletContext.contextPath}/resources/images/cabinet/zip.png';
    	}
    	debugger;
    	var reviewCId=review.codeReviewId;
    	
  
    	 return "<div   class='ui horizontal list '    onclick='loadCurrentReview("+reviewCId+")' name='"+reviewCId+"'><div   class='item'>"+
       	
    	 "<img  src="+ imageUrl +" >"+
			"<div class='content' style='margin-left: 80px; margin-top: 8px; ' >"
				+"<div style='min-width:272px; max-width:272px; '><b>"
						+(review.name.length > 30?jQuery.trim(review.name).substring(0, 30).trim(this) + "...":review.name)
							+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+(review.active==true ?"<b style='color:#66BB6A; float:right; '>"+"<u:message locale="${locale}" key="cgroup.status.active" />"+"</b>" :"<b style='color:#F3321D;float:right'>"+"<u:message locale="${locale}" key="cgroup.status.history" />"+"</b>")
								+''+"</b>"+"</div>"
								
								+"<i style=' font-size: 12px; '> "+"<u:message locale="${locale}" key="cgroup.owner" />"+":&nbsp; </i>"
									+(review.userName.length > 19?jQuery.trim(review.userName).substring(0, 19).trim(this) + "...":review.userName)
						+"</div></div>";

		}
    
    
 
    
        
	,updater: function ( selectedName ) {

              return "";
          }
        
   
    })});
    
    
    
    
    
    
function loadCurrentReview(reviewId) {

    $.get("${pageContext.servletContext.contextPath}/review/"+reviewId, function(responseText) {
    	location.replace("${pageContext.servletContext.contextPath}/review/"+reviewId);
	});
}





</script>




