<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="u" uri="../../WEB-INF/showMessage.tld"%>

<!DOCTYPE html>
<html>
<link rel="icon" type="image/png" href="${pageContext.servletContext.contextPath}/resources/images/codereview_icon.png">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" href="resources/css/index/socialButtons.css">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- ******************* FILE UPLOAD ****************** -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

<!-- ****************** NOTIFICATIONS ****************** -->

<script src="resources/bootstrap_notify/bootstrap-notify.js"></script>
<link rel="stylesheet" href="resources/bootstrap_notify/animate.css">

<!-- ************************************************** -->

<script type="text/javascript" src="resources/js/index/index.js"></script>
<script type="text/javascript" src="resources/js/main.js"></script>

<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/resources/css/main.css">

<link rel="stylesheet" href="resources/css/index/style.css">

<link href='https://fonts.googleapis.com/css?family=Lato:400,300'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/semantic_ui/css/semantic.css"/>"/>
<title><u:message locale="${locale}" key="index.password.restore"/></title>

<style>
body{
background-color: #fff !important;
}
</style>
</head>
<body>
	<div class="mainBlock" style="background-color: #fff;">

		<jsp:include page="../index/header.jsp"></jsp:include>

	
		
		<div class="container-fluid" style="background-color: #fff;">
				<div class="row" style="margin: 0;text-align: center;">
				<div class="col-md-6">
				
				<div style="padding-top:100px">
				<div class="row"><h1 style="font-size: 200px">404</h1></div>
				<div class="row" style="font-size: 20px; font-weight: 1000;">oops I guess something went wrong</div>
				</div>
				</div>
				
				<div class="col-md-6">
				<img style="width: 100%; padding-right:50px; padding-top:100px" src="<c:url value="/resources/images/404_image.png"/>">
				</div>
				</div>


			</div>
	</div>

</body>


</html>

